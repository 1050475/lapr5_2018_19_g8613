using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Arqsi_Projeto.Models;

namespace Arqsi_Projeto.DTOs
{
    public class ProdutoDto
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public string SKU { get; set; }

        public string Categoria { get; set; }

        public DimensaoDTO Altura { get; set; }
        public DimensaoDTO Largura { get; set; }
        public DimensaoDTO Profundidade { get; set; }
        public List<MaterialDto> Materiais { get; set; }

        public List<RestricaoDimensaoDTO> ListaRestricaoDimensao { get; set; }

        public List<RestricaoMaterialDTO> ListaRestricaoMaterial { get; set; }

        public ProdutoDto(int id,string Nome, string SKU, DimensaoDTO Altura, DimensaoDTO Largura, DimensaoDTO Profundidade, List<MaterialDto> Materiais, List<RestricaoDimensaoDTO> ListaRestricaoDimensao, List<RestricaoMaterialDTO> ListaRestricaoMaterial, string Categoria)
        {
            this.Id =  id;
            this.Nome = Nome;
            this.SKU = SKU;
            this.Altura = Altura;
            this.Largura = Largura;
            this.Profundidade = Profundidade;
            this.Materiais = Materiais;
            this.ListaRestricaoDimensao = ListaRestricaoDimensao;
            this.ListaRestricaoMaterial= ListaRestricaoMaterial;
            this.Categoria = Categoria;
        }

          public ProdutoDto(int id,string Nome, string SKU, DimensaoDTO Altura, DimensaoDTO Largura, DimensaoDTO Profundidade, List<MaterialDto> Materiais, List<RestricaoDimensaoDTO> ListaRestricaoDimensao, List<RestricaoMaterialDTO> ListaRestricaoMaterial)
        {
            this.Id =  id;
            this.Nome = Nome;
            this.SKU = SKU;
            this.Altura = Altura;
            this.Largura = Largura;
            this.Profundidade = Profundidade;
            this.Materiais = Materiais;
            this.ListaRestricaoDimensao = ListaRestricaoDimensao;
            this.ListaRestricaoMaterial= ListaRestricaoMaterial;

        }
    }


}