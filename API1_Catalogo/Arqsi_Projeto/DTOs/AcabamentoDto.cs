using Arqsi_Projeto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Arqsi_Projeto.DTOs
{
    public class AcabamentoDto
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public AcabamentoDto(string Nome)
        {
            this.Nome = Nome;
        }

    }
}