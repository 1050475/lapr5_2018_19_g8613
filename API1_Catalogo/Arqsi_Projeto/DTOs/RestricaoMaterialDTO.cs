using Arqsi_Projeto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Arqsi_Projeto.DTOs
{
    public class RestricaoMaterialDTO
    {

        public string Id { get; set; }
        public MaterialDto Material { get; set; }
        public RestricaoMaterialDTO(MaterialDto Material)
        {
            this.Material = Material;
        }
    }


}