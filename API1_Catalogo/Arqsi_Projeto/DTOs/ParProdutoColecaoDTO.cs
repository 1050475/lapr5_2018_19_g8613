using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Arqsi_Projeto.Models;

namespace Arqsi_Projeto.DTOs
{
    public class ParProdutoColecaoDTO
    {

        public string SKUProduto { get; set; }

        public string SKUColecao { get; set; }

        public ParProdutoColecaoDTO(){
            SKUProduto = "noName";
            SKUColecao = "noName";
        }

    }
}