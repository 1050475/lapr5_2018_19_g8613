using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Arqsi_Projeto.Models;

namespace Arqsi_Projeto.DTOs
{
    public class ParProdutoDto
    {

        public string SKU1 { get; set; }

        public string SKU2 { get; set; }

        public Boolean Obrigatorio { get; set; }
      
    }
}