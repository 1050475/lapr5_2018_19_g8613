using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Arqsi_Projeto.Models;

namespace Arqsi_Projeto.DTOs
{
    public class ParPrecoMaterialDTO
    {
        public int ID { get; set; }

        public int MaterialId { get; set; }
        
        public DateTime DataInicio { get; set; }

        public double PrecoMat { get; set; }



        public ParPrecoMaterialDTO(int ID, int MaterialId, DateTime DataInicio, double PrecoMat)
        {
            this.ID = ID;
            this.MaterialId = MaterialId;
            this.DataInicio = DataInicio;
            this.PrecoMat = PrecoMat;
            
        }
    }


}