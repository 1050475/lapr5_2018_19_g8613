using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Arqsi_Projeto.Models;

namespace Arqsi_Projeto.DTOs
{
    public class ParPrecoAcabamentoDTO
    {
        public int ID { get; set; }

        public int MaterialId { get; set; }

        public int AcabamentoId { get; set; }
        
        public DateTime DataInicio { get; set; }

        public double PrecoAcab { get; set; }



        public ParPrecoAcabamentoDTO(int ID, int MaterialId,int AcabamentoId, DateTime DataInicio, double PrecoAcab)
        {
            this.ID = ID;
            this.MaterialId = MaterialId;
            this.AcabamentoId = AcabamentoId;
            this.DataInicio = DataInicio;
            this.PrecoAcab = PrecoAcab;
            
        }
    }


}