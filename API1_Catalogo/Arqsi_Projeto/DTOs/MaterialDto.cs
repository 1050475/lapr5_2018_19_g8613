using Arqsi_Projeto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Arqsi_Projeto.DTOs
{
    public class MaterialDto
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public List<AcabamentoDto> Acabamentos { get; set; }
        
        public MaterialDto(string Nome, List<AcabamentoDto> Acabamentos)
        {
            this.Nome = Nome;
            this.Acabamentos = Acabamentos;
        }
    }


}