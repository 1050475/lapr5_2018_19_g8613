using Arqsi_Projeto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Arqsi_Projeto.DTOs
{
    public class DimensaoDTO
    {
        public string Id { get; set; }
        public IList<MedidaContinuaDTO> MedidasContinuas;
        public IList<MedidaDiscretaDTO> MedidasDiscretas;

        public DimensaoDTO(IList<MedidaContinuaDTO> MedidasContinuas, IList<MedidaDiscretaDTO> MedidasDiscretas){
            this.MedidasContinuas = MedidasContinuas;
            this.MedidasDiscretas = MedidasDiscretas;
        }

    }
}