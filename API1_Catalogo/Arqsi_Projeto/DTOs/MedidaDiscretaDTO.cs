using Arqsi_Projeto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Arqsi_Projeto.DTOs
{
    public class MedidaDiscretaDTO
    {
        public int Id { get; set; }

        public double Valor;

        public MedidaDiscretaDTO(double Valor){
            this.Valor = Valor;
        }

    }
}