using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Arqsi_Projeto.Models;

namespace Arqsi_Projeto.DTOs
{
    public class ProdutoSkuDTO
    {
        public int Id { get; set; }

        public string SKU { get; set; }

        public string Nome { get; set; }

        public string Categoria { get; set; }


        public ProdutoSkuDTO(string SKU)
        {
            this.SKU = SKU;
            this.Nome = "Nome Indisponível";
            this.Categoria = "Categoria Indisponível";
        }

        public ProdutoSkuDTO(Produto p){
            this.SKU = p.SKU;
            this.Nome = p.Nome;
            this.Categoria = p.Categoria.Nome;
        }
    }
}