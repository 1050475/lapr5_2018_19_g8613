using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Arqsi_Projeto.Models;

namespace Arqsi_Projeto.DTOs
{
    public class ProdutoDtoDetailed
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public string Categoria { get; set; }

        public string Dimensoes { get; set; }

        public string Material { get; set; }

        public string Acabamento { get; set;}
        public ProdutoDtoDetailed(ProdutoDto p)
        {
            Id = p.Id;
            Nome = p.Nome;
        }
    }
}