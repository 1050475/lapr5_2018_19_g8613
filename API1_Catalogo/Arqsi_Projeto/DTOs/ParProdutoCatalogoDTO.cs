using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Arqsi_Projeto.Models;

namespace Arqsi_Projeto.DTOs
{
    public class ParProdutoCatalogoDTO
    {

        public string SKUProduto { get; set; }

        public string SKUCatalogo { get; set; }

        public ParProdutoCatalogoDTO(){
            SKUProduto = "noName";
            SKUCatalogo = "noName";
        }

    }
}