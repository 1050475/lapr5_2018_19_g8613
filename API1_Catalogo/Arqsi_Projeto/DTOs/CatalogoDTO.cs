using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Arqsi_Projeto.Models;

namespace Arqsi_Projeto.DTOs
{
    public class CatalogoDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string SKU { get; set; }

        public DateTime? DataInicio { get; set; }

        public DateTime? DataFim { get; set; }



        public CatalogoDTO(int Id, string Name, string SKU, DateTime DataInicio, DateTime DataFim)
        {
            this.Id = Id;
            this.Name = Name;
            this.SKU = SKU;
            this.DataInicio = DataInicio;
            this.DataFim = DataFim;
        }
    }


}