using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Arqsi_Projeto.Models;

namespace Arqsi_Projeto.DTOs
{
    public class ParColecaoRestricaoDTO{
        public int ColecaoID{get;set;}

        public int RestricaoID{get;set;}

        public ParColecaoRestricaoDTO(){
            this.ColecaoID=-1;
            this.RestricaoID=-1;
        }
    }
}