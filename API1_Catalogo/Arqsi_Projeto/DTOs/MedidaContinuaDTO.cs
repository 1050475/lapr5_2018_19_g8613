using Arqsi_Projeto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Arqsi_Projeto.DTOs
{
    public class MedidaContinuaDTO
    {
        public int Id { get; set; }

        public double Minimo;

        public double Maximo;

        public MedidaContinuaDTO(double Minimo, double Maximo){
            this.Minimo = Minimo;
            this.Maximo = Maximo;
        }

    }
}