using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Arqsi_Projeto.Models;

namespace Arqsi_Projeto.DTOs
{
    public class CategoriaDto
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public string CategoriaPai { get; set; }

        public List<string> CategoriaFilhos { get; set; }

          public CategoriaDto(Categoria c)
        {
            Id = c.CategoriaId;
            CategoriaPai = c.CategoriaPai;
            Nome = c.Nome;
        }
    }
}