using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Arqsi_Projeto.Models;

namespace Arqsi_Projeto.DTOs
{
    public class RestricaoDTO
    {
        public string Type { get; set; }

        public int MaterialID { get; set; }

        public int AcabamentoID { get; set; }
        public double MinAltura { get; set; }
        public double MaxAltura { get; set; }

        public double MinLargura { get; set; }
        public double MaxLargura { get; set; }

        public double MinProfundidade { get; set; }
        public double MaxProfundidade { get; set; }

        public double Largura { get; set; }
        public double Altura { get; set; }
        public double Profundidade { get; set; }

    }
}