using Arqsi_Projeto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Arqsi_Projeto.DTOs
{
    public class RestricaoDimensaoDTO
    {

        public string Id { get; set; }
        public double MaxPercentagemOcupacao{ get; set; }

        public double MinPercentagemOcupacao{ get; set; }

        public RestricaoDimensaoDTO(double MaxPercentagemOcupacao,double MinPercentagemOcupacao)
        {
            this.MaxPercentagemOcupacao = MaxPercentagemOcupacao;
            this.MinPercentagemOcupacao = MinPercentagemOcupacao;
        }
    }
}