using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Arqsi_Projeto.Models;

namespace Arqsi_Projeto.DTOs
{
    public class NovoProdutoDTO
    {

        public string Id { get; set; }

        public string Nome { get; set; }

        public string SKU { get; set; }

        public int CategoriaId { get; set; }

        public IList<MedidaContinua> LimitesAltura { get; set; }

        public IList<MedidaContinua> LimitesLargura { get; set; }

        public IList<MedidaContinua> LimitesProfundidade { get; set; }

        public IList<MedidaDiscreta> ValoresAdmissiveisAltura { get; set; }

        public IList<MedidaDiscreta> ValoresAdmissiveisLargura { get; set; }

        public IList<MedidaDiscreta> ValoresAdmissiveisProfundidade { get; set; }

        public IList<MaterialDto> ListaMateriais { get; set; }
    
    
    }
}