using Arqsi_Projeto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Arqsi_Projeto.Data
{
    public class DbInitializer
    {

        public static void Initialize(ArmariosContext context)
        {
            context.Database.EnsureCreated();

            //dados

            Categoria cat_armarioSalaJantar = new Categoria
                           {
                               Nome = "Armario Sala Jantar",
                               CategoriaPai = "Armario Sala",
                               CategoriasFilho = new List<Categoria>()
                           };
                        
           Categoria cat_armarioSalaMuseu = new Categoria
                           {
                               Nome = "Armario Sala Museu",
                               CategoriaPai = "Armario Sala",
                               CategoriasFilho = new List<Categoria>()
                           };

            Categoria cat_armarioSala = new Categoria
                           {
                               Nome = "Armario Sala",
                               CategoriaPai = "Armario",
                               CategoriasFilho = {
                                   cat_armarioSalaJantar,
                                   cat_armarioSalaMuseu
                               }
                           };
            
            Categoria cat_armarioQuarto = new Categoria
                {
                    Nome = "Armario Quarto",
                    CategoriaPai = "Armario",
                    CategoriasFilho = new List<Categoria>()
                };

            Categoria cat_Armarios = new Categoria
            {
                Nome = "Armario",
                CategoriaPai = null,
                CategoriasFilho = new List<Categoria> {
                        cat_armarioSala, 
                        cat_armarioQuarto
            }
            };

            Categoria cat_gaveta =  new Categoria
                {
                    Nome = "Gaveta",
                    CategoriaPai = null,
                    CategoriasFilho = new List<Categoria>()
                };

            Categoria cat_prateleira = new Categoria
                {
                    Nome = "Prateleira",
                    CategoriaPai = null,
                    CategoriasFilho = new List<Categoria>()
                };

            Material material1 = new Material { Nome = "Madeira", Acabamentos = new List<Acabamento> { new Acabamento() { Nome = "Brilhante" } , new Acabamento() { Nome = "Mate" } } };
            Material material2 = new Material { Nome = "Metal", Acabamentos = new List<Acabamento> { new Acabamento() { Nome = "Brilhante" } , new Acabamento() { Nome = "Mate" } } };
            Material material3 = new Material { Nome = "PVC", Acabamentos = new List<Acabamento> { new Acabamento() { Nome = "Brilhante" } , new Acabamento() { Nome = "Mate" } } };
            Produto produto1 = new Produto { Nome = "OpenGL-Armario", SKU = "2001", Categoria = cat_armarioSala, Altura = new Dimensao { Medidas = new List<Medida> { new MedidaDiscreta { valor = 21 }, new MedidaContinua { Minimo = 1, Maximo = 20 } } }, Largura = new Dimensao { Medidas = new List<Medida> { new MedidaDiscreta { valor = 21 }, new MedidaContinua { Minimo = 1, Maximo = 20 } } }, Profundidade = new Dimensao { Medidas = new List<Medida> { new MedidaDiscreta { valor = 21 }, new MedidaContinua { Minimo = 1, Maximo = 20 } } }  } ;
            Produto produto2 = new Produto { Nome = "OpenGL-Gaveta", SKU = "2002", Categoria = cat_gaveta, Altura = new Dimensao { Medidas = new List<Medida> { new MedidaDiscreta { valor = 21 }, new MedidaContinua { Minimo = 1, Maximo = 20 } } }, Largura = new Dimensao { Medidas = new List<Medida> { new MedidaDiscreta { valor = 21 }, new MedidaContinua { Minimo = 1, Maximo = 20 } } }, Profundidade = new Dimensao { Medidas = new List<Medida> { new MedidaDiscreta { valor = 21 }, new MedidaContinua { Minimo = 1, Maximo = 20 } } } } ;
            Produto produto3 = new Produto { Nome = "OpenGL-Prateleira", SKU = "2003", Categoria = cat_prateleira, Altura = new Dimensao { Medidas = new List<Medida> { new MedidaDiscreta { valor = 21 }, new MedidaContinua { Minimo = 1, Maximo = 20 } } }, Largura = new Dimensao { Medidas = new List<Medida> { new MedidaDiscreta { valor = 21 }, new MedidaContinua { Minimo = 1, Maximo = 20 } } }, Profundidade = new Dimensao { Medidas = new List<Medida> { new MedidaDiscreta { valor = 21 }, new MedidaContinua { Minimo = 1, Maximo = 20 } } } };
            Produto produto4 = new Produto { Nome = "OpenGL-Slot", SKU = "2004", Categoria = cat_armarioSala, Altura = new Dimensao { Medidas = new List<Medida> { new MedidaDiscreta { valor = 21 }, new MedidaContinua { Minimo = 1, Maximo = 20 } } }, Largura = new Dimensao { Medidas = new List<Medida> { new MedidaDiscreta { valor = 21 }, new MedidaContinua { Minimo = 1, Maximo = 20 } } }, Profundidade = new Dimensao { Medidas = new List<Medida> { new MedidaDiscreta { valor = 21 }, new MedidaContinua { Minimo = 1, Maximo = 20 } } }  } ;
            ParProdutos parProdutos1 = new ParProdutos { ProdutoBase = produto1, ProdutoParte = produto4, Obrigatorio = true };
            ParProdutos parProdutos2 = new ParProdutos { ProdutoBase = produto4, ProdutoParte = produto2, Obrigatorio = false };
            ParProdutos parProdutos3 = new ParProdutos { ProdutoBase = produto4, ProdutoParte = produto3, Obrigatorio = false };

            ParProdutoMaterial parProdutoMaterial1 = new ParProdutoMaterial { Produto = produto1, Material = material1 };
            ParProdutoMaterial parProdutoMaterial2 = new ParProdutoMaterial { Produto = produto2, Material = material1 };
            ParProdutoMaterial parProdutoMaterial3 = new ParProdutoMaterial { Produto = produto3, Material = material1 };
            ParProdutoMaterial parProdutoMaterial4 = new ParProdutoMaterial { Produto = produto4, Material = material1 };
            ParProdutoMaterial parProdutoMaterial5 = new ParProdutoMaterial { Produto = produto1, Material = material2 };
            ParProdutoMaterial parProdutoMaterial6 = new ParProdutoMaterial { Produto = produto2, Material = material2 };
            ParProdutoMaterial parProdutoMaterial7 = new ParProdutoMaterial { Produto = produto3, Material = material2 };
            ParProdutoMaterial parProdutoMaterial8 = new ParProdutoMaterial { Produto = produto4, Material = material2 };
            ParProdutoMaterial parProdutoMaterial9 = new ParProdutoMaterial { Produto = produto1, Material = material3 };
            ParProdutoMaterial parProdutoMaterial10 = new ParProdutoMaterial { Produto = produto2, Material = material3 };
            ParProdutoMaterial parProdutoMaterial11 = new ParProdutoMaterial { Produto = produto3, Material = material3 };
            ParProdutoMaterial parProdutoMaterial12 = new ParProdutoMaterial { Produto = produto4, Material = material3 };
            //ParProdutoMaterial parProdutoMaterial1 = new ParProdutoMaterial();

            // Look for any Categoria
            if (!context.Categorias.Any())
            {
                var categorias = new Categoria[]
                {
                    cat_Armarios,
                    cat_gaveta,
                    cat_prateleira
                };
                foreach (Categoria c in categorias)
                {
                    context.Categorias.Add(c);
                }
                context.SaveChanges();
            }

            // Look for any Produto
            if (!context.Produtos.Any())
            {

                var produtos = new Produto[]
                {
                    produto1,
                    produto2,
                    produto3,
                    produto4
                };

                foreach (Produto p in produtos)
                {
                    context.Produtos.Add(p);
                }
                context.SaveChanges();
            }


            // Look for any ParProdutos
            if (!context.ParesProdutos.Any())
            {

                var paresProdutos = new ParProdutos[]
                {
                    parProdutos1,
                    parProdutos2,
                    parProdutos3
                };
                foreach (ParProdutos p in paresProdutos)
                {
                    context.ParesProdutos.Add(p);
                }
                context.SaveChanges();
            }

            // Look for any ParProdutoMaterial
            if (!context.ParesProdutoMaterial.Any())
            {

                var paresProdutoMaterial = new ParProdutoMaterial[]
                {
                    parProdutoMaterial1,
                    parProdutoMaterial2,
                    parProdutoMaterial3,
                    parProdutoMaterial4,
                    parProdutoMaterial5,
                    parProdutoMaterial6,
                    parProdutoMaterial7,
                    parProdutoMaterial8,
                    parProdutoMaterial9,
                    parProdutoMaterial10,
                    parProdutoMaterial11,
                    parProdutoMaterial12
                };
                foreach (ParProdutoMaterial p in paresProdutoMaterial)
                {
                    context.ParesProdutoMaterial.Add(p);
                }
                context.SaveChanges();
            }

        }
    }
}
