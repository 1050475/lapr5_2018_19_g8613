using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Arqsi_Projeto.Models;
using Arqsi_Projeto.DTOs;
using Microsoft.AspNetCore.Authorization;

namespace arqsi_projeto.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParProdutoCatalogoController : ControllerBase
    {
        private readonly ArmariosContext _context;

        public ParProdutoCatalogoController(ArmariosContext context)
        {
            _context = context;
        }

        // GET: api/ParProdutoCatalogo
        [HttpGet]
        public IEnumerable<ParProdutoCatalogo> GetParesProdutoCatalogo()
        {
            return _context.ParesProdutoCatalogo;
        }

        // GET: api/ParProdutoCatalogo/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetParProdutoCatalogo([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var parProdutoCatalogo = await _context.ParesProdutoCatalogo.FindAsync(id);

            if (parProdutoCatalogo == null)
            {
                return NotFound();
            }

            return Ok(parProdutoCatalogo);
        }


        // POST: api/ParProdutoCatalogo
        [HttpPost]
        public async Task<IActionResult> PostParProdutoCatalogo([FromBody] ParProdutoCatalogoDTO parProdutoCatalogoDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            var catalogo = _context.Catalogos.Where(c => c.SKU.Equals(parProdutoCatalogoDTO.SKUCatalogo)).First();
            var produto = _context.Produtos.Where(p => p.SKU.Equals(parProdutoCatalogoDTO.SKUProduto)).Include(p => p.Altura.Medidas).Include(p => p.Largura.Medidas).Include(p => p.Profundidade.Medidas).Include(p => p.ListaRestricoes).First();

            ParProdutoCatalogo parProdutoCatalogo = new ParProdutoCatalogo();
            parProdutoCatalogo.Catalogo = catalogo;
            parProdutoCatalogo.Produto = produto;

            _context.ParesProdutoCatalogo.Add(parProdutoCatalogo);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetParProdutoCatalogo", new { id = parProdutoCatalogo.ParProdutoCatalogoId }, parProdutoCatalogo);
        }

        // PUT: api/ParProdutoCatalogo/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutParProdutoCatalogo([FromRoute] int id, [FromBody] ParProdutoCatalogoDTO parProdutoCatalogoDTO)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ParProdutoCatalogo parProdutoCatalogo = await _context.ParesProdutoCatalogo.FindAsync(id);
            if (parProdutoCatalogoDTO.SKUCatalogo != "noName")
            {
                var catalogo = _context.Catalogos.Where(c => c.SKU.Equals(parProdutoCatalogoDTO.SKUCatalogo)).First();
                parProdutoCatalogo.Catalogo = catalogo;
            }

            if (parProdutoCatalogoDTO.SKUProduto != "noName")
            {
                var produto = _context.Produtos.Where(p => p.SKU.Equals(parProdutoCatalogoDTO.SKUProduto)).Include(p => p.Altura.Medidas).Include(p => p.Largura.Medidas).Include(p => p.Profundidade.Medidas).Include(p => p.ListaRestricoes).First();
                parProdutoCatalogo.Produto = produto;
            }

            _context.Update(parProdutoCatalogo);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ParProdutoCatalogoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Ok(parProdutoCatalogo);
        }


        // DELETE: api/ParProdutoCatalogo/5
        [HttpDelete("{id}")]
        public ActionResult DeleteParProdutoCatalogo([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var parProdutoCatalogo = _context.ParesProdutoCatalogo.Find(id);
            if (parProdutoCatalogo == null)
            {
                return NotFound();
            }

            _context.ParesProdutoCatalogo.Remove(parProdutoCatalogo);
            _context.SaveChanges();

            return Ok(parProdutoCatalogo);
        }

        private bool ParProdutoCatalogoExists(int id)
        {
            return _context.ParesProdutoCatalogo.Any(e => e.ParProdutoCatalogoId == id);
        }
    }
}
