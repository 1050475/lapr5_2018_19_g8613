using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Arqsi_Projeto.Models;
using Arqsi_Projeto.DTOs;
using Microsoft.AspNetCore.Authorization;

namespace arqsi_projeto.Controllers
{
    [Produces("application/json")]
    [Route("api/ParPrecoMaterial")]
    public class ParPrecoMaterialController : Controller
    {
        private readonly ArmariosContext _context;

        public ParPrecoMaterialController(ArmariosContext context)
        {
            _context = context;
        }


        // GET: api/ParPrecoMaterial
        [HttpGet]
        public IEnumerable<ParPrecoMaterial> GetParesPrecoMaterial()
        {
            return _context.ParesPrecoMaterial;
        }

        // GET: api/ParPrecoMaterial/1
        [HttpGet("{id}")]
        public async Task<IActionResult> GetParPrecoMaterial([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var ParPrecoMaterial = await _context.ParesPrecoMaterial.FindAsync(id);

            if (ParPrecoMaterial == null)
            {
                return NotFound();
            }

            return Ok(ParPrecoMaterial);
        }

        // POST: api/ParPrecoMaterial
        [HttpPost]
        public async Task<IActionResult> PostParPrecoMaterial([FromBody] ParPrecoMaterialDTO parPrecoMateriaDTO)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }



            var material = _context.Materiais.Where(m => m.MaterialId == parPrecoMateriaDTO.MaterialId).First();

            if (material == null)
            {

                return StatusCode(409, "Material Not Found");

            }


            ParPrecoMaterial parPrecoMaterial = new ParPrecoMaterial();
            parPrecoMaterial.MaterialId = parPrecoMateriaDTO.MaterialId;
            parPrecoMaterial.DataInicio = parPrecoMateriaDTO.DataInicio;
            parPrecoMaterial.PrecoMat = parPrecoMateriaDTO.PrecoMat;


            _context.ParesPrecoMaterial.Add(parPrecoMaterial);
            await _context.SaveChangesAsync();

            return CreatedAtAction("PostParPrecoMaterial", new { id = parPrecoMaterial.ID }, parPrecoMaterial);
        }

        // DELETE: api/ParPrecoMaterial/1
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteParPrecoMaterial([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var parPrecoMaterial = await _context.ParesPrecoMaterial.FindAsync(id);
            if (parPrecoMaterial == null)
            {
                return NotFound();
            }

            _context.ParesPrecoMaterial.Remove(parPrecoMaterial);
            await _context.SaveChangesAsync();

            return Ok(parPrecoMaterial);
        }

        private bool ParPrecoMaterialExists(int id)
        {
            return _context.ParesPrecoMaterial.Any(e => e.ID == id);
        }
    
        // PUT: api/ParPrecoMaterial/1
        [HttpPut("{id}")]
        public async Task<IActionResult> PutParPrecoMaterial([FromRoute] int id, [FromBody] ParPrecoMaterialDTO parPrecoMaterialDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var parPrecoMaterial = await _context.ParesPrecoMaterial.FindAsync(id);
            DateTime defaultDatetime = new DateTime(1, 1, 1, 0, 0, 0);

            if (parPrecoMaterialDTO.MaterialId != 0)
            {
                parPrecoMaterial.MaterialId = parPrecoMaterialDTO.MaterialId;
            }
            if (parPrecoMaterialDTO.DataInicio != defaultDatetime)
            {
                parPrecoMaterial.DataInicio = parPrecoMaterialDTO.DataInicio;

            }

            if (parPrecoMaterialDTO.PrecoMat != 0)
            {
                parPrecoMaterial.PrecoMat = parPrecoMaterialDTO.PrecoMat;
            }

            _context.Update(parPrecoMaterial);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ParPrecoMaterialExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(parPrecoMaterial);
        }
    }
}