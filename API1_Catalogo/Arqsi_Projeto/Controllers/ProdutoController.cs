using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Arqsi_Projeto.Models;
using Arqsi_Projeto.DTOs;
using Microsoft.AspNetCore.Authorization;

namespace arqsi_projeto.Controllers
{

    //[Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/Produto")]
    public class ProdutoController : Controller
    {
        private readonly ArmariosContext _context;

        public ProdutoController(ArmariosContext context)
        {
            _context = context;
        }

        // GET: api/Produto/Test
        [HttpGet("Test")]
        public IActionResult GetProdutosTest()
        {
            ProdutoSkuDTO produtoDto = new ProdutoSkuDTO("001");

            return Ok(produtoDto);
        }

        // GET: api/Produto
        [HttpGet]
        public IEnumerable<Produto> GetProdutos()
        {
            return _context.Produtos.Include(p => p.Categoria);
        }

        // GET: api/Produto/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetProduto([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var produto = await _context.Produtos.FindAsync(id);

            if (produto == null)
            {
                return NotFound();
            }

            return Ok(produto);
        }

        // PUT: api/Produto/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProduto([FromRoute] int id, [FromBody] NovoProdutoDTO dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Produto novoProduto = DTOToProduto(dto);

            //tratamento categoria do produto
            var categoria = await _context.Categorias.FindAsync(dto.CategoriaId);
            if (categoria != null)
            {
                novoProduto.Categoria = categoria;
            }

            var produtoParaModificar = await _context.Produtos.FindAsync(id);
            string SKU = produtoParaModificar.SKU;

            ////////////////////////////////////
            //Eliminar Produto Antigo
            ////////////////////////////////////
            var produtoParaRemover = _context.Produtos.Where(p => p.SKU.Equals(SKU)).Include(p => p.ListaRestricoes).First();


            if (produtoParaRemover == null)
            {
                return NotFound();
            }


            foreach (Restricao r in produtoParaRemover.ListaRestricoes)
            {
                _context.Restricoes.Remove(r);
                await _context.SaveChangesAsync();
            }

            var paresProduto1ParaRemover = _context.ParesProdutos.Where(pp => pp.ProdutoBase.SKU.Equals(SKU)).Include(pp => pp.listaRestricoes).ToList();
            var paresProduto2ParaRemover = _context.ParesProdutos.Where(pp => pp.ProdutoParte.SKU.Equals(SKU)).Include(pp => pp.listaRestricoes).ToList();
            var paresProdutoMaterialParaRemover = _context.ParesProdutoMaterial.Where(pp => pp.Produto.SKU.Equals(SKU)).ToList();
            var paresProdutoCatalogo = _context.ParesProdutoCatalogo.Where(p => p.Produto.SKU.Equals(SKU)).ToList();

            if (paresProdutoCatalogo != null)
            {
                foreach (ParProdutoCatalogo pp in paresProdutoCatalogo)
                {
                    _context.ParesProdutoCatalogo.Remove(pp);
                    await _context.SaveChangesAsync();
                }
            }

            foreach (ParProdutos pp in paresProduto1ParaRemover)
            {

                foreach (Restricao r in pp.listaRestricoes)
                {
                    _context.Restricoes.Remove(r);
                    await _context.SaveChangesAsync();
                }

                _context.ParesProdutos.Remove(pp);
                await _context.SaveChangesAsync();
            }

            foreach (ParProdutos pp in paresProduto2ParaRemover)
            {

                foreach (Restricao r in pp.listaRestricoes)
                {
                    _context.Restricoes.Remove(r);
                    await _context.SaveChangesAsync();
                }

                _context.ParesProdutos.Remove(pp);
                await _context.SaveChangesAsync();
            }

            foreach (ParProdutoMaterial pm in paresProdutoMaterialParaRemover)
            {
                _context.ParesProdutoMaterial.Remove(pm);
                await _context.SaveChangesAsync();
            }

            _context.Produtos.Remove(produtoParaRemover);
            await _context.SaveChangesAsync();
            ///////////////////////////////////




            ////////////////////////////////////////////
            //Eliminacao das relacoes antigas
            ////////////////////////////////////////////7
            /* 
                      foreach(Restricao r in produtoParaModificar.ListaRestricoes){
                            _context.Restricoes.Remove(r);
                            await _context.SaveChangesAsync();
                        }

                        var paresProduto1ParaRemover = _context.ParesProdutos.Where(pp => pp.ProdutoBase.SKU.Equals(SKU)).Include(pp => pp.listaRestricoes).ToList();
                        var paresProduto2ParaRemover = _context.ParesProdutos.Where(pp => pp.ProdutoParte.SKU.Equals(SKU)).Include(pp => pp.listaRestricoes).ToList();
                        var paresProdutoMaterialParaRemover = _context.ParesProdutoMaterial.Where(pp => pp.Produto.SKU.Equals(SKU)).ToList();

                        foreach(ParProdutos pp in paresProduto1ParaRemover){

                            foreach(Restricao r in pp.listaRestricoes){
                                _context.Restricoes.Remove(r);
                                await _context.SaveChangesAsync();
                            }

                            _context.ParesProdutos.Remove(pp);
                            await _context.SaveChangesAsync();
                        }

                        foreach(ParProdutos pp in paresProduto2ParaRemover){

                            foreach(Restricao r in pp.listaRestricoes){
                                _context.Restricoes.Remove(r);
                                await _context.SaveChangesAsync();
                            }

                            _context.ParesProdutos.Remove(pp);
                            await _context.SaveChangesAsync();
                        }

                        foreach(ParProdutoMaterial pm in paresProdutoMaterialParaRemover){
                            _context.ParesProdutoMaterial.Remove(pm);
                            await _context.SaveChangesAsync();
                        }
            */
            //////////////////////////////////////////////


            produtoParaModificar = new Produto();

            produtoParaModificar.Nome = novoProduto.Nome;
            produtoParaModificar.SKU = novoProduto.SKU;
            produtoParaModificar.Categoria = novoProduto.Categoria;
            produtoParaModificar.Altura = novoProduto.Altura;
            produtoParaModificar.Profundidade = novoProduto.Profundidade;
            produtoParaModificar.Largura = novoProduto.Largura;
            produtoParaModificar.ListaRestricoes = novoProduto.ListaRestricoes;

            _context.Produtos.Add(produtoParaModificar);


            //tratamento materiais e acabamentos do produto

            foreach (MaterialDto materialDto in dto.ListaMateriais)
            {

                Material novoMaterial = new Material();
                novoMaterial.Nome = materialDto.Nome;
                IList<Acabamento> listaAcabamentos = new List<Acabamento>();

                foreach (AcabamentoDto acabamentoDto in materialDto.Acabamentos)
                {
                    listaAcabamentos.Add(new Acabamento { Nome = acabamentoDto.Nome });
                }

                ParProdutoMaterial novoPpm = new ParProdutoMaterial();

                novoMaterial.Acabamentos = listaAcabamentos;

                novoPpm.Produto = produtoParaModificar;
                novoPpm.Material = novoMaterial;

                _context.ParesProdutoMaterial.Add(novoPpm);

            }

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProdutoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Produto
        [HttpPost]
        public async Task<IActionResult> PostProduto([FromBody] NovoProdutoDTO dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            /*  if(_context.Produtos.Any(p => p.SKU.Equals(dto.SKU))){

                 var produtoAtual = _context.Produtos.Where(p => p.SKU.Equals(dto.SKU)).First();
                 _context.Produtos.Remove(produtoAtual);

                 await _context.SaveChangesAsync();
             }*/


            Produto novoProduto = DTOToProduto(dto);

            //tratamento categoria do produto
            var categoria = await _context.Categorias.FindAsync(dto.CategoriaId);
            if (categoria != null)
            {
                novoProduto.Categoria = categoria;
            }

            //tratamento materiais e acabamentos do produto

            foreach (MaterialDto materialDto in dto.ListaMateriais)
            {

                Material novoMaterial = new Material();
                novoMaterial.Nome = materialDto.Nome;
                IList<Acabamento> listaAcabamentos = new List<Acabamento>();

                foreach (AcabamentoDto acabamentoDto in materialDto.Acabamentos)
                {
                    listaAcabamentos.Add(new Acabamento { Nome = acabamentoDto.Nome });
                }

                ParProdutoMaterial novoPpm = new ParProdutoMaterial();

                novoMaterial.Acabamentos = listaAcabamentos;

                novoPpm.Produto = novoProduto;
                novoPpm.Material = novoMaterial;

                _context.ParesProdutoMaterial.Add(novoPpm);

            }

            _context.Produtos.Add(novoProduto);

            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProduto", new { id = novoProduto.ProdutoId }, novoProduto);
        }

        // DELETE: api/Produto/sku=5
        [HttpDelete("sku={SKU}")]
        public async Task<IActionResult> DeleteProduto([FromRoute] string SKU)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var produtoParaRemover = _context.Produtos.Where(p => p.SKU.Equals(SKU)).Include(p => p.ListaRestricoes).First();

            var produto = await _context.Produtos.FindAsync(produtoParaRemover.ProdutoId);
            if (produto == null)
            {
                return NotFound();
            }


            foreach (Restricao r in produto.ListaRestricoes)
            {
                _context.Restricoes.Remove(r);
                await _context.SaveChangesAsync();
            }

            var paresProduto1ParaRemover = _context.ParesProdutos.Where(pp => pp.ProdutoBase.SKU.Equals(SKU)).Include(pp => pp.listaRestricoes).ToList();
            var paresProduto2ParaRemover = _context.ParesProdutos.Where(pp => pp.ProdutoParte.SKU.Equals(SKU)).Include(pp => pp.listaRestricoes).ToList();
            var paresProdutoMaterialParaRemover = _context.ParesProdutoMaterial.Where(pp => pp.Produto.SKU.Equals(SKU)).ToList();

            foreach (ParProdutos pp in paresProduto1ParaRemover)
            {

                foreach (Restricao r in pp.listaRestricoes)
                {
                    _context.Restricoes.Remove(r);
                    await _context.SaveChangesAsync();
                }

                _context.ParesProdutos.Remove(pp);
                await _context.SaveChangesAsync();
            }

            foreach (ParProdutos pp in paresProduto2ParaRemover)
            {

                foreach (Restricao r in pp.listaRestricoes)
                {
                    _context.Restricoes.Remove(r);
                    await _context.SaveChangesAsync();
                }

                _context.ParesProdutos.Remove(pp);
                await _context.SaveChangesAsync();
            }

            foreach (ParProdutoMaterial pm in paresProdutoMaterialParaRemover)
            {
                _context.ParesProdutoMaterial.Remove(pm);
                await _context.SaveChangesAsync();
            }

            _context.Produtos.Remove(produto);
            await _context.SaveChangesAsync();

            return Ok(produto);
        }

        // GET /Produto/sku=123/Partes/obrigatorio=true
        [HttpGet("sku={sku}/Partes/obrigatorio={obrigatorio}")]
        public IActionResult GetPartesFromProduto([FromRoute] string sku, [FromRoute] bool obrigatorio)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var paresProdutos = _context.ParesProdutos.Where(pp => pp.ProdutoBase.SKU.Equals(sku)).Include(pp => pp.ProdutoBase).Include(pp => pp.ProdutoParte).Include(pp => pp.ProdutoParte.Categoria).Include(pp => pp.ProdutoBase.Categoria).ToList();

            if (paresProdutos == null)
            {
                return NotFound();
            }

            List<ProdutoSkuDTO> listaProdutos = new List<ProdutoSkuDTO>();

            foreach (ParProdutos pp in paresProdutos)
            {
                if (obrigatorio)
                {
                    if (pp.Obrigatorio)
                    {
                        listaProdutos.Add(new ProdutoSkuDTO(pp.ProdutoParte));
                    }
                }
                else
                {
                    if (!pp.Obrigatorio)
                    {
                        listaProdutos.Add(new ProdutoSkuDTO(pp.ProdutoParte));
                    }
                }

            }

            return Ok(listaProdutos);
        }


        // GET /Produto/sku=123/Partes/obrigatorio=true/cat=Armario
        [HttpGet("sku={sku}/Partes/obrigatorio={obrigatorio}/cat={categoria}")]
        public IActionResult GetPartesFromProdutoByCategoria([FromRoute] string sku, [FromRoute] bool obrigatorio, [FromRoute] string categoria)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var paresProdutos = _context.ParesProdutos.Where(pp => pp.ProdutoBase.SKU.Equals(sku)).Include(pp => pp.ProdutoBase).Include(pp => pp.ProdutoParte).ToList();

            if (paresProdutos == null)
            {
                return NotFound();
            }

            List<ProdutoSkuDTO> listaProdutos = new List<ProdutoSkuDTO>();

            var listaCategorias = _context.Categorias.Include(c => c.CategoriasFilho).ToList();

            foreach (ParProdutos pp in paresProdutos)
            {
                if (Categoria.verificaIgualdadeCategoria(pp.ProdutoParte.Categoria, categoria, listaCategorias))
                {

                    if (obrigatorio)
                    {
                        if (pp.Obrigatorio)
                        {
                            listaProdutos.Add(new ProdutoSkuDTO(pp.ProdutoParte));
                        }
                    }
                    else
                    {
                        if (!pp.Obrigatorio)
                        {
                            listaProdutos.Add(new ProdutoSkuDTO(pp.ProdutoParte));
                        }
                    }
                }
            }

            return Ok(listaProdutos);
        }

        // GET: api/Produto/sku=1234
        [HttpGet("sku={sku}")]
        public IActionResult GetProdutoBySKU([FromRoute] string sku)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var produto = _context.Produtos.Where(p => p.SKU.Equals(sku)).Include(p => p.Categoria).Include(p => p.Altura.Medidas).Include(p => p.Largura.Medidas).Include(p => p.Profundidade.Medidas).Include(p => p.ListaRestricoes).First();

            if (produto == null)
            {
                return NotFound();
            }

            ProdutoDto produtoDto = ProdutoToDTO(produto);

            return Ok(produtoDto);
        }

        // GET: api/Produto/cat=Armario
        [HttpGet("cat={categoria}")]
        public IActionResult GetProdutoFromCategoria([FromRoute] string categoria)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var listaCategorias = _context.Categorias.Include(c => c.CategoriasFilho).ToList();
            var produtos = _context.Produtos.Where(p => Categoria.verificaIgualdadeCategoria(p.Categoria, categoria, listaCategorias)).ToList();

            if (produtos == null)
            {
                return NotFound();
            }

            List<ProdutoSkuDTO> listaProdutos = new List<ProdutoSkuDTO>();

            foreach (Produto p in produtos)
            {
                listaProdutos.Add(new ProdutoSkuDTO(p));
            }

            return Ok(listaProdutos);
        }

        //GET /Produto/sku=123+altura=12+largura=34+profundidade=54/partes

        [HttpGet("sku={skuBase}+altura={altura}+largura={largura}+profundidade={profundidade}/partes")]
        public IActionResult GetPartesProdutoValidadas([FromRoute] string skuBase, [FromRoute] double altura, [FromRoute] double largura, [FromRoute] double profundidade)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            List<ProdutoDto> produtosParteValidados = new List<ProdutoDto>();

            var paresProduto = _context.ParesProdutos.Where(pp => pp.ProdutoBase.SKU.Equals(skuBase)).Include(pp => pp.ProdutoParte).ThenInclude(p => p.Altura).ThenInclude(d => d.Medidas).Include(pp => pp.ProdutoParte).ThenInclude(p => p.Largura).ThenInclude(d => d.Medidas).Include(pp => pp.ProdutoParte).ThenInclude(p => p.Profundidade).ThenInclude(d => d.Medidas).Include(pp => pp.ProdutoParte).ThenInclude(p => p.ListaRestricoes).ToList();

            foreach (ParProdutos pp in paresProduto)
            {
                Produto produtoParteValidado = pp.produtoParteValidado(altura, largura, profundidade);
                if (produtoParteValidado != null)
                {
                    produtosParteValidados.Add(ProdutoToDTO(produtoParteValidado));
                }
            }

            return Ok(produtosParteValidados);
        }

        // GET: api/Produto/sku=1234/validarOcupacao/valor=123
        [HttpGet("sku={sku}/validarOcupacao/valor={ocupacao}")]
        public IActionResult GetProdutoBySKU([FromRoute] string sku, [FromRoute] double ocupacao)
        {

            bool resposta = true;

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var produto = _context.Produtos.Where(p => p.SKU.Equals(sku)).Include(p => p.ListaRestricoes).First();

            if (produto == null)
            {
                return NotFound();
            }

            foreach (Restricao r in produto.ListaRestricoes)
            {
                if (r is RestricaoDimensao)
                {
                    RestricaoDimensao rd = (RestricaoDimensao)r;
                    if (ocupacao > rd.MaxPercentagemOcupacao || ocupacao < rd.MinPercentagemOcupacao)
                    {
                        resposta = false;
                    }
                }
            }


            return Ok(resposta);
        }

        private bool ProdutoExists(int id)
        {
            return _context.Produtos.Any(e => e.ProdutoId == id);
        }

        public ProdutoDto ProdutoToDTO(Produto produto)
        {
            List<MedidaContinuaDTO> alturas_continuas = new List<MedidaContinuaDTO>();
            List<MedidaContinuaDTO> larguras_continuas = new List<MedidaContinuaDTO>();
            List<MedidaContinuaDTO> profundidades_continuas = new List<MedidaContinuaDTO>();

            List<MedidaDiscretaDTO> alturas_discretas = new List<MedidaDiscretaDTO>();
            List<MedidaDiscretaDTO> larguras_discretas = new List<MedidaDiscretaDTO>();
            List<MedidaDiscretaDTO> profundidades_discretas = new List<MedidaDiscretaDTO>();

            foreach (Medida m in produto.Altura.Medidas)
            {
                if (m is MedidaContinua)
                {
                    MedidaContinua mc = (MedidaContinua)m;
                    alturas_continuas.Add(new MedidaContinuaDTO(mc.Minimo, mc.Maximo));
                }
                else if (m is MedidaDiscreta)
                {
                    MedidaDiscreta md = (MedidaDiscreta)m;
                    alturas_discretas.Add(new MedidaDiscretaDTO(md.valor));
                }
            }

            foreach (Medida m in produto.Largura.Medidas)
            {
                if (m is MedidaContinua)
                {
                    MedidaContinua mc = (MedidaContinua)m;
                    larguras_continuas.Add(new MedidaContinuaDTO(mc.Minimo, mc.Maximo));
                }
                else if (m is MedidaDiscreta)
                {
                    MedidaDiscreta md = (MedidaDiscreta)m;
                    larguras_discretas.Add(new MedidaDiscretaDTO(md.valor));
                }
            }

            foreach (Medida m in produto.Profundidade.Medidas)
            {
                if (m is MedidaContinua)
                {
                    MedidaContinua mc = (MedidaContinua)m;
                    profundidades_continuas.Add(new MedidaContinuaDTO(mc.Minimo, mc.Maximo));
                }
                else if (m is MedidaDiscreta)
                {
                    MedidaDiscreta md = (MedidaDiscreta)m;
                    profundidades_discretas.Add(new MedidaDiscretaDTO(md.valor));
                }
            }

            DimensaoDTO Altura = new DimensaoDTO(alturas_continuas, alturas_discretas);
            DimensaoDTO Largura = new DimensaoDTO(larguras_continuas, larguras_discretas);
            DimensaoDTO Profundidade = new DimensaoDTO(profundidades_continuas, profundidades_discretas);

            List<MaterialDto> materiais = new List<MaterialDto>();

            var pares_produto_material = _context.ParesProdutoMaterial.Where(pm => pm.Produto.SKU.Equals(produto.SKU)).Include(pm => pm.Material).ThenInclude(m => m.Acabamentos).ToList();

            foreach (ParProdutoMaterial pm in pares_produto_material)
            {
                List<AcabamentoDto> acabamentos = new List<AcabamentoDto>();

                foreach (Acabamento a in pm.Material.Acabamentos)
                {
                    acabamentos.Add(new AcabamentoDto(a.Nome));
                }
                materiais.Add(new MaterialDto(pm.Material.Nome, acabamentos));
            }

            List<RestricaoDimensaoDTO> listaRestricaoDimensaoDTO = new List<RestricaoDimensaoDTO>();
            List<RestricaoMaterialDTO> listaRestricaoMaterialDTO = new List<RestricaoMaterialDTO>();


            foreach (Restricao r in produto.ListaRestricoes)
            {
                if (r is RestricaoDimensao)
                {
                    RestricaoDimensao rm = (RestricaoDimensao)r;
                    listaRestricaoDimensaoDTO.Add(new RestricaoDimensaoDTO(rm.MaxPercentagemOcupacao, rm.MinPercentagemOcupacao));
                }
                else if (r is RestricaoMaterial)
                {
                    RestricaoMaterial rmat = (RestricaoMaterial)r;

                    List<AcabamentoDto> listaAcabamentosDTO = new List<AcabamentoDto>();

                    foreach (Acabamento a in rmat.Material.Acabamentos)
                    {
                        listaAcabamentosDTO.Add(new AcabamentoDto(a.Nome));
                    }

                    listaRestricaoMaterialDTO.Add(new RestricaoMaterialDTO(new MaterialDto(rmat.Material.Nome, listaAcabamentosDTO)));
                }
            }


            ProdutoDto produtoDto = new ProdutoDto(produto.ProdutoId, produto.Nome, produto.SKU, Altura, Largura, Profundidade, materiais, listaRestricaoDimensaoDTO, listaRestricaoMaterialDTO, produto.Categoria.Nome);

            return produtoDto;
        }


        public Produto DTOToProduto(NovoProdutoDTO novoProdutoDTO)
        {
            Produto novoProduto = new Produto();

            novoProduto.Nome = novoProdutoDTO.Nome;
            novoProduto.SKU = novoProdutoDTO.SKU;
            /* 
            var categoria = _context.Categorias.FindAsync(dto.CategoriaId);
            if (categoria != null)
            {
                novoProduto.Categoria = categoria;
            }
            */

            if (novoProdutoDTO.LimitesAltura != null)
            {
                foreach (MedidaContinua md in novoProdutoDTO.LimitesAltura)
                {
                    novoProduto.Altura.Medidas.Add(md);
                }
            }

            if (novoProdutoDTO.LimitesLargura != null)
            {
                foreach (MedidaContinua md in novoProdutoDTO.LimitesLargura)
                {
                    novoProduto.Largura.Medidas.Add(md);
                }
            }

            if (novoProdutoDTO.LimitesProfundidade != null)
            {
                foreach (MedidaContinua md in novoProdutoDTO.LimitesProfundidade)
                {
                    novoProduto.Profundidade.Medidas.Add(md);
                }
            }

            if (novoProdutoDTO.ValoresAdmissiveisAltura != null)
            {
                foreach (MedidaDiscreta mc in novoProdutoDTO.ValoresAdmissiveisAltura)
                {
                    novoProduto.Altura.Medidas.Add(mc);
                }
            }


            if (novoProdutoDTO.ValoresAdmissiveisLargura != null)
            {
                foreach (MedidaDiscreta mc in novoProdutoDTO.ValoresAdmissiveisLargura)
                {
                    novoProduto.Largura.Medidas.Add(mc);
                }
            }

            if (novoProdutoDTO.ValoresAdmissiveisProfundidade != null)
            {
                foreach (MedidaDiscreta mc in novoProdutoDTO.ValoresAdmissiveisProfundidade)
                {
                    novoProduto.Profundidade.Medidas.Add(mc);
                }
            }

            return novoProduto;
        }




        // DELETE: api/Produto/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduto([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var produto = await _context.Produtos.FindAsync(id);
            if (produto == null)
            {
                return NotFound();
            }

            var SKU = produto.SKU;


            foreach (Restricao r in produto.ListaRestricoes)
            {
                _context.Restricoes.Remove(r);
                await _context.SaveChangesAsync();
            }

            var paresProduto1ParaRemover = _context.ParesProdutos.Where(pp => pp.ProdutoBase.SKU.Equals(SKU)).Include(pp => pp.listaRestricoes).ToList();
            var paresProduto2ParaRemover = _context.ParesProdutos.Where(pp => pp.ProdutoParte.SKU.Equals(SKU)).Include(pp => pp.listaRestricoes).ToList();
            var paresProdutoMaterialParaRemover = _context.ParesProdutoMaterial.Where(pp => pp.Produto.SKU.Equals(SKU)).ToList();

            foreach (ParProdutos pp in paresProduto1ParaRemover)
            {

                foreach (Restricao r in pp.listaRestricoes)
                {
                    _context.Restricoes.Remove(r);
                    await _context.SaveChangesAsync();
                }

                _context.ParesProdutos.Remove(pp);
                await _context.SaveChangesAsync();
            }

            foreach (ParProdutos pp in paresProduto2ParaRemover)
            {

                foreach (Restricao r in pp.listaRestricoes)
                {
                    _context.Restricoes.Remove(r);
                    await _context.SaveChangesAsync();
                }

                _context.ParesProdutos.Remove(pp);
                await _context.SaveChangesAsync();
            }

            foreach (ParProdutoMaterial pm in paresProdutoMaterialParaRemover)
            {
                _context.ParesProdutoMaterial.Remove(pm);
                await _context.SaveChangesAsync();
            }

            _context.Produtos.Remove(produto);
            await _context.SaveChangesAsync();

            return Ok(produto);
        }


    }
}
