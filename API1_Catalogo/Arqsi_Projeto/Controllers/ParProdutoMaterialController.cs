using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Arqsi_Projeto.Models;

namespace arqsi_projeto.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParProdutoMaterialController : ControllerBase
    {
        private readonly ArmariosContext _context;

        public ParProdutoMaterialController(ArmariosContext context)
        {
            _context = context;
        }

        // GET: api/ParProdutoMaterial
        [HttpGet]
        public IEnumerable<ParProdutoMaterial> GetParesProdutoMaterial()
        {
            return _context.ParesProdutoMaterial;
        }

        // GET: api/ParProdutoMaterial/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetParProdutoMaterial([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var parProdutoMaterial = await _context.ParesProdutoMaterial.FindAsync(id);

            if (parProdutoMaterial == null)
            {
                return NotFound();
            }

            return Ok(parProdutoMaterial);
        }

        // PUT: api/ParProdutoMaterial/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutParProdutoMaterial([FromRoute] int id, [FromBody] ParProdutoMaterial parProdutoMaterial)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != parProdutoMaterial.ParProdutoMaterialId)
            {
                return BadRequest();
            }

            _context.Entry(parProdutoMaterial).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ParProdutoMaterialExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ParProdutoMaterial
        [HttpPost]
        public async Task<IActionResult> PostParProdutoMaterial([FromBody] ParProdutoMaterial parProdutoMaterial)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.ParesProdutoMaterial.Add(parProdutoMaterial);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetParProdutoMaterial", new { id = parProdutoMaterial.ParProdutoMaterialId }, parProdutoMaterial);
        }

        // DELETE: api/ParProdutoMaterial/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteParProdutoMaterial([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var parProdutoMaterial = await _context.ParesProdutoMaterial.FindAsync(id);
            if (parProdutoMaterial == null)
            {
                return NotFound();
            }

            _context.ParesProdutoMaterial.Remove(parProdutoMaterial);
            await _context.SaveChangesAsync();

            return Ok(parProdutoMaterial);
        }

        private bool ParProdutoMaterialExists(int id)
        {
            return _context.ParesProdutoMaterial.Any(e => e.ParProdutoMaterialId == id);
        }
    }
}