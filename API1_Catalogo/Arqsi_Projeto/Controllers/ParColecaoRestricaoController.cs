using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Arqsi_Projeto.Models;
using Arqsi_Projeto.DTOs;
using Microsoft.AspNetCore.Authorization;

namespace arqsi_projeto.Controllers
{
    [Route("api/ParColecaoRestricao")]
    [ApiController]
    public class ParColecaoRestricaoController : ControllerBase
    {
        private readonly ArmariosContext _context;

        public ParColecaoRestricaoController(ArmariosContext context)
        {
            _context = context;
        }

        // GET: api/ParColecaoRestricao
        [HttpGet]
        public IEnumerable<ParColecaoRestricao> GetParesColecaoRestricao()
        {
            return _context.ParesColecaoRestricao;
        }

        // GET: api/ParColecaoRestricao/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetParColecaoRestricao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var parColecaoRestricao = await _context.ParesColecaoRestricao.FindAsync(id);

            if (parColecaoRestricao == null)
            {
                return NotFound();
            }

            return Ok(parColecaoRestricao);
        }


        // POST: api/ParColecaoRestricao
        [HttpPost]
        public async Task<IActionResult> PostParColecaoRestricao([FromBody] ParColecaoRestricaoDTO parColecaoRestricaoDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            var colecao = _context.Colecoes.Where(c => c.ColecaoId==parColecaoRestricaoDTO.ColecaoID).First();
            var restricao = _context.RestricoesColecao.Where(r => r.RestricaoColecaoId==parColecaoRestricaoDTO.RestricaoID).First();

            ParColecaoRestricao parColecaoRestricao = new ParColecaoRestricao();

            parColecaoRestricao.Colecao=colecao;
            parColecaoRestricao.RestricaoColecao=restricao;
            parColecaoRestricao.ColecaoId=parColecaoRestricaoDTO.ColecaoID;
            parColecaoRestricao.RestricaoColecaoId=parColecaoRestricaoDTO.RestricaoID;
            

            _context.ParesColecaoRestricao.Add(parColecaoRestricao);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetParColecaoRestricao", new { id = parColecaoRestricao.ParColecaoRestricaoId }, parColecaoRestricao);
        }

        // PUT: api/ParColecaoRestricao/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutParColecaoRestricao([FromRoute] int id, [FromBody] ParColecaoRestricaoDTO parColecaoRestricaoDTO)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            ParColecaoRestricao parColecaoRestricao = await _context.ParesColecaoRestricao.FindAsync(id);
            if(parColecaoRestricaoDTO.ColecaoID!=-1){
                var colecao = _context.Colecoes.Find(parColecaoRestricaoDTO.ColecaoID);
                parColecaoRestricao.Colecao=colecao;
            }
            if(parColecaoRestricaoDTO.RestricaoID!=-1){
                var restricao = _context.RestricoesColecao.Find(parColecaoRestricaoDTO.RestricaoID);
                parColecaoRestricao.RestricaoColecao=restricao;
            }

            _context.Update(parColecaoRestricao);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ParColecaoRestricaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Ok(parColecaoRestricao);
        }


        // DELETE: api/ParColecaoRestricao/5
        [HttpDelete("{id}")]
        public ActionResult DeleteParColecaoRestricao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var parColecaoRestricao = _context.ParesColecaoRestricao.Find(id);
            if (parColecaoRestricao == null)
            {
                return NotFound();
            }

            _context.ParesColecaoRestricao.Remove(parColecaoRestricao);
            _context.SaveChanges();

            return Ok(parColecaoRestricao);
        }

        private bool ParColecaoRestricaoExists(int id)
        {
            return _context.ParesColecaoRestricao.Any(e => e.ParColecaoRestricaoId == id);
        }
    }
}
