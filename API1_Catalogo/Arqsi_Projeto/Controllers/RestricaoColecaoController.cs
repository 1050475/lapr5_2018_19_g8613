using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Arqsi_Projeto.Models;
using Arqsi_Projeto.DTOs;

namespace arqsi_projeto.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RestricaoColecaoController : ControllerBase
    {
        private readonly ArmariosContext _context;

        public RestricaoColecaoController(ArmariosContext context)
        {
            _context = context;
        }

        // GET: api/RestricaoColecao
        [HttpGet]
        public IEnumerable<RestricaoColecao> GetRestricoesColecao()
        {
            return _context.RestricoesColecao;
        }

        // GET: api/RestricaoColecao/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetRestricaoColecao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            var restricaoColecao = await _context.RestricoesColecao.FindAsync(id);

            if (restricaoColecao == null)
            {
                return NotFound();
            }

            return Ok(restricaoColecao);
        }



        // POST: api/RestricaoColecao
        [HttpPost]
        public async Task<IActionResult> PostRestricaoColecao([FromBody] RestricaoDTO restricaoDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (restricaoDTO.Type.Equals("Material"))
            {
                RestricaoColecaoMaterial restricaoColecaoMaterial = new RestricaoColecaoMaterial();
                var material = await _context.Materiais.FindAsync(restricaoDTO.MaterialID);
                restricaoColecaoMaterial.Material = material;
                restricaoColecaoMaterial.MaterialId = material.MaterialId;
                _context.RestricoesColecao.Add(restricaoColecaoMaterial);
                await _context.SaveChangesAsync();
                return CreatedAtAction("GetRestricaoColecao", new { id = restricaoColecaoMaterial.RestricaoColecaoId }, restricaoColecaoMaterial);

            }
            else if (restricaoDTO.Type.Equals("Acabamento"))
            {
                RestricaoColecaoAcabamento restricaoColecaoAcabamento = new RestricaoColecaoAcabamento();
                var acabamento = await _context.Acabamentos.FindAsync(restricaoDTO.AcabamentoID);
                restricaoColecaoAcabamento.Acabamento = acabamento;
                restricaoColecaoAcabamento.AcabamentoId = acabamento.AcabamentoId;
                _context.RestricoesColecao.Add(restricaoColecaoAcabamento);
                await _context.SaveChangesAsync();
                return CreatedAtAction("GetRestricaoColecao", new { id = restricaoColecaoAcabamento.RestricaoColecaoId }, restricaoColecaoAcabamento);
            }
            else if (restricaoDTO.Type.Equals("Continua"))
            {
                RestricaoColecaoDimensaoContinua restricaoColecaoDimensaoContinua = new RestricaoColecaoDimensaoContinua();
                if (restricaoDTO.MaxAltura != 0 && restricaoDTO.MinAltura != 0)
                {
                    restricaoColecaoDimensaoContinua.MaxAltura = restricaoDTO.MaxAltura;
                    restricaoColecaoDimensaoContinua.MinAltura = restricaoDTO.MinAltura;
                }
                if (restricaoDTO.MaxLargura != 0 && restricaoDTO.MinLargura != 0)
                {
                    restricaoColecaoDimensaoContinua.MinLargura = restricaoDTO.MinLargura;
                    restricaoColecaoDimensaoContinua.MaxLargura = restricaoDTO.MaxLargura;
                }
                if (restricaoDTO.MaxProfundidade != 0 && restricaoDTO.MinProfundidade != 0)
                {
                    restricaoColecaoDimensaoContinua.MinProfundidade = restricaoDTO.MinProfundidade;
                    restricaoColecaoDimensaoContinua.MaxProfundidade = restricaoDTO.MaxProfundidade;
                }
                _context.RestricoesColecao.Add(restricaoColecaoDimensaoContinua);
                await _context.SaveChangesAsync();
                return CreatedAtAction("GetRestricaoColecao", new { id = restricaoColecaoDimensaoContinua.RestricaoColecaoId }, restricaoColecaoDimensaoContinua);
            }
            else
            {
                RestricaoColecaoDimensaoDiscreta restricaoColecaoDimensaoDiscreta = new RestricaoColecaoDimensaoDiscreta();
                if(restricaoDTO.Largura!=0){
                restricaoColecaoDimensaoDiscreta.Largura = restricaoDTO.Largura;
                }
                if(restricaoDTO.Altura!=0){
                restricaoColecaoDimensaoDiscreta.Altura = restricaoDTO.Altura;
                }
                if(restricaoDTO.Profundidade!=0){
                restricaoColecaoDimensaoDiscreta.Profundidade = restricaoDTO.Profundidade;
                }
                
                
                _context.RestricoesColecao.Add(restricaoColecaoDimensaoDiscreta);
                await _context.SaveChangesAsync();
                return CreatedAtAction("GetRestricaoColecao", new { id = restricaoColecaoDimensaoDiscreta.RestricaoColecaoId }, restricaoColecaoDimensaoDiscreta);
            }
        }

        // DELETE: api/RestricaoColecao/5
        [HttpDelete("{id}")]
        public ActionResult DeleteRestricaoColecao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var restricaoColecao = _context.RestricoesColecao.Find(id);
            if (restricaoColecao == null)
            {
                return NotFound();
            }

            _context.RestricoesColecao.Remove(restricaoColecao);
            _context.SaveChanges();

            return Ok(restricaoColecao);
        }

        private bool MaterialExists(int id)
        {
            return _context.RestricoesColecao.Any(e => e.RestricaoColecaoId == id);
        }
    }
}