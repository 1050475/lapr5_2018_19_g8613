using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Arqsi_Projeto.Models;
using Arqsi_Projeto.DTOs;
using Microsoft.AspNetCore.Authorization;

namespace arqsi_projeto.Controllers
{
    [Produces("application/json")]
    [Route("api/ParPrecoAcabamento")]
    public class ParPrecoAcabamentoController : Controller
    {
        private readonly ArmariosContext _context;

        public ParPrecoAcabamentoController(ArmariosContext context)
        {
            _context = context;
        }


        // GET: api/ParPrecoAcabamento
        [HttpGet]
        public IEnumerable<ParPrecoAcabamento> GetParesPrecoAcabamento()
        {
            return _context.ParesPrecoAcabamento;
        }

        // GET: api/ParPrecoAcabamento/1
        [HttpGet("{id}")]
        public async Task<IActionResult> GetParPrecoAcabamento([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var ParPrecoAcabamento = await _context.ParesPrecoAcabamento.FindAsync(id);

            if (ParPrecoAcabamento == null)
            {
                return NotFound();
            }

            return Ok(ParPrecoAcabamento);
        
    }
        // POST: api/ParPrecoAcabamento
        [HttpPost]
        public async Task<IActionResult> PostParPrecoAcabamento([FromBody] ParPrecoAcabamentoDTO parPrecoAcabamentoDTO)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }



            var material = _context.Materiais.Where(m => m.MaterialId == parPrecoAcabamentoDTO.MaterialId).First();

            var acabamento = _context.Acabamentos.Where(a => a.AcabamentoId == parPrecoAcabamentoDTO.AcabamentoId).First();

            if (material == null)
            {

                return StatusCode(409, "Material Not Found");

            }

            if (acabamento == null)
            {

                return StatusCode(409, "Acabamento Not Found");

            }

            ParPrecoAcabamento parPrecoAcabamento = new ParPrecoAcabamento();
            parPrecoAcabamento.MaterialId = parPrecoAcabamentoDTO.MaterialId;
            parPrecoAcabamento.AcabamentoId = parPrecoAcabamentoDTO.AcabamentoId;
            parPrecoAcabamento.DataInicio = parPrecoAcabamentoDTO.DataInicio;
            parPrecoAcabamento.PrecoAcab = parPrecoAcabamentoDTO.PrecoAcab;


            _context.ParesPrecoAcabamento.Add(parPrecoAcabamento);
            await _context.SaveChangesAsync();

            return CreatedAtAction("PostParPrecoAcabamento", new { id = parPrecoAcabamento.ID }, parPrecoAcabamento);
        }

        // DELETE: api/ParPrecoAcabamento/1
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteParPrecoAcabamento([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var parPrecoAcabamento = await _context.ParesPrecoAcabamento.FindAsync(id);
            if (parPrecoAcabamento == null)
            {
                return NotFound();
            }

            _context.ParesPrecoAcabamento.Remove(parPrecoAcabamento);
            await _context.SaveChangesAsync();

            return Ok(parPrecoAcabamento);
        }

        private bool ParPrecoAcabamentoExists(int id)
        {
            return _context.ParesPrecoAcabamento.Any(e => e.ID == id);
        }

        // PUT: api/ParPrecoAcabamento/1
        [HttpPut("{id}")]
        public async Task<IActionResult> PutParPrecoAcabamento([FromRoute] int id, [FromBody] ParPrecoAcabamentoDTO parPrecoAcabamentoDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var parPrecoAcabamento = await _context.ParesPrecoAcabamento.FindAsync(id);
            DateTime defaultDatetime = new DateTime(1, 1, 1, 0, 0, 0);

            if (parPrecoAcabamentoDTO.MaterialId != 0)
            {
                parPrecoAcabamento.MaterialId = parPrecoAcabamentoDTO.MaterialId;
            }

            if (parPrecoAcabamentoDTO.AcabamentoId != 0)
            {
                parPrecoAcabamento.AcabamentoId = parPrecoAcabamentoDTO.AcabamentoId;
            }

            if (parPrecoAcabamentoDTO.DataInicio != defaultDatetime)
            {
                parPrecoAcabamento.DataInicio = parPrecoAcabamentoDTO.DataInicio;

            }

            if (parPrecoAcabamentoDTO.PrecoAcab != 0)
            {
                parPrecoAcabamento.PrecoAcab = parPrecoAcabamentoDTO.PrecoAcab;
            }

            _context.Update(parPrecoAcabamento);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ParPrecoAcabamentoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(parPrecoAcabamento);
        }
    }
}

