using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Arqsi_Projeto.Models;

namespace arqsi_projeto.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MedidaController : ControllerBase
    {
        private readonly ArmariosContext _context;

        public MedidaController(ArmariosContext context)
        {
            _context = context;
        }

        // GET: api/Medida
        [HttpGet]
        public IEnumerable<Medida> GetMedidas()
        {
            return _context.Medidas;
        }



    }


}