using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Arqsi_Projeto.Models;
using Arqsi_Projeto.DTOs;
using Microsoft.AspNetCore.Authorization;

namespace arqsi_projeto.Controllers
{
    [Produces("application/json")]
    [Route("api/Colecao")]
    public class ColecaoController : Controller
    {
        private readonly ArmariosContext _context;

        public ColecaoController(ArmariosContext context)
        {
            _context = context;
        }


        // GET: api/Colecao
        [HttpGet]
        public IEnumerable<Colecao> GetColecao()
        {
            return _context.Colecoes;
        }


        // POST: api/Colecao
        [HttpPost]
        public async Task<IActionResult> PostCalecao([FromBody] ColecaoDTO colecaoDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Colecao colecao = this.DtoToColecao(colecaoDTO);

            _context.Colecoes.Add(colecao);

            await _context.SaveChangesAsync();

            return CreatedAtAction("GetColecao", new { id = colecao.ColecaoId }, colecao);
        }


        // PUT: api/Colecao/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutColecao([FromRoute] int id, [FromBody] ColecaoDTO colecaoDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var colecao = await _context.Colecoes.FindAsync(id);
            DateTime defaultDatetime = new DateTime(1, 1, 1, 0, 0, 0);

            if (colecaoDTO.Name != null)
            {
                colecao.ColecaoNome = colecaoDTO.Name;
            }
            if (colecaoDTO.DataInicio != defaultDatetime)
            {
                colecao.DataInicio = colecaoDTO.DataInicio;

            }

            if (colecaoDTO.DataFim != defaultDatetime)
            {
                colecao.DataFim = colecaoDTO.DataFim;
            }

            _context.Update(colecao);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ColecaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(colecao);
        }

        // GET: api/Colecao/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetColecao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var colecao = await _context.Colecoes.FindAsync(id);

            if (colecao == null)
            {
                return NotFound();
            }

            return Ok(colecao);
        }

        // DELETE: api/Colecao/5
        [HttpDelete("{id}")]
        public ActionResult DeleteColecao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var colecao = _context.Colecoes.Find(id);
            if (colecao == null)
            {
                return NotFound();
            }
            var parProdutoColecao = _context.ParesColecaoProduto.Where(p=>p.Colecao.ColecaoId==id);
            foreach(ParProdutoColecao pp in parProdutoColecao){
                _context.ParesColecaoProduto.Remove(pp);
            }
            var parColecaoRestricao = _context.ParesColecaoRestricao.Where(p=>p.Colecao.ColecaoId==id);
            foreach(ParColecaoRestricao pp  in parColecaoRestricao){
                _context.ParesColecaoRestricao.Remove(pp);
            }

            _context.Colecoes.Remove(colecao);
            _context.SaveChanges();

            return Ok(colecao);
        }

        private bool ColecaoExists(int id)
        {
            return _context.Colecoes.Any(e => e.ColecaoId == id);
        }

        public Colecao DtoToColecao(ColecaoDTO colecaoDTO){
                Colecao colecao = new Colecao();
                colecao.SKU = colecaoDTO.SKU;
                colecao.ColecaoNome = colecaoDTO.Name;
                colecao.DataInicio=colecaoDTO.DataInicio;
                colecao.DataFim=colecaoDTO.DataFim;
                return colecao;
    }
    }

    
}

