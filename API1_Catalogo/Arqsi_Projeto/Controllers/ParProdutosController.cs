using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Arqsi_Projeto.Models;
using Arqsi_Projeto.DTOs;
using Microsoft.AspNetCore.Authorization;

namespace arqsi_projeto.Controllers
{
    //[Authorize(AuthenticationSchemes = "Bearer")]
    [Produces("application/json")]
    [Route("api/ParProduto")]
    public class ParProdutosController : Controller
    {
        private readonly ArmariosContext _context;

        public ParProdutosController(ArmariosContext context)
        {
            _context = context;
        }

        // GET: api/ParProdutos
        [HttpGet]
        public IEnumerable<ParProdutos> GetParesProdutos()
        {
            return _context.ParesProdutos;
        }

        // GET: api/ParProdutos/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetParProdutos([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var parProdutos = await _context.ParesProdutos.FindAsync(id);

            if (parProdutos == null)
            {
                return NotFound();
            }

            return Ok(parProdutos);
        }

        // PUT: api/ParProdutos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutParProdutos([FromRoute] int id, [FromBody] ParProdutoDto parProdutosdto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var produto1 = _context.Produtos.Where(p => p.SKU.Equals(parProdutosdto.SKU1)).Include(p => p.Altura.Medidas).Include(p => p.Largura.Medidas).Include(p => p.Profundidade.Medidas).Include(p => p.ListaRestricoes).First();

            var produto2 = _context.Produtos.Where(p => p.SKU.Equals(parProdutosdto.SKU2)).Include(p => p.Altura.Medidas).Include(p => p.Largura.Medidas).Include(p => p.Profundidade.Medidas).Include(p => p.ListaRestricoes).First();

            Boolean flag = false;

            flag = validaAgregacao(produto1, produto2);

            if (flag)
            {
                ParProdutos parProdutos = new ParProdutos();

                parProdutos.ProdutoBaseId = produto1.ProdutoId;
                parProdutos.ProdutoParteId = produto2.ProdutoId;
                parProdutos.Obrigatorio = parProdutosdto.Obrigatorio;

                _context.Entry(parProdutos).State = EntityState.Modified;

                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ParProdutosExists(id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return NoContent();


            }
            else
            {
                return Ok("Agregação inválida");
            }

        }

        // POST: api/ParProdutos
        [HttpPost]
        public async Task<IActionResult> PostParProdutosSkus([FromBody] ParProdutoDto parProdutosdto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var produto1 = _context.Produtos.Where(p => p.SKU.Equals(parProdutosdto.SKU1)).Include(p => p.Altura.Medidas).Include(p => p.Largura.Medidas).Include(p => p.Profundidade.Medidas).Include(p => p.ListaRestricoes).First();

            var produto2 = _context.Produtos.Where(p => p.SKU.Equals(parProdutosdto.SKU2)).Include(p => p.Altura.Medidas).Include(p => p.Largura.Medidas).Include(p => p.Profundidade.Medidas).Include(p => p.ListaRestricoes).First();

            Boolean flag = false;

            flag = validaAgregacao(produto1, produto2);

            if (flag)
            {
                ParProdutos parProdutos = new ParProdutos();

                parProdutos.ProdutoBaseId = produto1.ProdutoId;
                parProdutos.ProdutoParteId = produto2.ProdutoId;
                parProdutos.Obrigatorio = parProdutosdto.Obrigatorio;

                _context.ParesProdutos.Add(parProdutos);
                await _context.SaveChangesAsync();

                return CreatedAtAction("GetParProdutos", new { id = parProdutos.ParProdutosId }, parProdutos);
            }
            else
            {
                return Ok("Agregação inválida");
            }

        }

        public Boolean validaAgregacao(Produto produto1, Produto produto2)
        {

            ProdutoDto p1 = ProdutoToDTO(produto1);
            ProdutoDto p2 = ProdutoToDTO(produto2);
            Boolean flag = true;

            flag = validaCaber(p1, p2);

            return flag;
        }

        //Entende-se por caber, haver pelo menos no Prod. Parte uma Dimensão Largura, uma Dim. Altura e uma Dim. Profundidade que caiba no Produto Pai
        public Boolean validaCaber(ProdutoDto p1, ProdutoDto p2)
        {
            Boolean flagLargura = false;

            Boolean flagAltura = false;

            Boolean flagProfundidade = false;


            //LARGURA
            foreach (MedidaContinuaDTO mcLarguraP2 in p2.Largura.MedidasContinuas)
            {
                do
                {
                    foreach (MedidaContinuaDTO mcLarguraP1 in p1.Largura.MedidasContinuas)
                    {
                        if (mcLarguraP2.Minimo >= mcLarguraP1.Minimo && mcLarguraP2.Maximo <= mcLarguraP1.Maximo)
                        {
                            flagLargura = true;
                            break;
                        }
                    }

                    foreach (MedidaDiscretaDTO mdLarguraP1 in p1.Largura.MedidasDiscretas)
                    {
                        if (mcLarguraP2.Minimo <= mdLarguraP1.Valor && mdLarguraP1.Valor <= mcLarguraP2.Maximo)
                        {
                            flagLargura = true;
                            break;
                        }
                    }
                } while (!flagLargura);

            }

            foreach (MedidaDiscretaDTO mdLarguraP2 in p2.Largura.MedidasDiscretas)
            {
                do
                {
                    foreach (MedidaDiscretaDTO mdLargura2P1 in p1.Largura.MedidasDiscretas)
                    {
                        if (mdLarguraP2.Valor == mdLargura2P1.Valor)
                        {
                            flagLargura = true;
                            break;
                        }
                    }

                    foreach(MedidaContinuaDTO mcLargura2P1 in p1.Largura.MedidasContinuas){
                        if(mcLargura2P1.Minimo <= mdLarguraP2.Valor && mdLarguraP2.Valor <= mcLargura2P1.Maximo ){
                            flagLargura = true;
                            break;
                        }
                    }
                } while (!flagLargura);
            }

            //ALTURA
            foreach (MedidaContinuaDTO mcAlturaP2 in p2.Altura.MedidasContinuas)
            {
                do
                {
                    foreach (MedidaContinuaDTO mcAlturaP1 in p1.Altura.MedidasContinuas)
                    {
                        if (mcAlturaP2.Minimo >= mcAlturaP1.Minimo && mcAlturaP2.Maximo <= mcAlturaP1.Maximo)
                        {
                            flagAltura = true;
                            break;
                        }
                    }

                    foreach (MedidaDiscretaDTO mdAlturaP1 in p1.Altura.MedidasDiscretas)
                    {
                        if (mcAlturaP2.Minimo <= mdAlturaP1.Valor && mdAlturaP1.Valor <= mcAlturaP2.Maximo)
                        {
                            flagAltura = true;
                            break;
                        }
                    }
                } while (!flagAltura);

            }

            foreach (MedidaDiscretaDTO mdAlturaP2 in p2.Altura.MedidasDiscretas)
            {
                do
                {
                    foreach (MedidaDiscretaDTO mdAltura2P1 in p1.Altura.MedidasDiscretas)
                    {
                        if (mdAlturaP2.Valor == mdAltura2P1.Valor)
                        {
                            flagAltura = true;
                            break;
                        }
                    }

                    foreach(MedidaContinuaDTO mcAltura2P1 in p1.Altura.MedidasContinuas){
                        if(mcAltura2P1.Minimo <= mdAlturaP2.Valor && mdAlturaP2.Valor <= mcAltura2P1.Maximo ){
                            flagAltura = true;
                            break;
                        }
                    }
                } while (!flagAltura);
            }

            //PROFUNDIDADE
        
            foreach (MedidaContinuaDTO mcProfundidadeP2 in p2.Profundidade.MedidasContinuas)
            {
                do
                {
                    foreach (MedidaContinuaDTO mcProfundidadeP1 in p1.Profundidade.MedidasContinuas)
                    {
                        if (mcProfundidadeP2.Minimo >= mcProfundidadeP1.Minimo && mcProfundidadeP2.Maximo <= mcProfundidadeP1.Maximo)
                        {
                            flagProfundidade = true;
                            break;
                        }
                    }

                    foreach (MedidaDiscretaDTO mdProfundidadeP1 in p1.Profundidade.MedidasDiscretas)
                    {
                        if (mcProfundidadeP2.Minimo <= mdProfundidadeP1.Valor && mdProfundidadeP1.Valor <= mcProfundidadeP2.Maximo)
                        {
                            flagProfundidade = true;
                            break;
                        }
                    }
                } while (!flagProfundidade);

            }

            foreach (MedidaDiscretaDTO mdProfundidadeP2 in p2.Profundidade.MedidasDiscretas)
            {
                do
                {
                    foreach (MedidaDiscretaDTO mdProfundidade2P1 in p1.Profundidade.MedidasDiscretas)
                    {
                        if (mdProfundidadeP2.Valor == mdProfundidade2P1.Valor)
                        {
                            flagProfundidade = true;
                            break;
                        }
                    }

                    foreach(MedidaContinuaDTO mcProfundidade2P1 in p1.Profundidade.MedidasContinuas){
                        if(mcProfundidade2P1.Minimo <= mdProfundidadeP2.Valor && mdProfundidadeP2.Valor <= mcProfundidade2P1.Maximo ){
                            flagProfundidade = true;
                            break;
                        }
                    }
                } while (!flagProfundidade);
            }


            if (flagLargura && flagAltura && flagProfundidade)
            {
                return true;
            }
            else
            {
                return false;
            }

        }


        // DELETE: api/ParProdutos/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteParProdutos([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var parProdutos = await _context.ParesProdutos.FindAsync(id);
            if (parProdutos == null)
            {
                return NotFound();
            }

            _context.ParesProdutos.Remove(parProdutos);
            await _context.SaveChangesAsync();

            return Ok(parProdutos);
        }

        private bool ParProdutosExists(int id)
        {
            return _context.ParesProdutos.Any(e => e.ParProdutosId == id);
        }



        public ProdutoDto ProdutoToDTO(Produto produto)
        {
            List<MedidaContinuaDTO> alturas_continuas = new List<MedidaContinuaDTO>();
            List<MedidaContinuaDTO> larguras_continuas = new List<MedidaContinuaDTO>();
            List<MedidaContinuaDTO> profundidades_continuas = new List<MedidaContinuaDTO>();

            List<MedidaDiscretaDTO> alturas_discretas = new List<MedidaDiscretaDTO>();
            List<MedidaDiscretaDTO> larguras_discretas = new List<MedidaDiscretaDTO>();
            List<MedidaDiscretaDTO> profundidades_discretas = new List<MedidaDiscretaDTO>();

            foreach (Medida m in produto.Altura.Medidas)
            {
                if (m is MedidaContinua)
                {
                    MedidaContinua mc = (MedidaContinua)m;
                    alturas_continuas.Add(new MedidaContinuaDTO(mc.Minimo, mc.Maximo));
                }
                else if (m is MedidaDiscreta)
                {
                    MedidaDiscreta md = (MedidaDiscreta)m;
                    alturas_discretas.Add(new MedidaDiscretaDTO(md.valor));
                }
            }

            foreach (Medida m in produto.Largura.Medidas)
            {
                if (m is MedidaContinua)
                {
                    MedidaContinua mc = (MedidaContinua)m;
                    larguras_continuas.Add(new MedidaContinuaDTO(mc.Minimo, mc.Maximo));
                }
                else if (m is MedidaDiscreta)
                {
                    MedidaDiscreta md = (MedidaDiscreta)m;
                    larguras_discretas.Add(new MedidaDiscretaDTO(md.valor));
                }
            }

            foreach (Medida m in produto.Profundidade.Medidas)
            {
                if (m is MedidaContinua)
                {
                    MedidaContinua mc = (MedidaContinua)m;
                    profundidades_continuas.Add(new MedidaContinuaDTO(mc.Minimo, mc.Maximo));
                }
                else if (m is MedidaDiscreta)
                {
                    MedidaDiscreta md = (MedidaDiscreta)m;
                    profundidades_discretas.Add(new MedidaDiscretaDTO(md.valor));
                }
            }

            DimensaoDTO Altura = new DimensaoDTO(alturas_continuas, alturas_discretas);
            DimensaoDTO Largura = new DimensaoDTO(larguras_continuas, larguras_discretas);
            DimensaoDTO Profundidade = new DimensaoDTO(profundidades_continuas, profundidades_discretas);

            List<MaterialDto> materiais = new List<MaterialDto>();

            var pares_produto_material = _context.ParesProdutoMaterial.Where(pm => pm.Produto.SKU.Equals(produto.SKU)).Include(pm => pm.Material).ThenInclude(m => m.Acabamentos).ToList();

            foreach (ParProdutoMaterial pm in pares_produto_material)
            {
                List<AcabamentoDto> acabamentos = new List<AcabamentoDto>();

                foreach (Acabamento a in pm.Material.Acabamentos)
                {
                    acabamentos.Add(new AcabamentoDto(a.Nome));
                }
                materiais.Add(new MaterialDto(pm.Material.Nome, acabamentos));
            }

            List<RestricaoDimensaoDTO> listaRestricaoDimensaoDTO = new List<RestricaoDimensaoDTO>();
            List<RestricaoMaterialDTO> listaRestricaoMaterialDTO = new List<RestricaoMaterialDTO>();


            foreach (Restricao r in produto.ListaRestricoes)
            {
                if (r is RestricaoDimensao)
                {
                    RestricaoDimensao rm = (RestricaoDimensao)r;
                    listaRestricaoDimensaoDTO.Add(new RestricaoDimensaoDTO(rm.MaxPercentagemOcupacao, rm.MinPercentagemOcupacao));
                }
                else if (r is RestricaoMaterial)
                {
                    RestricaoMaterial rmat = (RestricaoMaterial)r;

                    List<AcabamentoDto> listaAcabamentosDTO = new List<AcabamentoDto>();

                    foreach (Acabamento a in rmat.Material.Acabamentos)
                    {
                        listaAcabamentosDTO.Add(new AcabamentoDto(a.Nome));
                    }

                    listaRestricaoMaterialDTO.Add(new RestricaoMaterialDTO(new MaterialDto(rmat.Material.Nome, listaAcabamentosDTO)));
                }
            }


            ProdutoDto produtoDto = new ProdutoDto(produto.ProdutoId, produto.Nome, produto.SKU, Altura, Largura, Profundidade, materiais, listaRestricaoDimensaoDTO, listaRestricaoMaterialDTO);

            return produtoDto;
        }
    }

}
