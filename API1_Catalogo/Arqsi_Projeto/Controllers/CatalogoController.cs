using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Arqsi_Projeto.Models;
using Arqsi_Projeto.DTOs;
using Microsoft.AspNetCore.Authorization;

namespace arqsi_projeto.Controllers
{
    [Produces("application/json")]
    [Route("api/Catalogo")]
    public class CatalogoController : Controller
    {
        private readonly ArmariosContext _context;

        public CatalogoController(ArmariosContext context)
        {
            _context = context;
        }


        // GET: api/Catalogo
        [HttpGet]
        public IEnumerable<Catalogo> GetCatalogos()
        {
            return _context.Catalogos;
        }


        // POST: api/Catalogo
        [HttpPost]
        public async Task<IActionResult> PostCatalogo([FromBody] Catalogo catalogo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Catalogos.Add(catalogo);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCatalogo", new { id = catalogo.CatalogoId }, catalogo);
        }


        // PUT: api/Catalogo/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCatalogo([FromRoute] int id, [FromBody] CatalogoDTO catalogoDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var catalogo = await _context.Catalogos.FindAsync(id);
            DateTime defaultDatetime = new DateTime(1, 1, 1, 0, 0, 0);

            if (catalogoDTO.Name != null)
            {
                catalogo.CatalogoName = catalogoDTO.Name;
            }
            if (catalogoDTO.DataInicio != defaultDatetime)
            {
                catalogo.DataInicio = catalogoDTO.DataInicio;

            }

            if (catalogoDTO.DataFim != defaultDatetime)
            {
                catalogo.DataFim = catalogoDTO.DataFim;
            }

            _context.Update(catalogo);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CatalogoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(catalogo);
        }

        // GET: api/Catalogo/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCatalogo([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var catalogo = await _context.Catalogos.FindAsync(id);

            if (catalogo == null)
            {
                return NotFound();
            }

            return Ok(catalogo);
        }

        // DELETE: api/Catalogo/5
        [HttpDelete("{id}")]
        public ActionResult DeleteCatalogo([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var catalogo = _context.Catalogos.Find(id);
            if (catalogo == null)
            {
                return NotFound();
            }
            var parProdutoCatalogo = _context.ParesProdutoCatalogo.Where(p=>p.Catalogo.CatalogoId==id);
            foreach(ParProdutoCatalogo pp in parProdutoCatalogo){
                _context.ParesProdutoCatalogo.Remove(pp);
            }

            _context.Catalogos.Remove(catalogo);
            _context.SaveChanges();

            return Ok(catalogo);
        }

        private bool CatalogoExists(int id)
        {
            return _context.Catalogos.Any(e => e.CatalogoId == id);
        }

    }
}

