using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Arqsi_Projeto.Models;

namespace arqsi_projeto.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AcabamentoController : ControllerBase
    {
        private readonly ArmariosContext _context;

        public AcabamentoController(ArmariosContext context)
        {
            _context = context;
        }

        // GET: api/Acabamento
        [HttpGet]
        public IEnumerable<Acabamento> GetAcabamentos()
        {
            return _context.Acabamentos;
        }

        // GET: api/Acabamento/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAcabamento([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var acabamento = await _context.Acabamentos.FindAsync(id);

            if (acabamento == null)
            {
                return NotFound();
            }

            return Ok(acabamento);
        }

        

        // POST: api/Acabamento
        [HttpPost]
        public async Task<IActionResult> PostMaterial([FromBody] Acabamento acabamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Acabamentos.Add(acabamento);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAcabamento", new { id = acabamento.AcabamentoId }, acabamento);
        }

        // DELETE: api/Acabamento/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAcabamento([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var acabamento = await _context.Acabamentos.FindAsync(id);
            if (acabamento == null)
            {
                return NotFound();
            }

            _context.Acabamentos.Remove(acabamento);
            await _context.SaveChangesAsync();

            return Ok(acabamento);
        }

        private bool MaterialExists(int id)
        {
            return _context.Acabamentos.Any(e => e.AcabamentoId == id);
        }
    }
}