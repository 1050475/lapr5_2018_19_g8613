using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Arqsi_Projeto.Models;
using Arqsi_Projeto.DTOs;
using Microsoft.AspNetCore.Authorization;

namespace arqsi_projeto.Controllers
{
    [Route("api/ParProdutoColecao")]
    [ApiController]
    public class ParProdutoColecaoController : ControllerBase
    {
        private readonly ArmariosContext _context;

        public ParProdutoColecaoController(ArmariosContext context)
        {
            _context = context;
        }

        // GET: api/ParProdutoColecao
        [HttpGet]
        public IEnumerable<ParProdutoColecao> GetParesProdutoColecao()
        {
            return _context.ParesColecaoProduto;
        }

        // GET: api/ParProdutoColecao/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetParProdutoColecao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var parProdutoColecao = await _context.ParesColecaoProduto.FindAsync(id);

            if (parProdutoColecao == null)
            {
                return NotFound();
            }

            return Ok(parProdutoColecao);
        }


        // POST: api/ParProdutoColecao
        [HttpPost]
        public async Task<IActionResult> PostParProdutoColecao([FromBody] ParProdutoColecaoDTO parProdutoColecaoDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }


            var colecao = _context.Colecoes.Where(c => c.SKU.Equals(parProdutoColecaoDTO.SKUColecao)).First();
            var produto = _context.Produtos.Where(p => p.SKU.Equals(parProdutoColecaoDTO.SKUProduto)).Include(p => p.Altura.Medidas).Include(p => p.Largura.Medidas).Include(p => p.Profundidade.Medidas).Include(p => p.ListaRestricoes).First();

            ParProdutoColecao parProdutoColecao = new ParProdutoColecao();
            parProdutoColecao.Colecao = colecao;
            parProdutoColecao.ColecaoId=colecao.ColecaoId;
            parProdutoColecao.Produto = produto;
            parProdutoColecao.ProdutoId=produto.ProdutoId;

            _context.ParesColecaoProduto.Add(parProdutoColecao);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetParProdutoColecao", new { id = parProdutoColecao.ParProdutoColecaoId }, parProdutoColecao);
        }

        // PUT: api/ParProdutoColecao/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutParProdutoColecao([FromRoute] int id, [FromBody] ParProdutoColecaoDTO parProdutoColecaoDTO)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ParProdutoColecao parProdutoColecao = await _context.ParesColecaoProduto.FindAsync(id);
            if (parProdutoColecaoDTO.SKUColecao != "noName")
            {
                var colecao = _context.Colecoes.Where(c => c.SKU.Equals(parProdutoColecaoDTO.SKUColecao)).First();
                parProdutoColecao.Colecao = colecao;
            }

            if (parProdutoColecaoDTO.SKUProduto != "noName")
            {
                var produto = _context.Produtos.Where(p => p.SKU.Equals(parProdutoColecaoDTO.SKUProduto)).Include(p => p.Altura.Medidas).Include(p => p.Largura.Medidas).Include(p => p.Profundidade.Medidas).Include(p => p.ListaRestricoes).First();
                parProdutoColecao.Produto = produto;
            }

            _context.Update(parProdutoColecao);

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ParProdutoColecaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Ok(parProdutoColecao);
        }


        // DELETE: api/ParProdutoColecao/5
        [HttpDelete("{id}")]
        public ActionResult DeleteParProdutoColecao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var parProdutoColecao = _context.ParesColecaoProduto.Find(id);
            if (parProdutoColecao == null)
            {
                return NotFound();
            }

            _context.ParesColecaoProduto.Remove(parProdutoColecao);
            _context.SaveChanges();

            return Ok(parProdutoColecao);
        }

        private bool ParProdutoColecaoExists(int id)
        {
            return _context.ParesColecaoProduto.Any(e => e.ParProdutoColecaoId == id);
        }
    }
}
