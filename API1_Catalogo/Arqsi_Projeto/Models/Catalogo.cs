using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Arqsi_Projeto.Models
{
    public class Catalogo
    {
        public int CatalogoId { get; set; }
        public string CatalogoName { get; set; }
        public string SKU { get; set; }
        public DateTime? DataInicio { get; set; }
        public DateTime? DataFim { get; set; }


        public Catalogo()
        {
            this.CatalogoName = "noName";
            this.DataInicio = null;
            this.DataFim = null;
        }

        public Catalogo(string CatalogoName, DateTime DataInicio, DateTime DataFim)
        {
            this.CatalogoName = CatalogoName;
            this.DataInicio = DataInicio;
            this.DataFim = DataFim;
        }
    }
}