using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;


namespace Arqsi_Projeto.Models
{
        public class RestricaoColecaoDimensaoDiscreta:RestricaoColecao{
            public double Largura{get;set;}
            public double Altura{get;set;}
            public double Profundidade{get;set;}

            public RestricaoColecaoDimensaoDiscreta(){
                this.Largura=0;
                this.Altura=0;
                this.Profundidade=0;
            }
            
        }
}