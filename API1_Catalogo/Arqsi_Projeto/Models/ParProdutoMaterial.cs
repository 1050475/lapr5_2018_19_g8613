using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Arqsi_Projeto.Models
{
	    public class ParProdutoMaterial
    {
        public int ParProdutoMaterialId { get; set; }
        
        public int? ProdutoId { get; set; }
        public Produto Produto { get; set; }

        public int? MaterialId { get; set; }
        public Material Material { get; set; }

        public ParProdutoMaterial(){
            this.Produto = null;
            this.Material = null;
        }
    }
}