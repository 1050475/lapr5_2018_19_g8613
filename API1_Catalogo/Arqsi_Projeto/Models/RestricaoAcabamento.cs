using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;


namespace Arqsi_Projeto.Models
{
    public class RestricaoAcabamento : Restricao
    {

        public int AcabamentoId{ get; set; }
        public Acabamento Acabamento{ get; set; }

        public RestricaoAcabamento()
        {
            this.Acabamento = new Acabamento();
        }
    }
}