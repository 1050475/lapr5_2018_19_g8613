using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Arqsi_Projeto.Models
{
    public class Dimensao
    {
        public int DimensaoId { get; set; }
        
        public IList<Medida> Medidas { get; set; }

        public Dimensao(){
            this.Medidas = new List<Medida>();
        }
    }
}