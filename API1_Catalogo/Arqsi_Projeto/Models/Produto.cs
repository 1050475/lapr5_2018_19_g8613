using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Arqsi_Projeto.Models
{
    public class Produto
    {
        public int ProdutoId { get; set; }

        public string Nome { get; set; }

        public string SKU { get; set; }

        //public double Preco { get; set; }

        public int CategoriaId { get; set; }
        public Categoria Categoria { get; set; }

        public int? AlturaId { get; set; }
        public Dimensao Altura { get; set; }

        public int? LarguraId { get; set; }
        public Dimensao Largura { get; set; }

        public int? ProfundidadeId { get; set; }
        public Dimensao Profundidade { get; set; }

        public List<Restricao> ListaRestricoes { get; set; }

        public Produto()
        {
            this.Nome = "noName";
            this.Categoria = null;
            this.Altura = new Dimensao();
            this.Largura = new Dimensao();
            this.Profundidade = new Dimensao();
            this.ListaRestricoes = new List<Restricao>();
        }
    }


}