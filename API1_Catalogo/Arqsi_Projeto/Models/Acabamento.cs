using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Arqsi_Projeto.Models
{
    public class Acabamento
    {
        public int AcabamentoId { get; set; }
        public string Nome { get; set; }        

    }
}