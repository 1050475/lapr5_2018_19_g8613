using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Arqsi_Projeto.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Arqsi_Projeto.Models
{
    public class ArmariosContext : IdentityDbContext<UserEntity>
    {

        public ArmariosContext(DbContextOptions<ArmariosContext> options)
            : base(options)
        { }

        public DbSet<Arqsi_Projeto.Models.Produto> Produtos { get; set; }

        public DbSet<Arqsi_Projeto.Models.Dimensao> Dimensoes { get; set; }
        public DbSet<Arqsi_Projeto.Models.Material> Materiais { get; set; }
        public DbSet<Arqsi_Projeto.Models.Acabamento> Acabamentos { get; set; }
        public DbSet<Arqsi_Projeto.Models.Categoria> Categorias { get; set; }
        public DbSet<Arqsi_Projeto.Models.Medida> Medidas { get; set; }
        //public DbSet<Arqsi_Projeto.Models.Valor> Valores { get; set; }
        public DbSet<Arqsi_Projeto.Models.ParProdutos> ParesProdutos { get; set; }

        //falta para COLECOES
        public DbSet<Arqsi_Projeto.Models.Catalogo> Catalogos { get; set; }
        public DbSet<Arqsi_Projeto.Models.Colecao> Colecoes { get; set; }
        public DbSet<Arqsi_Projeto.Models.ParProdutoCatalogo> ParesProdutoCatalogo { get; set; }
        public DbSet<Arqsi_Projeto.Models.ParProdutoColecao> ParesColecaoProduto { get; set; }
        public DbSet<Arqsi_Projeto.Models.ParColecaoRestricao> ParesColecaoRestricao { get; set; }
        public DbSet<Arqsi_Projeto.Models.RestricaoColecao> RestricoesColecao{get;set;}
        public DbSet<Arqsi_Projeto.Models.ParPrecoMaterial> ParesPrecoMaterial { get; set; }
        public DbSet<Arqsi_Projeto.Models.ParPrecoAcabamento> ParesPrecoAcabamento { get; set; }
        
        public DbSet<Arqsi_Projeto.Models.ParProdutoMaterial> ParesProdutoMaterial { get; set; }
        public DbSet<Arqsi_Projeto.Models.Restricao> Restricoes { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MedidaContinua>().HasBaseType<Medida>();
            modelBuilder.Entity<MedidaDiscreta>().HasBaseType<Medida>();
            modelBuilder.Entity<RestricaoDimensao>().HasBaseType<Restricao>();
            modelBuilder.Entity<RestricaoMaterial>().HasBaseType<Restricao>();
            modelBuilder.Entity<RestricaoColecaoAcabamento>().HasBaseType<RestricaoColecao>();
            modelBuilder.Entity<RestricaoColecaoMaterial>().HasBaseType<RestricaoColecao>();
            modelBuilder.Entity<RestricaoColecaoDimensaoContinua>().HasBaseType<RestricaoColecao>();
            modelBuilder.Entity<RestricaoColecaoDimensaoDiscreta>().HasBaseType<RestricaoColecao>();
            base.OnModelCreating(modelBuilder);
        }
    }
}