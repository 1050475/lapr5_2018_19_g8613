using System;
using System.Collections.Generic;

namespace Arqsi_Projeto.Models
{
    public class MedidaContinua : Medida
    {
        public double Minimo { get; set; }

        public double Maximo { get; set; }


        public override string ToString()
        {
            return "[" + Minimo + "<->" + Maximo + "]";
        }
    }
}

