using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Arqsi_Projeto.Models
{
	    public class ParProdutoColecao
    {
        public int ParProdutoColecaoId { get; set; }
        
        public int? ProdutoId { get; set; }
        public Produto Produto { get; set; }

        public int? ColecaoId { get; set; }
        public Colecao Colecao { get; set; }

        public ParProdutoColecao(){
            this.Produto = null;
            this.Colecao = null;
        }
    }
}