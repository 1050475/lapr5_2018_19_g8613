using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Arqsi_Projeto.Models
{
    public class Categoria
    {
        public int CategoriaId { get; set; }
        public string Nome { get; set; }

        public string CategoriaPai { get; set; }
        public ICollection<Categoria> CategoriasFilho { get; set; }

        public Categoria()
        {
            this.Nome = "noName";
            this.CategoriaPai = null;
            this.CategoriasFilho = new List<Categoria>();
        }
        //verifica se o produto pertence a uma categoria
        public static bool verificaIgualdadeCategoria(Categoria categoriaProduto, string categoria, List<Categoria> listaCategorias)
        {

            Categoria proximaCategoria = new Categoria();

            if (categoriaProduto.Nome.Equals(categoria))
            {
                return true;
            }
            if (categoriaProduto.CategoriaPai == null)
            {
                return false;
            }
            else
            {
                //procura no pai da categoria atual
                proximaCategoria = listaCategorias.Where(c => c.Nome.Equals(categoriaProduto.CategoriaPai)).First();
            }

            return verificaIgualdadeCategoria(proximaCategoria, categoria, listaCategorias);
        }
    }
}