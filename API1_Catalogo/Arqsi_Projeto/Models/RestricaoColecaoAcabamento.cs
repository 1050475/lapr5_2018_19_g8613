using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;


namespace Arqsi_Projeto.Models
{
    public class RestricaoColecaoAcabamento : RestricaoColecao
    {

        public int AcabamentoId{ get; set; }
        public Acabamento Acabamento{ get; set; }

        public RestricaoColecaoAcabamento()
        {
            this.Acabamento = new Acabamento();
        }
    }
}