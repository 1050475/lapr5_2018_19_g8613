using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;


namespace Arqsi_Projeto.Models
{
    public class RestricaoColecaoDimensaoContinua : RestricaoColecao
    {
        
        public double MinAltura { get; set; }
        public double MaxAltura { get; set; }
        
        public double MinLargura { get; set; }
        public double MaxLargura { get; set; }

        public double MinProfundidade { get; set; }
        public double MaxProfundidade { get; set; }

        public RestricaoColecaoDimensaoContinua()
        {
            this.MinAltura = 0;
            this.MaxAltura = 0;
            this.MinLargura = 0;
            this.MaxLargura = 0;
            this.MinProfundidade = 0;
            this.MaxProfundidade = 0;
        }
    }
}