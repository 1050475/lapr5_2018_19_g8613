using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Arqsi_Projeto.Models
{
	    public class ParPrecoAcabamento
    {
        public int ID { get; set; }
        public int MaterialId { get; set; }
        public int AcabamentoId { get; set; }
        public DateTime? DataInicio  { get; set; }
        public double PrecoAcab  { get; set; }

    



        public ParPrecoAcabamento(){
            this.MaterialId=0;
            this.AcabamentoId=0;
            this.DataInicio=null;
            this.PrecoAcab=0;

        }
    }
}