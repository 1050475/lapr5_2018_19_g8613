using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Arqsi_Projeto.Models
{
    public class Material
    {
        public int MaterialId { get; set; }

        public string Nome { get; set; }

        public IList<Acabamento> Acabamentos {get; set;}


        public Material(){
            this.Nome = "noName";
            this.Acabamentos = new List<Acabamento>();
        }
    }
}