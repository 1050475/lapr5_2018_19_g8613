using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Arqsi_Projeto.Models
{
    public class ParProdutos
    {
        public int ParProdutosId { get; set; }

        public int? ProdutoBaseId { get; set; }
        public Produto ProdutoBase { get; set; }

        public int? ProdutoParteId { get; set; }
        public Produto ProdutoParte { get; set; }

        public bool Obrigatorio { get; set; }

        public List<Restricao> listaRestricoes{ get; set; }

        public ParProdutos()
        {
            this.ProdutoBase = null;
            this.ProdutoParte = null;
            this.listaRestricoes = new List<Restricao>();
        }

        //dimensoes recebidas como parametro referem-se ao produto base  
        public Produto produtoParteValidado(double alturaArmario, double larguraArmario, double profundidadeArmario)
        {

            Produto produtoParteValidado = this.ProdutoParte;
            Dimensao alturaValidada = new Dimensao();
            Dimensao larguraValidada = new Dimensao();
            Dimensao profundidadeValidada = new Dimensao();

            foreach (Medida m in this.ProdutoParte.Altura.Medidas)
            {
                if (m is MedidaDiscreta)
                {
                    MedidaDiscreta md = (MedidaDiscreta)m;
                    if (md.valor < alturaArmario)
                    {
                        alturaValidada.Medidas.Add(md);
                    }
                }
                else if (m is MedidaContinua)
                {
                    MedidaContinua mc = (MedidaContinua)m;
                    if (alturaArmario >= mc.Maximo)
                    {
                        alturaValidada.Medidas.Add(mc);
                    }
                    else if (mc.Minimo < alturaArmario && mc.Maximo > alturaArmario)
                    {
                        //medida do armario passa a ser o novo maximo
                        mc.Maximo = alturaArmario;
                        alturaValidada.Medidas.Add(mc);
                    }
                }
            }
            foreach (Medida m in this.ProdutoParte.Largura.Medidas)
            {
                if (m is MedidaDiscreta)
                {
                    MedidaDiscreta md = (MedidaDiscreta)m;
                    if (md.valor < larguraArmario)
                    {
                        larguraValidada.Medidas.Add(md);
                    }
                }
                else if (m is MedidaContinua)
                {
                    MedidaContinua mc = (MedidaContinua)m;
                    if (larguraArmario >= mc.Maximo)
                    {
                        larguraValidada.Medidas.Add(mc);
                    }
                    else if (mc.Minimo < larguraArmario && mc.Maximo > larguraArmario)
                    {
                        //medida do armario passa a ser o novo maximo
                        mc.Maximo = larguraArmario;
                        larguraValidada.Medidas.Add(mc);
                    }
                }
            }

            foreach (Medida m in this.ProdutoParte.Profundidade.Medidas)
            {
                if (m is MedidaDiscreta)
                {
                    MedidaDiscreta md = (MedidaDiscreta)m;
                    if (md.valor < profundidadeArmario)
                    {
                        profundidadeValidada.Medidas.Add(md);
                    }
                }
                else if (m is MedidaContinua)
                {
                    MedidaContinua mc = (MedidaContinua)m;
                    if (profundidadeArmario >= mc.Maximo)
                    {
                        profundidadeValidada.Medidas.Add(mc);
                    }
                    else if (mc.Minimo < profundidadeArmario && mc.Maximo > profundidadeArmario)
                    {
                        //medida do armario passa a ser o novo maximo
                        mc.Maximo = profundidadeArmario;
                        profundidadeValidada.Medidas.Add(mc);
                    }
                }
            }

            produtoParteValidado.Altura = alturaValidada;
            produtoParteValidado.Largura = larguraValidada;
            produtoParteValidado.Profundidade = profundidadeValidada;

            if(!alturaValidada.Medidas.Any() || !larguraValidada.Medidas.Any() || !profundidadeValidada.Medidas.Any()){
                return null;
            }

            return produtoParteValidado;
        }
    }
}