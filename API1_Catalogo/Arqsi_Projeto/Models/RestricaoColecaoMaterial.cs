using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;


namespace Arqsi_Projeto.Models
{
    public class RestricaoColecaoMaterial : RestricaoColecao
    {

        public int MaterialId{ get; set; }
        public Material Material{ get; set; }

        public RestricaoColecaoMaterial()
        {
            this.Material = new Material();
        }
    }
}