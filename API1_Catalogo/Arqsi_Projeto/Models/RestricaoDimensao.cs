using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;


namespace Arqsi_Projeto.Models
{
    public class RestricaoDimensao : Restricao
    {
        public double MaxPercentagemOcupacao{ get; set; }

        public double MinPercentagemOcupacao{ get; set; }

        public RestricaoDimensao()
        {
            this.MaxPercentagemOcupacao = 100;
            this.MinPercentagemOcupacao = 0;
        }

    }
}