using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;


namespace Arqsi_Projeto.Models
{
    public class RestricaoMaterial : Restricao
    {

        public int MaterialId{ get; set; }
        public Material Material{ get; set; }

        public RestricaoMaterial()
        {
            this.Material = new Material();
        }
    }
}