using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Arqsi_Projeto.Models
{
	    public class ParPrecoMaterial
    {
        public int ID { get; set; }
        public int MaterialId { get; set; }
        public DateTime? DataInicio  { get; set; }
        public double PrecoMat  { get; set; }

    



        public ParPrecoMaterial(){
            this.MaterialId=0;
            this.DataInicio=null;
            this.PrecoMat=0;

        }
    }
}