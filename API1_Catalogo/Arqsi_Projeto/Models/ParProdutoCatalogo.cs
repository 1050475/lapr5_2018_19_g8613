using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Arqsi_Projeto.Models
{
	    public class ParProdutoCatalogo
    {
        public int ParProdutoCatalogoId { get; set; }
        
        public int? ProdutoId { get; set; }
        public Produto Produto { get; set; }

        public int? CatalogoId { get; set; }
        public Catalogo Catalogo { get; set; }

        public ParProdutoCatalogo(){
            this.Produto = null;
            this.Catalogo = null;
        }
    }
}