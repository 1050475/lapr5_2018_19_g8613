using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Arqsi_Projeto.Models
{
	    public class Colecao
    {
        public int ColecaoId { get; set; }
        public string ColecaoNome { get; set; }
        public string SKU {get;set;}
        public DateTime? DataInicio { get; set; }
        public DateTime? DataFim { get; set; }
        
        public Colecao(){
            this.ColecaoNome="noName";
        }
        public Colecao(string ColecaoName, DateTime DataInicio, DateTime DataFim){
            this.ColecaoNome = "noName";
            this.DataInicio = null;
            this.DataFim = null;
             
        }

    }
}