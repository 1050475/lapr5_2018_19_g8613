using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Arqsi_Projeto.Models
{
    public class ParColecaoRestricao
    {
        public int ParColecaoRestricaoId{get;set;}
        public int? ColecaoId { get; set; }
        public Colecao Colecao { get; set; }
        public int? RestricaoColecaoId {get;set;}
        public RestricaoColecao RestricaoColecao{get;set;}

        public ParColecaoRestricao(){
            this.Colecao=null;
            this.RestricaoColecao=null;
        }

    }
}