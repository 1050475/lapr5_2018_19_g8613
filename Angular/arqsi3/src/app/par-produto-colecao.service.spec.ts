import { TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'; 
import { RouterTestingModule } from '@angular/router/testing';
import { ParProdutoColecaoService } from './par-produto-colecao.service';

describe('ParProdutoColecaoService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [ FormsModule,HttpClientModule,RouterTestingModule]
  }));

  it('should be created', () => {
    const service: ParProdutoColecaoService = TestBed.get(ParProdutoColecaoService);
    expect(service).toBeTruthy();
  });
});
