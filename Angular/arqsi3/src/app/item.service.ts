import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { HttpHeaders } from '@angular/common/http';
import { DataService } from './data.service';



@Injectable({
  providedIn: 'root'
})

export class ItemService {
  private WebApiIt2url = 'http://localhost:8080/';
  private WebApiIt4url = 'https://localhost:5010/';

  user:any;
  httpOptions ={
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

  constructor(private httpClient: HttpClient,private dataSrv:DataService) { 

  }

  getItems(): Observable<any> {
    return this.httpClient.get(this.WebApiIt2url + 'item').pipe(
    map(this.extractData));
  }

  getEncomendas(): Observable<any> {
    this.createHeader();
    console.log("entrouService",this.httpOptions)
    return this.httpClient.get(this.WebApiIt2url + 'encomenda/all',this.httpOptions).pipe(
    map(this.extractData));
  }

  getUserData(): Observable<any> {
    this.createHeader();
    console.log("entrouService",this.httpOptions)
    return this.httpClient.get(this.WebApiIt2url + 'user/userdata',this.httpOptions).pipe(
    map(this.extractData));
  }

  postArmarioPorMedida(body): Observable<any> {
    this.createHeader();
    //let headers = new Headers({ 'Content-Type': 'application/json' });
          return this.httpClient.post(this.WebApiIt4url + 'api/item', body,this.httpOptions).pipe(
                     map(this.extractData));
      }

  postItem(body): Observable<any> {
    this.createHeader();
    //let headers = new Headers({ 'Content-Type': 'application/json' });
          return this.httpClient.post(this.WebApiIt2url + 'item/create', body).pipe(
                     map(this.extractData));
      }

  putItem(body,id): Observable<any> {
    this.createHeader();
    //let headers = new Headers({ 'Content-Type': 'application/json' });
              return this.httpClient.put(this.WebApiIt2url + 'item/' + id + '/update', body).pipe(
                         map(this.extractData));
          }
  
  deleteItem(id): Observable<any> {
    this.createHeader();
    //let headers = new Headers({ 'Content-Type': 'application/json' });
              return this.httpClient.delete(this.WebApiIt2url + 'item/' + id + '/delete',this.httpOptions).pipe(
                map(this.extractData));
          }

  private extractData(res: Response) {
    return res || { };
  }
  createHeader(){
    this.dataSrv.user.subscribe(user=>this.user = user);
     this.httpOptions ={
          headers: new HttpHeaders({
            'Content-Type':  'application/json',
            'x-access-token':this.user.token
          })
        };;
    }
}
