import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class DataService {


  private idUtilizadorSource = new BehaviorSubject<string>("");
  idUtilizadorActual = this.idUtilizadorSource.asObservable();

  private userSource = new BehaviorSubject<any>("");
  user = this.userSource.asObservable();

  constructor() { }

  setIdUtilizadorActual(id:string){
    this.idUtilizadorSource.next(id);
  }

  setUser(user:any){
    this.userSource.next(user);
  }

  getUtilizadorActual(){
    return this.idUtilizadorSource;
  }
}
