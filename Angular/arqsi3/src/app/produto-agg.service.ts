import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
    // //'Access-Control-Allow-Origin': 'https://localhost:5001',
    // 'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
    // 'Access-Control-Allow-Headers': 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
    // 'Authorization': 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ProdutoAggService {
  private WebApiIt1url = 'https://localhost:5001/api/';

  constructor(private httpClient: HttpClient) { }

  getParProdutos(): Observable<any> {
    return this.httpClient.get(this.WebApiIt1url + 'ParProduto').pipe(
    map(this.extractData));
  }


  postParProduto(body): Observable<any> {
          return this.httpClient.post(this.WebApiIt1url + 'ParProduto', body,httpOptions).pipe(
                     map(this.extractData));
      }
  
  putProduto(body,id): Observable<any> {
      return this.httpClient.put(this.WebApiIt1url + 'ParProduto/' + id, body,httpOptions).pipe(
                  map(this.extractData));
  }

  deleteProdutos(id): Observable<any> {
    return this.httpClient.delete(this.WebApiIt1url + 'Producto/' + id,httpOptions).pipe(
    map(this.extractData));
  }
  private extractData(res: Response) {
    return res || { };
  }
}
