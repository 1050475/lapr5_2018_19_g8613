import { TestBed } from '@angular/core/testing';

import { ProdutoAggService } from './produto-agg.service';

describe('ProdutoAggService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProdutoAggService = TestBed.get(ProdutoAggService);
    expect(service).toBeTruthy();
  });
});
