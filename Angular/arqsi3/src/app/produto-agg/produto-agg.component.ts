import { Component, OnInit } from '@angular/core';
import { ProdutoAggService } from '../produto-agg.service';
import { ProdutoService } from '../produto.service';

@Component({
  selector: 'app-produto-agg',
  templateUrl: './produto-agg.component.html',
  styleUrls: ['./produto-agg.component.css']
})
export class ProdutoAggComponent implements OnInit {

  allParProdutos=[];
  allProdutos=[];
  criarParFlag=false;
  selectedPai:any;
  selectedFilho:any;
  paiToPost=null;
  filhoToPost=null;
  skuFToPost=null;
  skuPToPost=null;
  produtoActual=null;
  parToPost:any;

  constructor(private produtoSrv: ProdutoService,private produtoAggSrv: ProdutoAggService) { }

  ngOnInit() {
    this.getParProdutos();
    this.getProdutos();
  }

  newParProduto(){
    this.criarParFlag=true;
  }

  escolherPai(){
    this.paiToPost = this.selectedPai.produtoId;
  }

  escolherFilho(){
    this.filhoToPost= this.selectedFilho.produtoId;
  }


  private getParProdutos(): void {
    this.produtoAggSrv.getParProdutos().subscribe(data => {
      console.log("Data Recebida:",data);
      this.allParProdutos = data;
    });
  }

  private getProdutos(): void {
    this.produtoSrv.getProdutos().subscribe(data => {
      console.log("Data Recebida:",data);
      this.allProdutos = data;
    });
  }

  private getProdutoById(id): void {
    this.produtoSrv.getProdutoById2(id).subscribe(data => {
      console.log("Data Recebida:",data);
      //this.produtoActual = data;
      this.paiToPost=data;
    });
  }

  private getProdutoById2(id): void {
    this.produtoSrv.getProdutoById2(id).subscribe(data => {
      console.log("Data Recebida:",data);
      //this.produtoActual = data;
      this.filhoToPost=data;
    });
  }

  private postPar(body): void {
    this.produtoAggSrv.postParProduto(body).subscribe(data => {
      console.log("Data Recebida:",data);
      this.parToPost={};
    });
  }

  gravarPar():void{
    this.parToPost={};
    //this.parToPost=this.paiToPost;
    //this.filhoToPost=this.filhoToPost;

    this.getProdutoById(this.paiToPost.produtoId);
    this.getProdutoById2(this.filhoToPost.produtoId);


    this.postPar(this.parToPost);


    this.criarParFlag=false;


  }

}
