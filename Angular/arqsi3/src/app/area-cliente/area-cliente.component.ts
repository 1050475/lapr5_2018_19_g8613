import { Component, OnInit } from '@angular/core';
import { ItemService } from '../item.service';

@Component({
  selector: 'app-area-cliente',
  templateUrl: './area-cliente.component.html',
  styleUrls: ['./area-cliente.component.css']
})
export class AreaClienteComponent implements OnInit {

  constructor(private itemSrv:ItemService) { }

  allInfoUser:any;

  ngOnInit() {
    // this.itemSrv.getEncomendas().subscribe(data => {
    //   console.log("All Encomendas:", data);
      
    // });

    this.itemSrv.getUserData().subscribe(data => {
      console.log("All info:", data);
      this.allInfoUser = data;
    });

  }

}
