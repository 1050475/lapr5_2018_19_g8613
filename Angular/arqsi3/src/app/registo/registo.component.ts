import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { DataService } from '../data.service';
import { UtilizadorService } from '../utilizador.service';

@Component({
  selector: 'app-registo',
  templateUrl: './registo.component.html',
  styleUrls: ['./registo.component.css']
})
export class RegistoComponent implements OnInit {
  utilizadorToPost:any;

  //bind
  //inputs
  inputNome:any;
  inputPsw:any;
  inputPsw2:any;
  inputEmail:any;
  selectedCidade:any;
  //checkbox
  rgpdCheck:any;
  adminCheck:any;
  termosCheck:any;
  gestorCheck:any;
  //flags
  dadosIncompletosFlag=false;
  pswNoMatchFlag=false;
  erroRegistoFlag = false;
  rgpdFlag=false;
  adminFlag =false;
  termosTextoFlag = false;
  rgpdTextoFlag = false;
 

  //idUtilizador partilhado
  idUtilizadorActual :string;

  allCidades:any;

  


  constructor(private utilizadorSrv: UtilizadorService,private router: Router,private dataSrv:DataService) { }

  ngOnInit() {
    this.dataSrv.idUtilizadorActual.subscribe(idUtilizadorActual=>this.idUtilizadorActual = idUtilizadorActual);
    this.utilizadorToPost={};
    this.utilizadorSrv.getCidades().subscribe(data => {
      this.allCidades=data;
    });
  }

  gravarUtilizador(){
    this.termosTextoFlag = false;
    this.rgpdTextoFlag=false;
    this.utilizadorToPost = {};
    if((this.inputNome!="")&&(this.inputPsw!="")&&(this.inputPsw2!="")&&(this.selectedCidade)&&(this.inputEmail!="")){
      if(this.inputPsw==this.inputPsw2){
        if((this.rgpdCheck)&&(this.termosCheck)){
          this.utilizadorToPost.nome = this.inputNome;
          //this.utilizadorToPost.username = this.inputUser;
          this.utilizadorToPost.password = this.inputPsw;
          this.utilizadorToPost.email = this.inputEmail;
          //if cliente ou administrativo
          if(this.adminCheck){
            this.utilizadorToPost.role = 'administrativo';
          }else if(this.gestorCheck){
            this.utilizadorToPost.role = 'gestorcatalogo';
          }else{
            this.utilizadorToPost.role = 'cliente';
          }
          
          //cidade
          this.utilizadorToPost.cidade = this.cidadeActual.id;
          this.utilizadorToPost.aceitacaoTermos = true;

          this.postUtilizador(this.utilizadorToPost);
          this.setUtilizadorActual(this.utilizadorToPost);
          //this.router.navigate(['login']);
        }else{
          this.rgpdFlag = true;
        } 
      }else{
        this.pswNoMatchFlag = true;
      }
    }else{
      this.dadosIncompletosFlag = true;
    }    
    //console.log("log1: " + JSON.stringify(this.utilizadorToPost) + ";checked: " + this.rgpdCheck);
  }
  cidadeActual:any;
  escolherCidade(){
    this.cidadeActual =this.selectedCidade;
  }
  qrCodeFlag = false;
  qrCode:any;
  postUtilizador(body):any{
    this.utilizadorSrv.postUtilizador(body).subscribe(data => {
      //("Data RecebidaPost registo:",data);
        if(data.qrcode){
          //console.log("qrCode",data.qrcode)
          this.qrCode = data.qrcode;
          this.qrCodeFlag = true;
          //this.router.navigate(['verificacao2FA']);
        }else{
          console.log("erro de registo")
          this.erroRegistoFlag = true;
        }
    });
  }

  liTermos(){
    this.termosTextoFlag = true;
    this.rgpdTextoFlag = false;
  }

  liRGPD(){
    this.rgpdTextoFlag = true;
    this.termosTextoFlag = false;
  }

  okTermos(){
    this.termosTextoFlag = false;
  }

  okRGPD(){
    this.rgpdTextoFlag = false;
  }

  //mudar variavel global que contem utilizador
  setUtilizadorActual(id){
    this.dataSrv.setIdUtilizadorActual(id);
  }

  okButton(){
    this.router.navigate(['login']);
  }

}
