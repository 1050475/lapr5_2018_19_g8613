import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'; 
import { RouterTestingModule } from '@angular/router/testing';
import { RegistoComponent } from './registo.component';


// class MockServicoUtilizador{
//   data:any;
//   postUtilizador(body){
//     this.data={};
//     this.data.qrcode = 111;
//     return 
//   }
// }
// class MockRouter{

// }
// class MockServicoData{

// }

describe('RegistoComponent', () => {
  // let utilizadorSrv :MockServicoUtilizador;
  // let router : MockRouter;
  // let dataSrv:MockServicoData;

  let component: RegistoComponent;
  let fixture: ComponentFixture<RegistoComponent>;
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule,HttpClientModule,RouterTestingModule],
      declarations: [ RegistoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not register if nome is empty', () => {
    component.inputNome = "";
    component.inputEmail = "jorge@teste.pt";
    component.inputPsw = "aaa";
    component.inputPsw2 = "aaa";
    component.selectedCidade = "Porto";
    component.rgpdCheck = true;
    component.gravarUtilizador();
    expect(component.dadosIncompletosFlag).toBeTruthy();
  });

  it('should not register if email is empty', () => {
    component.inputNome = "jorge";
    component.inputEmail = "";
    component.inputPsw = "aaa";
    component.inputPsw2 = "aaa";
    component.selectedCidade = "Porto";
    component.rgpdCheck = true;
    component.gravarUtilizador();
    expect(component.dadosIncompletosFlag).toBeTruthy();
  });

  it('should not register if psw is empty', () => {
    component.inputNome = "jorge";
    component.inputEmail = "jorge@teste.pt";
    component.inputPsw = "";
    component.inputPsw2 = "aaa";
    component.selectedCidade = "Porto";
    component.rgpdCheck = true;
    component.gravarUtilizador();
    expect(component.dadosIncompletosFlag).toBeTruthy();
  });

  
  it('should not register if psw  confirmation is empty', () => {
    component.inputNome = "jorge";
    component.inputEmail = "jorge@teste.pt";
    component.inputPsw = "aaa";
    component.inputPsw2 = "";
    component.selectedCidade = "Porto";
    component.rgpdCheck = true;
    component.gravarUtilizador();
    expect(component.dadosIncompletosFlag).toBeTruthy();
  });

  it('should not register if cidade is not chosen', () => {
    component.inputNome = "jorge";
    component.inputEmail = "jorge@teste.pt";
    component.inputPsw = "aaa";
    component.inputPsw2 = "aaa";
    component.selectedCidade = null;
    component.rgpdCheck = true;
    component.gravarUtilizador();
    expect(component.dadosIncompletosFlag).toBeTruthy();
  });

  it('should not register if user doesnt accept rgpd', () => {
    component.inputNome = "jorge";
    component.inputEmail = "jorge@teste.pt";
    component.inputPsw = "aaa";
    component.inputPsw2 = "aaa";
    component.selectedCidade = "Porto";
    component.rgpdCheck = false;
    component.gravarUtilizador();
    expect(component.rgpdFlag).toBeTruthy();
  });

  it('should not register if psw dont match', () => {
    component.inputNome = "jorge";
    component.inputEmail = "jorge@teste.pt";
    component.inputPsw = "aaa";
    component.inputPsw2 = "bbb";
    component.selectedCidade = "Porto";
    component.rgpdCheck = true;
    component.gravarUtilizador();
    expect(component.pswNoMatchFlag).toBeTruthy();
  });

  it('should create json to register client', () => {
    component.inputNome = "jorge";
    component.inputEmail = "jorge@teste.pt";
    component.inputPsw = "aaa";
    component.inputPsw2 = "aaa";
    component.selectedCidade = "Porto";
    component.cidadeActual = {};
    component.cidadeActual.nome = "Porto";
    component.cidadeActual.id = 1111;
    component.rgpdCheck = true;
    component.adminCheck = undefined;
    component.gravarUtilizador();
    expect(component.utilizadorToPost.nome).toBe(component.inputNome);
    expect(component.utilizadorToPost.email).toBe(component.inputEmail);
    expect(component.utilizadorToPost.password).toBe(component.inputPsw);
    expect(component.utilizadorToPost.cidade).toBe(component.cidadeActual.id);
    expect(component.utilizadorToPost.role).toBe("cliente");
    expect(component.utilizadorToPost.aceitacaoTermos).toBe(true);
  });
  it('should create json to register administrativo', () => {
    component.inputNome = "jorge";
    component.inputEmail = "jorge@teste.pt";
    component.inputPsw = "aaa";
    component.inputPsw2 = "aaa";
    component.selectedCidade = "Porto";
    component.cidadeActual = {};
    component.cidadeActual.nome = "Porto";
    component.cidadeActual.id = 1111;
    component.rgpdCheck = true;
    component.adminCheck = true;
    component.gravarUtilizador();
    expect(component.utilizadorToPost.nome).toBe(component.inputNome);
    expect(component.utilizadorToPost.email).toBe(component.inputEmail);
    expect(component.utilizadorToPost.password).toBe(component.inputPsw);
    expect(component.utilizadorToPost.cidade).toBe(component.cidadeActual.id);
    expect(component.utilizadorToPost.role).toBe("administrativo");
    expect(component.utilizadorToPost.aceitacaoTermos).toBe(true);
  });
});
