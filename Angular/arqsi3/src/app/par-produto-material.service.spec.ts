import { TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'; 
import { RouterTestingModule } from '@angular/router/testing';
import { ParProdutoMaterialService } from './par-produto-material.service';

describe('ParProdutoMaterialService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [ FormsModule,HttpClientModule,RouterTestingModule]
  }));

  it('should be created', () => {
    const service: ParProdutoMaterialService = TestBed.get(ParProdutoMaterialService);
    expect(service).toBeTruthy();
  });
});
