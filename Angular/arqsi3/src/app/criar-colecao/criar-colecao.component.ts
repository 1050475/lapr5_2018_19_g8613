import { Component, OnInit } from '@angular/core';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { ProdutoService } from '../produto.service';
import { ColecaoService } from '../colecao.service';
import { ParProdutoColecaoService } from '../par-produto-colecao.service';
import { RestricaoColecaoService } from '../restricao-colecao.service';
import { MaterialService } from '../material.service';
import { AcabamentoService } from '../acabamento.service';
import { ParProdutoMaterialService } from '../par-produto-material.service';
import { ParColecaoRestricaoService } from '../par-colecao-restricao.service';
import { ignoreElements } from 'rxjs/operators';
import { SelectMultipleControlValueAccessor } from '@angular/forms';
import { Router } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
//import { timingSafeEqual } from 'crypto';

@Component({
  selector: 'app-colecao',
  templateUrl: './criar-colecao.component.html',
  styleUrls: ['./criar-colecao.component.css']
})
export class CriarColecaoComponent implements OnInit {

  colecaoToPost: any;
  restricaoColecaoToPost: any;
  restricaoColecao: any;
  restricoesColecao: any;
  allMateriais: any;
  allAcabamentos: any;
  armariosToColecao: any;
  produto: any;
  selectProduto: any;
  parProdutoColecaoToPost: any;
  allColecoes: any;
  allSkus: any;
  selectRestricao: any;
  selectMedida: any;
  restricoesMaterial: any;
  restricoesAcabamento: any;
  restricoesContinua: any;
  restricoesDiscreta: any;
  produtosByRestrictions: any;
  produtosForColecao: any;
  allParesProdutoMaterial: any;
  persistAllProdutos: any;


  //flag
  removerFlag = false;
  skuFlag = true;

  constructor(private router: Router, private produtoSrv: ProdutoService, private parColecaoRestricaoSrv: ParColecaoRestricaoService, private parProdutoMaterialSrv: ParProdutoMaterialService, private acabamentoSrv: AcabamentoService, private materialSrv: MaterialService, private restricaoColecaoSrv: RestricaoColecaoService, private colecaoSrv: ColecaoService, private parProdutoColecaoSrv: ParProdutoColecaoService) { }
  allProdutos: any;
  ngOnInit() {
    this.armariosToColecao = [];
    this.allSkus = [];
    this.restricoesColecao = [];
    this.restricoesDiscreta = [];
    this.restricoesMaterial = [];
    this.restricoesAcabamento = [];
    this.restricoesContinua = [];
    this.produtosByRestrictions = [];
    this.produtosForColecao = [];
    this.allProdutos = [];
    this.allParesProdutoMaterial = [];
    this.allMateriais = [];
    this.persistAllProdutos = [];

    this.produtoSrv.getProdutos().subscribe(data => {
      console.log("DataById:", data);
      this.allProdutos = data;
      this.persistAllProdutos = data;
      this.parProdutoMaterialSrv.getParProdutoMaterial().subscribe(data => {
        console.log("DataById:", data);
        this.allParesProdutoMaterial = data;
      });
    });

    this.colecaoSrv.getColecoes().subscribe(data => {
      console.log("DataById:", data);
      this.allColecoes = data;
      for (let col of this.allColecoes) {
        this.allSkus.push(col.sku);
      }
    });

    this.materialSrv.getMateriais().subscribe(data => {
      console.log("DataById:", data);
      this.allMateriais = data;
    });

  }

  adicionarProduto() {
    this.produto = this.selectProduto;
    this.produtosForColecao.push(this.produto);
  }

  colecaoId: any;
  colecaoSku: any;
  restricaoId: any;
  parColecaoRestricao: any;
  parProdutoColecao: any;

  gravarColecao(colecaoName, sku, dataInicio, dataFinal) {
    this.colecaoToPost = {};
    this.colecaoToPost.Name = colecaoName;
    this.colecaoToPost.SKU = sku;
    this.colecaoToPost.DataInicio = dataInicio;
    this.colecaoToPost.DataFim = dataFinal;
    this.restricaoId = {};

    if (!this.allSkus.includes(sku)) {
      this.colecaoSrv.postColecao(this.colecaoToPost).subscribe(data => {
        console.log("Colecao:", data);
        this.colecaoId = data.colecaoId;
        this.colecaoSku = data.sku;
        for (let restricao of this.restricoesMaterial) {
          this.restricaoColecaoSrv.postRestricaoColecao(restricao).subscribe(data => {
            console.log("Restricao", data);
            this.restricaoId = data.restricaoColecaoId;
            this.parColecaoRestricao = {};
            this.parColecaoRestricao.RestricaoID = this.restricaoId;
            this.parColecaoRestricao.ColecaoId = this.colecaoId;
            this.parColecaoRestricaoSrv.postParColecaoRestricao(this.parColecaoRestricao).subscribe(data => {
              console.log("Par Colecao Restricao", data);
            });
          });
        }

        for (let prod of this.produtosForColecao) {
          this.parProdutoColecao = {};
          this.parProdutoColecao.SKUProduto = prod.sku;
          this.parProdutoColecao.SKUColecao = this.colecaoSku;
          this.parProdutoColecaoSrv.postParProdutoColecao(this.parProdutoColecao).subscribe(data => {
            console.log("Par Produto Colecao", data);
          });
        }
      });
      window.alert("Coleção criado com sucesso!");
      this.router.navigate(['dashboard']);
    } else {
      window.alert("SKU já existe! Introduza de novo!");
    }
  }

  iActual: any;
  flagRemoveMaterial = false;
  removerRestricaoMaterial(i) {
    this.selectRestricao = this.restricoesMaterial[i];
    this.iActual = i;
    if (i > -1) {
      this.restricoesMaterial.splice(i, 1);
    }
    this.removerFlag = true;
    this.atualizaProdutos();
  }

  removerRestricaoDiscreta(i) {

    this.iActual = i;
    if (i > -1) {
      this.restricoesDiscreta.splice(i, 1);
    }
    this.removerFlag = true;
  }

  removerRestricaoContinua(i) {

    this.iActual = i;
    if (i > -1) {
      this.restricoesContinua.splice(i, 1);
    }
    this.removerFlag = true;
  }

  removerProduto(i) {

    this.iActual = i;
    if (i > -1) {
      this.produtosForColecao.splice(i, 1);
    }
    this.removerFlag = true;
  }



  materialFlag = false;
  acabamentoFlag = false;
  discretaFlag = false;
  continuaFlag = false;

  adicionarRestricao() {
    if (this.selectRestricao == 'Material') {
      this.materialFlag = true;
      this.acabamentoFlag = false;
      this.discretaFlag = false;
      this.continuaFlag = false;
    } else if (this.selectRestricao == 'Acabamento') {
      this.acabamentoFlag = true;
      this.acabamentoSrv.getAcabamentos().subscribe(data => {
        console.log("DataById:", data);
        this.allAcabamentos = data;
      });
      this.discretaFlag = false;
      this.continuaFlag = false;
      this.materialFlag = false;
    } else if (this.selectRestricao == 'Medida Discreta') {
      this.discretaFlag = true;
      this.acabamentoFlag = false;
      this.materialFlag = false;
      this.continuaFlag = false;
    } else if (this.selectRestricao == 'Medida Continua') {
      this.continuaFlag = true;
      this.acabamentoFlag = false;
      this.discretaFlag = false;
      this.materialFlag = false;
    }
  }

  larguraContinuaFlag = false;
  profundidadeContinuaFlag = false;
  alturaContinuaFlag = false;
  escolherMedidaContinua() {
    if (this.selectMedida == 'Largura') {
      this.larguraContinuaFlag = true;
    } else if (this.selectMedida == 'Profundidade') {
      this.profundidadeContinuaFlag = true;
    } else if (this.selectMedida == 'Altura') {
      this.alturaContinuaFlag = true;
    }
  }

  larguraDiscretaFlag = false;
  profundidadeDiscretaFlag = false;
  alturaDiscretaFlag = false;
  escolherMedidaDiscreta() {
    this.restricaoColecao = {};
    if (this.selectMedida == 'Largura') {
      this.larguraDiscretaFlag = true;
    } else if (this.selectMedida == 'Profundidade') {
      this.profundidadeDiscretaFlag = true;
    } else if (this.selectMedida == 'Altura') {
      this.alturaDiscretaFlag = true;
    }
  }

  guardarMedidaDiscreta(value) {
    this.restricaoColecao = {};
    if (this.selectMedida == 'Largura') {
      this.restricaoColecao.Type = 'Discreta';
      this.restricaoColecao.Largura = value;
      this.restricaoColecao.nome = 'Largura';
      this.restricoesDiscreta.push(this.restricaoColecao);
    } else if (this.selectMedida == 'Profundidade') {
      this.restricaoColecao.Type = 'Discreta';
      this.restricaoColecao.Largura = value;
      this.restricaoColecao.nome = 'Profundidade';
      this.restricoesDiscreta.push(this.restricaoColecao);
      this.profundidadeDiscretaFlag = true;
    } else if (this.selectMedida == 'Altura') {
      this.restricaoColecao.Type = 'Discreta';
      this.restricaoColecao.Largura = value;
      this.restricaoColecao.nome = 'Altura';
      this.restricoesDiscreta.push(this.restricaoColecao);
      this.alturaDiscretaFlag = true;
    }
    this.discretaFlag = false;
    this.alturaDiscretaFlag = false;
    this.profundidadeDiscretaFlag = false;
    this.larguraDiscretaFlag = false;
  }

  guardarMedidaContinua(min, max) {
    this.restricaoColecao = {};
    if (this.selectMedida == 'Largura') {
      this.restricaoColecao.Type = 'Continua';
      this.restricaoColecao.MinLargura = min;
      this.restricaoColecao.MaxLargura = max;
      this.restricaoColecao.nome = 'Largura';
      this.restricoesContinua.push(this.restricaoColecao);
    } else if (this.selectMedida == 'Profundidade') {
      this.restricaoColecao.Type = 'Continua';
      this.restricaoColecao.MinProfundidade = min;
      this.restricaoColecao.MaxProfundidade = max;
      this.restricaoColecao.nome = 'Profundidade';
      this.restricoesContinua.push(this.restricaoColecao);
      this.profundidadeDiscretaFlag = true;
    } else if (this.selectMedida == 'Altura') {
      this.restricaoColecao.Type = 'Continua';
      this.restricaoColecao.MinAltura = min;
      this.restricaoColecao.MaxAltura = max;
      this.restricaoColecao.nome = 'Altura';
      this.restricoesContinua.push(this.restricaoColecao);
      this.alturaDiscretaFlag = true;
    }
  }

  selectMaterial: any;
  guardarRestricaoMaterial() {
    this.restricaoColecao = {};
    this.restricaoColecao.Type = 'Material';
    this.restricaoColecao.MaterialID = this.selectMaterial.materialId;
    this.restricaoColecao.nome = this.selectMaterial.nome;
    this.restricoesMaterial.push(this.restricaoColecao);
    this.materialFlag = false;
    this.atualizaProdutos();
  }

  nomeMaterialRestricao: any;
  atualizaProdutos() {
    this.nomeMaterialRestricao = {};
    this.produtosByRestrictions = [];
    for (let prod of this.persistAllProdutos) {
      for (let parProdMat of this.allParesProdutoMaterial) {
        for (let mat of this.allMateriais) {
          if (prod.produtoId == parProdMat.produtoId && mat.materialId == parProdMat.materialId) {
            for (let resMat of this.restricoesMaterial) {
              this.nomeMaterialRestricao = this.getNomeMaterialRestricao(resMat);
              if (mat.nome == this.getNomeMaterialRestricao(resMat)) {
                this.produtosByRestrictions.push(prod);
              }
            }
          }
        }
      }
    }
  }

  getNomeMaterialRestricao(resMat): any {
    for (let mat of this.allMateriais) {
      if (mat.materialId == resMat.MaterialID) {
        return mat.nome;
      }
    }
  }

  getProdutoMaterial(produto) {
    for (let mat of this.allMateriais) {
      if (produto.MaterialId == mat.MaterialId) {
        return mat;
      }
    }
  }

  cancelarColecao() {
    this.colecaoToPost = {};
    this.restricaoColecaoToPost = {};
    this.restricaoColecao = {};
    this.restricoesColecao = [];
    this.allMateriais = [];
    this.allAcabamentos = [];
    this.armariosToColecao = [];
    this.produto = {};
    this.selectProduto = {};
    this.parProdutoColecaoToPost = {};
    this.allColecoes = [];
    this.allSkus = [];
    this.selectRestricao = {};
    this.selectMedida = {};
    this.restricoesMaterial = [];
    this.restricoesAcabamento = [];
    this.restricoesContinua = [];
    this.restricoesDiscreta = [];
    this.produtosByRestrictions = [];
    this.produtosForColecao = [];
    this.selectMaterial = {};
    this.iActual = {};
    this.router.navigate(['dashboard']);
  }


}
