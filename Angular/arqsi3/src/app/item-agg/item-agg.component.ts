import { Component, OnInit } from '@angular/core';
import { ItemAggService } from '../item-agg.service';
import { ItemService } from '../item.service';

@Component({
  selector: 'app-item-agg',
  templateUrl: './item-agg.component.html',
  styleUrls: ['./item-agg.component.css']
})
export class ItemAggComponent implements OnInit {

  allParItems=[];
  criarParItemFlag=false;
  editarParitemFlag=false;
  allItems=[];
  selectedPai:any;
  selectedFilho:any;
  filhoToPost:any;
  paiToPost:any;
  parItemToPost:any;
  idActual=null;

  constructor(private itemSrv: ItemService,private itemAggSrv: ItemAggService) { }

  ngOnInit() {
    this.getParItems();
    this.getItems();
    this.parItemToPost={};
  }

  associarIds() :void{

  }

  newParItem():void{
    this.criarParItemFlag=true;
  }

  editarPar(par){
    this.idActual=par._id;
    this.criarParItemFlag=true;
  }

  escolherPai(){
    this.paiToPost=this.selectedPai;
  }

  escolherFilho(){
    this.filhoToPost=this.selectedFilho;
  }

  gravarEditarParItem():void{
    this.parItemToPost.itemPaiCode=this.selectedPai.code;
    this.parItemToPost.itemFilhoCode=this.selectedFilho.code;

    this.putParItem(this.parItemToPost,this.idActual);


  }

  gravarParItem():void{
    this.parItemToPost.itemPaiCode=this.selectedPai.code;
    this.parItemToPost.itemFilhoCode=this.selectedFilho.code;

    this.postParItem(this.parItemToPost);

    this.criarParItemFlag=false;
    this.parItemToPost={};
    this.filhoToPost=null;
    this.paiToPost=null;


  }

  private getParItems(): void {
    this.itemAggSrv.getParItems().subscribe(data => {
      console.log("Data Recebida:",data);
      this.allParItems = data;
    });
  }

  private postParItem(body): any {
    this.itemAggSrv.postParItem(body).subscribe(data => {
      console.log("Data RecebidaPost:",data);
      this.getParItems();
      
    });
  }

  private putParItem(body,id): any {
    this.itemAggSrv.putParItem(body,id).subscribe(data => {
      console.log("Data RecebidaPost:",data);
      this.getParItems();
      this.idActual=null;
      this.criarParItemFlag=false;
    this.parItemToPost={};
    this.filhoToPost=null;
    this.paiToPost=null;
      
    });
  }

  private getItems(): void {
    this.itemSrv.getItems().subscribe(data => {
      console.log("Data Recebida:",data);
      this.allItems = data;
    });
  }


}
