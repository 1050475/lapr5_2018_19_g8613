import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { OnInit } from '@angular/core';


import { HttpHeaders } from '@angular/common/http';
import { DataService } from './data.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

// const httpOptions = {
//   headers: new HttpHeaders({
//     'Content-Type':  'application/json'
//     // //'Access-Control-Allow-Origin': 'https://localhost:5001',
//     // 'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',
//     // 'Access-Control-Allow-Headers': 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
//     // 'Authorization': 'my-auth-token'
//   })
// };


@Injectable({
  providedIn: 'root'
})
export class ProdutoService implements OnInit {
  private WebApiIt1url = 'https://localhost:5001/api/';
  user:any;
  //httpOptions:any;
  httpOptions ={
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

 

  constructor(private httpClient: HttpClient,private dataSrv:DataService) { }
  // httpOptions ={
  //   headers: new HttpHeaders({
  //     'Content-Type':  'application/json',
  //     'token':this.user.token
  //   })
  // };;
  ngOnInit() {
    // this.dataSrv.user.subscribe(user=>this.user = user);
    // console.log("servico",this.user.token)
    // if(this.user.token){
    //   this.httpOptions ={
    //     headers: new HttpHeaders({
    //       'Content-Type':  'application/json'
    //     })
    //   };
    // }else{
    //   this.httpOptions ={
    //     headers: new HttpHeaders({
    //       'Content-Type':  'application/json',
    //       'token': this.user.token
    //     })
    //   };
    // }
  }

 

  getProdutos(): Observable<any> {
    return this.httpClient.get(this.WebApiIt1url + 'Produto').pipe(
    map(this.extractData));
  }

  getProdutoById(sku): Observable<any> {
    return this.httpClient.get(this.WebApiIt1url + 'Produto/sku=' + sku).pipe(
    map(this.extractData));
  }

  getProdutoByCategoria(cat): Observable<any> {
    return this.httpClient.get(this.WebApiIt1url + 'Produto/cat=' + cat).pipe(
    map(this.extractData));
  }

  getProdutoPartes(sku,obrigatorio): Observable<any> {
    return this.httpClient.get(this.WebApiIt1url + 'Produto/sku=' + sku + "/Partes/obrigatorio=" + obrigatorio).pipe(
    map(this.extractData));
  }

  getProdutoPartesCategoria(sku,obrigatorio,cat): Observable<any> {
    return this.httpClient.get(this.WebApiIt1url + 'Produto/sku=' + sku + "/Partes/obrigatorio=" + obrigatorio + "/cat=" + cat).pipe(
    map(this.extractData));
  }

  getProdutoById2(id): Observable<any> {
    return this.httpClient.get(this.WebApiIt1url + 'Produto/' + id).pipe(
    map(this.extractData));
  }

  postProduto(body): Observable<any> {
       this.createHeader();
          console.log("httpHEDAERWORK",this.httpOptions)
          return this.httpClient.post(this.WebApiIt1url + 'Produto', body,this.httpOptions).pipe(
                     map(this.extractData));
      }
  
  putProduto(body,id): Observable<any> {
    this.createHeader();
      return this.httpClient.put(this.WebApiIt1url + 'Produto/' + id, body,this.httpOptions).pipe(
                  map(this.extractData));
  }

  deleteProdutos(id): Observable<any> {
    this.createHeader();
    return this.httpClient.delete(this.WebApiIt1url + 'Produto/' + id,this.httpOptions).pipe(
    map(this.extractData));
  }

  private extractData(res: Response) {
    return res || { };
  }

  createHeader(){
  this.dataSrv.user.subscribe(user=>this.user = user);
   this.httpOptions ={
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'x-access-token':this.user.token
        })
      };;
  }
}