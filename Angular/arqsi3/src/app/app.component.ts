import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { DataService } from './data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  logLbl:any;
  user:any;

  constructor(private router: Router,private dataSrv:DataService) { }

  getLogLbl(){
    return this.logLbl;
  }

  ngOnInit() {
    this.user={};
    this.dataSrv.user.subscribe(user=>this.user = user);
    this.router.navigate(['dashboard']);
  }

  logoutBtn(){
    this.user = {};
    this.dataSrv.setUser(this.user);
    this.router.navigate(['dashboard']);
  }

  


}
