import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ColecaoService {

  private WebApiIt1url = 'http://localhost:5000/api/';
  constructor(private httpClient: HttpClient) { }

  getColecoes(): Observable<any> {
    return this.httpClient.get(this.WebApiIt1url + 'Colecao').pipe(
      map(this.extractData));
  }

  getColecaoById(id): Observable<any> {
    return this.httpClient.get(this.WebApiIt1url + 'Colecao/' + id).pipe(
      map(this.extractData));
  }

  postColecao(body): Observable<any> {
    return this.httpClient.post(this.WebApiIt1url + 'Colecao', body, httpOptions).pipe(
      map(this.extractData));
  }

  putColecao(body, id): Observable<any> {
    return this.httpClient.put(this.WebApiIt1url + 'Colecao/' + id, body, httpOptions).pipe(
      map(this.extractData));
  }

  deleteColecao(id): Observable<any> {
    return this.httpClient.delete(this.WebApiIt1url + 'Colecao/' + id, httpOptions).pipe(
      map(this.extractData));
  }

  private extractData(res: Response) {
    return res || {};
  }
}

