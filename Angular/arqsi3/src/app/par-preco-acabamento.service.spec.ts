import { TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'; 
import { RouterTestingModule } from '@angular/router/testing';
import { ParPrecoAcabamentoService } from './par-preco-acabamento.service';

describe('ParPrecoAcabamentoService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [ FormsModule,HttpClientModule,RouterTestingModule]
  }));

  it('should be created', () => {
    const service: ParPrecoAcabamentoService = TestBed.get(ParPrecoAcabamentoService);
    expect(service).toBeTruthy();
  });
});
