import { TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'; 
import { RouterTestingModule } from '@angular/router/testing';
import { ParColecaoRestricaoService } from './par-colecao-restricao.service';

describe('ParColecaoRestricaoService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [ FormsModule,HttpClientModule,RouterTestingModule]
  }));

  it('should be created', () => {
    const service: ParColecaoRestricaoService = TestBed.get(ParColecaoRestricaoService);
    expect(service).toBeTruthy();
  });
});
