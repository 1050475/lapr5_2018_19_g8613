import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class CatalogoService {

  private WebApiIt1url = 'http://localhost:5000/api/';
  constructor(private httpClient: HttpClient) { }

  getCatalogos(): Observable<any> {
    return this.httpClient.get(this.WebApiIt1url + 'Catalogo').pipe(
      map(this.extractData));
  }

  getCatalogoById(id): Observable<any> {
    return this.httpClient.get(this.WebApiIt1url + 'Catalogo/' + id).pipe(
      map(this.extractData));
  }

  postCatalogo(body): Observable<any> {
    return this.httpClient.post(this.WebApiIt1url + 'Catalogo', body, httpOptions).pipe(
      map(this.extractData));
  }

  putCatalogo(body, id): Observable<any> {
    return this.httpClient.put(this.WebApiIt1url + 'Catalogo/' + id, body, httpOptions).pipe(
      map(this.extractData));
  }

  deleteCatalogo(id): Observable<any> {
    return this.httpClient.delete(this.WebApiIt1url + 'Catalogo/' + id, httpOptions).pipe(
      map(this.extractData));
  }

  private extractData(res: Response) {
    return res || {};
  }
}
