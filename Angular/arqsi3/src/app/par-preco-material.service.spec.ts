import { TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'; 
import { RouterTestingModule } from '@angular/router/testing';
import { ParPrecoMaterialService } from './par-preco-material.service';

describe('ParPrecoMaterialService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [ FormsModule,HttpClientModule,RouterTestingModule]
  }));

  it('should be created', () => {
    const service: ParPrecoMaterialService = TestBed.get(ParPrecoMaterialService);
    expect(service).toBeTruthy();
  });
});
