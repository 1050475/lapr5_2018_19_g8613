import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GestaoCatalogoComponent } from './gestao-catalogo/gestao-catalogo.component';
import { ProdutoComponent } from './produto/produto.component';
import { MaterialComponent } from './material/material.component';
import { GestaoItemComponent } from './gestao-item/gestao-item.component';
import { ItemComponent } from './item/item.component';
import { ItemAggComponent } from './item-agg/item-agg.component';
import { ProdutoAggComponent } from './produto-agg/produto-agg.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RegistoComponent } from './registo/registo.component';
import { ConfRegistoComponent } from './conf-registo/conf-registo.component';
import { EncomendaComponent } from './encomenda/encomenda.component';
import { DoubleFAComponent } from './double-fa/double-fa.component';
import { CatalogoComponent } from './catalogo/catalogo.component';
import { EditarCatalogoComponent } from './editar-catalogo/editar-catalogo.component';
import { PrecosComponent } from './precos/precos.component';
import { CriarColecaoComponent } from './criar-colecao/criar-colecao.component';
import { AreaClienteComponent } from './area-cliente/area-cliente.component';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'registo', component: RegistoComponent },
  { path: 'confirmacao', component: ConfRegistoComponent },
  { path: 'item', component: ItemComponent },
  { path: 'encomenda', component: EncomendaComponent },
  { path: 'verificacao2FA', component: DoubleFAComponent },
  { path: 'catalogo', component: CatalogoComponent },
  { path: 'editar-catalogo', component: EditarCatalogoComponent },
  { path: 'preco', component: PrecosComponent },
  {path: 'dashboard', component: DashboardComponent},
  //path: 'catalogo', component: GestaoCatalogoComponent,
  { path: 'produto', component: ProdutoComponent },
  { path: 'produtoAgg', component: ProdutoAggComponent },
  { path: 'criar-colecao', component: CriarColecaoComponent },
  { path: 'dashboard', component: DashboardComponent},
  { path: 'cliente', component: AreaClienteComponent},
  ];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [GestaoCatalogoComponent, GestaoItemComponent, ProdutoComponent, MaterialComponent,
  ItemAggComponent, ProdutoAggComponent, ItemComponent, LoginComponent, DashboardComponent, RegistoComponent,
  ConfRegistoComponent, EncomendaComponent, CatalogoComponent, DoubleFAComponent, EditarCatalogoComponent, PrecosComponent,
   CriarColecaoComponent,AreaClienteComponent]
