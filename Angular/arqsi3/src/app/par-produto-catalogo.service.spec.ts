import { TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'; 
import { RouterTestingModule } from '@angular/router/testing';
import { ParProdutoCatalogoService } from './par-produto-catalogo.service';

describe('ParProdutoCatalogoService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [ FormsModule,HttpClientModule,RouterTestingModule]
  }));

  it('should be created', () => {
    const service: ParProdutoCatalogoService = TestBed.get(ParProdutoCatalogoService);
    expect(service).toBeTruthy();
  });
});
