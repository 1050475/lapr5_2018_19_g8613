import { Component, OnInit } from '@angular/core';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { ProdutoService } from '../produto.service';
import { ColecaoService } from '../colecao.service';
import { ParProdutoColecaoService } from '../par-produto-colecao.service';
import { ignoreElements } from 'rxjs/operators';
import { SelectMultipleControlValueAccessor } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-editar-colecao',
  templateUrl: './editar-colecao.component.html',
  styleUrls: ['./editar-colecao.component.css']
})
export class EditarColecaoComponent implements OnInit {

  //Estrutura Original
  colecaoToPut: any;
  parProdutoColecaoToPost: any;
  produtosFromColecao: any;
  paresProdutos: any;
  colecao: any;
  produto: any;
  selectColecao: any;
  selectProduto: any;
  colecaoName: any;
  dataInicio: any;
  dataFim: any;

  //Copia
  paresFromColecaoCopia: any;
  //flag
  removerFlag = false;

  constructor(private router: Router, private produtoSrv: ProdutoService, private colecaoSrv: ColecaoService, private parProdutoColecaoSrv: ParProdutoColecaoService) { }
  allProdutos: any;
  allColecoes: any;

  ngOnInit() {
    this.colecaoSrv.getColecoes().subscribe(data => {
      console.log("DataById:", data);
      this.allColecoes = data;
    });

    this.produtoSrv.getProdutos().subscribe(data => {
      console.log("DataById:", data);
      this.allProdutos = data;
    });
  }

  setColecao() {
    this.colecao = this.selectColecao;
    this.colecaoName = this.colecao.colecaoName;
    this.dataInicio = this.colecao.dataInicio;
    this.dataFim = this.colecao.dataFim;
    this.setProdutosFromColecao();
    console.log(this.produtosFromColecao);
  }

  setProdutosFromColecao() {
    this.produtosFromColecao = [];
    this.paresFromColecaoCopia = [];
    this.parProdutoColecaoSrv.getParProdutoColecao().subscribe(data => {
      console.log("DataById:", data);
      this.paresProdutos = data;
      for (let par of this.paresProdutos) {
        if (par.colecaoId == this.colecao.colecaoId) {
          for (let prod of this.allProdutos) {
            if (prod.produtoId == par.produtoId) {
              this.produtosFromColecao.push(prod);
              this.paresFromColecaoCopia.push(par);
            }
          }
        }
      }
    });
  }

  adicionarProduto() {
    this.produto = this.selectProduto;
    this.produtosFromColecao.push(this.produto);
  }

  iActual: any;
  produtoActual: any;
  removerProduto(i) {

    this.produtoActual = this.selectProduto;
    this.iActual = i;
    if (i > -1) {
      this.produtosFromColecao.splice(i, 1);
    }
    this.removerFlag = true;
  }

  apagarColecao() {
    for (let par of this.paresFromColecaoCopia) {
      this.parProdutoColecaoSrv.deleteParProdutoColecao(par.parProdutoColecaoId).subscribe(data => {
        console.log("DataById:", data);
      });
    }

    this.colecaoSrv.deleteColecao(this.colecao.colecaoId).subscribe(data => {
      console.log("DataById:", data);
    });
    window.alert("Coleção removida com sucesso!");
    this.router.navigate(['dashboard']);
  }

  flag: any;
  produtoSku: any;
  parId: any;
  gravarColecao(colecaoName, dataInicio, dataFim) {
    this.colecaoToPut = {};
    this.colecaoToPut.Name = colecaoName;
    this.colecaoToPut.DataInicio = dataInicio;
    this.colecaoToPut.DataFim = dataFim;
    this.colecaoSrv.putColecao(this.colecaoToPut, this.colecao.colecaoId).subscribe(data => {
      console.log("DataById:", data)
    });


    //Adicionar novos pares existentes na coleção
    for (let prod of this.produtosFromColecao) {
      this.flag = false;
      for (let par of this.paresFromColecaoCopia) {
        if (par.produtoId == prod.produtoId) {
          this.flag = true;
        }
      }
      this.produtoSku = prod.sku;
      if (this.flag == false) {
        this.parProdutoColecaoToPost = {};
        this.parProdutoColecaoToPost.SKUProduto = this.produtoSku;
        this.parProdutoColecaoToPost.SKUColecao = this.colecao.sku;
        this.parProdutoColecaoSrv.postParProdutoColecao(this.parProdutoColecaoToPost).subscribe(data => {
          console.log("DataById:" + data)
        });
      }
    }

    //Remover pares não existentes na coleção
    for (let par of this.paresFromColecaoCopia) {
      this.flag = false;
      for (let prod of this.produtosFromColecao) {
        if (par.produtoId == prod.produtoId) {
          this.flag = true;
        }
      }
      this.parId = par.parProdutoColecaoId;
      console.log(this.parId);
      if (this.flag == false) {
        this.parProdutoColecaoSrv.deleteParProdutoColecao(this.parId).subscribe(data => {
          console.log("DataById:" + data)
        });
      }
    }
    window.alert("Coleção alterado com sucesso!");
    this.router.navigate(['dashboard']);
  }

  cancelarColecao() {
    this.allProdutos = [];
    this.allColecoes = [];
    this.paresFromColecaoCopia = [];
    this.colecaoToPut = {};
    this.parProdutoColecaoToPost = {};
    this.produtosFromColecao = [];
    this.paresProdutos = [];
    this.colecao = {};
    this.produto = {};
    this.selectColecao = {};
    this.selectProduto = {};
    this.colecaoName = {};
    this.dataInicio = {};
    this.dataFim = {};
    this.flag = {};
    this.produtoSku = {};
    this.parId = {};
    this.iActual = {};
    this.produtoActual = {};
    this.router.navigate(['dashboard']);
  }


}
