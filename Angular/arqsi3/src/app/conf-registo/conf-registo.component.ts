import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-conf-registo',
  templateUrl: './conf-registo.component.html',
  styleUrls: ['./conf-registo.component.css']
})
export class ConfRegistoComponent implements OnInit {

  
  //idUtilizador partilhado
  idUtilizadorActual :string;

  constructor(private dataSrv:DataService) { }

  ngOnInit() {
    this.dataSrv.idUtilizadorActual.subscribe(idUtilizadorActual=>this.idUtilizadorActual = idUtilizadorActual);
    console.log("idShare",this.idUtilizadorActual);
  }

}
