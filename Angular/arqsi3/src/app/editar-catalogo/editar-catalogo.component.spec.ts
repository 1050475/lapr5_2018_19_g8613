import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'; 
import { RouterTestingModule } from '@angular/router/testing';
import { EditarCatalogoComponent } from './editar-catalogo.component';
import { DashboardComponent } from '../dashboard/dashboard.component';



describe('EditarCatalogoComponent', () => {
  let component: EditarCatalogoComponent;
  let fixture: ComponentFixture<EditarCatalogoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule,HttpClientModule,RouterTestingModule],
      declarations: [ EditarCatalogoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarCatalogoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  RouterTestingModule.withRoutes([
    { path: 'dashboard', component: DashboardComponent }
  ]);
  
  it('should create a json to update catalogo', () => {
    component.catalogoToPut = {};
    component.catalogoName = "catalogoName";
    component.dataInicio = "2019-10-10";
    component.dataFim = "2019-10-10";
    component.gravarCatalogo();
    expect(component.editarFlag).toBeTruthy();
  });

});
