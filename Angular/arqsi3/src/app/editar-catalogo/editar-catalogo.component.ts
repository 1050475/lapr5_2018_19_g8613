import { Component, OnInit } from '@angular/core';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { ProdutoService } from '../produto.service';
import { CatalogoService } from '../catalogo.service';
import { ParProdutoCatalogoService } from '../par-produto-catalogo.service';
import { ignoreElements } from 'rxjs/operators';
import { SelectMultipleControlValueAccessor } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-editar-catalogo',
  templateUrl: './editar-catalogo.component.html',
  styleUrls: ['./editar-catalogo.component.css']
})
export class EditarCatalogoComponent implements OnInit {

  //Estrutura Original
  catalogoToPut: any;
  parProdutoCatalogoToPost: any;
  produtosFromCatalogo: any;
  paresProdutos: any;
  catalogo: any;
  produto: any;
  selectCatalogo: any;
  selectProduto: any;
  catalogoName: any;
  dataInicio: any;
  dataFim: any;
  catalogoId: any;
  //Copia
  paresFromCatalogoCopia: any;
  //flag
  removerFlag = false;
  editarFlag = false;

  constructor(private router: Router, private produtoSrv: ProdutoService, private catalogoSrv: CatalogoService, private parProdutoCatalogoSrv: ParProdutoCatalogoService) { }
  allProdutos: any;
  allCatalogos: any;

  ngOnInit() {
    this.catalogoSrv.getCatalogos().subscribe(data => {
      console.log("DataById:", data);
      this.allCatalogos = data;
    });
    this.produtosFromCatalogo = [];
    this.paresFromCatalogoCopia = [];
    this.produtoSrv.getProdutos().subscribe(data => {
      console.log("DataById:", data);
      this.allProdutos = data;
    });
  }

  setCatalogo() {
    this.catalogo = this.selectCatalogo;
    this.catalogoId = this.catalogo.catalogoId;
    this.catalogoName = this.catalogo.catalogoName;
    this.dataInicio = this.catalogo.dataInicio;
    this.dataFim = this.catalogo.dataFim;
    this.setProdutosFromCatalogo();
    console.log(this.produtosFromCatalogo);
  }

  setProdutosFromCatalogo() {
    this.produtosFromCatalogo = [];
    this.paresFromCatalogoCopia = [];
    this.parProdutoCatalogoSrv.getParProdutoCatalogo().subscribe(data => {
      console.log("DataById:", data);
      this.paresProdutos = data;
      for (let par of this.paresProdutos) {
        if (par.catalogoId == this.catalogo.catalogoId) {
          for (let prod of this.allProdutos) {
            if (prod.produtoId == par.produtoId) {
              this.produtosFromCatalogo.push(prod);
              this.paresFromCatalogoCopia.push(par);
            }
          }
        }
      }
    });
  }

  adicionarProduto() {
    this.produto = this.selectProduto;
    this.produtosFromCatalogo.push(this.produto);
  }

  iActual: any;
  produtoActual: any;
  removerProduto(i) {

    this.produtoActual = this.selectProduto;
    this.iActual = i;
    if (i > -1) {
      this.produtosFromCatalogo.splice(i, 1);
    }
    this.removerFlag = true;
  }

  apagarCatalogo() {
    for (let par of this.paresFromCatalogoCopia) {
      this.parProdutoCatalogoSrv.deleteParProdutoCatalogo(par.parProdutoCatalogoId).subscribe(data => {
        console.log("DataById:", data);
      });
    }

    this.catalogoSrv.deleteCatalogo(this.catalogo.catalogoId).subscribe(data => {
      console.log("DataById:", data);
    });
    window.alert("Catálogo removido com sucesso!");
    this.router.navigate(['dashboard']);
  }

  flag: any;
  produtoSku: any;
  parId: any;
  gravarCatalogo() {
    this.catalogoToPut = {};
    this.catalogoToPut.Name = this.catalogoName;
    this.catalogoToPut.DataInicio = this.dataInicio;
    this.catalogoToPut.DataFim = this.dataFim;
    this.catalogoSrv.putCatalogo(this.catalogoToPut, this.catalogoId).subscribe(data => {
      console.log("DataById:", data)
    });


    //Adicionar novos pares existentes no catalogo
    for (let prod of this.produtosFromCatalogo) {
      this.flag = false;
      for (let par of this.paresFromCatalogoCopia) {
        if (par.produtoId == prod.produtoId) {
          this.flag = true;
        }
      }
      this.produtoSku = prod.sku;
      if (this.flag == false) {
        this.parProdutoCatalogoToPost = {};
        this.parProdutoCatalogoToPost.SKUProduto = this.produtoSku;
        this.parProdutoCatalogoToPost.SKUCatalogo = this.catalogo.sku;
        this.parProdutoCatalogoSrv.postParProdutoCatalogo(this.parProdutoCatalogoToPost).subscribe(data => {
          console.log("DataById:" + data)
        });
      }
    }

    //Remover pares não existentes no catalogo
    for (let par of this.paresFromCatalogoCopia) {
      this.flag = false;
      for (let prod of this.produtosFromCatalogo) {
        if (par.produtoId == prod.produtoId) {
          this.flag = true;
        }
      }
      this.parId = par.parProdutoCatalogoId;
      console.log(this.parId);
      if (this.flag == false) {
        this.parProdutoCatalogoSrv.deleteParProdutoCatalogo(this.parId).subscribe(data => {
          console.log("DataById:" + data)
        });
      }
    }
    this.editarFlag = true;
    window.alert("Catálogo alterado com sucesso!");
    this.router.navigate(['dashboard']);
  }

  cancelarCatalogo() {
    this.allProdutos = [];
    this.allCatalogos = [];
    this.paresFromCatalogoCopia = [];
    this.catalogoToPut = {};
    this.parProdutoCatalogoToPost = {};
    this.produtosFromCatalogo = [];
    this.paresProdutos = [];
    this.catalogo = {};
    this.produto = {};
    this.selectCatalogo = {};
    this.selectProduto = {};
    this.catalogoName = {};
    this.dataInicio = {};
    this.dataFim = {};
    this.flag = {};
    this.produtoSku = {};
    this.parId = {};
    this.iActual = {};
    this.produtoActual = {};
    this.router.navigate(['dashboard']);
  }


}
