import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';

import { DataService } from './data.service';

@Injectable({
  providedIn: 'root'
})
export class EncomendaService {

  private WebApiIt1url = 'http://localhost:8080/';

  user:any;
  httpOptions ={
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

  constructor(private httpClient: HttpClient,private dataSrv:DataService) { 

  }

  getEncomendas(): Observable<any> {
    return this.httpClient.get(this.WebApiIt1url + 'Encomenda').pipe(
    map(this.extractData));
  }

  getEncomendaById(id): Observable<any> {
    return this.httpClient.get(this.WebApiIt1url + 'Encomenda/' + id).pipe(
    map(this.extractData));
  }

  postEncomenda(body): Observable<any> {
    return this.httpClient.post(this.WebApiIt1url + 'Encomenda/create', body,this.httpOptions).pipe(
               map(this.extractData));
}
  
  putEncomenda(body,id): Observable<any> {
      return this.httpClient.put(this.WebApiIt1url + 'Encomenda/' + id, body,this.httpOptions).pipe(
                  map(this.extractData));
  }

  deleteEncomenda(id): Observable<any> {
    return this.httpClient.delete(this.WebApiIt1url + 'Encomenda/' + id,this.httpOptions).pipe(
    map(this.extractData));
  }
  private extractData(res: Response) {
    return res || { };
  }

  createHeader(){
    this.dataSrv.user.subscribe(user=>this.user = user);
     this.httpOptions ={
          headers: new HttpHeaders({
            'Content-Type':  'application/json',
            'x-access-token':this.user.token
          })
        };;
    }
}
