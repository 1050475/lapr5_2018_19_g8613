import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'; 
import { RouterTestingModule } from '@angular/router/testing';


import { LoginComponent } from './login.component';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule,HttpClientModule,RouterTestingModule],
      declarations: [ LoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not login if email is empty', () => {
    component.emailInput = "";
    component.pswInput= "aaaaaa";

    component.loginBtn();
    expect(component.dadosIncompletosFlag).toBeTruthy();
  });

  it('should not register if password is empty', () => {
    component.emailInput = "";
    component.pswInput= "dddd";

    component.loginBtn();
    expect(component.dadosIncompletosFlag).toBeTruthy();
  });

  it('should login', () => {
    component.emailInput = "jorge@email.pt";
    component.pswInput= "aaaaaa";

    component.loginBtn();
    expect(component.authToPost.email).toBe(component.emailInput);
    expect(component.authToPost.password).toBe(component.pswInput);
  });


});
