import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { DataService } from '../data.service';
import { UtilizadorService } from '../utilizador.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  //bindings
  emailInput:any;
  pswInput:any;

  //vars
  authToPost:any;

  
  //flags
  erroLoginFlag = false;
  dadosIncompletosFlag = false;

  
  //idUtilizador partilhado
  idUtilizadorActual :string;
  user:any;
  userToShareTemp:any;
    
  
  constructor(private utilizadorSrv: UtilizadorService,private router: Router,private dataSrv:DataService) { }

  ngOnInit() {
    this.userToShareTemp={};
    this.dataSrv.idUtilizadorActual.subscribe(idUtilizadorActual=>this.idUtilizadorActual = idUtilizadorActual);
    this.dataSrv.user.subscribe(user=>this.user = user);
    this.authToPost = {};
  }

  loginBtn(){
    if((this.emailInput!="")&&(this.pswInput!="")){

      this.authToPost.email = this.emailInput;
      this.authToPost.password = this.pswInput;
      console.log("login btn");
      this.utilizadorSrv.postAuth(this.authToPost).subscribe(data => {
        //console.log("login resp",data)
        if(data.id){
          //console.log("utilizadorActual1",this.idUtilizadorActual)
          this.dataSrv.setIdUtilizadorActual(data.id);
          
          this.userToShareTemp.id = data.id;
          //this.userToShareTemp.role = data.role;
          //this.userToShareTemp.activo = true;
          this.dataSrv.setUser(this.userToShareTemp);

         //console.log("utilizadorActual2",this.idUtilizadorActual)
          this.erroLoginFlag=false;
          this.router.navigate(['verificacao2FA']);
        }else{
          this.erroLoginFlag=true;
        } 
      });
    }else{
      this.dadosIncompletosFlag = true;
    }
  }
}


