import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { UtilizadorService } from '../utilizador.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-double-fa',
  templateUrl: './double-fa.component.html',
  styleUrls: ['./double-fa.component.css']
})
export class DoubleFAComponent implements OnInit {
  verifInput:any;
  jsonToPost:any;
  utilizadorT:any;
  idUtilizadorActual:any;
  user:any;
  codigoErroFlag = false;
  dadosIncompletosFlag = false;

  constructor(private utilizadorSrv: UtilizadorService,private router: Router,private dataSrv:DataService) { }

  ngOnInit() {
    this.dataSrv.user.subscribe(user=>this.user = user);
    this.dataSrv.idUtilizadorActual.subscribe(idUtilizadorActual=>this.idUtilizadorActual = idUtilizadorActual);
    //console.log("utilizadorActual3",this.idUtilizadorActual)
    this.jsonToPost={};
  }

  userTemp:any;
  okButton(){
    if(this.verifInput!=""){
      this.jsonToPost.code = this.verifInput;
      this.jsonToPost.id = this.idUtilizadorActual;
     // console.log("toPost",this.jsonToPost);
      this.utilizadorSrv.postAuth2FA(this.jsonToPost).subscribe(data => {
        //console.log("dataLogin",data);
        if(data.token){
          this.userTemp={};
          this.userTemp.nome = data.nome;
          this.userTemp.role = data.role;
          this.userTemp.token = data.token;
          this.userTemp.id = data.id;
          this.userTemp.activo = true;
          this.dataSrv.setUser(this.userTemp);
          this.router.navigate(['dashboard']);
        }else{
          this.codigoErroFlag = true;
        }
      });
    }else{
      this.dadosIncompletosFlag = true;
    }
  }

}
