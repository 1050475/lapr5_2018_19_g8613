import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'; 
import { RouterTestingModule } from '@angular/router/testing';

import { DoubleFAComponent } from './double-fa.component';

describe('DoubleFAComponent', () => {
  let component: DoubleFAComponent;
  let fixture: ComponentFixture<DoubleFAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule,HttpClientModule,RouterTestingModule],
      declarations: [ DoubleFAComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DoubleFAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not try to 2FA auth if code is empty', () => {
    component.verifInput = "";
    component.okButton();
    expect(component.dadosIncompletosFlag).toBeTruthy();
  });

  it('should send 2FA verification data', () => {
    component.verifInput = "555";
    component.idUtilizadorActual = 111

    component.okButton();
    expect(component.jsonToPost.id).toBe(component.idUtilizadorActual);
    expect(component.jsonToPost.code).toBe(component.verifInput);
  });
});
