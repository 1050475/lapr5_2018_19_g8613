import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ParProdutoCatalogoService {

  private WebApiIt1url = 'http://localhost:5000/api/';

  constructor(private httpClient: HttpClient) { }

  getParProdutoCatalogo(): Observable<any> {
    return this.httpClient.get(this.WebApiIt1url + 'ParProdutoCatalogo', httpOptions).pipe(
      map(this.extractData));
  }

  postParProdutoCatalogo(body): Observable<any> {
    return this.httpClient.post(this.WebApiIt1url + 'ParProdutoCatalogo', body, httpOptions).pipe(
      map(this.extractData));
  }

  deleteParProdutoCatalogo(id): Observable<any> {
    return this.httpClient.delete(this.WebApiIt1url + 'ParProdutoCatalogo/' + id, httpOptions).pipe(
      map(this.extractData));
  }

  private extractData(res: Response) {
    return res || {};
  }
}
