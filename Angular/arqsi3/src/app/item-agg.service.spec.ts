import { TestBed } from '@angular/core/testing';

import { ItemAggService } from './item-agg.service';

describe('ItemAggService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ItemAggService = TestBed.get(ItemAggService);
    expect(service).toBeTruthy();
  });
});
