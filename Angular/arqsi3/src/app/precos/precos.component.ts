import { Component, OnInit } from '@angular/core';
import { ignoreElements } from 'rxjs/operators';
import { SelectMultipleControlValueAccessor } from '@angular/forms';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { ParPrecoMaterialService } from '../par-preco-material.service';
import { ParPrecoAcabamentoService } from '../par-preco-acabamento.service';
import { MaterialService } from '../material.service';
import { AcabamentoService } from '../acabamento.service'



@Component({
  selector: 'app-precos',
  templateUrl: './precos.component.html',
  styleUrls: ['./precos.component.css']
})
export class PrecosComponent implements OnInit {

  constructor(private parPrecoMaterialSrv: ParPrecoMaterialService, private parPrecoAcabamentoSrv: ParPrecoAcabamentoService, private materialSrv: MaterialService, private acabamentoSrv: AcabamentoService) { }

  selectedMaterial: any;
  selectedAcabamento: any;
  allParPrecoMaterial: any;
  allParPrecoAcabamento: any;
  allMateriais: any;
  allAcabamentos: any;
  materialId: any;
  acabamentoId: any;
  dataInicioMat: any;
  dataInicioAcab: any;
  precoMat: any;
  precoAcab: any;
  materiais: any;
  acabamentos: any;
  material: any;
  acabamentosMat: any;
  precoFromMaterial: any;
  precoFromAcabamento: any;
  parPrecoMaterialToPost: any;
  parPrecoAcabamentoToPost: any;
  parPrecoMaterialToGet: any;
  parPrecoAcabamentoToGet: any;

  ngOnInit() {

    this.materialSrv.getMateriais().subscribe(data => {
      console.log("DataById:", data);
      this.allMateriais = data;
    });

    this.acabamentoSrv.getAcabamentos().subscribe(data => {
      console.log("DataById:", data);
      this.allAcabamentos = data;
    });

    this.parPrecoMaterialSrv.getParPrecoMaterial().subscribe(data => {
      console.log("DataById:", data);
      this.allParPrecoMaterial = data;
    });

    this.parPrecoAcabamentoSrv.getParPrecoAcabamento().subscribe(data => {
      console.log("DataById:", data);
      this.allParPrecoAcabamento = data;
    });
  }


  escolherMaterial() {
    this.material = this.selectedMaterial;
    this.materialId = this.material.materialId;
    this.dataInicioMat = this.material.DataInicio;
    this.precoMat = {};
    this.getPrecoFromMaterial();
  }


  escolherAcabamento() {
    this.acabamentosMat = this.selectedAcabamento;
    this.acabamentoId = this.acabamentosMat.acabamentoId;
    this.dataInicioAcab = this.acabamentosMat.dataInicioAcab;
    this.precoAcab = {}
    this.getPrecoFromAcabamento();
  }

  getPrecoFromMaterial() {
    this.precoFromMaterial = [];
    for (let par of this.allParPrecoMaterial) {
      if (this.selectedMaterial.materialId == par.materialId) {
        this.precoMat = par.precoMat;


      }
    }
  }

  getPrecoFromAcabamento() {
    this.precoFromAcabamento = [];
    for (let par of this.allParPrecoAcabamento) {
      if (this.selectedMaterial.materialId == par.materialId) {
        if (this.selectedAcabamento.acabamentoId == par.acabamentoId)
          this.precoAcab = par.precoAcab;
      }
    }
  }

  flagTestPostPrecoMaterial = false;
  postPrecoMaterial(dataInicioMat, precoMat) {
    this.parPrecoMaterialToPost = {};
    this.parPrecoMaterialToPost.MaterialId = this.materialId;
    this.parPrecoMaterialToPost.DataInicio = dataInicioMat;
    this.parPrecoMaterialToPost.PrecoMat = precoMat;
    console.log("teste", this.parPrecoMaterialToPost);
    this.parPrecoMaterialSrv.postParPrecoMaterial(this.parPrecoMaterialToPost).subscribe(data => {
      console.log("DataById:", data)
    })
    this.flagTestPostPrecoMaterial=true;
  }
  
  flagTestPostPrecoAcabemneto = false;
  postPrecoAcabamento(dataInicioAcab, precoAcab) {
    this.parPrecoAcabamentoToPost = {};
    this.parPrecoAcabamentoToPost.MaterialId = this.materialId;
    this.parPrecoAcabamentoToPost.AcabamentoId = this.acabamentoId;
    this.parPrecoAcabamentoToPost.DataInicio = dataInicioAcab;
    this.parPrecoAcabamentoToPost.PrecoAcab = precoAcab;
    console.log("teste", this.parPrecoAcabamentoToPost);
    this.parPrecoAcabamentoSrv.postParPrecoAcabamento(this.parPrecoAcabamentoToPost).subscribe(data => {
      console.log("DataById:", data)
    })
    this.flagTestPostPrecoAcabemneto = true;
  }


  getHistoricoPrecosMaterial() {
    this.parPrecoMaterialToGet = [];
    for (let par of this.allParPrecoMaterial) {
      if (this.selectedMaterial.materialId == par.materialId) {
        this.parPrecoMaterialToGet.push(par);
      }
    }
  }

  getHistoricoPrecosAcabamento() {
    this.parPrecoAcabamentoToGet = [];
    for (let par of this.allParPrecoAcabamento) {
      if (this.selectedMaterial.materialId == par.materialId) {
        if (this.selectedAcabamento.acabamentoId == par.acabamentoId) {
          this.parPrecoAcabamentoToGet.push(par);
        }
      }
    }
  }


}
