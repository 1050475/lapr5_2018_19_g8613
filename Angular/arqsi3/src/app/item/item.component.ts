import { Component, OnInit } from '@angular/core';
import { ItemService } from '../item.service';
import { ProdutoService } from '../produto.service';


@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  itemActual=null;
  criarItemFlag=false;
  editarItemFlag=false;
  allItems:any;
  allProdutos:any;
  itemToPost:any;
  produtoActual=null;//variavel que recebe detalhes do prod da it1
  produtoToPost=null;//produtoEscolhido
  selectedProd:any;//variavel para binding
  materiaisDisp:any;//materiais disponiveis pelo prod
  acabamentosDisp:any;
  selectedMat:any;
  materialToPost=null;
  acabamentoToPost=null;
  selectedAcabamento:any;
  altInput:any;//bind da altura
  largInput:any;
  profInput:any;
  itemCode=null;


  constructor(private itemSrv: ItemService,private produtoSrv: ProdutoService) { }
  
  ngOnInit() {
    this.getItems();
    this.itemToPost={};
  }
  
  //adicionar item
  newItem(): void{
    this.criarItemFlag=true;
    this.getProdutos();
    console.log
  }

  //editar item
  editarItem(item) :void{
    this.editarItemFlag=true;
    this.criarItemFlag=false;
    this.itemActual = item;
    this.getProdutos();

    this.altInput = item.altura;
    this.largInput = item.largura;
    this.profInput = item.profundidade;

    this.itemCode = item.code;
  }

  gravarEditarItem(){
    this.itemToPost.sku= this.produtoToPost.sku;
    this.itemToPost.altura =this.altInput;
    this.itemToPost.largura = this.largInput;
    this.itemToPost.profundidade = this.profInput;
    this.itemToPost.material = this.materialToPost.nome;
    this.itemToPost.acabamento = this.acabamentoToPost.nome;
    this.itemToPost.name = this.produtoToPost.nome;
    if(this.itemCode){
      this.itemToPost.code = this.itemCode;
    }
    

    console.log("itemToPut",this.itemToPost);
    this.putItem(this.itemToPost,this.itemActual._id);

    this.resetValores();
    this.getItems();
  }

  private resetValores():void{
    this.criarItemFlag=false;
    this.editarItemFlag=false;
    this.itemToPost={};
    this.altInput="";
    this.largInput="";
    this.profInput="";
    this.materiaisDisp=null;
    this.materialToPost=null;
    this.acabamentoToPost=null;
    this.acabamentosDisp=null;
    this.itemCode=null;
    
  }

  gravarItem(): void{
    this.itemToPost.sku= this.produtoToPost.sku;
    this.itemToPost.altura =this.altInput;
    this.itemToPost.largura = this.largInput;
    this.itemToPost.profundidade = this.profInput;
    this.itemToPost.material = this.materialToPost.nome;
    this.itemToPost.acabamento = this.acabamentoToPost.nome;
    this.itemToPost.name = this.produtoToPost.nome;

    console.log("itemToPost",this.itemToPost);
    this.postItem(this.itemToPost);

    this.resetValores();
    this.getItems();
  }

  deleteItem(item):any{
    console.log("delItem",item._id)
    this.criarItemFlag=false;
    this.editarItemFlag=false;
    this.apagarItem(item._id);
    this.getItems();
  }

  private postItem(body): any {
    this.itemSrv.postItem(body).subscribe(data => {
      console.log("Data RecebidaPost:",data);
      this.getItems();
    });
  }

  private putItem(body,id): any {
    console.log("entrou put Item")
    this.itemSrv.putItem(body,id).subscribe(data => {
      console.log("DPut:",data);
      //return data;
    });
  }

  private getProdutos(): void {
    this.produtoSrv.getProdutos().subscribe(data => {
      console.log("Data RecebidaProdutos:",data);
      this.allProdutos = data;
    });
  }

  private getItems(): void {
    this.itemSrv.getItems().subscribe(data => {
      console.log("Data Recebida:",data);
      this.allItems = data;
    });
  }

  private getProdutoById(sku): void {
    this.produtoSrv.getProdutoById(sku).subscribe(data => {
      console.log("DataById:",data);
      this.produtoActual=data;
      this.materiaisDisp = this.produtoActual.materiais;
      
    });
  }

  private apagarItem(id): void {
    this.itemSrv.deleteItem(id).subscribe(data => {
      console.log("deleted:",data)
      this.getItems();
    });
  }


  escolherProduto(): void{
    this.produtoToPost = this.selectedProd;
    //console.log("sku",this.selectedProd);
    this.getProdutoById(this.produtoToPost.sku);
    
    
  }
 
  escolherMaterial(): void{
    this.materialToPost=this.selectedMat;
    console.log("mat",this.materialToPost);
    this.acabamentosDisp = this.selectedMat.acabamentos;
    
    this.getProdutoById(this.produtoToPost.sku);
    console.log("disp",this.materiaisDisp);
  }

  escolherAcabamento(): void{
    this.acabamentoToPost = this.selectedAcabamento;
    console.log("acabamentoToPost",this.acabamentoToPost)
  }



}
