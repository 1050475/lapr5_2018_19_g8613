import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
    // //'Access-Control-Allow-Origin': 'https://localhost:5001',
    // 'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',                        
    // 'Access-Control-Allow-Headers': 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
    // 'Authorization': 'my-auth-token'
  })
}; 

@Injectable({
  providedIn: 'root'
})
export class ParPrecoAcabamentoService {

  private WebApiIt1url = 'https://localhost:5001/api/';                                              

  constructor(private httpClient: HttpClient) { }

  getParPrecoAcabamento(): Observable<any> {
    return this.httpClient.get(this.WebApiIt1url + 'ParPrecoAcabamento').pipe(
    map(this.extractData));
  }

  postParPrecoAcabamento(body): Observable<any> {
    return this.httpClient.post(this.WebApiIt1url + 'ParPrecoAcabamento', body,httpOptions).pipe(
               map(this.extractData));
}

getParPrecoAcabamentoById(id): Observable<any> {
  return this.httpClient.get(this.WebApiIt1url+ 'ParPrecoAcabamento/' + id).pipe(
  map(this.extractData));
}

  private extractData(res: Response) {
    return res || { };
  }
}
