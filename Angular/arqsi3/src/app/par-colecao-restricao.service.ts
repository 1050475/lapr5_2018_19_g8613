import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ParColecaoRestricaoService {

  private WebApiIt1url = 'http://localhost:5000/api/';

  constructor(private httpClient: HttpClient) { }

  getParColecaoRestricao(): Observable<any> {
    return this.httpClient.get(this.WebApiIt1url + 'ParColecaoRestricao', httpOptions).pipe(
      map(this.extractData));
  }

  postParColecaoRestricao(body): Observable<any> {
    return this.httpClient.post(this.WebApiIt1url + 'ParColecaoRestricao', body, httpOptions).pipe(
      map(this.extractData));
  }

  deleteParColecaoRestricao(id): Observable<any> {
    return this.httpClient.delete(this.WebApiIt1url + 'ParColecaoRestricao/' + id, httpOptions).pipe(
      map(this.extractData));
  }

  private extractData(res: Response) {
    return res || {};
  }
}
