import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class RestricaoColecaoService {

  private WebApiIt1url = 'http://localhost:5000/api/';
  constructor(private httpClient: HttpClient) { }

  getRestricaoColecoes(): Observable<any> {
    return this.httpClient.get(this.WebApiIt1url + 'RestricaoColecao').pipe(
      map(this.extractData));
  }

  getRestricaoColecaoById(id): Observable<any> {
    return this.httpClient.get(this.WebApiIt1url + 'RestricaoColecao/' + id).pipe(
      map(this.extractData));
  }

  postRestricaoColecao(body): Observable<any> {
    return this.httpClient.post(this.WebApiIt1url + 'RestricaoColecao', body, httpOptions).pipe(
      map(this.extractData));
  }

  putRestricaoColecao(body, id): Observable<any> {
    return this.httpClient.put(this.WebApiIt1url + 'RestricaoColecao/' + id, body, httpOptions).pipe(
      map(this.extractData));
  }

  deleteColecao(id): Observable<any> {
    return this.httpClient.delete(this.WebApiIt1url + 'RestricaoColecao/' + id, httpOptions).pipe(
      map(this.extractData));
  }

  private extractData(res: Response) {
    return res || {};
  }
}