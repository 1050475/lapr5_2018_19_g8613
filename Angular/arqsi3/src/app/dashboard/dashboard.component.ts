import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { DataService } from '../data.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  
  user:any;
  clienteFlag = false;
  adminFlag = false;
  constructor(private router:Router,private dataSrv:DataService) { }

  ngOnInit() {
    this.dataSrv.user.subscribe(user=>this.user = user);
    console.log("user",this.user)
  }

  criarNovoArmario(){
    this.router.navigate(['encomenda']);  
  }

  criarNovoCatalogo(){
    this.router.navigate(['catalogo']);
  }

  editarCatalogo(){
    this.router.navigate(['editar-catalogo']);
  }


  consultarPrecos(){
    this.router.navigate(['preco']);
  }

  listarCriarItem(){
    this.router.navigate(['item']);
  }

  listarCriarProduto(){
    this.router.navigate(['produto']);
  }

  criarColecao(){
    this.router.navigate(['criar-colecao']);
  }

  editarColecao(){
    this.router.navigate(['editar-colecao']);
  }
}
