import { Component, OnInit } from '@angular/core';
import { Produto } from '../model/Produto';
import { ProdutoService } from '../produto.service';
import { MaterialService } from '../material.service';

@Component({
  selector: 'app-produto',
  templateUrl: './produto.component.html',
  styleUrls: ['./produto.component.css']
})
export class ProdutoComponent implements OnInit {
  allProdutos: Produto[];
  allMateriais=null;
  produtoActual=null;
  skuActual=null;
  editarMaterialFlag=false;//flag que controla se a janela "ediar material" esta activa
  editarProdutoFlag=false;
  criarProdutoFlag=false;
  materialArrayToPost=[];
  productToPost:any;
  materialTemp:any;
  materialActual=null;
  materialActualAcabamentos=false;
  selectedValue:any;
  nomeMaterial2:any;//bind input nome de material em "editar material"

  //acabamentos
  acabamentoTemp :any;//acabamento a ser criado
  nomeAcabamento :any;//bind input nome acabamento em "Editar material"
  acabamentosArrayTemp=[];//array de acabamentos antes de gravar a ediçao material
  alturaToPost:any;
  larguraToPost:any;
  profundidadeToPost:any;

  //dimensoes
  //dimensoes
  alturaMax:any;
  alturaMin:any;
  
  radioD:any;
  radioTipo:any;
  alturaTempArray=[];
  dimensaoTemp:any;

  inputSku:any;


  constructor(private produtoSrv: ProdutoService,private materialSrv:MaterialService) { }
  emptyAll():void{
    this.alturaToPost={};
    this.alturaToPost.medidasDiscretas=[];
    this.alturaToPost.medidasContinuas=[];
    this.larguraToPost={};
    this.larguraToPost.medidasDiscretas=[];
    this.larguraToPost.medidasContinuas=[];
    this.profundidadeToPost={};
    this.profundidadeToPost.medidasDiscretas=[];
    this.profundidadeToPost.medidasContinuas=[];
  }
  ngOnInit() { 
    this.getProdutos(); 
    this.getMateriais();
    this.emptyAll();
    
    
  }

  cancelarProduto(){
    this.emptyAll();
    this.materialArrayToPost=[];
    this.editarProdutoFlag=false;
    this.criarProdutoFlag=false;
    this.idActual=null;
    this.inputNome="";
    this.inputSku="";
    this.materialArrayToPost=[];
    this.skuActual=null;
    this.produtoActual=null;
    this.alturaToPost={};
    this.larguraToPost={};
    this.profundidadeToPost={};
  }
  private getProdutos(): void {
    this.produtoSrv.getProdutos().subscribe(data => {
      console.log("Data Recebida:",data);
      this.allProdutos = data;
    });
  }
  idActual=null;
  private getProdutoById(sku): void {
    this.produtoSrv.getProdutoById(sku).subscribe(data => {
      console.log("DataById:",data);
      this.idActual=data.id;
      this.inputNome = data.nome;
      this.inputSku = this.skuActual;
      this.produtoActual=data;
      this.alturaToPost.medidasDiscretas=data.altura.medidasDiscretas;
      this.alturaToPost.medidasContinuas=data.altura.medidasContinuas;
      this.larguraToPost={};
      this.larguraToPost.medidasDiscretas=data.largura.medidasDiscretas;
      this.larguraToPost.medidasContinuas=data.largura.medidasContinuas;
      this.profundidadeToPost={};
      this.profundidadeToPost.medidasDiscretas=data.profundidade.medidasDiscretas;
      this.profundidadeToPost.medidasContinuas=data.profundidade.medidasContinuas;
      this.materialArrayToPost=data.materiais;
      
    });
  }

  private postProduto(body): void{
    this.produtoSrv.postProduto(body).subscribe(data => {
      console.log("DataById:",data);
      this.getProdutos();
      
    });
  }

  private putProduto(body,id): void{
    this.produtoSrv.putProduto(body,id).subscribe(data => {
      console.log("DataById:",data);
      this.getProdutos();
      
    });
  }

  private getMateriais(): void {
    this.materialSrv.getMateriais().subscribe(data => {
      console.log("Data Recebida materiais:",data);
      this.allMateriais = data;
    });
  }

  private apagarProduto(id){
    this.produtoSrv.deleteProdutos(id).subscribe(data => {
      console.log("Data Recebida materiais:",data);
      this.getProdutos();
    });
  }

  deleteProduto(produto) :void{
    console.log("Produto",produto);
    this.apagarProduto(produto);
  }
  inputNome:any;
  editProduto(sku) :void{
    this.editarProdutoFlag=true;
    this.criarProdutoFlag=false;
    this.skuActual=sku;
    //this.inputNome = this.productToPost.nome;
    this.getProdutoById(sku);
    console.log("skuActual",this.skuActual);
    
  }
 
 newProduto() :void{
   this.criarProdutoFlag=true;
    this.editarProdutoFlag=false;
    this.editarMaterialFlag=false;
    this.skuActual=null;
    this.produtoActual=null;
    this.emptyAll();
  }
  
  addMaterial() :void{
    console.log("select",this.selectedValue);
    this.materialArrayToPost.push(this.selectedValue);
  }

  gravarEditarProduto(nomeProduto,sku) :void{
    this.productToPost={};
    this.productToPost.Nome = nomeProduto;
    this.productToPost.SKU = sku;
    this.productToPost.ListaMateriais = this.materialArrayToPost;
    this.productToPost.LimitesAltura = this.alturaToPost.medidasContinuas;
    this.productToPost.LimitesProfundidade = this.profundidadeToPost.medidasContinuas;
    this.productToPost.LimitesLargura = this.larguraToPost.medidasContinuas;
    this.productToPost.ValoresAdmissiveisAltura=this.alturaToPost.medidasDiscretas;
    this.productToPost.ValoresAdmissiveisLargura=this.larguraToPost.medidasDiscretas;
    this.productToPost.ValoresAdmissiveisProfundidade=this.profundidadeToPost.medidasDiscretas;
    this.productToPost.CategoriaId=2;

    console.log("producto para put",this.productToPost)
    this.putProduto(this.productToPost,this.idActual);

    this.editarProdutoFlag=false;
    this.criarProdutoFlag=false;
    this.idActual=null;
    this.inputNome="";
    this.inputSku="";
    this.materialArrayToPost=[];
    this.skuActual=null;
    this.produtoActual=null;
    this.alturaToPost={};
    this.larguraToPost={};
    this.profundidadeToPost={};

  }

  gravarProduto(nomeProduto,sku) :void{
    this.productToPost={};
    this.productToPost.Nome = nomeProduto;
    this.productToPost.SKU = sku;
    this.productToPost.ListaMateriais = this.materialArrayToPost;
    this.productToPost.LimitesAltura = this.alturaToPost.medidasContinuas;
    this.productToPost.LimitesProfundidade = this.profundidadeToPost.medidasContinuas;
    this.productToPost.LimitesLargura = this.larguraToPost.medidasContinuas;
    this.productToPost.ValoresAdmissiveisAltura=this.alturaToPost.medidasDiscretas;
    this.productToPost.ValoresAdmissiveisLargura=this.larguraToPost.medidasDiscretas;
    this.productToPost.ValoresAdmissiveisProfundidade=this.profundidadeToPost.medidasDiscretas;
    this.productToPost.CategoriaId=2;

    console.log("prod",this.productToPost);

    this.postProduto(this.productToPost);


    this.editarProdutoFlag=false;
    this.criarProdutoFlag=false;
    this.inputNome="";
    this.inputSku="";
    this.materialArrayToPost=[];
    this.skuActual=null;
    this.produtoActual=null;
    this.alturaToPost={};
    this.larguraToPost={};
    this.profundidadeToPost={};
  }

  criarMaterial(nome,ele) :void{
    this.materialTemp={};
    this.materialTemp.nome = nome;
    this.materialTemp.acabamentos =[];
    this.materialArrayToPost.push(this.materialTemp);
    ele.value ="";
  }
  acabamentosArrayTemp2=[];
  
  editarMaterial(i) :void{
    console.log("i",i)
    this.editarMaterialFlag=true;
    this.materialActual = this.materialArrayToPost[i];
    console.log("matActual",this.materialArrayToPost);
    this.nomeMaterial2 = this.materialActual.nome;
    this.materialActualAcabamentos=true;

    this.acabamentosArrayTemp=this.materialActual.acabamentos;

    
    //this.materialActualAcabamentos = this.materialActual
    console.log("te",this.materialActual)
  }

  apagarMaterial(i) :void{
    console.log("i",i);
    if (i > -1) {
      this.materialArrayToPost.splice(i, 1);
    }
  }

  criarAcabamento() :void{
    console.log("criaA-ma",this.materialActual);
    this.acabamentoTemp={};
    this.acabamentoTemp.nome=this.nomeAcabamento;
    this.acabamentosArrayTemp.push(this.acabamentoTemp);
    this.nomeAcabamento="";

    console.log("criaA-ma2",this.materialActual);
    

  }

  apagarAcabamento(i):void{
    if (i > -1) {
      this.acabamentosArrayTemp.splice(i, 1);
    }
  }

  gravarEditarMaterial() :void{
    this.materialActual.nome = this.nomeMaterial2;
    this.materialActual.acabamentos = this.acabamentosArrayTemp;
    this.editarMaterialFlag=false;
    this.materialActual=null;
    this.acabamentosArrayTemp=[];
    console.log("fravaM-ma",this.materialActual);
  }

  cancelarEditarMaterial() :void{
    console.log("camcelaM-arrayTemp",this.acabamentosArrayTemp);
    this.acabamentosArrayTemp=[];
    console.log("camcelaM-arrayTemp2",this.acabamentosArrayTemp);
    console.log("camcelaM-ma",this.materialActual);
    this.editarMaterialFlag=false;
    this.materialActual=null;
    
  }
    
  criarDimensao() :void{
    this.dimensaoTemp={};

    
      if(this.radioD=='disc'){
        this.dimensaoTemp.valor=this.alturaMin;
        if(this.radioTipo=='alt'){
          console.log("entrou1",this.dimensaoTemp)
          console.log("entrou4",this.alturaToPost)
          this.alturaToPost.medidasDiscretas.push(this.dimensaoTemp);
          console.log("entrou2",this.alturaToPost)
          
        }else if(this.radioTipo=='larg'){
          this.larguraToPost.medidasDiscretas.push(this.dimensaoTemp);
        }else{
          this.profundidadeToPost.medidasDiscretas.push(this.dimensaoTemp);
        }
        
      }else{
        this.dimensaoTemp.Minimo=this.alturaMin;
        this.dimensaoTemp.Maximo=this.alturaMax;
        if(this.radioTipo=='alt'){
          this.alturaToPost.medidasContinuas.push(this.dimensaoTemp);
        }else if(this.radioTipo=='larg'){
          this.larguraToPost.medidasContinuas.push(this.dimensaoTemp);
        }else{
          this.profundidadeToPost.medidasContinuas.push(this.dimensaoTemp);
        }
      }
    this.dimensaoTemp=null;
    this.alturaMin="";
    this.alturaMax="";
  }



}

