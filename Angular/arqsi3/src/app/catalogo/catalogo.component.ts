import { Component, OnInit } from '@angular/core';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { ProdutoService } from '../produto.service';
import { CatalogoService } from '../catalogo.service';
import { ParProdutoCatalogoService } from '../par-produto-catalogo.service';
import { ignoreElements } from 'rxjs/operators';
import { SelectMultipleControlValueAccessor } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-catalogo',
  templateUrl: './catalogo.component.html',
  styleUrls: ['./catalogo.component.css']
})
export class CatalogoComponent implements OnInit {

  catalogoToPost: any;
  armariosToCatalogo: any;
  produto: any;
  selectProduto: any;
  parProdutoCatalogoToPost: any;
  allCatalogos: any;
  allSkus: any;


  //flag
  removerFlag = false;
  skuFlag = true;

  constructor(private router: Router, private produtoSrv: ProdutoService, private catalogoSrv: CatalogoService, private parProdutoCatalogoSrv: ParProdutoCatalogoService) { }
  allProdutos: any;
  ngOnInit() {
    this.armariosToCatalogo = [];
    this.allSkus = [];

    this.produtoSrv.getProdutos().subscribe(data => {
      console.log("DataById:", data);
      this.allProdutos = data;
    });

    this.catalogoSrv.getCatalogos().subscribe(data => {
      console.log("DataById:", data);
      this.allCatalogos = data;
      for (let cat of this.allCatalogos) {
        this.allSkus.push(cat.sku);
      }
    });
  }

  adicionarProduto() {
    this.produto = this.selectProduto;
    this.armariosToCatalogo.push(this.produto);
  }

  gravarCatalogo(catalogoName, sku, dataInicio, dataFinal) {
    this.catalogoToPost = {};
    this.catalogoToPost.CatalogoName = catalogoName;
    this.catalogoToPost.SKU = sku;
    this.catalogoToPost.DataInicio = dataInicio;
    this.catalogoToPost.DataFim = dataFinal;

    if (!this.allSkus.includes(sku)) {

      this.catalogoSrv.postCatalogo(this.catalogoToPost).subscribe(data => {
        console.log("DataById:", data);

        for (let armario of this.armariosToCatalogo) {

          this.parProdutoCatalogoToPost = {};
          console.log(this.parProdutoCatalogoToPost);
          this.parProdutoCatalogoToPost.SKUProduto = armario.sku;
          this.parProdutoCatalogoToPost.SKUCatalogo = sku;
          this.parProdutoCatalogoSrv.postParProdutoCatalogo(this.parProdutoCatalogoToPost).subscribe(data => {
            console.log("DataById:", data);
          });
        }
      });
      window.alert("Catálogo criado com sucesso!");
      this.router.navigate(['dashboard']);
    } else {
      window.alert("SKU já existe! Introduza de novo!");
    }
  }

  iActual: any;
  produtoActual: any;
  removerProduto(i) {

    if (this.selectProduto) {
      this.produtoActual = this.selectProduto;
      this.iActual = i;
      if (i > -1) {
        this.armariosToCatalogo.splice(i, 1);
      }
      this.removerFlag = true;
    }
  }

  cancelarCatalogo() {
    this.iActual = {};
    this.produtoActual = {};
    this.catalogoToPost = {};
    this.armariosToCatalogo = [];
    this.produto = {};
    this.selectProduto = {};
    this.parProdutoCatalogoToPost = {};
    this.allProdutos = [];
    this.allCatalogos = [];
    this.allSkus = [];

    this.router.navigate(['dashboard']);
  }


}
