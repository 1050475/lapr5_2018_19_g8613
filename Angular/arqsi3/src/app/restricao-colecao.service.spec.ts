import { TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'; 
import { RouterTestingModule } from '@angular/router/testing';
import { RestricaoColecaoService } from './restricao-colecao.service';

describe('RestricaoColecaoService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [ FormsModule,HttpClientModule,RouterTestingModule]
  }));

  it('should be created', () => {
    const service: RestricaoColecaoService = TestBed.get(RestricaoColecaoService);
    expect(service).toBeTruthy();
  });
});
