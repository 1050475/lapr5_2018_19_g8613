import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': 'my-auth-token'
  })
};


@Injectable({
  providedIn: 'root'
})
export class UtilizadorService {
  private WebApiIt2url = 'http://localhost:8080/';

 

  constructor(private httpClient: HttpClient) { 

  }

  postUtilizador(body): Observable<any> {
    //let headers = new Headers({ 'Content-Type': 'application/json' });
          return this.httpClient.post(this.WebApiIt2url + 'user/register', body).pipe(
                     map(this.extractData));
  }

  getCidades(): Observable<any> {
    return this.httpClient.get(this.WebApiIt2url + 'manager/cidade').pipe(
    map(this.extractData));
  }
  
  postAuth(body): Observable<any> {
    //let headers = new Headers({ 'Content-Type': 'application/json' });
    console.log("service entrou")
          return this.httpClient.post(this.WebApiIt2url + 'user/authenticate', body).pipe(
                     map(this.extractData));
  }

  postAuth2FA(body): Observable<any> {
    //let headers = new Headers({ 'Content-Type': 'application/json' });
    console.log("service entrou")
          return this.httpClient.post(this.WebApiIt2url + 'user/login', body).pipe(
                     map(this.extractData));
  }

  private extractData(res: Response) {
    return res || { };
  }
}
