import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { HttpHeaders } from '@angular/common/http';


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
    // //'Access-Control-Allow-Origin': 'https://localhost:5001',
    // 'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, OPTIONS',                        
    // 'Access-Control-Allow-Headers': 'X-Requested-With, Content-Type, Accept, Origin, Authorization',
    // 'Authorization': 'my-auth-token'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ParPrecoMaterialService {
  private WebApiIt1url = 'https://localhost:5001/api/';

  constructor(private httpClient: HttpClient) { }

  getParPrecoMaterial(): Observable<any> {
    return this.httpClient.get(this.WebApiIt1url + 'ParPrecoMaterial').pipe(
      map(this.extractData));
  }

  postParPrecoMaterial(body): Observable<any> {
    return this.httpClient.post(this.WebApiIt1url + 'ParPrecoMaterial', body, httpOptions).pipe(
      map(this.extractData));
  }

  getParPrecoMaterialById(id): Observable<any> {
    return this.httpClient.get(this.WebApiIt1url + 'ParPrecoMaterial/' + id).pipe(
      map(this.extractData));
  }

  putParPrecoMaterial(body, id): Observable<any> {
    return this.httpClient.put(this.WebApiIt1url + 'ParPrecoMaterial/' + id, body, httpOptions).pipe(
      map(this.extractData));
  }

  private extractData(res: Response) {
    return res || {};
  }
}
