import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';
import { DataService } from './data.service';



@Injectable({
  providedIn: 'root'
})
export class ItemAggService {
  private WebApiIt2url = 'http://localhost:8080/';

 

  user:any;
  httpOptions ={
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

  constructor(private httpClient: HttpClient,private dataSrv:DataService) { 

  }

  getParItems(): Observable<any> {
    return this.httpClient.get(this.WebApiIt2url + 'parItem').pipe(
    map(this.extractData));
  }

  postParItem(body): Observable<any> {
    this.createHeader();
    //let headers = new Headers({ 'Content-Type': 'application/json' });
          return this.httpClient.post(this.WebApiIt2url + 'parItem/create', body,this.httpOptions).pipe(
                     map(this.extractData));
      }

  putParItem(body,id): Observable<any> {
    this.createHeader();
    //let headers = new Headers({ 'Content-Type': 'application/json' });
              return this.httpClient.put(this.WebApiIt2url + 'paritem/' + id + '/update', body,this.httpOptions).pipe(
                         map(this.extractData));
          }
  
  deleteItem(id): Observable<any> {
    this.createHeader();
    //let headers = new Headers({ 'Content-Type': 'application/json' });
              return this.httpClient.delete(this.WebApiIt2url + 'item/' + id + '/delete',this.httpOptions).pipe(
                map(this.extractData));
          }

  private extractData(res: Response) {
    return res || { };
  }
  createHeader(){
    this.dataSrv.user.subscribe(user=>this.user = user);
     this.httpOptions ={
          headers: new HttpHeaders({
            'Content-Type':  'application/json',
            'x-access-token':this.user.token
          })
        };;
    }
}
