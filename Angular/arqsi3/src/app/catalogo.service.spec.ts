import { TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'; 
import { RouterTestingModule } from '@angular/router/testing';
import { CatalogoService } from './catalogo.service';

describe('CatalogoService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [ FormsModule,HttpClientModule,RouterTestingModule]
  }));

  it('should be created', () => {
    const service: CatalogoService = TestBed.get(CatalogoService);
    expect(service).toBeTruthy();
  });
});
