import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EncomendaComponent } from './encomenda.component';

import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'; 
import { RouterTestingModule } from '@angular/router/testing';

describe('EncomendaComponent', () => {
  let component: EncomendaComponent;
  let fixture: ComponentFixture<EncomendaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule,HttpClientModule,RouterTestingModule],
      declarations: [ EncomendaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EncomendaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  
  it('should calculate largura armario pai', () => {
    
    component.allModulosNaoObr = [{"largura":"12"},{"largura":"11"}];
    component.allModulosObr = [{"largura":"1"},{"largura":"1"}];
    let larguraTotal = component.calcularLarguraPai();
    expect(larguraTotal).toBe(25);
  });

  it('should turn visualFlag on', () => {
    component.visualizarArmario();
    expect(component.visualFlag).toBeTruthy();
  });

  it('should turn visualizar mode off', () => {
    component.fecharVisual();
    expect(component.visualFlag).toBeFalsy();
  });

  it('should open gaveta', () => {
    component.armarioPaiToPost = {"itemFilhos":[{"itemFilhos":[{"aberto":false}]}]};
    component.abrirGaveta(0,0);
    expect(component.armarioPaiToPost[0].itemFilhos[0].aberto).toBeTruthy();
  });

  //testes para pagina armario pai
  it('should choose product pai', () => {
    component.selectProdPai = {"nome":"Produto1"}
    component.escolherProdutoPai();
    expect(component.produtoPai.nome).toBe("Produto1");
  });

  it('should choose material pai', () => {
    component.selectedMatPai = "mat1"
    component.escolherMaterialPai();
    expect(component.materialPai).toBe("mat1");
  });

  it('should choose acabamento pai', () => {
    component.selectedAcabPai = "acab1"
    component.escolherMaterialPai();
    expect(component.acabamentoPai).toBe("acab1");
  });

  it('should not create modulo if prod pai not selected', () => {
    component.selectProdPai = undefined;
    component.selectedMatPai = "mat1";
    component.selectedAcabPai = "mat1";
    component.profPaiInput="";
    component.criarModulo();
    spyOn(window, 'alert');
    expect(window.alert).toHaveBeenCalledWith('Dados invalidos');
  });


});
