import { Component, OnInit } from '@angular/core';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { ProdutoService } from '../produto.service';
import { ItemAggService } from '../item-agg.service';
import { EncomendaService } from '../encomenda.service';
import { ItemService } from '../item.service';
import { ignoreElements } from 'rxjs/operators';
import { ParPrecoMaterialService } from '../par-preco-material.service';
import { ParPrecoAcabamentoService } from '../par-preco-acabamento.service';
import { DataService } from '../data.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-encomenda',
  templateUrl: './encomenda.component.html',
  styleUrls: ['./encomenda.component.css']
})
export class EncomendaComponent implements OnInit {

  armarioPaiToPost: any;//armarioPai objecto
  moduloToPost: any;

  //bind
  selectProdPai: any;//produto armarioPai
  altPaiInput: any;//altura armarioPai
  largPaiInput: any;//largura aramarioPai
  profPaiInput: any;//profundidade armarioPai
  selectedMatPai: any;//material pai
  selectedAcabPai: any;//acabamento pai
  selectProdFilho: any;
  altFilhoInput: any;
  largFilhoInput: any;
  profFilhoInput: any;
  acabamentoPai: any;

  parItemToPost: any;

  precoFinal: number;

  precoMaterialPai: any;

  //flags
  visualFlag = false;
  criarModuloFlag = false;
  criarGavetaFlag = false;
  editarGavetaFlag = false;
  criarPrateleiraFlag = false;
  editarModuloObrFlag = false;
  editarModuloFlag= false;


  constructor(private router:Router,private dataSrv:DataService,private produtoSrv: ProdutoService, private itemAgrSrv: ItemAggService, private encomendaSrv: EncomendaService, private itemSrv: ItemService, private parPrecoMatSrv: ParPrecoMaterialService, private parPrecoAcabSrv: ParPrecoAcabamentoService) { }
  allProdutosPai: any;
  user:any;
  ngOnInit() {
    this.dataSrv.user.subscribe(user=>this.user = user);
    this.armarioPaiToPost = {};
    this.armarioPaiToPost.itemFilhos = [];
    this.allModulosNaoObr = [];
    this.precoFinal = 0;


    this.produtoSrv.getProdutoByCategoria("Armario").subscribe(data => {
      console.log("DataById:", data);
      this.allProdutosPai = data;
    });

    this.parPrecoMatSrv.getParPrecoMaterial().subscribe(data => {
      //console.log("preco do material:", data[0].precoMat);
      //this.precoMaterialPai = data[0].precoMat;
      this.precoMaterialPai = data[0].precoMat;
    });

  }

  voltarDash(){
    this.router.navigate(['dashboard']);  
  }

  //funcoes da pagina criar PAI----------------------------------------Criar BASE
  produtoPai: any;
  allMateriaisPai: any;
  allProdutosObr: any;
  allProdutosNaoObr: any;
  allModulosObr: any;
  allModulosNaoObr: any;
  allProdutosFilho: any;
  moduloTemp: any;

  postArmarioPorMedida() {
    this.itemSrv.postArmarioPorMedida(this.armarioPaiToPost).subscribe(data => {
      console.log("enviado Armario");
    });
  }

  visualizarArmario() {
    this.formatarJsonArmarioPai();
    //this.formatarParaVisualizacao()
    this.resetDadosModulo();
    this.visualFlag = true;
    console.log("AP", JSON.stringify(this.armarioPaiToPost));
    this.postArmarioPorMedida();
  }

  fecharVisual() {
    this.visualFlag = false;
  }

  abrirGaveta(i, j) {
    console.log("i:" + i + ";j:" + j);
    this.armarioPaiToPost.itemFilhos[i].itemFilhos[j].aberto = true;
    this.postArmarioPorMedida();
  }
  fecharGaveta(i, j) {
    console.log("i:" + i + ";j:" + j);
    this.armarioPaiToPost.itemFilhos[i].itemFilhos[j].aberto = false;
    this.postArmarioPorMedida();
  }

  modulosObrigatoriosValidos() {
    for (let i = 0; i < this.allModulosObr.length; i++) {
      if (this.allModulosObr[i].largura == undefined) {
        return false;
      }
    }
    return true;
  }
  larguraTotal: any;
  calcularLarguraPai() {
    this.larguraTotal = 0;
    for (let i = 0; i < this.allModulosObr.length; i++) {
      if (this.allModulosNaoObr != undefined) {
        this.larguraTotal += +this.allModulosObr[i].largura;
      }

    }
    for (let i = 0; i < this.allModulosNaoObr.length; i++) {
      this.larguraTotal += +this.allModulosNaoObr[i].largura;
    }
    this.largPaiInput = " ";
    this.largPaiInput = +this.larguraTotal;
    return this.larguraTotal
  }



  escolherProdutoPai() {
    this.produtoPai = this.selectProdPai;

    //produto by id para ir buscar materiais desse produto
    this.produtoSrv.getProdutoById(this.produtoPai.sku).subscribe(data => {
      console.log("produtoById", data)
      this.allMateriaisPai = data.materiais;

      this.produtoSrv.getProdutoPartes(this.produtoPai.sku, "true").subscribe(data => {
        console.log("ProdObr:", data);
        this.allProdutosObr = data;
        this.allModulosObr = [];

        for (let i = 0; i < data.length; i++) {
          this.moduloTemp = {};
          this.moduloTemp.sku = data[i].sku;
          this.moduloTemp.nome = data[i].nome;
          this.moduloTemp.itemFilhos = [];
          this.moduloTemp.allModuloGavetaObr = [];
          this.moduloTemp.allModuloGavetaNaoObr = [];
          this.allModulosObr.push(this.moduloTemp);
        }
        console.log("ModulosObr", this.allModulosObr)
        this.produtoSrv.getProdutoPartes(this.produtoPai.sku, "false").subscribe(data => {
          console.log("ProdNaoObr:", data);
          this.allProdutosNaoObr = data;
          this.allProdutosFilho = this.allProdutosNaoObr.concat(this.allProdutosObr);
        });
      });
    });
  }

  materialPai: any;
  allAcabamentosPai: any;
  escolherMaterialPai() {
    this.materialPai = this.selectedMatPai;
    this.allAcabamentosPai = this.materialPai.acabamentos;
  }

  escolherAcabamentoPai() {
    this.acabamentoPai = this.selectedAcabPai;
  }

  formatarJsonArmarioPai(){
    this.armarioPaiToPost = {};
    this.armarioPaiToPost.sku = this.selectProdPai.sku;
    this.armarioPaiToPost.nome = this.selectProdPai.nome;
    this.armarioPaiToPost.altura = this.altPaiInput;
    this.armarioPaiToPost.largura = this.calcularLarguraPai();
    this.armarioPaiToPost.profundidade = this.profPaiInput;
    this.armarioPaiToPost.material = this.materialPai.nome;
    this.armarioPaiToPost.acabamento = this.acabamentoPai.nome;
    this.armarioPaiToPost.itemFilhos = this.allModulosObr.concat(this.allModulosNaoObr);
    this.formatarParaVisualizacao();
    console.log("JSON-FINAL-armarioPai", this.armarioPaiToPost);
  }

  gravarArmarioPai() {
    if (this.modulosObrigatoriosValidos()) {
      if(this.user.activo){
        this.formatarJsonArmarioPai();
        this.criarEncomenda();
      }else{
        window.alert("Tem de estar registado para gravar uma encomenda, vá a login/registo");
      }
    } else {
      window.alert("Tem de editar Modulos Obrigatorios");
    }
  }

  formatarParaVisualizacao() {
    for (let i = 0; i < this.armarioPaiToPost.itemFilhos.length; i++) {
      console.log("moduloGavetaNaoObr", this.armarioPaiToPost.itemFilhos[i].allModuloGavetaNaoObr)
      this.armarioPaiToPost.itemFilhos[i].itemFilhos =
        this.armarioPaiToPost.itemFilhos[i].allModuloGavetaNaoObr
          .concat(this.armarioPaiToPost.itemFilhos[i].allModuloGavetaObr);
    }
  }
  //criar Slot
  criarModulo() {
    //teste
    //if dados preenchidos

    console.log("selectedProdTest",this.selectProdPai);
    console.log("altProd",this.altPaiInput);
    if((this.selectProdPai)&&(this.selectedMatPai)&&(this.selectedAcabPai)&&(this.altPaiInput!="")
    &&(this.profPaiInput!="")){
      this.resetDadosModulo();
      this.criarModuloFlag = true;
      this.moduloToPost = {};
      this.moduloToPost.altura = this.altPaiInput;
      this.moduloToPost.profundidade = this.profPaiInput;
      this.moduloToPost.material = this.armarioPaiToPost.material;
      this.moduloToPost.itemFilhos = [];
      this.moduloToPost.allModuloGavetaObr = [];
      this.moduloToPost.allModuloGavetaNaoObr = [];

      //valores do armario pai

      this.altFilhoInput = this.altPaiInput;
      this.profFilhoInput = this.profPaiInput;
    } else {
      window.alert("Dados invalidos");
    }


  }

  moduloEditarTemp: any;
  iActual: any;
  obrActual: any;
  skuTempActual: any;
  prodObjTemp: any;
  editarModulo(i, obr, skuTemp) {
    this.prodObjTemp = {};

    console.log("skuTemp", skuTemp);
    if (skuTemp != -1) {
      this.prodObjTemp.sku = skuTemp;
      this.produtoFilho = this.prodObjTemp;
    }


    if ((this.selectProdPai) && (this.selectedMatPai) && (this.selectedAcabPai) && (this.altPaiInput)
      && (this.profPaiInput)) {
      this.iActual = i;
      this.obrActual = obr;

      if (obr) {
        this.moduloToPost = this.allModulosObr[i];
        this.moduloToPost.material = this.materialPai.nome;
        this.moduloToPost.acabamento = this.acabamentoPai.nome;
      } else {
        this.moduloToPost = this.allModulosNaoObr[i];
      }

      console.log("i:" + i + ";moduloObr" + this.allModulosObr[i])

      this.moduloToPost.altura = this.altPaiInput;
      this.moduloToPost.profundidade = this.profPaiInput;

      this.altFilhoInput = this.altPaiInput;
      this.profFilhoInput = this.profPaiInput;
      if (this.moduloToPost.largura) {
        this.largFilhoInput = this.moduloToPost.largura;
      }

      this.editarModuloObrFlag = true;
    }
  }


  apagarModuloNaoObr(i) {
    if (i > -1) {
      this.allModulosNaoObr.splice(i, 1);
    }
  }


  resetDadosPai() {
    this.altPaiInput = "";
    this.largPaiInput = "";
    this.profPaiInput = "";
    this.selectProdPai = null;
    this.selectedAcabPai = null;
    this.selectedMatPai = null;
    this.allModulosNaoObr = [];
    this.allModulosObr = [];

    this.editarModuloObrFlag = false;
    this.visualFlag = false;
    this.moduloToPost = {};
    this.resetDadosModulo();

  }


  //funcoes da pagina criar MODULO-----------------------------------------------------MODULO

  validarCamposParaGravarModulo() {
    if ((this.selectProdFilho != null) && (this.largFilhoInput != "")) {
      return true;
    } else {
      return false;
    }
  }

  gravarModulo() {
    this.moduloToPost.sku = this.produtoFilho.sku;
    this.moduloToPost.nome = this.produtoFilho.nome;
    this.moduloToPost.largura = this.largFilhoInput;
    this.moduloToPost.acabamento = this.acabamentoPai.nome;
    this.moduloToPost.material = this.materialPai.nome;

    this.allModulosNaoObr.push(this.moduloToPost);
    this.calcularLarguraPai();
    this.resetDadosModulo();
  }

  gravarEditarModulo() {
    this.moduloToPost.largura = this.largFilhoInput;
    this.moduloToPost.acabamento = this.acabamentoPai.nome;
    this.moduloToPost.material = this.materialPai.nome;
    if (this.obrActual) {
      this.allModulosObr[this.iActual] = this.moduloToPost;
    } else {
      this.moduloToPost.sku = this.produtoFilho.sku;
      this.moduloToPost.nome = this.produtoFilho.nom
      this.allModulosNaoObr[this.iActual] = this.moduloToPost;
    }
    this.calcularLarguraPai();
    this.resetDadosModulo();
  }


  produtoFilho: any;
  allMateriaisFilho: any;
  allProdutosGaveta: any;
  allProdGavetaNaoObr: any;
  allProdGavetaObr: any;
  allProdGaveta: any;
  allModuloGavetaObr: any;
  allModuloGavetaNaoObr: any;
  gavetaTemp: any;
  escolherProdutoFilho() {
    this.produtoFilho = this.selectProdFilho;
    console.log("proFilho", this.produtoFilho);
  }

  // funçoes para guardar a encomenda e dar post na API2

  private postItem(body): void {
    this.itemSrv.postItem(body).subscribe(data => {
      console.log("DataById:", data);
    });
  }


  private postParItem(body): void {
    this.itemAgrSrv.postParItem(body).subscribe(data => {
      console.log("DataById:", data);
    });
  }

  private postEncomenda(body): void {
    this.encomendaSrv.postEncomenda(body).subscribe(data => {
      console.log("DataById:", data);
    });
  }

  gavetaToPost: any;
  agregarGaveta() {
    this.gavetaToPost = {};
    this.criarGavetaFlag = true;

    this.allProdGavetaNaoObr = [];
    this.allProdGavetaObr = [];
    this.allModuloGavetaObr = [];
    this.allModuloGavetaNaoObr = [];
    console.log("produtoFilho", this.produtoFilho);
    this.produtoSrv.getProdutoPartes(this.produtoFilho.sku, "true").subscribe(data => {
      this.allProdGavetaObr = data;
      console.log("partes de filho obr", data);
      for (let i = 0; i < data.length; i++) {
        this.gavetaTemp = {};
        this.gavetaTemp.largura = this.largFilhoInput;
        this.gavetaTemp.profundidade = this.profFilhoInput;
        this.gavetaTemp.altura = this.altPaiInput;
        if (data[i].categoria == "Gaveta") {
          this.gavetaTemp.aberto = false;
        }
        this.moduloToPost.allModuloGavetaObr.push(this.gavetaTemp);
      }
      this.produtoSrv.getProdutoPartes(this.produtoFilho.sku, "false").subscribe(data => {
        console.log("partes de filho nao obr", data);
        this.allProdGavetaNaoObr = data;
        this.allProdGaveta = this.allProdGavetaObr.concat(this.allProdGavetaNaoObr);
        console.log("partes de filho todas", this.allProdGaveta);
      });
    });
  }

  iGActual: any;
  obrGActual: any;
  editarGaveta(i, obr) {
    this.iGActual = i;
    this.obrGActual = obr;

    if (obr) {
      this.gavetaToPost = this.moduloToPost.allModuloGavetaObr[i];
    } else {
      this.gavetaToPost = this.moduloToPost.allModuloGavetaNaoObr[i];
    }

    if (this.gavetaToPost.posicao) {
      this.posicaoGaveta = this.gavetaToPost.posicao;
    }


    this.editarGavetaFlag = true;
  }
  apagarGaveta(i) {
    if (i > -1) {
      this.moduloToPost.allModuloGavetNaoObr.splice(i, 1);
    }
  }

  resetDadosModulo() {
    this.moduloToPost = {};
    this.criarModuloFlag = false;
    this.largFilhoInput = "";
    this.profFilhoInput = "";
    this.altFilhoInput = "";
    this.selectProdFilho = null;
    this.moduloToPost.itemFilhos = [];
    this.editarModuloObrFlag = false;
    this.resetDadosGaveta();
  }


  //Funcoes da pagina criar GAVETA/PRATELEIRA------------------------------------------------GAVETA
  posicaoGaveta: any;
  //gavetaToPost:any;
  selectedMatGaveta: any;
  selectedAcabGaveta: any;
  selectedProdGaveta: any;

  gravarGaveta() {

    this.gavetaToPost.sku = this.produtoGaveta.sku;
    this.gavetaToPost.nome = this.produtoGaveta.nome;
    this.gavetaToPost.largura = this.largFilhoInput;
    this.gavetaToPost.profundidade = this.profFilhoInput;
    this.gavetaToPost.altura = this.altFilhoInput;
    this.gavetaToPost.material = this.materialGaveta.nome;
    this.gavetaToPost.acabamento = this.acabamentoGaveta.nome;
    this.gavetaToPost.posicao = this.posicaoGaveta;
    if (this.produtoGaveta.categoria == "Gaveta") {
      this.gavetaToPost.aberto = false;
    }
    this.gavetaToPost.categoria = this.produtoGaveta.categoria;
    console.log("categoria", this.produtoGaveta.categoria);
    this.moduloToPost.allModuloGavetaNaoObr.push(this.gavetaToPost);
    //this.moduloToPost.itemFilhos.push(this.gavetaToPost);

    console.log("moduloActual", this.moduloToPost)
    this.resetDadosGaveta();
  }

  gravarEditarGaveta() {
    this.gavetaToPost.sku = this.produtoGaveta.sku;
    this.gavetaToPost.nome = this.produtoGaveta.nome;
    this.gavetaToPost.largura = this.largFilhoInput;
    this.gavetaToPost.profundidade = this.profFilhoInput;
    this.gavetaToPost.altura = this.altFilhoInput;
    this.gavetaToPost.material = this.materialGaveta.nome;
    this.gavetaToPost.acabamento = this.acabamentoGaveta.nome;
    this.gavetaToPost.posicao = this.posicaoGaveta;
    if (this.produtoGaveta.categoria = "Gaveta") {
      this.gavetaToPost.aberto = false;
    }
    this.gavetaToPost.categoria = this.produtoGaveta.categoria;
    if (this.obrGActual) {
      this.moduloToPost.allModuloGavetaObr[this.iGActual] = this.gavetaToPost;
    } else {
      this.moduloToPost.allModuloGavetaNaoObr[this.iGActual] = this.gavetaToPost;
    }
    this.resetDadosGaveta();
  }



  produtoGaveta: any;
  allMateriaisGaveta: any;
  allAcabamentosGaveta: any;
  escolherProdutoGaveta() {
    console.log("entrou")
    this.produtoGaveta = this.selectedProdGaveta;
    this.produtoSrv.getProdutoById(this.produtoGaveta.sku).subscribe(data => {
      console.log("dataProdGaveta", data)
      this.allMateriaisGaveta = data.materiais;
    });

  }

  materialGaveta: any;
  escolherMaterialGaveta() {
    this.materialGaveta = this.selectedMatGaveta;
    this.allAcabamentosGaveta = this.materialGaveta.acabamentos;
  }

  acabamentoGaveta: any;
  escolherAcabamentoGaveta() {
    this.acabamentoGaveta = this.selectedAcabGaveta;
  }

  resetDadosGaveta() {
    this.criarGavetaFlag = false;
    this.criarPrateleiraFlag = false;
    this.gavetaToPost = {};
    this.posicaoGaveta = "";
    this.selectedMatGaveta = null;
    this.selectedAcabGaveta = null;
    this.selectedProdGaveta = null;
    this.editarGavetaFlag = false;

  }

  areaPai: any;
  area1: any;
  area2: any;
  area3: any;
  private calculaAreaPai(): void {
    this.area1 = this.altPaiInput * this.largPaiInput;
    this.area2 = this.altPaiInput * this.profPaiInput;
    this.area3 = this.largPaiInput * this.profPaiInput;
    this.areaPai = this.area1 + (this.area2 * 2) + (this.area3 * 2);


  }
  precoAcabamentoPai: any;




  private getPrecoAcabamentoPai(): any {
    this.parPrecoAcabSrv.getParPrecoAcabamento().subscribe(data => {
      console.log("DataById:", data);
      for (const ppm in data) {
        //if (data[0].precoMat == ) {


        //}
      }
      return data[3];
    });
  }



  precoArmarioPai: any;
  private calculaPrecoArmarioPai(): void {


    this.precoAcabamentoPai = this.getPrecoAcabamentoPai();
    this.calculaAreaPai();

    this.precoArmarioPai = this.precoMaterialPai * this.areaPai;
    //this.precoArmarioPai = 10;

    console.log("preco material" + this.precoMaterialPai + "area pai" + this.areaPai);

  }


  
  itemArmarioPaiToPost: any;
  itemModuloToPost: any;
  itemGavPratToPost: any;

  idArmarioPaiToPost: any;
  ind: number;
  idsModulosItemToPost: any
  idsGvaPratItemToPost: any;
  parItensToPost:any;
  idItemModulo:any;
  idItemGavPrat:any;
  listParItens:any;
  encomendaToPost:any;

  indice:number;

  private criarEncomenda(): void {
    this.ind = 1;
    this.itemArmarioPaiToPost = {};
    this.itemModuloToPost = {};
    this.itemGavPratToPost = {};
    this.idsModulosItemToPost = {};
    this.parItensToPost = {};
    this.listParItens = {};
    this.encomendaToPost = {};
    this.indice = 0;
    this.encomendaToPost.paritens = [];

    this.encomendaToPost.user = this.user.id;
    //this.encomendaToPost.user = "5c2f8df1f786183814ee1aa6";
    this.encomendaToPost.estado = 1;
    //this.encomendaToPost.nome = "armario encomenda OpenGL";
    this.encomendaToPost.nome = "armario encomenda OpenGL-" + this.user.nome;



    this.itemArmarioPaiToPost.sku = this.armarioPaiToPost.sku;

    this.itemArmarioPaiToPost.nome = this.armarioPaiToPost.nome;
    this.itemArmarioPaiToPost.largura = this.armarioPaiToPost.largura;
    this.itemArmarioPaiToPost.altura = this.armarioPaiToPost.altura;
    this.itemArmarioPaiToPost.profundidade = this.armarioPaiToPost.profundidade;
    this.itemArmarioPaiToPost.acabamento = this.armarioPaiToPost.acabamento;
    this.itemArmarioPaiToPost.material = this.armarioPaiToPost.material;
    console.log(this.itemArmarioPaiToPost.sku);

    // post do item pai armario
    this.itemSrv.postItem(this.itemArmarioPaiToPost).subscribe(data => {
      console.log("item armario pai:", data);

      this.armarioPaiToPost.code = data.code;
      this.itemArmarioPaiToPost.id = data.id;
      this.encomendaToPost.itemPai = this.itemArmarioPaiToPost.id;
      //this.encomendaToPost.itemPai = this.itemArmarioPaiToPost.code;
      //console.log("id item pai post", this.armarioPaiToPost.code );


      //para cada item filho do armario pai
      //console.log("sjvskjdv",this.armarioPaiToPost);

      for (let itemFilho of this.armarioPaiToPost.itemFilhos) {

          this.itemModuloToPost.sku = itemFilho.sku;
          this.itemModuloToPost.nome = itemFilho.nome;
          this.itemModuloToPost.material = itemFilho.material;
          this.itemModuloToPost.acabamento = itemFilho.acabamento;
          this.itemModuloToPost.largura = itemFilho.largura;
          this.itemModuloToPost.profundidade = itemFilho.profundidade;
          this.itemModuloToPost.altura = itemFilho.altura;

          //ppost dos itens filhos Modulos
          this.itemSrv.postItem(this.itemModuloToPost).subscribe(data => {
            console.log("item modulo :", data);
            this.idItemModulo = data.code;

            this.parItensToPost.itemFilhoCode = this.idItemModulo;
            this.parItensToPost.itemPaiCode = this.armarioPaiToPost.code;

            // post par itens pai armario e filhos modulos
            this.itemAgrSrv.postParItem(this.parItensToPost).subscribe(data => {
              console.log("par item pai e modulo:", data);
              //this.encomendaToPost.paritens[this.indice]= data.id;
              this.parItensToPost.paritemID = data.id;
              this.encomendaToPost.paritens.push(this.parItensToPost.paritemID);
              //this.listParItens[this.indice]=data.id;
              this.indice++;



              //this.idsModulosItemToPost[this.ind] = data.id;

              if (itemFilho.itemFilhos !== null) {

                for (let gavprat of itemFilho.itemFilhos) {


                  this.itemGavPratToPost.sku = gavprat.sku;
                  this.itemGavPratToPost.nome = gavprat.nome;
                  this.itemGavPratToPost.acabamento = gavprat.acabamento;
                  this.itemGavPratToPost.material = gavprat.material;
                  this.itemGavPratToPost.largura = gavprat.largura;
                  this.itemGavPratToPost.altura = gavprat.altura;
                  this.itemGavPratToPost.profundidade = gavprat.profundidade;

                  this.itemSrv.postItem(this.itemGavPratToPost).subscribe(data => {
                    console.log("item gaveta/prateleira :", data);

                    this.parItensToPost = {};

                    this.parItensToPost.itemFilhoCode = data.code;
                    console.log("dslfknslidvs", data.code);
                    this.parItensToPost.itemPaiCode = this.idItemModulo;

                    this.itemAgrSrv.postParItem(this.parItensToPost).subscribe(data => {
                      console.log("par item modulo e gaveta/prateleira:", data);
                      //this.encomendaToPost.paritens[this.indice] = data.id;
                      this.parItensToPost.paritemID = data.id;
                      this.encomendaToPost.paritens.push(this.parItensToPost.paritemID);

                      //this.listParItens[this.indice]=data.id;
                      this.indice++;
                      console.log("ksdbgbdfsbnskvs ", data.id);
                      console.log("lista de paresitens ", this.encomendaToPost.paritens);

                      this.encomendaSrv.postEncomenda(this.encomendaToPost).subscribe(data => {
                        console.log("post da encomenda: ", data);
                      });
                    });
                  });
                }
              }
            });
          });
        this.ind++;
      }
    });
  }


}
