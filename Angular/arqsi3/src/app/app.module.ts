import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule,routingComponents  } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProdutoComponent } from './produto/produto.component';
import { MaterialComponent } from './material/material.component';
import { GestaoItemComponent } from './gestao-item/gestao-item.component';
import { ItemComponent } from './item/item.component';
import { ItemAggComponent } from './item-agg/item-agg.component';
import { ProdutoAggComponent } from './produto-agg/produto-agg.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { RegistoComponent } from './registo/registo.component';
import { ConfRegistoComponent } from './conf-registo/conf-registo.component';
import { EncomendaComponent } from './encomenda/encomenda.component';
import { DoubleFAComponent } from './double-fa/double-fa.component';
import { CatalogoComponent } from './catalogo/catalogo.component';
import { EditarCatalogoComponent } from './editar-catalogo/editar-catalogo.component';
import { PrecosComponent } from './precos/precos.component';
import { CriarColecaoComponent } from './criar-colecao/criar-colecao.component';
import { AreaClienteComponent } from './area-cliente/area-cliente.component';

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    ProdutoComponent,
    MaterialComponent,
    GestaoItemComponent,
    ItemComponent,
    ItemAggComponent,
    ProdutoAggComponent,
    DashboardComponent,
    LoginComponent,
    RegistoComponent,
    ConfRegistoComponent,
    EncomendaComponent,
    DoubleFAComponent,
    CatalogoComponent,
    EditarCatalogoComponent,
    PrecosComponent,
    CriarColecaoComponent,
    AreaClienteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
