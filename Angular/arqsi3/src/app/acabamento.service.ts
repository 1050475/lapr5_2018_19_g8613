import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class AcabamentoService {

  private WebApiIt1url = 'https://localhost:5001/api/';

  constructor(private httpClient: HttpClient) { }

  getAcabamentos(): Observable<any> {
    return this.httpClient.get(this.WebApiIt1url + 'Acabamento').pipe(
      map(this.extractData));
  }

  private extractData(res: Response) {
    return res || {};
  }
}
