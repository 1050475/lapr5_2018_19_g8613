# Formato breve
O cliente inicia a visualização do armário que está a configurar. O sistema produz a visualização e mostra-a ao cliente.

# SSD
![UC10SSD](UC10SSD.png)

# Ator Principal
Cliente 

# Partes Interessadas e seus interesses
-Cliente:  Quer visualizar o armário que está a configurar.

# Pré-Condições:
- O Cliente tem de estar registado no sistema.
- Tem de haver produtos no sistema para fazer o armário.

# Pós-Condições:
- O armário é gerado graficamente para apresentação ao cliente.

# Cenário de sucesso principal(fluxo básico):
1. O cliente inicia a ação de visualizar o armário graficamente.
2. O sistema recolhe os dados selecionados e gera o armário para ser apresentado.
3. O utilizador visualiza o armário pretendido.


# Extensões (fluxos alternativos):
*a. O cliente a qualquer momento cancela a operação.

# SD
![UC10SD](UC10SD.png)

