Formato breve:
O utilizador efectua qualquer acção que faz uso da api1 ou api2, o sistema verifica atraves de uma chamada à api3 se a sessao se encontra activa e dentro do tempo de timeout, a api3 responde com um codigo e informações sobre a sessao, utilizador e seu role. O método de identificação de sessão é um token.

SSD:
ssdUc1.4.jpg

Ator Principal:
Sistema do lado do cliente.

Partes Interessadas e seus interesses:
-Empresa: Garantir que os utilizadores usam sessões verificadas e utilizadores tem acesso adquado a seus roles.
-Sistema: receber a vrificação da sessão e informações sobre utilizador e sua sessão.

Pré-Condições:
- O utilizador está registado.
- O utilizador tem sessão.

Pós-Condições:
- Sistema detem inforação sobre sessão e utilizador.

Cenário de sucesso principal(fluxo básico):
1. O Sistema faz um pedido à api3.
2. A api3 verifica se a sessão é válida.
3. Devolve código de sucesso e informação sobre utilizador.

Extensões (fluxos alternativos):
*a. O utilizador cancela o registo

2a. A sessão expirou(atingiu timeout).
2b. A sessão não existe.	


3a. Devolve código de erro de validação.

