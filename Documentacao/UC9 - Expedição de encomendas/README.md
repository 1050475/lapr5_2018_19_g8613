PROBLEMA
{
	A empresa tem capacidade de produzir armários em vários locais (fábricas). A produção de
	cada encomenda é entregue à fabrica cuja localização tem uma distância menor para a
	morada (cidade) de entrega do cliente. 
}

ANÁLISE
{
	*In this section you should describe the study/analysis/research you developed in order to design a solution.*
	
	
}


USER CASES
{

	UC9.1 - Verificar que encomendas há para entregar por fábrica numa data (neste caso, final duma semana)
	UC9.2 - Algoritmo que calcula a rota mais curta (distância) para entregar encomenda
	
	![Use Cases](UC9.US1.png)

}

PRE-CONDIÇÕES
{
	EncomendaEstado = "prontaaexpedir"
	Dia = "Sexta-feira"
	
	
	
}

PÓS-CONDIÇÕES
{

}

CENÁRIO PRIMÁRIO
{

}

CENÁRIOS ALTERNATIVOS 
{
	#minimo 4 e maximo 8 no total de cenários
}

PLANO TESTES
{
	#funcionais e/ou integração e/ou unitários
}


COMMITS
{
	
}