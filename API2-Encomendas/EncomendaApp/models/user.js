var mongoose = require('mongoose');
var validator = require('validator');
var Schema = mongoose.Schema;

// set up a mongoose model and pass it using module.exports
module.exports = mongoose.model('User', new Schema({
    nome: {
        type: String,
        validate: {
            validator: function (v) {
                return /^\D+$/i.test(v);
            },
            message: 'Nome inválido'
        },
        required: true
    },
    email: {
        type: String,
        validate: [{
            validator: value => validator.isEmail(value),
            msg: 'Invalid email.'
        }],
        required: function () {
            return this.role != 'anonimo';
        }
    },
    emailVerificationCode:{
        type: String
    },
    emailVerification: {
        type: Boolean,
    },
    password: {
        type: String,
        required: function () {
            return this.role != 'anonimo';
        }
    },
    cidade: {
        type: mongoose.Schema.Types.ObjectId, ref: 'Cidade',
    },
    role: {
        type: String,
        enum: ['cliente', 'administrativo', 'gestorcatalogo', 'anonimo'],
        required: true
    },
    aceitacaoTermos: {
        type: Boolean,
        required: function () {
            return this.role == 'cliente';
        }
    },
    aceiteEm: {
        type: Date,
        default: Date.now,
        required: function () {
            return this.role == 'cliente';
        }
    },
    secret: {
        type: {
            base32: {
                type: String
            },
            expiration: {
                type: Date
            }
        }
    },
    auth: {
        type: Boolean
    }
}));