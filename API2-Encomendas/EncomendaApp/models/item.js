const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ItemSchema = new Schema({
    itemID: String,
    code: String,
    sku: String,
    nome: String,
    altura: Number,
    largura: Number,
    profundidade: Number,
    material: String,
    acabamento: String,
    ocupacao : Number
});
// Export the model
module.exports = mongoose.model('Item', ItemSchema);