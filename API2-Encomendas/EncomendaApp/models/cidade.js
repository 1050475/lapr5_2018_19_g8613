const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let CidadeSchema = new Schema({
    cidadeID: String,
    nome: {
        type: String,
        required: true,
        unique: true
    },
    latitude: {
        type: String,
        required: true,
    },
    longitude: {
        type: String,
        required: true,
    },
});
// Export the model
module.exports = mongoose.model('Cidade', CidadeSchema);