const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var estado = {
       Submetida: 1,

       Validada: 2,

       Em_producao: 3,

       Embalamento: 4,

       Pronta_a_expedir: 5,

       Expedida: 6,

       Entregue: 7,

       Recebida: 8,
}

let EncomendaSchema = new Schema({
       encomendaID: String,
       nome: String,
       //tem de ser armario
       itemPai: { type: mongoose.Schema.Types.ObjectId, ref: 'Item' },
       paritens: [{ type: mongoose.Schema.Types.ObjectId, ref: 'ParItem' }],
       data: { type: Date, default: new Date() },
       user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
       fabrica: {
              type: mongoose.Schema.Types.ObjectId, ref: 'Fabrica',
       },
       estado: Number,
});

module.exports = mongoose.model('Encomenda', EncomendaSchema);