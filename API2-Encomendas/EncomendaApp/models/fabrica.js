const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let FabricaSchema = new Schema({
    fabricaID: String,
    nome: {
        type: String,
        required: true,
        unique: true
    },
    cidade: {
        type: mongoose.Schema.Types.ObjectId, ref: 'Cidade',
        required: true,
        unique: true
    }
});

module.exports = mongoose.model('Fabrica', FabricaSchema);