const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ParItemSchema = new Schema({
    paritemID: String,

    itemPaiID: {type: mongoose.Schema.Types.ObjectId, ref: 'Item'},
    itemPaiCode: String,
    itemFilhoID: {type: mongoose.Schema.Types.ObjectId, ref: 'Item'},
    itemFilhoCode: String

});
// Export the model
module.exports = mongoose.model('ParItem', ParItemSchema);