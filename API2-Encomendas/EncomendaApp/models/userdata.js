const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let UserDataSchema = new Schema({
    logid: { type: String },
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    date: { type: Date }
});
// Export the model
module.exports = mongoose.model('UserData', UserDataSchema);