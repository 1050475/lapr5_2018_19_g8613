// app/models/itemDTO.js
var ItemDTO  = {
    itemID: String,
    sku: String,
    name: String,
    altura: Number,
    largura: Number,
    profundidade: Number,
    material: String,
    acabamento: String,
    ocupacao: Number
};
module.exports = ItemDTO;
