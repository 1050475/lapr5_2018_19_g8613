function UserDTO(user) {
    return { id: user.id, nome: user.nome, email: user.email, cidadeid: user.cidadeid, emailVerification: user.emailVerification, role: user.role };
}

module.exports = UserDTO;