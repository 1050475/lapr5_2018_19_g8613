// server.js

var config = require('./config'); // get our config file

const express = require('express');
const bodyParser = require('body-parser');
var config = require('./config'); // get our config file
var cors = require('cors');

var morganSaveLoggly = require('./services/log/morganSaveLoggly');
var morganbody = require('morgan-body');

//Imports routes
const encomenda = require('./routes/encomenda.route'); // Imports routes 
const item = require('./routes/item.route');
const paritem = require('./routes/paritem.route');
const user = require('./routes/user.route');
const rotina = require('./routes/rotina.route');
const manager = require('./routes/manager.route');

//End IMPORTS


// initialize our express app
const app = express();

// Set up mongoose connection
const mongoose = require('mongoose');
let dev_db_url = config.database;
let mongoDB = process.env.MONGODB_URI || dev_db_url;

mongoose.connect(mongoDB, { useNewUrlParser: true });
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());
app.use(function requireHTTPS(req, res, next) {
	if (req.get('x-site-deployment-id') && !req.get('x-arr-ssl')) {
	  return res.redirect('https://' + req.get('host') + req.url);
	}
	next();
  });

morganbody(app);
// app.use(morganSaveFile);
app.use(morganSaveLoggly);
  
//Use routes
app.use('/encomenda', encomenda);
app.use('/item', item);
app.use('/paritem', paritem);
app.use('/user', user);
app.use('/rotina', rotina);
app.use('/manager', manager);

// catch 404 Errors and Forward them to Error Handler
app.use((req, res, next) => {
	const err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// Error Handler Function
app.use((err, req, res, next) => {
	const error = app.get('env') === 'development' ? err : {};
	const status = error.status || 500;

	// response to client
	res.status(status).json({
		error: {
			message: error.message
		}
	});

	// show in console
	console.error(err);
});

//End ROUTES

let port = 8080;

app.listen(port, () => {
	console.log('Server is up and running on port number ' + port);
});
