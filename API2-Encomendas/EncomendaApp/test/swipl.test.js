const SWIPL = require('../services/swipl/swipl')
const assert = require('assert');

describe('Fabrica Mais Proxima', function () {
    var cidadeFabricaPorto = '5c20115a8ff55f0e24b023a6';
    var oviedo = '5c30b4b322ee522aa44e1733';
    var cidadeFabricaCorunha = '5c2013488ff55f0e24b023ab';

    describe.only('Test 1', function () {
        it('Vila do Conde mais proximo da fabrica do Porto', async function () {
            let fabrica = await SWIPL.fabricaMaisProxima(vilaDoConde);
            assert.equal(fabrica.cidadeFabrica == cidadeFabricaPorto, true);
        });

        it('Vila do Conde não é mais proximo da fabrica da Corunha', async function () {
            let fabrica = await SWIPL.fabricaMaisProxima(vilaDoConde);
            assert.notEqual(fabrica.cidadeFabrica == cidadeFabricaCorunha, true);
        });

        it('Oviedo é mais proximo da fabrica da Corunha', async function () {
            let fabrica = await SWIPL.fabricaMaisProxima(oviedo);
            assert.equal(fabrica.cidadeFabrica == cidadeFabricaCorunha, true);
        });
    });
});

describe('calculo Rotas',async function () {

    before(async function () {
        // runs before all tests in this block
        await SWIPL.escreveCidadesBC('cidadesAux.pl','city', listacidades).catch(err => { return err });
    });

    describe.only('Test 2', function () {
        var cidadeFabricaPorto = '5c20115a8ff55f0e24b023a6';

        it('Rota começa e acaba na cidade do Porto', async function () {
            let rota = await SWIPL.calculaRota(cidadeFabricaPorto);
            var tamanho = rota.caminho.length;
            assert.equal(rota.caminho[0] == cidadeFabricaPorto && rota.caminho[tamanho-1] == cidadeFabricaPorto, true);
        });

        it('Rota passa por todas as cidades', async function () {
            let rota = await SWIPL.calculaRota(cidadeFabricaPorto);
            var flag = true;

            for(let i = 0; i < listacidades.length; i++){
                if(!rota.caminho.includes(listacidades[i].cidade)){
                    flag = false;
                }
            }
            assert.equal(flag, true);
        });

        it('Rota passa mais de 15 cidades', async function () {
            let rota = await SWIPL.calculaRota(cidadeFabricaPorto);

            assert.equal(rota.caminho.length > 17, true); //17 contanto com a cidade da fabrica no inicio e fim
        });

        it('Rota com menos de 4482 (rota feita sem cruzamentos num mapa)', async function () {
            let rota = await SWIPL.calculaRota(cidadeFabricaPorto);

            assert.equal(rota.distancia/1000 < 4482, true);
        });
    });
});


// // TESTE CALCULO ROTAS ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

// var CalculaRotaService = require('../services/calcularrota.service');

var vilaDoConde = '5c2012988ff55f0e24b023a7';

var listacidades = [{
    cidade: '5c20115a8ff55f0e24b023a6',
    nome: 'Porto',
    latitude: '41.15',
    longitude: '-8.61024'
},
{
    cidade: '5c2012988ff55f0e24b023a7',
    nome: 'Vila do Conde',
    latitude: '41.35173',
    longitude: '-8.74786'
},
{
    cidade: '5c2012cc8ff55f0e24b023a8',
    nome: 'Vila Real',
    latitude: '41.30104',
    longitude: '-7.74224'
},
{
    cidade: '5c2012f88ff55f0e24b023a9',
    nome: 'Viseu',
    latitude: '40.65659',
    longitude: '-7.91247'
},
{
    cidade: '5c2013178ff55f0e24b023aa',
    nome: 'Vigo',
    latitude: '42.2406',
    longitude: '-8.72073'
},
{
    cidade: '5c2013488ff55f0e24b023ab',
    nome: 'Corunha',
    latitude: '43.36234',
    longitude: '-8.41154'
},
{
    cidade: '5c20182b3b41b81144cc50dc',
    nome: 'Madrid',
    latitude: '40.41678',
    longitude: '-3.70379'
},
{
    cidade: "5c30b47e22ee522aa44e172f",
    nome: "Beja",
    latitude: "38.0154479",
    longitude: "-7.8650368",
},
{
    cidade: "5c30b49b22ee522aa44e1731",
    nome: "Barcelona",
    latitude: "41.3828939",
    longitude: "2.1774322"
},
{
    cidade: "5c30b4b322ee522aa44e1733",
    nome: "Oviedo",
    latitude: "43.3605977",
    longitude: "-5.8448989"
},
{
    cidade: "5c30b4d222ee522aa44e1737",
    nome: "Lugo",
    latitude: "43.0462247",
    longitude: "-7.4739921"
},
{
    cidade: "5c30b4eb22ee522aa44e1739",
    nome: "Ourense",
    latitude: "43.0462247",
    longitude: "-7.8674242"
},
{
    cidade: "5c30b50922ee522aa44e173d",
    nome: "Valladolid",
    latitude: "41.6521328",
    longitude: "-4.728562"
},
{
    cidade: "5c30b51f22ee522aa44e173f",
    nome: "Toledo",
    latitude: "39.8560679",
    longitude: "-4.0239568"
},
{
    cidade: "5c30b54222ee522aa44e1741",
    nome: "Faro",
    latitude: "37.0537571",
    longitude: "-7.931936"
},
{
    cidade: "5c30b58722ee522aa44e1743",
    nome: "Sevilha",
    latitude: "37.3886303",
    longitude: "-5.9953171"
},
{
    cidade: "5c30b59f22ee522aa44e1745",
    nome: "Malaga",
    latitude: "36.7213028",
    longitude: "-4.4216366"
}
];



















