const Encomenda = require('../models/encomenda');
const UserRepository = require('../repository/user.repository');
const CalculaRota = require('../services/calcularrota.service');
const EncomendaRepository = require('../repository/encomenda.repository');

exports.encomenda_create = async function (req, res) {

    if (req.user) {
        let encomenda = new Encomenda();
        encomenda.nome = req.body.nome;
        encomenda.itemPai = req.body.itemPai;
        encomenda.paritens = req.body.paritens;
        encomenda.user = req.user; // apenas o user pode fazer as suas próprias encomendas
        encomenda.estado = req.body.estado;

        let userDetail = await UserRepository.GetById(encomenda.user).catch(err => { res.json({ message: 'encomenda sem utilizador' + err }) });

        if (userDetail != undefined) {
            let fabrica = await CalculaRota.fabricaMaisProxima(userDetail.cidade).catch(err => { console.log('não há fábricas disponíveis para a encomenda') });
            console.log(fabrica);

            encomenda.fabrica = fabrica;

            let enc = await EncomendaRepository.SaveEncomenda(encomenda).catch(err => { return { message: 'erro in save' + err } });

            return { message: 'Criada Encomenda com id:', id: encomenda.id, fabrica: encomenda.fabrica };
        }

    }
};


exports.encomenda_all = async function (req) {
    console.log(req.user)

    if (req.user != undefined && req.user.role == 'administrativo') {
        var list = await EncomendaRepository.getAllEncomendas().catch(err => { return err });
        return list;
    } else {
        var list = await EncomendaRepository.getAllByUser(req.user.id).catch(err => { return err });
        return list;
    }  
}