const User = require('../models/user');
const bcrypt = require('bcryptjs');
var UserRepository = require('../repository/user.repository');
var CidadeRepository = require('../repository/cidade.repository');
var QRCode = require('qrcode');
var speakeasy = require('speakeasy');
var jwt = require('jsonwebtoken');
var config = require('../config');
var alerta = require('../services/alerta');
var uniqid = require('uniqid');


exports.user_register = async function (body) {
    if (!body.password) return 'password';

    if (!body.email) return 'email';

    try {
        let user = await UserRepository.GetUserByEmail(body.email);
        if (!user) console.log('creatingnewuser')

        if (user) return 'exist';
    } catch (err) { return 'errordatabase' }

    var hashedPassword = bcrypt.hashSync(body.password, 8);

    let newUser = new User();

    newUser.nome = body.nome;
    newUser.email = body.email;
    newUser.password = hashedPassword;
    newUser.cidade = body.cidade;
    newUser.role = body.role;
    newUser.aceitacaoTermos = body.aceitacaoTermos;
    newUser.emailVerificationCode = uniqid.time();
    newUser.emailVerification = false;


    let secret = speakeasy.generateSecret({ length: 10, name: "EncomendasAPI" });
    let data_url = await QRCode.toDataURL(secret.otpauth_url).catch(err => { return err });
    newUser.secret = {
        base32: secret.base32,
        expiration: new Date().setUTCSeconds(new Date().getUTCSeconds() + 300)
    };

    let userDTO = await UserRepository.SaveUser(newUser).catch(err => { return err });


    var link = config.localhostaddress + '/user/verify?id=' + userDTO.id + '&code=' + newUser.emailVerificationCode;
    var html = 'Olá,<br> Pressione o link para verificar o seu email<br><a href=' + link + '>Clica aqui para verificar o email</a>';
        

    var email = '';

    //se o role for administrativo, o registo é verificado pelo email do webmaster
    if (newUser.role == 'administrativo' || newUser.role == 'gestorcatalogo') {
        email = config.alertEmail;
    } else {
        email = userDTO.email;
    }

    alerta.emailUserHtml(email, 'EncomendasAPP Email verification', html);

    // return userDTO;
    return {
        qrcode: data_url
    }
};

exports.user_authentication = async function (body) {

    if (!body.password) return 'Autenticação falhada. Password é necessária';

    if (!body.email) return 'Autenticação falhada. Email é necessária';

    let user = await UserRepository.GetUserByEmail(body.email).catch(err => { return 'errordatabase' })


    if (!user || !bcrypt.compareSync(body.password, user.password)) {
        return 'Autenticação falhada. Email ou password inválidos'
    }
    else if (!user.emailVerification) {
        return 'email não verificado'
    }
    else {
        // let secret = speakeasy.generateSecret({ length: 10, name: "EncomendasAPI" });
        // let data_url = await QRCode.toDataURL(secret.otpauth_url).catch(err => { return err });
        // user.secret = {
        //     base32: secret.base32,
        //     expiration: new Date().setUTCSeconds(new Date().getUTCSeconds() + 300)
        // };
        user.auth = true;

        UserRepository.SaveUser(user).catch(err => { return err });

        var resposta = {
            id: user.id,
            role: user.role,
            auth: true
        }

        return resposta;
    };
}

exports.user_login = async function (body) {
    if (!body.id) return { code: 0, message: 'id is required!' }

    if (!body.code) return { code: 0, message: 'authentication code is required' }

    let user = await UserRepository.GetById(body.id).catch(err => { return { code: 0, message: 'user not found' + err } });

    if (!user) return { code: 1, message: 'user not found' }

    if(!user.auth) return { code:0, message: 'authentication is required'}

    if (!user.emailVerification) {
        return 'email não verificado'
    }

    if (!user.secret) return { code: 1, message: 'login error, no 2FA generated' }

    // if (user.secret.expiration < new Date()) return { code: 2, message: 'authentication required expired' }

    var verified = speakeasy.totp.verify({
        secret: user.secret.base32,
        encoding: 'base32',
        token: body.code,
        window: 2
    })

    if (verified) {
        const payload = {
            id: user.id,
            nome: user.nome,
            email: user.email,
            cidade: user.cidade,
            role: user.role,
            agreeToTerms: user.agreeToTerms
        };

        //criação novo token
        var token = jwt.sign(payload, config.secret, {
            expiresIn: 3600
        });

        user.auth = false;

        //user.secret = undefined;
        UserRepository.SaveUser(user).catch(err => { return { message: 'authentication required expired', error: err } });

        return {
            id: user.id,
            nome: user.nome,
            cidade: user.cidade,
            role: user.role,
            success: true,
            message: 'Enjoy your token!',
            token: token
        };

    } else {
        return { code: 3, message: 'Invalid code, verification failed' };
    }
}

exports.user_emailverification = async function (req) {
    let user = await UserRepository.GetById(req.query.id).catch(err => { return { code: 0, message: 'user inválido' + err } });
    if (!user) return 'user inválido'

    if (user.emailVerification) return { code: 1, message: 'email já foi verificado' }

    if (user.emailVerificationCode == req.query.code) {
        user.emailVerification = true;
        user.emailVerificationCode = undefined;

        // await UserRepository.SaveUser(user).catch(err => { return err });
        let newUser = await UserRepository.SaveUser(user).catch(err => 'user inválido')
        console.log(newUser)
        return { code: 200, message: 'email verificado com sucesso!' }
    } else {
        return { code: 0, message: 'verificação inválida' }
    }
};

var Encriptar = require('../services/encriptar');

exports.user_portability = async function (req){
    let user = await UserRepository.GetById(req.user.id).catch(err => {return 'user notfound' + err});
    let cidade = await CidadeRepository.GetById(user.cidade).catch(err => {return 'user notfound' + err});

    // let userEncryptData = await Encriptar.encrypt(JSON.stringify(user));
    let nome = await Encriptar.encrypt(user.nome);
    let email = await Encriptar.encrypt(user.email);
    let nomeCidade = await Encriptar.encrypt(cidade.nome);

    return {
        userid: user.id,
        nome: nome,
        email: email,
        cidade:  nomeCidade
    }
}

exports.user_tokenverifiedwithsuccess = async function (req){
    return {
        userid: req.user.id,
        nome: req.user.nome,
        email: req.user.email,
    }
}

exports.user_renovacaotermos = async function (req){

    let user = UserRepository.GetById(req.user.id).catch(err => {return {message: 'renovação falhada'}});

    if(user){
        if(req.body.termos == true){
            user.aceiteEm = new Date();
            user.aceitacaoTermos = true;

            return {
                userid: user.id,
                nome: user.nome,
                data: user.aceiteEm,
                aceitacaoTermos: user.aceitacaoTermos
            }
        }else{
            user.aceiteEm = new Date();
            user.aceitacaoTermos = false;
            let newUser = await UserRepository.ToAnonimoUser(user).catch(err => { console.log(err) });

            return {
                userid: user.id,
                nome: user.nome,
                data: user.aceiteEm,
                aceitacaoTermos: user.aceitacaoTermos
            }
        }
    }else{
       return {message: 'user não existe'};
    }
    
    
}