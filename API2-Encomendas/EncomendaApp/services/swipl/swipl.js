const swipl = require('swipl');
const EditFiles = require('../editfiles.services');

const {
    list,
    compound,
    variable,
    serialize
} = swipl.term;

const string = 'working_directory(_,\'services/swipl\')';
swipl.call(string);


exports.calculaRota = async function (origem) {
    
    swipl.call('consult(tp2)');

    const escaped = serialize(
        compound('tps3', [
            'z' + origem,
            variable('Cam'),
            variable('Custo')])
    );

    var ret = swipl.call(escaped);
    var arr = [];
    // console.log(ret.Cam)
    convertRecursiveListToArray(ret.Cam, arr);
    // console.log(arr);

    return {
        caminho: arr,
        distancia: ret.Custo
    }
}

exports.fabricaMaisProxima = async function (cidade) {
    // let string = 'working_directory(_,\'services/swipl\')';
    // swipl.call(string);
    swipl.call('consult(tp2)');

    const escaped = serialize(
        compound('fabricaMaisProxima', [
            'z' + cidade,
            variable('FabricaProxima'),
            variable('Distancia')])
    );

    var ret = swipl.call(escaped);


    return {
        cidadeFabrica: ret.FabricaProxima.substring(1),
        distancia: ret.Distancia
    }
}

//escrevecidadesnabaseconhecimento
exports.escreveCidadesBC = async function (file, tipo, cidades) {
    // console.log('funçao escreveCidades:::::::::::::::::::::::::::::::::::::')
    // console.log(cidades);
    // var dir = 'services/swipl/'
    var filename = file;
    var doc = '';
    if (cidades == undefined) {
        return 'sem cidades para guardar na BC'
    }

    for (let i = 0; i < cidades.length; i++) {
        doc = doc + tipo + '('
            + 'z' + cidades[i].cidade + ','
            + cidades[i].latitude + ','
            + cidades[i].longitude + ').\r\n'

    }

    let flag = await EditFiles.writeFile(filename, doc).catch(err => {
        return 'erro ao escrever base conhecimento erro:' + err;
    });
    // console.log(doc);
    return flag;
}


function convertRecursiveListToArray(ret, arr) {
    if (ret == null || ret == [] || ret == "[]") return;
    //console.log(ret.head);
    arr.push(ret.head.substring(1));
    convertRecursiveListToArray(ret.tail, arr);
}
