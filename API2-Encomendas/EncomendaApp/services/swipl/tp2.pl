% ----------------------------------------------------------------------------------------------
%
%      _____  .____     ________    _________   ____           _____________________________
%     /  _  \ |    |   /  _____/   /  _  \   \ /   /           \__    ___/\______   \_____  \
%    /  /_\  \|    |  /   \  ___  /  /_\  \   Y   /    ______    |    |    |     ___//  ____/
%   /    |    \    |__\    \_\  \/    |    \     /    /_____/    |    |    |    |   /       \
%   \____|__  /_______ \______  /\____|__  /\___/                |____|    |____|   \_______ \
%           \/        \/      \/         \/                                                 \/
%
% -----------------------------------------------------------------------------------------------

:-consult('fabricas.pl').
:-consult('cidades.pl').
:-consult('cidadesAux.pl').
%:-consult('base_conhecimento.pl').
:-consult('deteta_intersecoes.pl').
:-consult('reordena_segmentos.pl').


%  dist_cities(brussels,prague,D).
%  D = 716837.
dist_cities(C1,C2,Dist):-
    city(C1,Lat1,Lon1),
    city(C2,Lat2,Lon2),
    distance(Lat1,Lon1,Lat2,Lon2,Dist).


dist_fabrica(C1,C2,Dist):-
    cidade(C1,Lat1,Lon1),
    fabrica(C2,Lat2,Lon2),
    distance(Lat1,Lon1,Lat2,Lon2,Dist).


degrees2radians(Deg,Rad):-
	Rad is Deg*0.0174532925.

% distance(latitude_first_point,longitude_first_point,latitude_second_point,longitude_second_point,distance
% in meters)
distance(Lat1, Lon1, Lat2, Lon2, Dis2):-
	degrees2radians(Lat1,Psi1),
	degrees2radians(Lat2,Psi2),
	DifLat is Lat2-Lat1,
	DifLon is Lon2-Lon1,
	degrees2radians(DifLat,DeltaPsi),
	degrees2radians(DifLon,DeltaLambda),
	A is sin(DeltaPsi/2)*sin(DeltaPsi/2)+ cos(Psi1)*cos(Psi2)*sin(DeltaLambda/2)*sin(DeltaLambda/2),
	C is 2*atan2(sqrt(A),sqrt(1-A)),
	Dis1 is 6371000*C,
	Dis2 is round(Dis1).

cidadeIgual(_,[]):- false.
cidadeIgual(C1,[C1|_]):- true,!.
cidadeIgual(C1, [_|T]):- cidadeIgual(C1,T).



fabricaProxima(_,[],_,100000000).
fabricaProxima(C1,[H|T],FP,D):- dist_fabrica(C1,H,Dist), fabricaProxima(C1,T,FP1,D1), ((Dist < D1, FP = H, D = Dist); (D = D1, FP = FP1)).


fabricaMaisProxima(C1,FabricaProxima,Distancia):- findall(X,fabrica(X,_,_), ListaFabricas),!, ((cidadeIgual(C1,ListaFabricas),FabricaProxima = C1,Distancia = 0); fabricaProxima(C1,ListaFabricas,FabricaProxima,Distancia)).

teste(L):- findall(X, city(X,_,_), L).



%-------------%
%  Itera��o 1
%-------------%

% 1.

% Exemplo (utilizando as 5 primeiras cidades):
% tps1(tirana, R).
% R =  (5889474, [tirana, andorra, brussels, minsk, vienna, tirana]).

tps1(Orig, CustoCam):- findall(X, city(X,_,_),ListaCidades), length(ListaCidades,NCidadesAVisitar),
findall(Caminho,(dfs(Orig,X,Cam1),length(Cam1,TamAtual),TamAtual=NCidadesAVisitar,append(Cam1,[Orig],Caminho)),Caminhos),
getCustoMenor(Caminhos, CustoCam), !.


/*ordena a lista de permutacoes de caminhos por ordem de custo e de seguida seleciona o primeiro da lista resultado (o de menor custo) como sendo o caminho ideal ('Resposta')*/
getCustoMenor(Lista, Resposta):- associarCustos(Lista, [], ListaCustos), sort(ListaCustos, [Resposta|_]).

/*coloca na lista 'ListaCustos' elementos no formato (CustoCaminho, ListaDeCidadesDoCaminho). (Exemplo: (5889474, [tirana, andorra, brussels, minsk, vienna, tirana]))*/
associarCustos([], LCustos, ListaCustos):- !, true, ListaCustos = LCustos.
associarCustos([H|T], LCustos, ListaCustos):- getCusto(H, CustoH),associarCustos(T, [(CustoH, H)|LCustos], ListaCustos).

/*devolve o custo de um caminho*/
getCusto([_|[]], 0).
getCusto([H|[H1|T]], CustoListaTotal):- dist_cities(H, H1, Custo), getCusto([H1|T], CustoAux), CustoListaTotal is Custo+CustoAux.

dfs(Orig,Dest,Cam):-dfs(Orig,Dest,[Orig],Cam).
dfs(Dest,Dest,LA,Cam):-reverse(LA,Cam).
dfs(Act,Dest,LA,Cam):-dist_cities(Act,X,_),\+ member(X,LA),dfs(X,Dest,[X|LA],Cam).

% 2.
% O predicado tps1 tem capacidade de resolver no m�ximo 9 cidades, antes de encontrar problemas de escassez de mem�ria.


%-------------%
%  Itera��o 2
%-------------%

% 3.

tps2(Orig,Cam,CustoCam):- findall(X, city(X,_,_),ListaCidades),length(ListaCidades,NCidadesAVisitar),bestfs2([Orig],NCidadesAVisitar,Cam1), append(Cam1,[Orig],Cam), getCusto(Cam,CustoCam).

bestfs2(LA,1,Cam):- !,reverse(LA,Cam). /*inversao do caminho final*/

bestfs2(LA,NCidadesAVisitar,Cam):- LA=[Orig|_],
										findall((Custo,[X|LA]),
													(city(X,_,_),\+ member(X,LA), dist_cities(X,Orig,Custo))
												,Novos)
										, sort(Novos,NovosOrd),
								NovosOrd = [(_,Melhor)|_],
                                                                Aux1 is NCidadesAVisitar,
                                                                NCidadesAVisitar2 is Aux1-1,
                                                               bestfs2(Melhor,NCidadesAVisitar2,Cam).

%-------------%
% Itera��o  3 %
%-------------%
%tps2semcusto
tps2adaptado(Orig,Cam):- findall(X, city(X,_,_),ListaCidades),length(ListaCidades,NCidadesAVisitar),bestfs2([Orig],NCidadesAVisitar,Cam1), append(Cam1,[Orig],Cam).

tps3(Origem, CamSemCruzamentos, Custo):-
	tps2adaptado(Origem, CamIntermedio),opt(CamIntermedio, CamIntermedio,CamSemCruzamentos),write(CamSemCruzamentos),
	getCusto(CamSemCruzamentos,Custo),! ,true.

%se o resultado � igual � lista inicial
opt(List,[_,_,_],List):-
		!.
%
opt(List, [A,B,C,D|T], CamSemCruzamentos):-
	opt(List,[A,B],[C,D|T],CamSemCruzamentos),
	!.

%vai buscar os proximos 4 elementos da lista
opt(List,[_,B],[_|[]], CamSemCruzamentos):-
	getNextPivot(List,B,NewC,NewT),
	opt(List, [B,NewC|NewT],CamSemCruzamentos),
	!.

opt(List, [A,B],[_,A|_],CamSemCruzamentos):-
	getNextPivot(List,B,NewC,NewT),
	opt(List, [B,NewC|NewT],CamSemCruzamentos),
	!.

%CORE : verifica se intersecta ou n�o
opt(List,[A,B],[C,D|T],CamSemCruzamentos):-
	getCoord(A,C, AX,AY,CX,CY),
	getCoord(B,D,BX,BY,DX,DY),
	(
		doIntersect((AX,AY), (BX,BY), (CX,CY), (DX,DY)),
	        !,
		desfazCruzamento(List,B,C,ListDepoisTroca),
		write(A),write('-'),write(B),write(' cruza com '),write(C),write('-'),write(D),nl,
		opt(ListDepoisTroca, ListDepoisTroca,CamSemCruzamentos),!
	;
	opt(List,[A,B],[D|T],CamSemCruzamentos),!).


getCoord(A,C, AX, AY, CX,CY) :-
	city(A, AX, AY),
	city(C, CX, CY).

desfazCruzamento(List,B,C,NewList):-
	nth1(BPos,List,B),
	nth1(CPos,List,C),
	delete(List,B,NewList1),
	delete(NewList1,C,NewList2),
	nth1(BPos,NewList3,C,NewList2),
	nth1(CPos,NewList,B,NewList3).


	getNextPivot(List,B,NewPivot,RestoLista):-
nth1(BPos,List,B),
C is BPos+1,
nth1(C,List,NewPivot),
nth1(PosInicial,List,NewPivot),
length(List,Postotal),
getSubList(List,PosInicial,Postotal,ListAux), reverse(ListAux,RestoLista).


getSubList(_, PosInicial,PosInicial,_):-!.
getSubList(List,PosInicial,Postotal,NewList):-
	PosInicial =< Postotal,
	J1 is PosInicial+1,
	nth1(J1,List,Element),
	getSubList(List, J1, Postotal, NewList1),
	append(NewList1,[Element], NewList),!.


%-------------%
% Itera��o  4 %
%-------------%

values('Temperatura', 1.0).
values('NumeroIteracoes', 1000).
values('Alpha', 0.99).

tsp4(Orig, CamFinal, CustoFinal):-
				gerarSolucaoAleatoria(Orig, SolucaoAleatoria),
				getCusto(SolucaoAleatoria,Custo),
				write('Custo Aleat�rio => '), write(Custo),nl,
				values('Temperatura', T),
				values('NumeroIteracoes',It),
				simulatedAnnealing(SolucaoAleatoria,Custo,It,T,Cam,_)
				, append([Orig], Cam, A)
				, append(A, [Orig], CamFinal)
				, getCusto(CamFinal, CustoFinal), !.

simulatedAnnealing(Original,Cold,0,_,Original,Cold).
simulatedAnnealing(Original,Cold,It,T,CamFinal,CustoFinal):-
													It2 is It-1,
													values('Alpha', A),
													T1 is A*T,
													gerarVizinhoAleatorio(Original,NewL),
													getCusto(NewL,Cnew),
													((Cnew < Cold),!,
														simulatedAnnealing(NewL,Cnew,It2,T1,CamFinal,CustoFinal)
														;
														ifTroca(Original,Cold,NewL,Cnew,T,LRes,CRes),
														simulatedAnnealing(LRes,CRes,It2,T1,CamFinal,CustoFinal)).


gerarVizinhoAleatorio(S1,Sn):-
								length(S1,T1),
								Start is 2,
								End is T1-1,
								random_between(Start,End,Pos1),
								random_between(Start,End,Pos2),
								nth1(Pos1,S1,E1), nth1(Pos2,S1,E2),
								removeElementPos(Pos1,S1,S2), insertElementPos(Pos1,E2,S2,S3),
								removeElementPos(Pos2,S3,S4), insertElementPos(Pos2,E1,S4,Sn).

removeElementPos(Pos,List,NewList):- nth1(Pos, List, _, NewList),!.
insertElementPos(Pos,Elem,List,NewList):- nth1(Pos, NewList, Elem, List),!.

ifTroca(Original,Cold,NewLista,Cnew,T,NewL,NewC):-
												random(R),
												acceptance_Probability(Cold,Cnew,T,Ap),
												(	(	(Ap>1; Ap>R),
														(NewC = Cnew, NewL = NewLista)
													);
												NewL = Original, NewC = Cold).




%gena solucao aleatoria entre as cidades
gerarSolucaoAleatoria(Orig, L):-
									findall(City, (city(City,_,_),
									City \== Orig), CityList),
									random_permutation(CityList, L).



acceptance_Probability(Cost1, Cost2, T, Result) :-
												Result is e^((Cost1-Cost2)/ T).






