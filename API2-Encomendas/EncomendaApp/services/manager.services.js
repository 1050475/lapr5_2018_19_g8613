const Cidade = require('../models/cidade');
const Fabrica = require('../models/fabrica');
const CidadeRepository = require('../repository/cidade.repository');
const FabricaRepository = require('../repository/fabrica.repository');
const UserDataRepository = require('../repository/userdata.repository');
const CalculaRota = require('../services/calcularrota.service');
const LogService = require('./log/userdataSave')

exports.cidade_create = async function(body){
    var cidade = new Cidade();
    cidade.nome = body.nome;
    cidade.latitude = body.latitude;
    cidade.longitude = body.longitude;

    let newcidade = await CidadeRepository.SaveCidade(cidade).catch(err => { return {message: 'erro in save Cidade'} });

    return newcidade;
}

exports.cidade_all = async function(){
    let todasCidades = CidadeRepository.AllCidades().catch(err => { return {message: 'erro na database'} });
    
    return todasCidades;
}

exports.fabrica_create = async function(body){
    var fabrica = new Fabrica();
    fabrica.nome = body.nome;
    fabrica.cidade = body.cidadeID;

    let newfabrica = await FabricaRepository.SaveFabrica(fabrica).catch(err => { return {message: 'erro in save Fabrica'} });

    return newfabrica;
}

exports.fabrica_all = async function(){
    let todasFabricas = FabricaRepository.AllFabricas().catch(err => { return {message: 'erro na database'} });
    
    return todasFabricas;
}

exports.log_byId = async function(logid){
    let res = await LogService.getLogById(logid).catch(err => { return err })
    
    return res;
}

exports.delete_logById = async function(logid){
    let res = await LogService.deleteLog(logid).catch(err => { return err })
    
    return res;
}

exports.deleteAllLogsByUser = async function(userid){
    let listauserdata = await UserDataRepository.GetUserDataByUser(userid).catch(err => { return err });

    if(listauserdata && listauserdata.length > 0){
        for(let i = 0; i < listauserdata.length; i++){
           let flag =  await LogService.deleteLog(listauserdata[i].logid).catch(err => { return err })
        }
    }

    return 'done';
}

exports.deleteAllLogsByUserByDaysofExistence = async function(userid, dias){
    let listauserdata = await UserDataRepository.GetUserDataByUser(userid).catch(err => { return err });

    if(listauserdata && listauserdata.length > 0){
        for(let i = 0; i < listauserdata.length; i++){
            let timeDifference = Math.abs(new Date().getTime() - listauserdata[i].date.getTime());

            let differentDays = Math.ceil(timeDifference / (1000 * 3600 * 24));
            
            if(differentDays > dias){
                console.log('log com '+ differentDays + ' dias. apagando log')
                await LogService.deleteLog(listauserdata[i].logid).catch(err => { return err })
            }
            
        }
    }

    return 'done';
}

exports.atualizaFabricas = async function(){
    let todasFabricas = await FabricaRepository.AllFabricas().catch(err => { return {message: 'erro na database'} });

    var listaCidades = [];
    

    if(todasFabricas.length > 0){
        for (let i = 0; i < todasFabricas.length; i++){
            let cidade = await CidadeRepository.GetById(todasFabricas[i].cidade).catch(err => {message: 'erro na database'});

            listaCidades.push({
                cidade: cidade.id,
                nome: cidade.nome,
                latitude: cidade.latitude,
                longitude: cidade.longitude,
            });
        }
        // console.log('lista cidades do atualiza fabricas.pl');
        // console.log(listaCidades);

        CalculaRota.atualizaFabricas(listaCidades);
    }
    
}

exports.atualizaCidades = async function(){
    let cidades = await CidadeRepository.AllCidades().catch(err => { return {message: 'erro na database'} });

    var listaCidades = [];
    

    if(cidades.length > 0){
        for (let i = 0; i < cidades.length; i++){

            listaCidades.push({
                cidade: cidades[i].id,
                nome: cidades[i].nome,
                latitude: cidades[i].latitude,
                longitude: cidades[i].longitude,
            });
        }
        // console.log('lista cidades do atualiza cidades.pl');
        // console.log(listaCidades);

        CalculaRota.atualizaCidades(listaCidades);
    }
    
}
