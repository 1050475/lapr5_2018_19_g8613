var SWIPL = require('./swipl/swipl');
var FabricaRepository = require('../repository/fabrica.repository');
var CidadeRepository = require('../repository/cidade.repository');

exports.calculoRota = async function (cidadeFabrica, listaCidades){
    console.log('calculando rota')
    let flag = await SWIPL.escreveCidadesBC('cidadesAux.pl','city',listaCidades).catch(err => { return err });
    
    let rota = await SWIPL.calculaRota(cidadeFabrica).catch(err => { return err });   
    
    let cidades = [];
    if(rota.caminho != undefined){
        for(let i = 0; i < rota.caminho.length; i++){
            let cidade = await CidadeRepository.GetById(rota.caminho[i]).catch(err => { return err });
            cidades.push(cidade.nome);
        }
    
        let fabrica = await FabricaRepository.GetFabricaByCidade(cidadeFabrica).catch(err => { return err });
    
        let resultado = {
            fabrica: fabrica.nome,
            morada: cidades[0],
            rota: cidades
        };
        console.log(resultado);
        return resultado;
    }
    
    return {message: 'sem rotas'};
}

exports.fabricaMaisProxima = async function(cidadeEncomenda){
    
    let resultado = await SWIPL.fabricaMaisProxima(cidadeEncomenda).catch(err => { return err });
    let fabrica = await FabricaRepository.GetFabricaByCidade(resultado.cidadeFabrica).catch(err => { return err });

    console.log('fabrica mais proxima de cidade: ' + cidadeEncomenda + ' é fabrica: '+ fabrica.nome + ' a distancia:'+ resultado.distancia);
    console.log(fabrica.nome)
    return fabrica.id;
}

exports.atualizaFabricas = async function(listaCidades){
    return await SWIPL.escreveCidadesBC('fabricas.pl','fabrica',listaCidades).catch(err => { return err });
}

exports.atualizaCidades = async function(listaCidades){
    return await SWIPL.escreveCidadesBC('cidades.pl','cidade',listaCidades).catch(err => { return err });
}