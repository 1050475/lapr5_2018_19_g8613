const alerta = require('./alerta');
const EncomendaRepository = require('../repository/encomenda.repository');
const FabricaRepository = require('../repository/fabrica.repository');
const CidadeRepository = require('../repository/cidade.repository');
const UserRepository = require('../repository/user.repository')
const CalculaRota = require('../services/calcularrota.service');
const Manager = require('../services/manager.services');
const config = require('../config');

exports.rotinaCalculoRota = async function () {
    var mensagem = '';

    //foreach fabrica
    let fabricas = await FabricaRepository.AllFabricas().catch(err => { return err });
    //calcular melhor rota para encomendas no estado Pronta_a_expedir : 5,
    if (!fabricas) {
        mensagem = 'não há fábricas'
        console.log(mensagem)
        return mensagem
    }

    listaAdministrativos = await UserRepository.GetUsersByRole('administrativo').catch(err => { return err });

    if (listaAdministrativos == undefined) {
        mensagem = 'não há administrativos';
        console.log('não há administrativos');
        return mensagem;
    }

    var estado = 5; // estado pronta a expedir
    for (let i = 0; i < fabricas.length; i++) {

        let listaCidades = await getListaCidadesEncomendasPorFabrica(fabricas[i], estado);

        if (listaCidades == undefined) return 'não encomendas por entregar';
        let rota;

        if (listaCidades.length > 1) {
            //calcularRota(listaCidades)
            rota = await CalculaRota.calculoRota(fabricas[i].cidade, listaCidades).catch(err => { return 'erro no calculo rota' + err });
        } else if (listaCidades[0].entregas == 0) {
            rota = 'não há entregas para esta fábrica'
        } else {
            rota = 'todas as entregas são na cidade da fábrica: ' + listaCidades[0].nome;
        }

        //enviar rota para os administrativos
        enviarRota(fabricas[i], listaCidades[0], listaAdministrativos, rota);
    }

}

exports.verificacaoConsentimento = async function () {
    listaUsers = await UserRepository.AllUsers().catch(err => { return err });

    if (listaUsers.length > 0) {
        for (let i = 0; i < listaUsers.length; i++) {
            
            let timeDifference = Math.abs(new Date().getTime() - listaUsers[i].aceiteEm.getTime());

            let differentDays = Math.ceil(timeDifference / (1000 * 3600 * 24));

            console.log(differentDays + ' dias');

            //remoção de dados
            if(differentDays >= config.prazoconservacaodados){
                if (listaUsers[i].email && listaUsers[i].role != 'anonimo') {

                    var text = 'A sua conta expirou. Todos os seus dados foram removidos';

                    let flag = await UserRepository.ToAnonimoUser(listaUsers[i]).catch(err => { console.log(err) });

                    await alerta.emailUser(user.email, 'Conta Expirada' + listaUsers[i].nome, text, function (err2, info) {
                            if (err2) return err2;
                            console.log(info);
                    });
                }
            
            } else if (differentDays >= config.prazoconservacaodados - 10){ //aviso data conservação de dados proxima
                if (listaUsers[i].email  && listaUsers[i].role != 'anonimo') {

                    var text = 'A sua conta tem 10 dias até expirar. Por favor renove faça login novamente e renove a aceitação de termos';

                    await alerta.emailUser(user.email, 'Conta a expirar' + listaUsers[i].nome, text, function (err2, info) {
                            if (err2) return err2;
                            console.log(info);
                    });
                }
            }

            //remoção de logs (são mantidos apenas por 72h por exigência das autoridades)
            if(listaUsers[i].role != 'anonimo'){
                await Manager.deleteAllLogsByUserByDaysofExistence(listaUsers[i].id, config.prazoconservacaolog).catch(err => {console.log(err)});
            } 
        }
    }


}


getListaCidadesEncomendasPorFabrica = async function (fabrica, estado) {
    let listaEncomendas = await EncomendaRepository.GetEncomendaByEstadoAndByFabrica(fabrica.id, estado);
    mensagem = 'fabrica.nome:' + fabrica.nome + ' não tem encomendas';
    if (!listaEncomendas) {
        console.log(mensagem)
        return false;
    }


    //não repetir cidades, não incluir cidades da fábrica
    //fazer por ids e não por nomes
    let listaCidades = [];

    let cidadeFabrica = await CidadeRepository.GetById(fabrica.cidade).catch(err => { return err });

    listaCidades.push({
        cidade: cidadeFabrica.id,
        nome: cidadeFabrica.nome,
        latitude: cidadeFabrica.latitude,
        longitude: cidadeFabrica.longitude,
        entregas: 0
    });

    for (let j = 0; j = listaEncomendas.length; j++) {
        let encomenda = listaEncomendas[i];

        //morada user da encomenda
        let user = await UserRepository.GetById(encomenda.user).catch(err => { return err });

        //se a cidade for diferente da cidade da fábrica e se não for repetida
        if (cidadeFabrica.id != user.cidade) {
            let cidade = await CidadeRepository.GetById(user.cidade).catch(err => { return err });
            if (listaCidades.indexOf(user.cidade) > -1) {
                listaCidades.indexOf(user.cidade).entregas++;
            } else {
                listaCidades.push({
                    cidade: cidade.id,
                    nome: cidade.nome,
                    latitude: cidade.latitude,
                    longitude: cidade.longitude,
                    entregas: 1
                });
            }
        }
    }
    console.log('lista cidades = ' + listaCidades.length);
    return listaCidades;
}

function enviarRota(fabrica, cidadeFabrica, listaAdministrativos, rota) {
    listaAdministrativos.forEach(user => {
        if (user.email) {
            alerta.emailUser(user.email, 'Rotas da ' + fabrica.nome, 'Rota da Fábrica: ' + fabrica.nome
                + '\nFábrica: ' + fabrica.nome
                + '\nCidade: ' + cidadeFabrica.nome
                + '\nData: ' + new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')
                + '\n' + rota, function (err2, info) {
                    if (err2) return err2;

                    console.log(info);
                });
        }
    });
}