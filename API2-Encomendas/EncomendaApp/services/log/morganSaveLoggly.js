const morgan = require('morgan');
const morganJSON = require('morgan-json');
var config = require('../../config');
const MorganLogglyLoggerStream = require('./morganLoggly');
var uniqid = require('uniqid');

// * CUSTOM TOKENS * //
morgan.token('decoded', (req) => {
    return req.decoded.id;
});


morgan.token('protocol', (req) => {
    return req.protocol;
});

morgan.token('host', (req) => {
    return req.headers.host;
});

morgan.token('role', (req) => {
    return req.user ? req.user.role : undefined;
});

morgan.token('idUser', (req) => {
    return req.user ? req.user.id : undefined; 
});

morgan.token('body', (req) => {
    return req.body ? req.body : undefined; 
});

morgan.token('statusMessage', (res) => {
    return res.statusMessage;
});

morgan.token('message', (res) => {
    if (res.statusCode >= 400) {
        return res.res.__morgan_body_response ? res.res.__morgan_body_response.message ? res.res.__morgan_body_response.message : undefined : undefined;
    } else
        return '-';
});

// * CUSTOM FORMAT * //
const format = morganJSON({
    // logid: ':logid',
    date: ':date[iso]',
    http_version: ':http-version',
    method: ':method',
    host: ':host',
    url: ':url',
    idUser: ':idUser',
    role: ':role',
    status: ':status',
    statusMessage: ':statusMessage',
    message: ':message',
    response_time: ':response-time ms'
});

const logglyStream = new MorganLogglyLoggerStream(
    {
        TOKEN: config.winstonToken,
        SUBDOMAIN: config.winstonSubdomain
    }
);

// * Logger loggly * //
module.exports = morgan(format, { stream: logglyStream });