var EditFiles = require('../editfiles.services');
var Encriptar = require('../encriptar');
var uniqid = require('uniqid');
var UserDataRepository = require('../../repository/userdata.repository');
var UserData = require('../../models/userdata');

exports.saveLogFile = async function (line) {
    var ljson = JSON.parse(line);
    // var filename = 'api2log.json';
    var logid = uniqid.time();
    var logdate = ljson.date;

    let lineEncrypted = await Encriptar.encrypt(line).catch(err => { console.log('erro ao encriptar\n' + err) });

    var newLine = {
        logid: logid,
        date: logdate,
        data: lineEncrypted
    }

    if (ljson.idUser != '-') {
        let userdata = new UserData();
        userdata.logid = logid;
        userdata.date = logdate;
        userdata.user = ljson.idUser;

        console.log(userdata);

        await UserDataRepository.SaveUserData(userdata).catch(err => { console.log(err) })
    }

    EditFiles.createOrUpdateFile(newLine).catch(err => { 'erro ao criar/editar ficheiro ' + '\n' + err });
}

//fazer o readlog para administrativo
exports.getLogById = async function (logid) {

    let buffer = await EditFiles.readFile().catch(err => { console.log('erro ao ler ficheiro ' + filename + '\n' + err) });
    var bufferjson = JSON.parse(buffer);
    var logs = bufferjson.table;

    if (logs.length > 0) {
        for (let i = 0; i < logs.length; i++) {
            if (logs[i].logid == logid) {
                let dataDec = await Encriptar.decrypt(logs[i].data);
                dataDec = JSON.parse(dataDec);
                return {
                    index: i,
                    date: logs[i].date,
                    data: dataDec
                }
            }
        }
    } else {
        return 'vazio';
    }
}

//fazer o readlog para administrativo
exports.getAllLogs = async function (logid) {

    var filename = 'api2log.json';
    let buffer = await EditFiles.readFile().catch(err => { console.log('erro ao ler ficheiro ' + filename + '\n' + err) });
    var bufferjson = JSON.parse(buffer);
    var logs = bufferjson.table;

    if (logs.length > 0) {
        return logs;
    } else {
        return 'vazio';
    }
}

exports.deleteLog = async function (logid) {

    let buffer = await EditFiles.readFile().catch(err => { console.log('erro ao ler ficheiro ' + filename + '\n' + err) });
    var bufferjson = JSON.parse(buffer);
    var logs = bufferjson.table;

    if (logs.length > 0) {
        for (let i = 0; i < logs.length; i++) {
            if (logs[i].logid == logid) {
                logs.splice(i,1);
                await EditFiles.deleteByIndex(i);
                try{
                    await UserDataRepository.DeleteUserByLogId(logid).catch(err => {
                        console.log('erro a remover entry no Userdata [delete log action]' + err)
                    });
                }catch(err){
                    console.log('user não existe');
                }
                
            }
        }
    } else {
        return 'vazio';
    }
}
