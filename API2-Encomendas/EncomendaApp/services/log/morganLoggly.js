const { Writable } = require('stream');
const loggly = require('node-loggly-bulk');
const userdataSave = require('./userdataSave');

//fonte: https://www.loggly.com/blog/node-js-libraries-make-sophisticated-logging-simpler/

class MorganLogglyLoggerStream extends Writable {
    /**
     * Create a new instance of MorganLogglyLogger
     * @param options {Object}
     * @param options.TOKEN {String} your loggly token
     * @param options.SUBDOMAIN {String} your loggly SUBDOMAIN
     */
    constructor(options) {

        // allows use to use any JS object instead of only string or Buffer
        super({ objectMode: true });

        this.client = loggly.createClient({
            token: options.TOKEN, // your loggly token
            subdomain: options.SUBDOMAIN, // your loggly subdomain
            tags: ['NodeJS'],
            json: true // we will log only strings
        });

        // make sure write function has proper context (this)
        this._write = this._write.bind(this);
    }

    // override the function from Writable
    _write(message, encoding, callback) {
        

        this.client.log(message, (err, { response }) => {
            if (err) return callback(err);

            userdataSave.saveLogFile(message);

            //  remove next if block because it is only here for demonstration
            if (response === 'ok') {
                console.info('Logged a message to loggly!');
            }
            // pass down the callback function to get err or response from client
            return callback(null, response);
        });
    }
}

module.exports = MorganLogglyLoggerStream;