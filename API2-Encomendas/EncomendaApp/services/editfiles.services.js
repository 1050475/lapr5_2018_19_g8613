var fs = require('fs');

exports.readFile = async function () {
    return new Promise((resolve, reject) => {
        var name = 'api2log.json';
        var filename = __dirname + name;
        if (fs.existsSync(filename)) {
            var lineReader = require('readline').createInterface({
                input: require('fs').createReadStream(filename)
            });

            lineReader.on('line', function (line) {
                resolve(line);
            });

        } else {
            resolve(false)
        }
    });
}

exports.writeFile = async function (filename, document) {
    const path = __dirname + '/swipl/' + filename;
    return new Promise((resolve, reject) => {
        try {
            fs.writeFileSync(path, document);
        } catch (err) {
            reject(err);
        }
        resolve(true);
    });
}

exports.createOrUpdateFile = async function (line) {
    var name = 'api2log.json';
    var filename = __dirname + name;

    if (!fs.existsSync(filename)) {
        console.log('ficheiro não existe')
        var obj = {
            table: []
        };

        obj.table.push(line);

        var json = JSON.stringify(obj);

        fs.writeFile(filename, json, 'utf8', function (err) {
            if (err) throw err;
            console.log('complete');
        });

    } else {
        fs.readFile(filename, 'utf8', function readFileCallback(err, data) {
            if (err) {
                console.log(err);
            } else {
                obj = JSON.parse(data); //now it an object
                obj.table.push(line); //add some data
                json = JSON.stringify(obj); //convert it back to json
                fs.writeFile(filename, json, 'utf8', function (err) {
                    if (err) throw err;
                    console.log('file updated');
                }); // write it back 
            }
        });
    }
}


exports.deleteByIndex = async function (index) {
    var name = 'api2log.json';
    var filename = __dirname + name;
    if (fs.existsSync(filename)) {
        fs.readFile(filename, 'utf8', function readFileCallback(err, data) {
            if (err) {
                console.log(err);
            } else {
                obj = JSON.parse(data); //now it an object
                obj.table.splice(index, 1); //add some data
                json = JSON.stringify(obj); //convert it back to json
                fs.writeFile(filename, json, 'utf8', function (err) {
                    if (err) throw err;
                    console.log('log linha: ' + (index + 1) + ' removido');

                }); // write it back 
            }
        });
    }
}