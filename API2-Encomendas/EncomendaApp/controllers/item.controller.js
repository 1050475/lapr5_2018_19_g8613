const Item = require('../models/item');
const ParItem = require('../models/paritem');
var config = require('../config');
var itemDTO = require('../dtos/itemDTO');
var apiGestorCatalogo = require('../auth/connectAPI');
var uniqid = require('uniqid');

var Client = require('node-rest-client').Client;
var client = new Client();


//Simple version, without validation or sanitation
exports.test = function (req, res) {
    res.send('Greetings from the item controller!');
};

exports.item_all = function (req, res) {
    Item.find(function (err, result) {
        if (err) return next(err);
        res.send(result)
    })
};

exports.item_details = function (req, res) {
    Item.findById(req.params.id, function (err, item) {
        if (err) return next(err);
        res.send(item);
    })
};

exports.item_bycode = function (req, res) {
    Item.findOne({ code: req.params.code }, function (err, item) {
        if (err) return next(err);
        res.send(item);
    });
};

exports.item_dtotest = function (req, res) {
    Item.findById(req.params.id, function (err, item) {
        if (err) return next(err);
        var itemDTO = new itemDTO();
        itemDTO.nome = item.nome;
        itemDTO.altura = item.altura;
        itemDTO.largura = item.largura;
        itemDTO.profundidade = item.profundidade;
        itemDTO.acabamentos = item.acabamentos;
        itemDTO.materiais = item.materiais;

        res.send(item);
    })
};


exports.item_update = async function (req, res) {

    let item_update = new Item();

    item_update.sku = req.body.sku;
    item_update.altura = req.body.altura;
    item_update.largura = req.body.largura;
    item_update.profundidade = req.body.profundidade;
    item_update.material = req.body.material;
    item_update.acabamento = req.body.acabamento;

    let flag = await validItem(item_update);
    console.log(flag);

    if (flag) {
        Item.findOneByIdAndUpdate(req.params.id, { $set: req.body }, function (err, item) {
            if (err) return next(err);
            res.send('Item udpated.');
        });
    } else {
        res.json({ message: 'item inválido seu pateta' });
    }
};

exports.item_delete = async function (req, res) {
    await eliminaPares(req.params.id);

    Item.findByIdAndRemove(req.params.id, function (err) {
        if (err) return next(err);
        res.json({ message: 'Deleted' });
    })
    // res.json({ message: 'Testado' });
};

async function eliminaPares(id) {
    try {
        let itemAux = await Item.findById(id);

        try {
            //todos Pares com item Pai item
            var queryPai = { itemPaiCode: itemAux.code };
            let listaParesPai = await ParItem.find(queryPai);
            console.log(listaParesPai);
            
            if(listaParesPai != undefined && listaParesPai.length > 0 ){
                for(let i = 0; i < listaParesPai.length; i++){
                    try{
                        await ParItem.findByIdAndRemove(listaParesPai[i].id)
                        console.log("paritem eliminado");
                    }catch(err){
                        console.log(err)
                        console.log('db ParItem delete fail')
                    }
                    
                }
            }
            

        } catch (err) {
            console.log(err);
            console.log("db err: query pares em que é pai")
        }

        try {
            //todos Pares com item Filho item
            var queryFilho = { itemFilhoCode: itemAux.code };
            let listaParesFilho = await ParItem.find(queryFilho);
            console.log(listaParesFilho);

            if(listaParesFilho != undefined && listaParesFilho.length > 0 ){
                for(let i = 0; i < listaParesFilho.length; i++){
                    try{
                        await ParItem.findByIdAndRemove(listaParesFilho[i].id)
                        console.log("paritem eliminado");
                    }catch(err){
                        console.log(err)
                        console.log('db ParItem delete fail')
                    }
                    
                }
            }
        } catch (err) {
            console.log(err);
            console.log("db err: query pares em que é filho")
        }
    } catch (error) {
        console.log("db err: não encontrou item or fail")
    }
}

exports.item_create = async function (req, res) {
    let item = new Item();

    item.sku = req.body.sku;
    item.code = uniqid.time();
    item.altura = req.body.altura;
    item.largura = req.body.largura;
    item.profundidade = req.body.profundidade;
    item.material = req.body.material;
    item.acabamento = req.body.acabamento;
    item.ocupacao = 0;

    let flag = await validItem(item);
    console.log(flag);

    if (flag) {
        item.save(function (err) {
            console.log("in save");

            if (err)
                res.json({ message: 'error in save' });

            res.json({ message: 'Criado item com id: ', code: item.code, id : item.id });
        });
    } else {
        res.json({ message: 'item inválido seu pateta' });
    }
};

async function validItem(item) {
    var url = "Produto/sku=" + item.sku;

    let x = await apiGestorCatalogo.getFromApi(url);

    item.nome = x.nome;
    console.log("x.nome: " + x.nome);

    var flagmaterial = false;
    var flagacabamento = false;
    var flagaltura = false;
    var flaglargura = false;
    var flagprofundidade = false;

    console.log("item.material: ", item.material);


    var arraymaterial = x.materiais;
    var arrayacabamentos;
    if (arraymaterial == undefined || arraymaterial == 0) {
        return false;
    }
    for (let i = 0; i < arraymaterial.length; i++) {
        console.log("arraymaterial[i].nome: " + arraymaterial[i].nome);
        if (arraymaterial[i].nome == item.material) {
            flagmaterial = true;
            arrayacabamentos = arraymaterial[i].acabamentos;
            break;
        }
    }

    if (arrayacabamentos == undefined || arrayacabamentos == 0) {
        if (item.acabamento == undefined) {
            flagacabamento = true;
        }
    } else {
        if (arrayacabamentos.length != undefined)
            for (let j = 0; j < arrayacabamentos.length; j++) {
                console.log(item.acabamento);
                if (arrayacabamentos[j].nome == item.acabamento) {
                    flagacabamento = true;
                    break;
                }
            }
    }

    //altura
    var medidasContinuasaltura = x.altura.medidasContinuas;
    for (let w = 0; w < medidasContinuasaltura.length; w++) {
        if (medidasContinuasaltura[w].minimo <= item.altura && item.altura <= medidasContinuasaltura[w].maximo) {
            flagaltura = true;
        }
    }
    var medidasDiscretasaltura = x.altura.medidasDiscretas;
    for (let k = 0; k < medidasDiscretasaltura.length; k++) {
        if (medidasDiscretasaltura[k].valor == item.altura) {
            flagaltura = true;
        }
    }

    //largura
    var medidasContinuaslargura = x.largura.medidasContinuas;
    for (let w = 0; w < medidasContinuaslargura.length; w++) {
        if (medidasContinuaslargura[w].minimo <= item.largura && item.largura <= medidasContinuaslargura[w].maximo) {
            flaglargura = true;
        }
    }
    var medidasDiscretaslargura = x.largura.medidasDiscretas;
    for (let k = 0; k < medidasDiscretaslargura.length; k++) {
        if (medidasDiscretaslargura[k].valor == item.largura) {
            flaglargura = true;
        }
    }

    //profundidade
    var medidasContinuasprof = x.profundidade.medidasContinuas;
    for (let w = 0; w < medidasContinuasprof.length; w++) {
        if (medidasContinuasprof[w].minimo <= item.profundidade && item.profundidade <= medidasContinuasprof[w].maximo) {
            flagprofundidade = true;
        }
    }
    var medidasDiscretasprof = x.profundidade.medidasDiscretas;
    for (let k = 0; k < medidasDiscretasprof.length; k++) {
        if (medidasDiscretasprof[k].valor == item.profundidade) {
            flagprofundidade = true;
        }
    }
    var flag = false;
    if (flagaltura == true
        && flaglargura == true
        && flagprofundidade == true
        && flagmaterial == true
        && flagacabamento == true
    ) {
        flag = true;
    }
    console.log("item válido: " + flag);

    return flag;
}