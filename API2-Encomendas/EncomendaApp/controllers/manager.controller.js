const Cidade = require('../models/cidade');
const Fabrica = require('../models/fabrica');
const ManagerServices = require('../services/manager.services');
const Swipltest = require('../services/swipl/swipl');
const CalculaRota = require('../services/calcularrota.service');

//Simple version, without validation or sanitation
exports.test = function (req, res) {
    Swipltest.convertRecursiveListToArray

    res.send('Greetings from the item controller!');
};



exports.cidade_create = async (req, res) => {
    var success = await ManagerServices.cidade_create(req.body);

    if (success == false) {
        res.status(400).send(success);
    } else if (success == null) {
        res.status(404).send(success);
    } else if (success == 'exist') {
        res.status(404).send(success);
    } else {
        atualizaCidades();
        res.status(201).send(success);
    }
}

exports.cidade_all = async (req, res) => {
    var success = await ManagerServices.cidade_all();

    if (success == false) {
        res.status(400).send(success);
    } else if (success == null) {
        res.status(404).send(success);
    } else if (success == 'exist') {
        res.status(404).send(success);
    } else {
        res.status(201).send(success);
    }
}

exports.fabrica_create = async (req, res) => {
    var success = await ManagerServices.fabrica_create(req.body);

    if (success == false) {
        res.status(400).send(success);
    } else if (success == null) {
        res.status(404).send(success);
    } else if (success == 'exist') {
        res.status(404).send(success);
    } else {
        atualizaFabricas();
        res.status(201).send(success);
    }
}

exports.fabrica_all = async (req, res) => {
    var success = await ManagerServices.fabrica_all();

    if (success == false) {
        res.status(400).send(success);
    } else if (success == null) {
        res.status(404).send(success);
    } else if (success == 'exist') {
        res.status(404).send(success);
    } else {
        res.status(201).send(success);
    }
}

function atualizaFabricas() {
    ManagerServices.atualizaFabricas();
}

function atualizaCidades() {
    ManagerServices.atualizaCidades();
}

exports.log_byId = async (req, res) => {
    var success = await ManagerServices.log_byId(req.query.logid);

    if (success == false) {
        res.status(400).send(success);
    } else if (success == null) {
        res.status(404).send('null');
    } else {
        res.status(201).send(success);
    }
}

exports.deleteLogByID = async (req, res) => {
    var success = await ManagerServices.delete_logById(req.query.logid);

    if (success == false) {
        res.status(400).send(success);
    } else if (success == null) {
        res.status(404).send('null');
    } else {
        res.status(201).send(success);
    }
}



exports.deleteAllLogsByUser = async (req, res) => {
    var success = await ManagerServices.deleteAllLogsByUser(req.user.id);

    if (success == false) {
        res.status(400).send(success);
    } else if (success == null) {
        res.status(404).send('null');
    } else {
        res.status(201).send(success);
    }
}


// atualizaCidades();
// atualizaFabricas();


// var encomendaCidade = '5c2012f88ff55f0e24b023a9';


// CalculaRota.fabricaMaisProxima(encomendaCidade);

// // //TEST SIMULAÇAO
// async function rota() {
//     var rota = await CalculaRota.calculoRota('5c2013488ff55f0e24b023ab',
//         [{
//             cidade: '5c20115a8ff55f0e24b023a6',
//             nome: 'Porto',
//             latitude: '41.15',
//             longitude: '-8.61024'
//         },
//         {
//             cidade: '5c2012988ff55f0e24b023a7',
//             nome: 'Vila do Conde',
//             latitude: '41.35173',
//             longitude: '-8.74786'
//         },
//         {
//             cidade: '5c2012cc8ff55f0e24b023a8',
//             nome: 'Vila Real',
//             latitude: '41.30104',
//             longitude: '-7.74224'
//         },
//         {
//             cidade: '5c2012f88ff55f0e24b023a9',
//             nome: 'Viseu',
//             latitude: '40.65659',
//             longitude: '-7.91247'
//         },
//         {
//             cidade: '5c2013178ff55f0e24b023aa',
//             nome: 'Vigo',
//             latitude: '42.2406',
//             longitude: '-8.72073'
//         },
//         {
//             cidade: '5c2013488ff55f0e24b023ab',
//             nome: 'Corunha',
//             latitude: '43.36234',
//             longitude: '-8.41154'
//         },
//         {
//             cidade: '5c20182b3b41b81144cc50dc',
//             nome: 'Madrid',
//             latitude: '40.41678',
//             longitude: '-3.70379'
//         },
//         {
//             cidade: "5c30b47e22ee522aa44e172f",
//             nome: "Beja",
//             latitude: "38.0154479",
//             longitude: "-7.8650368",
//         },
//         {
//             cidade: "5c30b49b22ee522aa44e1731",
//             nome: "Barcelona",
//             latitude: "41.3828939",
//             longitude: "2.1774322"
//         },
//         {
//             cidade: "5c30b4b322ee522aa44e1733",
//             nome: "Oviedo",
//             latitude: "43.3605977",
//             longitude: "-5.8448989"
//         },
//         {
//             cidade: "5c30b4d222ee522aa44e1737",
//             nome: "Lugo",
//             latitude: "43.0462247",
//             longitude: "-7.4739921"
//         },
//         {
//             cidade: "5c30b4eb22ee522aa44e1739",
//             nome: "Ourense",
//             latitude: "43.0462247",
//             longitude: "-7.8674242"
//         },
//         {
//             cidade: "5c30b50922ee522aa44e173d",
//             nome: "Valladolid",
//             latitude: "41.6521328",
//             longitude: "-4.728562"
//         },
//         {
//             cidade: "5c30b51f22ee522aa44e173f",
//             nome: "Toledo",
//             latitude: "39.8560679",
//             longitude: "-4.0239568"
//         },
//         {
//             cidade: "5c30b54222ee522aa44e1741",
//             nome: "Faro",
//             latitude: "37.0537571",
//             longitude: "-7.931936"
//         },
//         {
//             cidade: "5c30b58722ee522aa44e1743",
//             nome: "Sevilha",
//             latitude: "37.3886303",
//             longitude: "-5.9953171"
//         },
//         {
//             cidade: "5c30b59f22ee522aa44e1745",
//             nome: "Malaga",
//             latitude: "36.7213028",
//             longitude: "-4.4216366"
//         }
//         ]);

//         // console.log(rota.rota);
// }

// rota();


