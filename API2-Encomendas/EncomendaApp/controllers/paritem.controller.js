const ParItem = require('../models/paritem');
const Item = require('../models/item');
var config = require('../config');
var apiGestorCatalogo = require('../auth/connectAPI');

//Simple version, without validation or sanitation
exports.test = function (req, res) {
    res.send('Greetings from the ParItem controller!');
};

exports.paritem_all = function (req, res) {
    ParItem.find(function (err, result) {
        if (err) return next(err);
        res.send(result)
    })
};

exports.paritem_details = function (req, res) {
    ParItem.findById(req.params.id, function (err, paritem) {
        if (err) return next(err);
        res.send(paritem);
    })
};


exports.paritem_delete = function (req, res) {
    ParItem.findByIdAndRemove(req.params.id, function (err) {
        if (err) return next(err);
        res.send('Deleted successfully!');
    })
};

exports.paritem_update = async function (req, res) {

    var paritem = new ParItem();

    paritem.itemPaiCode = req.body.codepai;
    paritem.itemFilhoCode = req.body.codefilho;

    let flag = await validaParITem(paritem);

    if (flag) {
        ParItem.findOneByIdAndUpdate(req.params.id, { $set: req.body }, function (err, paritem) {
            if (err) return next(err);
            res.send('ParItem udpated.');
        });
    } else {
        res.json({ message: 'paritem inválido seu pateta' });
    }
};

exports.paritem_create = async function (req, res) {
    var paritem = new ParItem();

    
    paritem.itemPaiCode = req.body.itemPaiCode;
    paritem.itemFilhoCode = req.body.itemFilhoCode;
 
    let flag = await validaParITem(paritem);

    if (flag) {
        paritem.save(function (err) {
            console.log("in save");

            if (err)
                res.json({ message: 'error in save' });

            res.json({ message: 'Criado ParItem com id:', id: paritem.id });
        });
    } else {

        res.json({ message: 'Agregação inválida seu pateta'});
    }
}

async function validaParITem(paritem) {


    try {

        // let itemPai = await Item.findById(paritem.itemPaiId);
        // let itemFilho = await Item.findById(paritem.itemFilhoId);
        console.log("Aviso: ", paritem.itemPaiCode);
        let itemPai = await Item.findOne({ code: paritem.itemPaiCode });
        let itemFilho = await Item.findOne({ code: paritem.itemFilhoCode });



        //verifica se o itemPai de uma order já tem ocupação esgotada
        if (itemPai.ocupacao == 100) {
            console.log('itemPai está lotado')

            return false;
        }

        //verifica se itemFilho pode pertencer ao itemPai

        let flagPertenceA = await validaSePertenceA(itemPai, itemFilho);

        if (flagPertenceA == false) {
            return false;
        }

        //verifica se filho cabe dentro do pai (inicial)
        if (itemFilho.altura > itemPai.altura && itemFilho.largura > itemPai.largura && itemFilho.profundidade > itemPai.profundidade) {
            console.log('itemFilho não cabe no itemPai');

            return false;
        }

        //verifica se ocupação do filho ultrapassa o espaço existente no pai
        var ocupacao_filho = (100 * (itemFilho.altura * itemFilho.largura * itemFilho.profundidade) / (itemPai.altura * itemPai.largura * itemPai.profundidade));
        var nova_taxaOcupacao = ocupacao_filho + itemPai.ocupacao;

        if (nova_taxaOcupacao > 100) {
            console.log('itemFilho ultrapassará a lotação do itemPai');

            return false;
        }

        //verifica se o itemFilho cabe no itemPai tendo em atençao tanto as dimensoes como as restrições de ocupação
        let flagCaber = await validaAgregacao(itemPai, itemFilho, nova_taxaOcupacao);

        if (flagCaber == false) {
            return false;
        }

        itemPai.ocupacao = nova_taxaOcupacao;

        try {
            await itemPai.save();
            console.log('itemPai.ocupacao updated ' + itemPai.ocupacao);
        } catch (err) {
            console.log('error: cant save itemPai updating ocupacao');
            return false;
        }

        return true;
    } catch (error) {
        console.log('ids invalid or database offline');
        return false;
    }

}

async function validaSePertenceA(itemPai, itemFilho) {
    console.log('isto é ' + itemPai.sku);

    var url_obg = "Produto/sku=" + itemPai.sku + "/Partes" + "/obrigatorio=" + "true";
    var url_opc = "Produto/sku=" + itemPai.sku + "/Partes" + "/obrigatorio=" + "false";
    var flag = false;

    try {
        let listaPartesObrigatorias = await apiGestorCatalogo.getFromApi(url_obg);
        console.log(listaPartesObrigatorias);

        for (let i = 0; i < listaPartesObrigatorias.length; i++) {
            if (listaPartesObrigatorias[i].sku == itemFilho.sku) {
                console.log('Produto sku: ' + itemPai.sku + 'tem parte sku: ' + itemFilho.sku);
                return true;
            }
        }
    } catch (error) {
        console.log('no parts available as mandatory or GestorCatalogoAPI offline');
        return false;
    }

    try {
        let listaPartesOpcionais = await apiGestorCatalogo.getFromApi(url_opc);
        console.log(listaPartesOpcionais);

        for (let i = 0; i < listaPartesOpcionais.length; i++) {
            if (listaPartesOpcionais[i].sku === itemFilho.sku) {
                console.log('Produto sku: ' + itemPai.sku + 'tem parte sku: ' + itemFilho.sku);
                return true;
            }
        }
    } catch (error) {
        console.log('no parts available as optional or GestorCatalogoAPI offline');
        return false;
    }

    return flag;

}



async function validaAgregacao(itemPai, itemFilho, nova_taxaOcupacao) {

    var url_prd = "Produto/sku=" + itemPai.sku;

    try {
        let produtoPai = await apiGestorCatalogo.getFromApi(url_prd);
        // console.log(produtoPai);
        // console.log(produtoPai.listaRestricaoMaterial);
        // console.log(produtoPai.listaRestricaoMaterial[0].material.acabamentos);

        //verifica se itemFilho cumpre restrições de ocupação
        if (produtoPai.listaRestricaoDimensao != undefined && produtoPai.listaRestricaoDimensao.length > 0) {
            for (let i = 0; i < produtoPai.listaRestricaoDimensao.length; i++) {
                if (produtoPai.listaRestricaoDimensao[i].minPercentagemOcupacao > nova_taxaOcupacao || produtoPai.listaRestricaoDimensao[i].maxPercentagemOcupacao < nova_taxaOcupacao) {
                    console.log('itemFilho provoca uma ocupação fora dos limites de restrição ocupação');
                    return false;
                }
            }
        }

        let flagMaterial = false;
        let flagAcabamento = false;
        //verifica se os materiais do itemFilho respeita as restrições do ProdutoPai
        if (produtoPai.listaRestricaoMaterial.length > 0) {
            for (let i = 0; i < produtoPai.listaRestricaoMaterial.length; i++) {

                //se o itemFilho respeita restrição material
                if (produtoPai.listaRestricaoMaterial[i].material.nome == itemFilho.material) {
                    console.log('itemFilho respeita a restrição de material');
                    flagMaterial = true;

                    if (produtoPai.listaRestricaoMaterial[i].material.acabamentos.length > 0) {
                        for (let j = 0; j < produtoPai.listaRestricaoMaterial[i].material.acabamentos.length; j++) {

                            //se itemFilho respeita restrição acabamento
                            if (produtoPai.listaRestricaoMaterial[i].material.acabamentos[j].nome == itemFilho.acabamento) {
                                flagAcabamento = true;
                                break;
                            }
                        }
                    } else {
                        flagAcabamento = true
                    }

                    break;
                }
            }
        } else {
            return true;
        }

        console.log('Restrições: Material Filho: ' + flagMaterial + ' Acabamento Filho: ' + flagAcabamento);

        if (flagMaterial && flagAcabamento) {
            return true;
        } else {
            return false;
        }

    } catch (error) {
        console.log('produto Pai não existe ou Gestor Catalogo Api offline');
        return false;
    }

}