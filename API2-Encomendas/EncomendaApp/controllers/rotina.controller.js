var rotina = require('../services/rotina.service');
var CronJob = require('cron').CronJob;
var config = require('../config');

//Simple version, without validation or sanitation
exports.test = function (req, res) {
    res.send('Greetings from the encomenda controller!');
};

console.log('rotina de proteção de dados ON');


//verificaçao de licenciamento de utilização de dados
// rotina.verificacaoConsentimento();
//verificaçao de conservaçao de dados de utilizador durante 5 anos
//conservação de dados


var tarefaVerificacaoConsentimento = new CronJob({
    
    cronTime: config.diaVerificacaoRGPD,
    onTick: function () {
        console.log('\nrotina programada verificação expiração de consentimento e aceitação dos termos\n');
        
        rotina.verificacaoConsentimento();
    },
    timeZone: ''

});

tarefaVerificacaoConsentimento.start();



// //TESTE ROTINA CALCULO ROTAS E NOTIFICAÇAO
// rotina.rotinaCalculoRota();

console.log('rotina de rotas ON');
var tarefaRotas = new CronJob({
    
    cronTime: config.diaCalculoRotas,
    onTick: function () {
        console.log('\nrotina programada calculo de rotas de encomendas prontas a expedir acionada\n');
        //rotina calculo rotas por fabrica e envial email ao administrativo
        rotina.rotinaCalculoRota();
    },
    timeZone: ''

});

tarefaRotas.start();