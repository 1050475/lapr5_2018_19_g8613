const Encomenda = require('../models/encomenda');
const EncomendaService = require('../services/encomenda.service')
const EncomendaRepository = require('../repository/encomenda.repository');


//Simple version, without validation or sanitation
exports.test = function (req, res) {
    console.log(req.user);


    res.send('Greetings from the encomenda controller!');
};


exports.encomenda_create = async (req, res) => {
    var success = await EncomendaService.encomenda_create(req);

    if (success == false) {
        res.status(400).send(success);
    } else if (success == null) {
        res.status(404).send('null');
    } else {
        res.status(201).send(success);
    }
}

exports.encomenda_details = function (req, res) {
    Encomenda.findById(req.params.id, function (err, encomenda) {
        if (err) return err;

        if(req.user == encomenda.user){
            res.send(encomenda);
        }else{
            res.status(403).send({message: 'no permission. access violation, this isnt your order'})
        }
        
    })
};


exports.encomenda_all = async (req, res) => {
    var success = await EncomendaService.encomenda_all(req);

    if (success == false) {
        res.status(400).send(USER_REGISTER_ERROR);
    } else if (success == null) {
        res.status(404).send(ERROR);
    } else if (success == 'exist') {
        res.status(404).send(USER_REGISTER_EXISTS);
    } else {
        res.status(201).send(success);
    }
}

exports.encomenda_update = function (req, res) {
    Encomenda.findByIdAndUpdate(req.params.id, { $set: req.body }, function (err, product) {
        if (err) return next(err);
        res.send('Encomenda udpated.');
    });
};

exports.encomenda_delete = function (req, res) {
    Encomenda.findByIdAndRemove(req.params.id, function (err) {
        if (err) return next(err);
        res.send('Deleted successfully!');
    })
};

exports.encomenda_em_produção = function (req, res) {
    Encomenda.arguments.estado = 3;
};

exports.encomenda_embalamento = function (req, res) {
    Encomenda.arguments.estado = 4;
};

exports.encomenda_pronta_a_expedir = function (req, res) {
    Encomenda.arguments.estado = 5;
};

exports.encomenda_expedida = function (req, res) {
    Encomenda.arguments.estado = 6;
};

exports.encomenda_entregue = function (req, res) {
    Encomenda.arguments.estado = 7;
};

exports.encomenda_recebida = function (req, res) {
    Encomenda.arguments.estado = 8;
};