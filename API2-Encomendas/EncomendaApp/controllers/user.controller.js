const config = require('../config');
var alerta = require('../services/alerta');


const User = require('../models/user');
const UserService = require('../services/user.service');

var jwt = require('jsonwebtoken');
//var VerifyToken = require('../auth/VerifyToken');

const bcrypt = require('bcryptjs');

//Simple version, without validation or sanitation
exports.test = function (req, res) {
    res.send('Greetings from the Test controller!');
};

exports.testEmail = function (req, res) {
    alerta.emailUser(req.body.email, req.body.subject, req.body.text);

    res.send('Greetings from the Test Email controller!');
};

const USER_REGISTER_ERROR = 'Utilizador não registado';
const USER_REGISTER_EXISTS = 'Utilizador já existe'

exports.user_create = async (req, res) => {
    var success = await UserService.user_register(req.body);

    if (success == false) {
        res.status(400).send(USER_REGISTER_ERROR);
    } else if (success == null) {
        res.status(404).send(ERROR);
    } else if (success == 'exist') {
        res.status(404).send(USER_REGISTER_EXISTS);
    } else {
        res.status(201).send(success);
    }
}


// router.post('/autenticate', user_controller.user_authentication);

exports.user_authentication = async (req, res) => {
    var success = await UserService.user_authentication(req.body);

    if (success == false) {
        res.status(400).send(USER_REGISTER_ERROR);
    } else if (success == 'exist') {
        res.status(404).send(USER_REGISTER_EXISTS);
    } else {
        res.status(201).send(success);
    }
}
// router.post('/login', user_controller.user_login);
exports.user_login = async (req, res) => {
    var success = await UserService.user_login(req.body);

    if (success == false) {
        res.status(400).send(USER_REGISTER_ERROR);
    } else if (success == 'exist') {
        res.status(404).send(USER_REGISTER_EXISTS);
    } else {
        res.status(201).send(success);
    }
}


exports.user_emailverification = async (req, res) => {
    var success = await UserService.user_emailverification(req);

    if (success == false) {
        res.status(400).send(USER_REGISTER_ERROR);
    } else if (success == 'exist') {
        res.status(404).send(USER_REGISTER_EXISTS);
    } else {
        res.status(201).send(success);
    }
}



exports.user_portability = async (req, res) => {
    var success = await UserService.user_portability(req);

    if (success == false) {
        res.status(400).send(USER_REGISTER_ERROR);
    } else if (success == null) {
        res.status(404).send(ERROR);
    } else if (success == 'exist') {
        res.status(404).send(USER_REGISTER_EXISTS);
    } else {
        res.status(201).send(success);
    }
}

exports.user_tokenverifiedwithsuccess = async (req, res) => {
    var success = await UserService.user_tokenverifiedwithsuccess(req);

    res.status(201).send(success);
}

exports.user_renovacaotermos = async (req, res) => {
    var success = await UserService.user_renovacaotermos(req);

    if (success == false) {
        res.status(400).send(USER_REGISTER_ERROR);
    } else if (success == null) {
        res.status(404).send(ERROR);
    } else if (success == 'exist') {
        res.status(404).send(USER_REGISTER_EXISTS);
    } else {
        res.status(201).send(success);
    }
}