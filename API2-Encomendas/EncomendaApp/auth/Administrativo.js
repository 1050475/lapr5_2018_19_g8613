
function administrativo(req, res, next) {
    var role = req.user.role;
    if (role == 'administrativo') {
        next();
    }else{
        return res.status(401).json({ success: false, message: 'Unauthorized access.' });
    }
}

module.exports = administrativo;