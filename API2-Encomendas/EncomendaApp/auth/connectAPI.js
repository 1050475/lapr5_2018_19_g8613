let config = require('../config');
var Client = require('node-rest-client').Client;
var client = new Client();
var token = '';

function register() {
    var url = config.urlApiGestorCatalogo + 'Account';
    var argumentos = {
        data: {
            email: config.UserApiGestorCatalogo,
            password: config.PasswordApiGestorCatalogo
        },
        headers: {
            "Content-Type": "application/json"
        }
    };
    client.post(url, argumentos, function (data, response) {

    });
}

async function getToken(callback) {
    var url = config.urlApiGestorCatalogo + 'Account/Token'
    var args = {
        data: {
            email: config.UserApiGestorCatalogo,
            password: config.PasswordApiGestorCatalogo
        },
        headers: {
            "Content-Type": "application/json"
        }
    };

    client.post(url, args, function (data, response) {
        token = data.token;
        callback();
    });
}

async function getData(URL, callback) {

     await getToken(async function () {
        var headers = {
            "Content-Type": "application-json",
            "Accept-Version": "2014-06",
            "Authorization": "Bearer " + token
        }

        var httpArgs = {
            "headers": headers
        }

        client.get(URL, httpArgs, function (data, response) {
            return callback(data);
        });
    });
}

async function getFromApi(uri) {
    return new Promise((resolve, reject) => {
        //var url = config.urlApiGestorCatalogo + 'Produto/sku=' + sku;
        var url = config.urlApiGestorCatalogo + uri;
        var token = getData(url, async function (data) {
            resolve(data);
            //JSON.stringify(data));
        });
    });
}

exports.getData = getData;
exports.getToken = getToken;
exports.register = register;
exports.getFromApi = getFromApi;