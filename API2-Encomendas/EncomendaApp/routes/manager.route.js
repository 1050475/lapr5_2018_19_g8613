const express = require('express');
const router = express.Router();
const VerifyToken = require('../auth/VerifyToken');
const Cliente = require('../auth/Cliente');
const Administrativo = require('../auth/Administrativo');

const manager_controller = require('../controllers/manager.controller');

router.get('/test',  VerifyToken, Administrativo, manager_controller.test);
router.post('/cidade/create', VerifyToken, Administrativo, manager_controller.cidade_create);
router.get('/cidade', manager_controller.cidade_all);
router.post('/fabrica/create', VerifyToken, Administrativo, manager_controller.fabrica_create);
router.get('/fabrica', VerifyToken, Administrativo, manager_controller.fabrica_all);
router.get('/log', VerifyToken,Administrativo,manager_controller.log_byId);
router.get('/log/delete', VerifyToken,Administrativo,manager_controller.deleteLogByID);
router.get('/log/all/delete', VerifyToken,Administrativo,manager_controller.deleteAllLogsByUser);

module.exports = router;