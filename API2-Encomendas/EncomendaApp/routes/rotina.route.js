const express = require('express');
const router = express.Router({ mergeParams: true });
var VerifyToken = require('../auth/VerifyToken');

const rotina_controller = require('../controllers/rotina.controller');

router.get('/test', rotina_controller.test);

module.exports = router;