const express = require('express');
const VerifyToken = require('../auth/VerifyToken');
const GestorCatalogo = require('../auth/GestorCatalogo');

const router = express.Router();

// Require the controllers WHICH WE DID NOT CREATE YET!!
const user_controller = require('../controllers/user.controller');


// a simple test url to check that all of our files are communicating correctly.
router.get('/test', user_controller.test);
//em reconstrução
router.post('/register', user_controller.user_create);
router.post('/authenticate', user_controller.user_authentication);
router.post('/login', user_controller.user_login);
router.get('/verify', user_controller.user_emailverification);

router.get('/userdata', VerifyToken, user_controller.user_portability);
router.get('/verifytoken', VerifyToken, GestorCatalogo, user_controller.user_tokenverifiedwithsuccess);
router.post('/renovacaotermos', VerifyToken, user_controller.user_renovacaotermos);

module.exports = router;