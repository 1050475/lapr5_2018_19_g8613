const express = require('express');
const router = express.Router();
var VerifyToken = require('../auth/VerifyToken');

const paritem_controller = require('../controllers/paritem.controller');

// a simple test url to check that all of our files are communicating correctly.
router.get('/test', /*VerifyToken,*/ paritem_controller.test);
router.get('/', /*VerifyToken,*/ paritem_controller.paritem_all);
router.get('/:id', /*VerifyToken,*/paritem_controller.paritem_details);
router.post('/create', /*VerifyToken,*/ paritem_controller.paritem_create);
router.delete('/:id/delete', /*VerifyToken,*/ paritem_controller.paritem_delete);
router.put('/:id/update', /*VerifyToken,*/ paritem_controller.paritem_update);

module.exports = router;