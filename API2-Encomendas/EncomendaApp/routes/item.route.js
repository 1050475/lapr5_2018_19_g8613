const express = require('express');
const router = express.Router({ mergeParams: true });
var VerifyToken = require('../auth/VerifyToken');

const item_controller = require('../controllers/item.controller');


// a simple test url to check that all of our files are communicating correctly.
router.get('/test',  /*VerifyToken,*/item_controller.test);
router.get('/', /*VerifyToken,*/item_controller.item_all);
router.post('/create', /*VerifyToken,*/item_controller.item_create);
router.get('/:id', /*VerifyToken,*/item_controller.item_details);
router.get('/code/:code', /*VerifyToken,*/item_controller.item_bycode);
router.get('/:id/dtotest', /*VerifyToken,*/item_controller.item_dtotest);
router.put('/:id/update', /*VerifyToken,*/item_controller.item_update);
router.delete('/:id/delete', /*VerifyToken,*/item_controller.item_delete);


//deve apresentar todos os produtos "armario" que estão disponíveis no catalogo
//router.get('/item/armarios', item_controller.catalogo_armarios);

module.exports = router;