const express = require('express');
const router = express.Router();
const VerifyToken = require('../auth/VerifyToken');
const Cliente = require('../auth/Cliente');
const Administrativo = require('../auth/Administrativo');

const encomenda_controller = require('../controllers/encomenda.controller');


// a simple test url to check that all of our files are communicating correctly.
router.get('/test', VerifyToken, Cliente, encomenda_controller.test);
router.post('/create', VerifyToken, encomenda_controller.encomenda_create);
/*
router.get('/:id/itempai', encomenda_controller.encomenda_itempai);
router.get('/:id/itens', encomenda_controller.encomenda_itens);
router.get('/:id/itens/:iditem', encomenda_controller.encomenda_itensid);
*/
router.get('/:id', VerifyToken, encomenda_controller.encomenda_details);
router.put('/:id/update', VerifyToken, encomenda_controller.encomenda_update);
router.get('/', VerifyToken, encomenda_controller.encomenda_all);

//add itempai                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
router.put('/:id/itempai');

//change states
router.put('/:id/producao', VerifyToken, Administrativo, encomenda_controller.encomenda_em_produção);
router.put('/:id/embalamento', VerifyToken, Administrativo, encomenda_controller.encomenda_embalamento);
router.put('/:id/expedir', VerifyToken, Administrativo, encomenda_controller.encomenda_pronta_a_expedir);
router.put('/:id/expedida', VerifyToken, Administrativo, encomenda_controller.encomenda_expedida);
router.put('/:id/entregue', VerifyToken, Administrativo, encomenda_controller.encomenda_entregue);
router.put('/:id/recebida', VerifyToken, Administrativo, encomenda_controller.encomenda_recebida);


router.delete('/:id/delete', VerifyToken, Administrativo, encomenda_controller.encomenda_delete);

module.exports = router; 