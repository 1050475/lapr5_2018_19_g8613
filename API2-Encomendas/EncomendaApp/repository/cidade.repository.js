const Cidade = require('../models/cidade');


exports.GetById = async function (id) {
    return new Promise((resolve, reject) => {
        Cidade.findById(id, function (err, cidade) {
            if (err) reject(err);
            resolve(cidade);
        });
    });
}

exports.AllCidades = async function () {
    return new Promise((resolve, reject) => {
        Cidade.find({}, function (err, result) {
            if (err) reject(err);
            console.log(result)
            resolve(result);
        });
    });
}

exports.GetCidadeByName = async function (nome) {
    return new Promise((resolve, reject) => {
        Cidade.findOne({ nome: nome }, function (err, cidade) {
            if (err) reject(err);
            resolve(cidade);
        });
    });
}

exports.SaveCidade = async function (cidade) {
    return new Promise((resolve, reject) => {
        cidade.save(function (err) {
            if (err) reject(err)
            else resolve(cidade);
        });
    });
}