const User = require('../models/user');
const UserDTO = require('../dtos/userDTO');

exports.GetById = async function (id) {
    return new Promise((resolve, reject) => {
        User.findById(id, function (err, user) {
            if (err) reject(err);
            resolve(user);
        });
    });
}

exports.GetUserByEmail = async function (useremail) {
    return new Promise((resolve, reject) => {
        User.findOne({ email: useremail }, function (err, user) {
            if (err) reject(err);
            resolve(user);
        });
    });
}

exports.GetUsersByRole = async function (role) {
    return new Promise((resolve, reject) => {
        User.find({ role: role }, function (err, users) {
            if (err) reject(err);
            resolve(users);
        });
    });
}

exports.SaveUser = async function (user) {
    return new Promise((resolve, reject) => {
        user.save(function (err) {
            if (err) reject(err)
            else resolve(new UserDTO(user));
        });
    });
}

exports.UpdateUser = async function (user) {
    return new Promise((resolve, reject) => {
        console.log(user)
        User.findByIdAndUpdate(user.id, { $set: user }, function (err, result) {
            if (err) {
                reject(err)
            }
            resolve('Done')
        });
    });

}

exports.AllUsers = async function () {
    return new Promise((resolve, reject) => {
        User.find({}, function (err, result) {
            if (err) reject(err);

            resolve(result);
        });
    });
}
var uniqid = require('uniqid');
exports.ToAnonimoUser = async function (user) {
    
    var random = uniqid.time();

    user.email = random+'@mail.pt';
    //user.cidade = undefined;
    user.nome = random;
    user.aceitacaoTermos = false;
    user.role = 'anonimo';
    return new Promise((resolve, reject) => {
        user.save(function (err) {
            if (err) reject(err)
            else resolve(new UserDTO(user));
        });
    });

};


// exports.DeleteUser = function (id) {
//     return new Promise((resolve, reject) => {
//         User.findByIdAndDelete(id, function (err, user) {
//             if (err) reject(err);
//             resolve(user);
//         });
//     });
// }