const Fabrica = require('../models/fabrica');


exports.GetById = async function (id) {
    return new Promise((resolve, reject) => {
        Fabrica.findById(id, function (err, fabrica) {
            if (err) reject(err);
            resolve(fabrica);
        });
    });
}

exports.AllFabricas = async function () {
    return new Promise((resolve, reject) => {
        Fabrica.find({},function (err, result) {
            if (err) reject(err);
            console.log(result)
            resolve(result);
        });
    });
}

exports.GetFabricaByName = async function (nome) {
    return new Promise((resolve, reject) => {
        Fabrica.findOne({ nome: nome }, function (err, fabrica) {
            if (err) reject(err);
            resolve(fabrica);
        });
    });
}

exports.GetFabricaByCidade = async function (cidade) {
    return new Promise((resolve, reject) => {
        Fabrica.findOne({ cidade: cidade }, function (err, fabrica) {
            if (err) reject(err);
            resolve(fabrica);
        });
    });
}

exports.SaveFabrica = async function (fabrica) {
    return new Promise((resolve, reject) => {
        fabrica.save(function (err) {
            if (err) reject(err)
            else resolve(fabrica);
        });
    });
}
