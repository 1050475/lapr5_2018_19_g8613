const Encomenda = require('../models/encomenda');

exports.GetEncomendaByEstado = async function(estadodesejado){
    await Encomenda.find({ estado: estadodesejado });
}


//procura encomendas de uma fabrica com um estado desejado
exports.GetEncomendaByEstadoAndByFabrica = async function(fabricaID, estadodesejado){
    return new Promise((resolve, reject) => {
        Encomenda.find({ fabrica: fabricaID, estado: estadodesejado }, function (err, listaEncomendas) {
            if (err) reject(err);
            resolve(listaEncomendas);
        });
    });
}

exports.SaveEncomenda = async function (encomenda) {
    return new Promise((resolve, reject) => {
        encomenda.save(function (err) {
            if (err) reject(err)
            else resolve(encomenda);
        });
    });
}

exports.getAllByUser = async function(userID){
    return new Promise((resolve, reject) => {
        Encomenda.find({ user: userID }, function (err, listaEncomendas) {
            if (err) reject(err);
            resolve(listaEncomendas);
        });
    });
}

exports.getAllEncomendas = async function(){
    return new Promise((resolve, reject) => {
        Encomenda.find({ }, function (err, listaEncomendas) {
            if (err) reject(err);
            resolve(listaEncomendas);
        });
    });
}