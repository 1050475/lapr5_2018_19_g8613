const ParItem = require('../models/paritem');
const Item = require('../models/item');

//todos os paritens com itemPai.code = itemPaiCode
exports.ParItemByItemPaiCode = async function (itemPaiCode){
    var queryPai = { itemPaiCode: itemPaiCode };
    return await ParItem.find(queryPai);
}

//todos os paritens com itemFilho.code = itemFilho Code
exports.ParItemByItemFilhoCode = async function (itemFilhoCode){
    var queryFilho = { itemFilhoCode: itemFilhoCode };
    return await ParItem.find(queryFilho);
}