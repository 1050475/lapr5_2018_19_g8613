const UserData = require('../models/userdata');

// logid: { type: String },
// user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
// date: { type: Date }

exports.GetById = async function (id) {
    return new Promise((resolve, reject) => {
        UserData.findById(id, function (err, userdata) {
            if (err) reject(err);
            resolve(userdata);
        });
    });
}

exports.GetUserDataByUser = async function (user) {
    return new Promise((resolve, reject) => {
        UserData.find({ user: user }, function (err, userdata) {
            if (err) reject(err);
            resolve(userdata);
        });
    });
}

exports.SaveUserData = async function (userdata) {
    return new Promise((resolve, reject) => {
        userdata.save(function (err) {
            if (err) reject(err)
            else resolve(userdata);
        });
    });
}

exports.DeleteUserByLogId = function (logid) {
    return new Promise((resolve, reject) => {
        UserData.findOne({ logid: logid }, function (err, userdata) {
            if (err) reject(err);
            
            if(!userdata){
                resolve(false);
            }else{
                UserData.findByIdAndDelete(userdata.id, function (err, result) {
                    if (err) reject(err);
                    resolve(result);
                });
            }
        });
    });
}