using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace HttpService
{
    public class ItemContext : DbContext
    {
        public ItemContext(DbContextOptions<ItemContext> options)
            : base(options)
        { }

        public DbSet<Item> ItemRepo { get; set; }
    }

    public class Item
    {
        public int ItemId { get; set; }
        public string sku { get; set; }

        public string nome { get; set; }

        public double altura { get; set; }

        public double largura { get; set; }
        public double profundidade { get; set; }
        public string material { get; set; }

        public string acabamento { get; set; }
        
        public double posicao { get; set; }
        public bool aberto { get; set; }
        public List<Item> itemFilhos { get; set; }
    }
}