using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HttpService;
using Newtonsoft.Json;

namespace HttpService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ItemController : ControllerBase
    {
        private readonly ItemContext _context;

        public ItemController(ItemContext context)
        {
            _context = context;
        }

        // GET: api/Item
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Item>>> GetItemRepo()
        {
            return await _context.ItemRepo.Include(i => i.itemFilhos).ToListAsync();
        }

        // GET: api/Item/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Item>> GetItem(int id)
        {
            var item = await _context.ItemRepo.FindAsync(id);

            if (item == null)
            {
                return NotFound();
            }

            return item;
        }

        // PUT: api/Item/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutItem(int id, Item item)
        {
            if (id != item.ItemId)
            {
                return BadRequest();
            }

            _context.Entry(item).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Item
        [HttpPost]
        public IActionResult PostItem(Item item)
        {
            string json = JsonConvert.SerializeObject(item);

            Console.Write("----------" + json + "----------\n\n");
            //_context.ItemRepo.Add(item);
            //await _context.SaveChangesAsync();


            using(System.IO.StreamWriter writetext = new System.IO.StreamWriter("../armario.itn"))
            {
                writetext.WriteLine(json);
            }

            if(!Utils.ProgramExec.IsProcessRunning("prog")){
                Utils.ProgramExec.LaunchCommandLineApp();
            }

            //return NoContent();
            return CreatedAtAction("GetItem", new { id = item.ItemId }, item);
        }

        // DELETE: api/Item/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Item>> DeleteItem(int id)
        {
            var item = await _context.ItemRepo.FindAsync(id);
            if (item == null)
            {
                return NotFound();
            }

            _context.ItemRepo.Remove(item);
            await _context.SaveChangesAsync();

            return item;
        }

        private bool ItemExists(int id)
        {
            return _context.ItemRepo.Any(e => e.ItemId == id);
        }
    }
}
