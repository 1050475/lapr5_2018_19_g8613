﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HttpService.Migrations
{
    public partial class InitialCreate2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "aberto",
                table: "ItemRepo",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<double>(
                name: "posicao",
                table: "ItemRepo",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "aberto",
                table: "ItemRepo");

            migrationBuilder.DropColumn(
                name: "posicao",
                table: "ItemRepo");
        }
    }
}
