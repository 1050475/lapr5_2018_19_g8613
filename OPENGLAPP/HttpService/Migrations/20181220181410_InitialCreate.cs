﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HttpService.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ItemRepo",
                columns: table => new
                {
                    ItemId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    sku = table.Column<string>(nullable: true),
                    altura = table.Column<double>(nullable: false),
                    largura = table.Column<double>(nullable: false),
                    profundidade = table.Column<double>(nullable: false),
                    material = table.Column<string>(nullable: true),
                    acabamento = table.Column<string>(nullable: true),
                    ItemId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemRepo", x => x.ItemId);
                    table.ForeignKey(
                        name: "FK_ItemRepo_ItemRepo_ItemId1",
                        column: x => x.ItemId1,
                        principalTable: "ItemRepo",
                        principalColumn: "ItemId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ItemRepo_ItemId1",
                table: "ItemRepo",
                column: "ItemId1");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ItemRepo");
        }
    }
}
