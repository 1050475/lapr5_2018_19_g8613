using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.EntityFrameworkCore;
using HttpService;


namespace HttpService.Utils
{
class ProgramExec
{

    /// <summary>
    /// Launch the application with some options set.
    /// </summary>
    public static void LaunchCommandLineApp()
    {
        // For the example
        //const string ex1 = "C:\\";
        //const string ex2 = "C:\\Dir";

        // Use ProcessStartInfo class
        ProcessStartInfo startInfo = new ProcessStartInfo();
        startInfo.CreateNoWindow = false;
        startInfo.UseShellExecute = false;
        startInfo.FileName = "../OpenGLApp/prog.exe";
        startInfo.WorkingDirectory = System.IO.Path.GetDirectoryName("../OpenGLApp/prog.exe");
        //startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        //startInfo.Arguments = "-f j -o \"" + ex1 + "\" -z 1.0 -s y " + ex2;
        try
        {
            // Start the process with the info we specified.
            // Call WaitForExit and then the using statement will close.
           // using (Process exeProcess = Process.Start(startInfo))
            //{
             //   exeProcess.WaitForExit();
            //}
            Process.Start(startInfo);

        }
        catch
        {
            Console.WriteLine("Error: Program could not be executed.\n");
        }
        
    }

    public static bool IsProcessRunning(string sProcessName)
  {
      System.Diagnostics.Process[] proc = System.Diagnostics.Process.GetProcessesByName(sProcessName);
      if (proc.Length > 0)
      {
         Console.Write("RUNNING!!!\n");
         return true;
      }
      
          Console.Write("NOT RUNNING!!!\n");
          return false;
       
 }
}
}