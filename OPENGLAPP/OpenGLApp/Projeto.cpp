#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <iostream>
#include <list>
#include <fstream>
#include <streambuf>
#include <string>
#include "nlohmann/json.hpp"




#ifdef __APPLE__  
#include <GLUT/glut.h>
#else 
#include <GL/glut.h>
#endif 

#ifdef _WIN32
#include "msvc_warnings.h" 
#endif 

#include "SOIL.h" 
// Simple OpenGL Image Library --> https://www.lonesock.net/soil.html

#include "mathlib.h"
#include "studio.h"
#include "mdlviewer.h"

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795
#endif

#define RAD(x)          (M_PI*(x)/180)
#define GRAUS(x)        (180*(x)/M_PI)

#define DEBUG               1

#define DELAY_MOVIMENTO     20

#define NOME_TEXTURA_CHAO         "Chao.jpg"
#define NOME_TEXTURA_MADEIRA      "Madeira.png"
#define NOME_TEXTURA_METAL        "Metal.png"
#define NOME_TEXTURA_PVC          "PVC.png"

#define NUM_TEXTURAS              4
#define ID_TEXTURA_CHAO           0
#define ID_TEXTURA_MADEIRA        1
#define ID_TEXTURA_METAL		  2
#define ID_TEXTURA_PVC   		  3
#define ID_ACABAMENTO_MATE        0
#define ID_ACABAMENTO_BRILHANTE   1

#define GROSSURA_DEFAULT			0.2

#define MAX_ELEMENTS			100

/* VARIAVEIS GLOBAIS */

typedef struct {
	GLboolean   q, a, z, x, up, down, left, right;
}Teclas;

typedef struct {
	GLfloat    x, y, z;
}Pos;

typedef struct {
	Pos      eye, center, up;
	GLfloat  fov;
	GLfloat x = 0;
	GLfloat y = 0;
	GLfloat z = 0;
	GLfloat RotateY = 0;

	GLfloat dist_center;
	GLfloat ang_teta;
	GLfloat ang_fi;

}Camera;

typedef struct {
	GLboolean   doubleBuffer;
	GLint       delayMovimento;
	Teclas      teclas;
	GLuint      menu_id;
	GLboolean   menuActivo;
	Camera      camera;
	GLboolean   debug;
	GLenum		mode;
	GLint		readFile;
}Estado;

typedef struct {
	GLfloat       x, y, z;
}Light;

typedef struct {
	GLfloat altura_prateleira;
	GLint material;
	GLint acabamento;
} Prateleira;

typedef struct {
	GLfloat altura_gaveta;
	GLfloat posicao_gaveta;
	GLint material;
	GLint acabamento;
	GLboolean gaveta_aberta;
} Gaveta;

typedef struct {
	GLfloat		altura_suporte;
}Suporte;

typedef struct {
	GLfloat     grossura, altura, largura, profundidade;
	GLint		material;
	GLint		acabamento;
	GLint		num_prateleiras;
	GLint		num_gavetas;
	GLint		num_suportes;
	Prateleira	prateleira[MAX_ELEMENTS];
	Gaveta		gaveta[MAX_ELEMENTS];
	Suporte		suporte[MAX_ELEMENTS];
}Armario;

typedef struct {
	Armario     armario[MAX_ELEMENTS];
	GLint		num_armarios;
	GLfloat		angulo_portas;
	GLint		material_portas;
	GLint		acabamento_portas;
	GLboolean	portas_abertas;
	Light		light1;
	GLuint      texID[NUM_TEXTURAS];
	GLboolean   parado;
}Modelo;

Estado estado;
Modelo modelo;

using namespace std;
using json = nlohmann::json;

GLint getMaterialID(string material) {
	if (material.compare("\"PVC\"") == 0) {
		return ID_TEXTURA_PVC;
	}
	if (material.compare("\"Metal\"") == 0) {
		return ID_TEXTURA_METAL;
	}

	return ID_TEXTURA_MADEIRA;
}

GLint getAcabamentoID(string material) {
	if (material.compare("\"Brilhante\"") == 0) {
		return ID_ACABAMENTO_BRILHANTE;
	}

	return ID_ACABAMENTO_MATE;

}


string removerAspas(string s) {

	// Remove all double-quote characters
	s.erase(
		remove(s.begin(), s.end(), '\"'),
		s.end()
	);

	return s;

}
void lerFicheiro() {

	if (estado.readFile < 100) {

		estado.readFile++;

	}
	else {

		estado.readFile = 0;
		/*
		std::ifstream t("../armario.itn");
		std::string str;

		t.seekg(0, std::ios::end);
		str.reserve(t.tellg());
		t.seekg(0, std::ios::beg);

		str.assign((std::istreambuf_iterator<char>(t)),
			std::istreambuf_iterator<char>());

		printf("Ficheiro lido!\n");



		//print do JSON
		cout << str;
*/
// Parse example data
///////////////////////////////////////////////////////////////////////////////
		std::ifstream i("../armario.itn");
		json j;
		i >> j;

		printf("\n\n");
		// the same code as range for
		for (auto& el : j.items()) {
			std::cout << el.key() << " : " << el.value() << "\n";
		}

		GLfloat altura_armario = stof(removerAspas(j["altura"].dump().c_str()));
		GLfloat profundidade_armario = stof(removerAspas(j["profundidade"].dump().c_str()));
		string material_armario = j["material"].dump();
		string acabamento_armario = j["acabamento"].dump();
		modelo.material_portas = getMaterialID(material_armario);
		modelo.acabamento_portas = getAcabamentoID(acabamento_armario);

		estado.camera.center.z = altura_armario * 0.5;

		//cout << j["itemFilhos"];

		GLint n_slots = 0;
		for (auto& el : j["itemFilhos"].items()) {
			printf("\n----------slot----------\n");
			json slot = el.value();
			cout << slot;

			GLfloat largura_armario = stof(removerAspas(slot["largura"].dump().c_str()));

			modelo.armario[n_slots].grossura = 0.2;
			modelo.armario[n_slots].altura = altura_armario;
			modelo.armario[n_slots].profundidade = profundidade_armario;
			modelo.armario[n_slots].largura = largura_armario;
			modelo.armario[n_slots].material = getMaterialID(material_armario);
			modelo.armario[n_slots].acabamento = getAcabamentoID(acabamento_armario);
			modelo.armario[n_slots].num_gavetas = 0;
			modelo.armario[n_slots].num_prateleiras = 0;
			modelo.armario[n_slots].num_suportes = 0;


			printf("\n----------slot----------\n");

			GLint n_prat = 0, n_gav = 0;
			for (auto& elf : slot["itemFilhos"].items()) {
				printf("\n----------filho----------\n");
				json filho = elf.value();
				cout << filho;

				//prateleira
				if (filho["sku"].dump().compare("\"2003\"") == 0) {

					string material_prateleira = filho["material"].dump();
					string acabamento_prateleira = filho["acabamento"].dump();
					GLfloat posicao_prateleira = stof(removerAspas(filho["posicao"].dump().c_str()));

					//GLint next_altura = n_prat;
					//modelo.armario[n_slots].prateleira[n_prat].altura_prateleira = next_altura + 1;
					modelo.armario[n_slots].prateleira[n_prat].material = getMaterialID(material_prateleira);
					modelo.armario[n_slots].prateleira[n_prat].acabamento = getAcabamentoID(acabamento_prateleira);
					modelo.armario[n_slots].prateleira[n_prat].altura_prateleira = posicao_prateleira;

					n_prat++;
				}
				//gaveta
				if (filho["sku"].dump().compare("\"2002\"") == 0) {

					string material_gaveta = filho["material"].dump();
					string acabamento_gaveta = filho["acabamento"].dump();
					string aberto = filho["aberto"].dump().c_str();
					GLfloat posicao_gaveta = stof(removerAspas(filho["posicao"].dump().c_str()));

					//GLint next_altura = n_gav;
					//modelo.armario[n_slots].gaveta[n_gav].altura_gaveta = next_altura + 1;
					modelo.armario[n_slots].gaveta[n_gav].material = getMaterialID(material_gaveta);
					modelo.armario[n_slots].gaveta[n_gav].acabamento = getAcabamentoID(acabamento_gaveta);
					modelo.armario[n_slots].gaveta[n_gav].altura_gaveta = posicao_gaveta;

					if (aberto.compare("true") == 0) {
						modelo.portas_abertas = GL_TRUE;
						modelo.armario[n_slots].gaveta[n_gav].gaveta_aberta = GL_TRUE;
					}
					else {
						if (aberto.compare("false") == 0) {
							modelo.armario[n_slots].gaveta[n_gav].gaveta_aberta = GL_FALSE;
						}
					}

					n_gav++;
				}

				printf("\n----------filho----------\n");

			}

			modelo.armario[n_slots].num_gavetas = n_gav;
			modelo.armario[n_slots].num_prateleiras = n_prat;
			modelo.armario[n_slots].num_suportes = 0;
			printf("ATENCAO! O armario %d tem %d gavetas e %d prateleiras\n", n_slots, n_gav, n_prat);

			n_slots++;
		}

		modelo.num_armarios = n_slots;

		//json test2;

		/*for (auto& el : test.items()) {
			std::cout << el.key() << " : " << el.value() << "\n";
			test2 = el.value();


		}

		printf("\n-----------------------\n");
		cout << test2;
		printf("\n-----------------------\n");
		cout << test2["sku"].dump();
		string s1 = test2["sku"].dump();

		if (s1.compare("\"2004\"") == 0) {
			printf("ALELUIA\n");
		}

		cout << s1;

		//cout << test;

		printf("\n\n\n\n\n\n\n\n");*/

	}
}


void inicia_modelo()
{

	estado.readFile = 1000;

	estado.mode = GL_RENDER;

	modelo.light1.x = 0;
	modelo.light1.y = 10;
	modelo.light1.z = 25;

	modelo.angulo_portas = 0.5;
//	modelo.material_portas = ID_TEXTURA_MADEIRA;
//	modelo.acabamento_portas = ID_ACABAMENTO_MATE;
	modelo.portas_abertas = GL_FALSE;

	modelo.num_armarios = 0;

	for (int a = 0; a < MAX_ELEMENTS; a++) {
		for (int g = 0; g < MAX_ELEMENTS; g++) {
				modelo.armario[a].gaveta[g].gaveta_aberta = GL_FALSE;
				modelo.armario[a].gaveta[g].posicao_gaveta = 0.1;
		}
	}

	/*
	modelo.armario[0].num_prateleiras = 1;
	modelo.armario[0].num_gavetas = 1;
	modelo.armario[0].num_suportes = 0;

	modelo.armario[0].grossura = 0.2;
	modelo.armario[0].altura = 10;
	modelo.armario[0].largura = 6;
	modelo.armario[0].profundidade = 3;
	modelo.armario[0].material = ID_TEXTURA_MADEIRA;
	modelo.armario[0].acabamento = ID_ACABAMENTO_MATE;

	modelo.armario[0].prateleira[0].altura_prateleira = 3;
	modelo.armario[0].prateleira[0].material = ID_TEXTURA_MADEIRA;
	modelo.armario[0].prateleira[0].acabamento = ID_ACABAMENTO_MATE;

	modelo.armario[0].gaveta[0].altura_gaveta = 4.5;
	modelo.armario[0].gaveta[0].posicao_gaveta = 0.1;
	modelo.armario[0].gaveta[0].material = ID_TEXTURA_MADEIRA;
	modelo.armario[0].gaveta[0].acabamento = ID_ACABAMENTO_MATE;
	modelo.armario[0].gaveta[0].gaveta_aberta = GL_FALSE;


	//armario1

	modelo.armario[1].num_prateleiras = 1;
	modelo.armario[1].num_gavetas = 1;
	modelo.armario[1].num_suportes = 0;

	modelo.armario[1].grossura = 0.2;
	modelo.armario[1].altura = 10;
	modelo.armario[1].largura = 6;
	modelo.armario[1].profundidade = 3;
	modelo.armario[1].material = ID_TEXTURA_MADEIRA;
	modelo.armario[1].acabamento = ID_ACABAMENTO_MATE;

	modelo.armario[1].prateleira[0].altura_prateleira = 2;
	modelo.armario[1].prateleira[0].material = ID_TEXTURA_MADEIRA;
	modelo.armario[1].prateleira[0].acabamento = ID_ACABAMENTO_MATE;

	modelo.armario[1].gaveta[0].altura_gaveta = 3.5;
	modelo.armario[1].gaveta[0].posicao_gaveta = 0.1;
	modelo.armario[1].gaveta[0].material = ID_TEXTURA_MADEIRA;
	modelo.armario[1].gaveta[0].acabamento = ID_ACABAMENTO_MATE;
	modelo.armario[1].gaveta[0].gaveta_aberta = GL_FALSE;

	//armario2

	modelo.armario[2].num_prateleiras = 0;
	modelo.armario[2].num_gavetas = 0;
	modelo.armario[2].num_suportes = 1;

	modelo.armario[2].grossura = 0.2;
	modelo.armario[2].altura = 10;
	modelo.armario[2].largura = 4;
	modelo.armario[2].profundidade = 3;
	modelo.armario[2].material = ID_TEXTURA_MADEIRA;
	modelo.armario[2].acabamento = 0;

	modelo.armario[2].suporte[0].altura_suporte = 8;


	estado.camera.center.z = modelo.armario[0].altura*0.5;
*/
}


/* Inicialização do ambiente OPENGL */
void Init(void)
{

	srand((unsigned)time(NULL));

	modelo.parado = GL_FALSE;

	estado.debug = DEBUG;
	estado.menuActivo = GL_FALSE;
	estado.delayMovimento = DELAY_MOVIMENTO;
	estado.camera.eye.x = 1;
	estado.camera.eye.y = 3;
	estado.camera.eye.z = 5;
	estado.camera.center.x = 0;
	estado.camera.center.y = 0;
	estado.camera.center.z = 0;
	estado.camera.up.x = 0;
	estado.camera.up.y = 0;
	estado.camera.up.z = 1;
	estado.camera.fov = 60;
	estado.camera.dist_center = 15;
	estado.camera.ang_teta = M_PI * 0.5;
	estado.camera.ang_fi = M_PI * 0.5;

	estado.teclas.a = estado.teclas.q = estado.teclas.z = estado.teclas.x = \
		estado.teclas.up = estado.teclas.down = estado.teclas.left = estado.teclas.right = GL_FALSE;

	inicia_modelo();

	glClearColor(0.0, 0.0, 0.0, 0.0);

	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_NORMALIZE);
	//glutIgnoreKeyRepeat(GL_TRUE);

}

/**************************************
***  callbacks de janela/desenho    ***
**************************************/

// CALLBACK PARA REDIMENSIONAR JANELA

void Reshape(int width, int height)
{
	// glViewport(botom, left, width, height)
	// define parte da janela a ser utilizada pelo OpenGL

	glViewport(0, 0, (GLint)width, (GLint)height);


	// Matriz Projeccao
	// Matriz onde se define como o mundo e apresentado na janela
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(60, (GLfloat)width / height, .5, 100);

	// Matriz Modelview
	// Matriz onde são realizadas as tranformacoes dos modelos desenhados
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void desenhaRoda() {
	//glRotatef(90, 0, 1, 0);
	//glRotatef(90, 1, 0, 0);
	glColor3d(1.0, 1.0, 1.0);
	GLUquadric *quad;
	quad = gluNewQuadric();
	gluQuadricDrawStyle(quad, GLU_FILL);
	gluCylinder(quad, 0.5, 0.5, 0.4, 20, 2);
}


void setLight()
{

	GLfloat dist_bet_lights = 20;

	//GLfloat light_pos1[4] = { modelo.light1.x, modelo.light1.y, modelo.light1.z, 0.0 };
	GLfloat light_pos2[4] = { modelo.light1.x + dist_bet_lights, modelo.light1.y + dist_bet_lights, modelo.light1.z, 0.0 };
	GLfloat light_pos3[4] = { modelo.light1.x - dist_bet_lights, modelo.light1.y - dist_bet_lights, modelo.light1.z, 0.0 };
	GLfloat light_pos4[4] = { modelo.light1.x + dist_bet_lights, modelo.light1.y - dist_bet_lights, modelo.light1.z, 0.0 };
	GLfloat light_pos5[4] = { modelo.light1.x - dist_bet_lights, modelo.light1.y + dist_bet_lights, modelo.light1.z, 0.0 };
	GLfloat light_ambient[] = { 0.2f, 0.2f, 0.2f, 1.0f };
	GLfloat light_diffuse[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };

	// ligar iluminação
	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
	glEnable(GL_LIGHTING);

	// ligar e definir fonte de luz 0
	/*glEnable(GL_LIGHT0);
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_pos2);
	//glLightf(GL_LIGHT0, GL_SPOT_EXPONENT, 2.0);
	//glLightf(GL_LIGHT1, GL_SPOT_CUTOFF, 2.0);
	*/
	// ligar e definir fonte de luz 1
	glEnable(GL_LIGHT1);
	glLightfv(GL_LIGHT1, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT1, GL_POSITION, light_pos2);


	// ligar e definir fonte de luz 2
	glEnable(GL_LIGHT2);
	glLightfv(GL_LIGHT2, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT2, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT2, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT2, GL_POSITION, light_pos3);


	// ligar e definir fonte de luz 3
	glEnable(GL_LIGHT3);
	glLightfv(GL_LIGHT3, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT3, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT3, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT3, GL_POSITION, light_pos4);


	/*glPushMatrix();
		glTranslatef(modelo.light1.x, modelo.light1.y, modelo.light1.z);
		desenhaRoda();
	glPopMatrix();*/
	/*
	glPushMatrix();
		glTranslatef(modelo.light1.x + dist_bet_lights, modelo.light1.y + dist_bet_lights, modelo.light1.z);
		desenhaRoda();
	glPopMatrix();

	glPushMatrix();
		glTranslatef(modelo.light1.x - dist_bet_lights, modelo.light1.y - dist_bet_lights, modelo.light1.z);
		desenhaRoda();
	glPopMatrix();


	glPushMatrix();
		glTranslatef(modelo.light1.x + dist_bet_lights, modelo.light1.y - dist_bet_lights, modelo.light1.z);
		desenhaRoda();
	glPopMatrix();

	glPushMatrix();
		glTranslatef(modelo.light1.x - dist_bet_lights, modelo.light1.y + dist_bet_lights, modelo.light1.z);
		desenhaRoda();
	glPopMatrix();
	*/

}

void setMaterialMate()
{
	GLfloat mat_ambient[] = { 0.2f, 0.2f, 0.2f, 1.0f };
	GLfloat mat_diffuse[] = { 0.7f, 0.7f, 0.7f, 1.0f };
	GLfloat mat_specular[] = { 0.3f, 0.3f, 0.3f, 1.0f };
	GLfloat mat_shininess = 5;

	// criação automática das componentes Ambiente e Difusa do material a partir das cores
	glDisable(GL_COLOR_MATERIAL);
	//glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

	// definir de outros parâmetros dos materiais estáticamente
	glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, mat_shininess);
}


void setMaterialBrilhante()
{
	GLfloat mat_ambient[] = { 0.3f, 0.3f, 0.3f, 1.0f };
	GLfloat mat_diffuse[] = { 0.9f, 0.9f, 0.9f, 1.0f };
	GLfloat mat_specular[] = { 1.0, 1.0f, 1.0f, 1.0f };
	GLfloat mat_shininess = 128;

	// criação automática das componentes Ambiente e Difusa do material a partir das cores
	glDisable(GL_COLOR_MATERIAL);
	//glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

	// definir de outros parâmetros dos materiais estáticamente
	glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, mat_shininess);
}


void selecionarAcabamento(GLint acabID) {
	switch (acabID) {

	case 0: {
		setMaterialMate();
	}
			break;

	case 1: {
		setMaterialBrilhante();
	}
			break;
	}
}


void desenhaPoligono(GLfloat a[], GLfloat b[], GLfloat c[], GLfloat  d[], GLfloat normal[], GLfloat texturas[4][2])
{
	glBegin(GL_POLYGON);
	glNormal3fv(normal);
	glTexCoord2f(texturas[0][0], texturas[0][1]);
	glVertex3fv(a);
	glTexCoord2f(texturas[1][0], texturas[1][1]);
	glVertex3fv(b);
	glTexCoord2f(texturas[2][0], texturas[2][1]);
	glVertex3fv(c);
	glTexCoord2f(texturas[3][0], texturas[3][1]);
	glVertex3fv(d);
	glEnd();
}


void desenhaCubo(GLuint texID)
{
	GLfloat vertices[][3] = { {-0.5,-0.5,-0.5},
							  {0.5,-0.5,-0.5},
							  {0.5,0.5,-0.5},
							  {-0.5,0.5,-0.5},
							  {-0.5,-0.5,0.5},
							  {0.5,-0.5,0.5},
							  {0.5,0.5,0.5},
							  {-0.5,0.5,0.5} };
	GLfloat normais[][3] = { {0,0,-1},
							  {0,-1,0},
							  {-1,0,0},
							  {0,0,1},
							  {0,1,0},
							  {1,0,0},

		// acrescentar as outras normais...
	};

	GLfloat texturas[][4][2] = {
		{{0, 1},{0, 0.75},{0.25, 0.75},{0.25, 1}},
		{{0, 0.75},{0, 0.5},{0.25, 0.5},{0.25, 0.75}},
		{{0, 0.5},{0, 0.25},{0.25, 0.25},{0.25, 0.5}},
		{{0, 0.25},{0, 0},{0.25, 0},{0.25, 0.25}}
	};


	glBindTexture(GL_TEXTURE_2D, texID);

	desenhaPoligono(vertices[1], vertices[0], vertices[3], vertices[2], normais[0], texturas[0]);
	desenhaPoligono(vertices[2], vertices[3], vertices[7], vertices[6], normais[4], texturas[0]);
	desenhaPoligono(vertices[3], vertices[0], vertices[4], vertices[7], normais[2], texturas[0]);
	desenhaPoligono(vertices[6], vertices[5], vertices[1], vertices[2], normais[5], texturas[0]);
	desenhaPoligono(vertices[4], vertices[5], vertices[6], vertices[7], normais[3], texturas[0]);
	desenhaPoligono(vertices[5], vertices[4], vertices[0], vertices[1], normais[1], texturas[0]);

	glBindTexture(GL_TEXTURE_2D, 0);
}


void desenhaArmario(GLuint texID, GLfloat grossura, GLfloat altura, GLfloat largura, GLfloat profundidade) {



	//(largura,altura,profundidade)

	//base
	glPushMatrix();
	glTranslatef(0, grossura*0.5, 0);
	glScalef(largura, grossura, profundidade);
	desenhaCubo(texID);
	glPopMatrix();

	//fundo
	glPushMatrix();
	glTranslatef(0, grossura*0.5 + altura * 0.5, profundidade*0.5 - grossura * 0.5);
	glRotatef(90, 1, 0, 0);
	glScalef(largura, grossura, altura);
	desenhaCubo(texID);
	glPopMatrix();

	//teto
	glPushMatrix();
	glTranslatef(0, grossura*0.5 * 2 + altura, 0);
	glScalef(largura, grossura, profundidade);
	desenhaCubo(texID);
	glPopMatrix();

	//parede esquerda
	glPushMatrix();
	glTranslatef(largura*0.5 - grossura * 0.5, grossura*0.5 + altura * 0.5, 0);
	//glRotatef(90, 0, 1, 0);
	glScalef(grossura, altura, profundidade);
	desenhaCubo(texID);
	glPopMatrix();

	//parede direita
	glPushMatrix();
	glTranslatef(-largura * 0.5 + grossura * 0.5, grossura*0.5 + altura * 0.5, 0);
	//glRotatef(90, 0, 1, 0);
	glScalef(grossura, altura, profundidade);
	desenhaCubo(texID);
	glPopMatrix();

}

void desenhaPrateleira(GLuint texID, GLfloat grossura, GLfloat altura, GLfloat largura, GLfloat profundidade, GLfloat localizacao_z) {

	GLfloat grossura_prateleira = grossura * 0.6;

	glPushMatrix();
	glTranslatef(0, grossura + localizacao_z, 0);
	glRotatef(90, 1, 0, 0);
	glScalef(largura - 0.01, profundidade - 0.01, grossura_prateleira);
	desenhaCubo(texID);
	glPopMatrix();

}


void desenhaGaveta(GLuint texID, GLfloat grossura, GLfloat altura, GLfloat largura, GLfloat profundidade, GLfloat posicao_gaveta, GLfloat localizacao_z) {

	//(largura,altura,profundidade)


	//medidas gaveta
	GLfloat altura_gaveta = altura * 0.1;
	GLfloat grossura_gaveta = grossura * 0.5;

	//desenha suporte
	glPushMatrix();
	glTranslatef(0, 0, localizacao_z);
	glRotatef(90, 1, 0, 0);
	desenhaArmario(texID, grossura_gaveta, altura_gaveta, largura - grossura, profundidade - grossura);
	glPopMatrix();


	//desenha gaveta (abre e fecha)

	glPushMatrix();
	glTranslatef(0, profundidade*0.5 + posicao_gaveta, localizacao_z);
	glRotatef(180, 1, 0, 0);
	glTranslatef(0, 0, -altura_gaveta * 0.5 - grossura_gaveta * 0.5);
	desenhaArmario(texID, grossura_gaveta, profundidade - grossura_gaveta * 2 - grossura, largura - grossura_gaveta * 3 - grossura, altura_gaveta);

	//desenha pega gaveta
	glRotatef(90, 1, 0, 0);
	glScalef(0.3, 0.3, 0.3);
	desenhaRoda();

	glPopMatrix();
}

void desenhaVarao(GLfloat comprimento) {
	//glRotatef(90, 0, 1, 0);
	//glRotatef(90, 1, 0, 0);
	glColor3d(1.0, 1.0, 1.0);
	GLUquadric *quad;
	quad = gluNewQuadric();
	gluQuadricDrawStyle(quad, GLU_FILL);
	gluCylinder(quad, 0.5, 0.5, comprimento, 20, 2);
}

void desenhaSuporte(GLfloat altura, GLfloat largura_armario) {
	glPushMatrix();
	glTranslatef(-largura_armario * 0.5, 0, GROSSURA_DEFAULT + altura);
	glRotatef(90, 0, 1, 0);
	glScalef(0.2, 0.2, 1);
	desenhaVarao(largura_armario);
	glPopMatrix();
}

void bitmapString(char *str, double x, double y)
{
	int i, n;

	// fonte pode ser:
	// GLUT_BITMAP_8_BY_13
	// GLUT_BITMAP_9_BY_15
	// GLUT_BITMAP_TIMES_ROMAN_10
	// GLUT_BITMAP_TIMES_ROMAN_24
	// GLUT_BITMAP_HELVETICA_10
	// GLUT_BITMAP_HELVETICA_12
	// GLUT_BITMAP_HELVETICA_18
	//
	// int glutBitmapWidth  	(	void *font , int character);
	// devolve a largura de um carácter
	//
	// int glutBitmapLength 	(	void *font , const unsigned char *string );
	// devolve a largura de uma string (soma da largura de todos os caracteres)

	n = strlen(str);
	glRasterPos2d(x, y);
	for (i = 0; i < n; i++)
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, (int)str[i]);
}

void bitmapCenterString(char *str, double x, double y)
{
	int i, n;

	n = strlen(str);
	glRasterPos2d(x - glutBitmapLength(GLUT_BITMAP_HELVETICA_18, (const unsigned char *)str)*0.5, y);
	for (i = 0; i < n; i++)
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, (int)str[i]);
}


// ... definicao das rotinas auxiliares de desenho ...



void setMaterialChao()
{
	GLfloat mat_ambient[] = { 0.2f, 0.2f, 0.2f, 1.0f };
	GLfloat mat_diffuse[] = { 0.25f, 0.25f, 0.25f, 1.0f };
	GLfloat mat_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat mat_shininess = 100;

	// criação automática das componentes Ambiente e Difusa do material a partir das cores
	glDisable(GL_COLOR_MATERIAL);
	//glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

	// definir de outros parâmetros dos materiais estáticamente
	glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, mat_shininess);
}

#define STEP    1

void desenhaChao(GLfloat dimensao, GLuint texID)
{

	glBindTexture(GL_TEXTURE_2D, texID);

	GLfloat i, j;
	for (i = -dimensao; i <= dimensao; i += STEP)
		for (j = -dimensao; j <= dimensao; j += STEP)
		{
			glBegin(GL_POLYGON);
			glNormal3f(0, 0, 1);
			glTexCoord2f(1, 1);
			glVertex3f(i + STEP, j + STEP, 0);
			glTexCoord2f(0, 1);
			glVertex3f(i, j + STEP, 0);
			glTexCoord2f(0, 0);
			glVertex3f(i, j, 0);
			glTexCoord2f(1, 0);
			glVertex3f(i + STEP, j, 0);
			glEnd();
		}

	glBindTexture(GL_TEXTURE_2D, 0);

}

void desenhaPecaPorta() {

	GLfloat altura_portas = modelo.armario[0].altura + modelo.armario[0].grossura;
	GLfloat largura_total = 0;

	for (int a = 0; a < modelo.num_armarios; a++) {
		largura_total += modelo.armario[a].largura;
	}

	//porta 
	glPushMatrix();
	glTranslatef(largura_total*0.5, modelo.armario[0].profundidade*0.5 + modelo.armario[0].grossura*0.5, altura_portas*0.5);
	glRotatef(-modelo.angulo_portas, 0, 0, 1);
	glTranslatef(-largura_total * 0.25, 0, 0);
	glScalef(largura_total*0.5, GROSSURA_DEFAULT, altura_portas+GROSSURA_DEFAULT);
	desenhaCubo(modelo.texID[modelo.material_portas]);
	glPopMatrix();

	//puxador 
	glPushMatrix();
	glTranslatef(largura_total*0.5, modelo.armario[0].profundidade*0.5 + modelo.armario[0].grossura*0.5, altura_portas*0.5);
	glRotatef(-modelo.angulo_portas, 0, 0, 1);
	glTranslatef(-largura_total * 0.45, GROSSURA_DEFAULT, 0);
	glRotatef(90, 1, 0, 0);
	glScalef(0.3, 0.3, 0.3);
	desenhaRoda();
	glPopMatrix();
}

void desenhaPortas() {

	glPushAttrib(GL_COLOR_BUFFER_BIT | GL_CURRENT_BIT | GL_ENABLE_BIT);

	glPushMatrix();
	glTranslatef(0,GROSSURA_DEFAULT*1.5,0);
	desenhaPecaPorta();
	glPopMatrix();


	glPushMatrix();
	glScalef(-1, 1, 1);
	glTranslatef(0, GROSSURA_DEFAULT*1.5, 0);
	desenhaPecaPorta();
	glPopMatrix();

	glPopAttrib();

}

void desenhaModuloArmario(GLint n_armario) {

	//medidas armario
	GLfloat grossura = modelo.armario[n_armario].grossura;
	GLfloat altura = modelo.armario[n_armario].altura;
	GLfloat largura = modelo.armario[n_armario].largura;
	GLfloat profundidade = modelo.armario[n_armario].profundidade;

	GLint textID_armario = modelo.armario[n_armario].material;
	GLint acabID_armario = modelo.armario[n_armario].acabamento;


	glPushAttrib(GL_COLOR_BUFFER_BIT | GL_CURRENT_BIT | GL_ENABLE_BIT);
	glPushMatrix();
	glRotatef(90, 1, 0, 0);
	selecionarAcabamento(acabID_armario);
	desenhaArmario(modelo.texID[textID_armario], grossura, altura+ GROSSURA_DEFAULT, largura + GROSSURA_DEFAULT, profundidade + GROSSURA_DEFAULT*3);
	glPopMatrix();
	glPopAttrib();


	GLint texID_prateleira;
	GLint acabID_prateleira;
	//length temporario
	for (int p = 0; p < modelo.armario[n_armario].num_prateleiras; p++) {

		texID_prateleira = modelo.armario[n_armario].prateleira[p].material;
		acabID_prateleira = modelo.armario[n_armario].prateleira[p].acabamento;

		glPushAttrib(GL_COLOR_BUFFER_BIT | GL_CURRENT_BIT | GL_ENABLE_BIT);
		glPushMatrix();
		glRotatef(90, 1, 0, 0);
		selecionarAcabamento(acabID_prateleira);
		desenhaPrateleira(modelo.texID[texID_prateleira], grossura, altura, largura, profundidade, modelo.armario[n_armario].prateleira[p].altura_prateleira);
		glPopMatrix();
		glPopAttrib();
	}


	GLint texID_gaveta;
	GLint acabID_gaveta;
	//length temporario
	for (int g = 0; g < modelo.armario[n_armario].num_gavetas; g++) {

		texID_gaveta = modelo.armario[n_armario].gaveta[g].material;
		acabID_gaveta = modelo.armario[n_armario].gaveta[g].acabamento;

		glPushAttrib(GL_COLOR_BUFFER_BIT | GL_CURRENT_BIT | GL_ENABLE_BIT);
		glPushMatrix();
		selecionarAcabamento(acabID_gaveta);
		desenhaGaveta(modelo.texID[texID_gaveta], grossura, altura, largura, profundidade, modelo.armario[n_armario].gaveta[g].posicao_gaveta, modelo.armario[n_armario].gaveta[g].altura_gaveta);
		glPopMatrix();
		glPopAttrib();
	}

	for (int s = 0; s < modelo.armario[n_armario].num_suportes; s++) {
		desenhaSuporte(modelo.armario[n_armario].suporte[s].altura_suporte, modelo.armario[n_armario].largura);
	}
}

void executaMovimentos() {
	//portas
	if (modelo.portas_abertas == GL_TRUE) {
		if (modelo.angulo_portas > 0 && modelo.angulo_portas < 119) {
			modelo.angulo_portas++;
		}
	}
	else {
		if (modelo.angulo_portas > 1 && modelo.angulo_portas < 120) {
			modelo.angulo_portas--;
		}
	}

	//gavetas

		for (int a = 0; a < modelo.num_armarios; a++) {
			for (int g = 0; g < modelo.armario[a].num_gavetas; g++) {

				if (modelo.armario[a].gaveta[g].gaveta_aberta == GL_TRUE) {
					if (modelo.armario[a].gaveta[g].posicao_gaveta > 0 && modelo.armario[a].gaveta[g].posicao_gaveta < modelo.armario[a].profundidade*0.5 - 0.1) {
						modelo.armario[a].gaveta[g].posicao_gaveta += 0.1;
					}
				}
				else {
					if (modelo.armario[a].gaveta[g].posicao_gaveta > 0.1 && modelo.armario[a].gaveta[g].posicao_gaveta <= modelo.armario[a].profundidade*0.5) {
						modelo.armario[a].gaveta[g].posicao_gaveta -= 0.1;
					}
				}


			}
		}
	
}

// Callback de desenho

void Draw(void)
{

	/*if (estado.mode == GL_RENDER) {
		printf("RENDER\n");
	}
	else {
		printf("SELECT\n");
	}*/

	lerFicheiro();


		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glLoadIdentity();


		estado.camera.eye.x = estado.camera.center.x + estado.camera.dist_center * cos(estado.camera.ang_teta) * sin(estado.camera.ang_fi);
		estado.camera.eye.y = estado.camera.center.x + estado.camera.dist_center * sin(estado.camera.ang_teta) * sin(estado.camera.ang_fi);
		estado.camera.eye.z = estado.camera.center.z + estado.camera.dist_center * cos(estado.camera.ang_fi);

		gluLookAt(estado.camera.eye.x, estado.camera.eye.y, estado.camera.eye.z, \
			estado.camera.center.x, estado.camera.center.y, estado.camera.center.z, \
			estado.camera.up.x, estado.camera.up.y, estado.camera.up.z);
		glTranslatef(estado.camera.x, estado.camera.y, estado.camera.z);
		glRotatef(0, estado.camera.RotateY + estado.camera.RotateY, 0, 0);


		// ... chamada das rotinas auxiliares de desenho ...

		glPushMatrix();
		setLight();
		glPopMatrix();

		glPushAttrib(GL_COLOR_BUFFER_BIT | GL_CURRENT_BIT | GL_ENABLE_BIT);
		glPushMatrix();
		setMaterialChao();
		desenhaChao(100, modelo.texID[ID_TEXTURA_CHAO]);
		glPopMatrix();
		glPopAttrib();

		//portas e gavetas
		executaMovimentos();

		GLfloat center_offset = 0;

		//length temporario
		for (int m = 0; m < modelo.num_armarios; m++) {

			if (m != 0) {
				center_offset += modelo.armario[m].largura*0.5;
			}
		}

		glPushMatrix();
		glTranslatef(center_offset, 0, 0);

		glPushMatrix();
		//length temporario

		for (int m = 0; m < modelo.num_armarios; m++) {
			if (m != 0) {
				glTranslatef(-modelo.armario[m].largura*0.5, 0, 0);
			}
			desenhaModuloArmario(m);
			glTranslatef(-modelo.armario[m].largura*0.5, 0, 0);
		}

		glPopMatrix();

		glPopMatrix();

	glPushMatrix();
	selecionarAcabamento(modelo.acabamento_portas);
	desenhaPortas();
	glPopMatrix();

	//setLight();

	glFlush();
	if (estado.doubleBuffer)
		glutSwapBuffers();
}


/*******************************
***   callbacks timer/idle   ***
*******************************/

void Timer(int value)
{
	glutTimerFunc(estado.delayMovimento, Timer, 0);
	// ... accoes do temporizador ... 

	if (estado.menuActivo || modelo.parado) // sair em caso de o jogo estar parado ou menu estar activo
		return;

	// redesenhar o ecra 
	glutPostRedisplay();
}



void imprime_ajuda(void)
{

	printf("h,H - Ajuda \n");

	printf("a,A - Rodar Cam Esquerda\n");
	printf("d,D - Rodar Cam Direita\n");
	printf("w,W - Mover Cam Cima\n");
	printf("s,S - Rodar Cam Baixo\n");
	printf("e,E - Mover Cam frente\n");
	printf("q,Q - Mover Cam tras\n");

	printf("8 - Desloca fontes de luz para cima\n");
	printf("2 - Desloca fontes de luz para baixo\n");
	printf("4 - Desloca fontes de luz para esquerda\n");
	printf("6 - Desloca fontes de luz para direita\n");

	printf("ESC - Sair\n");
}

/*******************************
***  callbacks de teclado    ***
*******************************/

// Callback para interaccao via teclado (carregar na tecla)

void Key(unsigned char key, int x, int y)
{

	double distanceToCenter;
	switch (key) {


	case 27:
		exit(0);
		// ... accoes sobre outras teclas ... 

	case 'h':
	case 'H':
		imprime_ajuda();
		break;

	case 'e':
	case 'E':
	{

		if (estado.camera.dist_center > 0) {
			estado.camera.dist_center--;
		}
	}
	break;

	case 'd':
	case 'D':

		estado.camera.ang_teta += 0.05;
		break;

	case 'a':
	case 'A':
		estado.camera.ang_teta -= 0.05;
		break;
	case 's':
	case 'S':

		estado.camera.ang_fi += 0.05;

		break;
	case 'W':
	case 'w':

		estado.camera.ang_fi -= 0.05;

		break;
	case 'i':
	case 'I':
		lerFicheiro();
		break;
	case 'o':
	case 'O':

		break;
	case 'Q':
	case 'q':
	{

		estado.camera.dist_center++;

	}

	break;

	case 'Z':
	case 'z':
		estado.camera.RotateY = estado.camera.RotateY - 0.1f;
		break;
	case 'X':
	case 'x':
		estado.camera.RotateY = estado.camera.RotateY + 0.1f;
		break;
	case 'f':
	case 'F':
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		break;
	case 'p':
	case 'P':
		glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
		break;
	case 'l':
	case 'L':
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		break;
	case '4':
		modelo.light1.x++;
		printf("luz.x = %f\n", modelo.light1.x);


		break;
	case '6':
		modelo.light1.x--;
		printf("luz.x = %f\n", modelo.light1.x);
		break;

	case '8':

		modelo.light1.y--;
		printf("luz.y = %f\n", modelo.light1.y);
		break;
	case '2':

		modelo.light1.y++;
		printf("luz.y = %f\n", modelo.light1.y);

		break;

	case '9':
		modelo.light1.z++;

		printf("luz.z = %f\n", modelo.light1.z);


		break;
	case '7':
		modelo.light1.z--;
		printf("luz.z = %f\n", modelo.light1.z);


		break;
		/* case 's' :
		 case 'S' :
					modelo.parado=!modelo.parado;
				break;*/


	}

	if (estado.debug)
		printf("Carregou na tecla %c\n", key);

}

// Callback para interaccao via teclado (largar a tecla)

void KeyUp(unsigned char key, int x, int y)
{


	if (estado.debug)
		printf("Largou a tecla %c\n", key);
}

// Callback para interaccao via teclas especiais  (carregar na tecla)

void SpecialKey(int key, int x, int y)
{
	// ... accoes sobre outras teclas especiais ... 
	//    GLUT_KEY_F1 ... GLUT_KEY_F12
	//    GLUT_KEY_UP
	//    GLUT_KEY_DOWN
	//    GLUT_KEY_LEFT
	//    GLUT_KEY_RIGHT
	//    GLUT_KEY_PAGE_UP
	//    GLUT_KEY_PAGE_DOWN
	//    GLUT_KEY_HOME
	//    GLUT_KEY_END
	//    GLUT_KEY_INSERT 

	switch (key) {
	case GLUT_KEY_LEFT:
		estado.camera.x--;

		break;
	case GLUT_KEY_RIGHT:
		estado.camera.x++;

		break;
	case GLUT_KEY_UP:
		estado.camera.y++;
		break;
	case GLUT_KEY_DOWN:
		estado.camera.y--;
		break;
	case GLUT_KEY_PAGE_UP:
		estado.camera.z--;

		break;
	case GLUT_KEY_PAGE_DOWN:
		estado.camera.z++;

		break;
	}

	if (estado.debug)
		printf("Carregou na tecla especial %d\n", key);
}

// Callback para interaccao via teclas especiais (largar na tecla)

void SpecialKeyUp(int key, int x, int y)
{

	if (estado.debug)
		printf("Largou a tecla especial %d\n", key);

}


void createTextures(GLuint texID[])
{

	char *image;
	int w, h, bpp;

	glGenTextures(NUM_TEXTURAS, texID);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);


	//  Call the SOIL Library 
	SOIL_load_OGL_texture
	(
		NOME_TEXTURA_CHAO,
		SOIL_LOAD_AUTO,
		texID[ID_TEXTURA_CHAO],
		SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB |
		SOIL_FLAG_COMPRESS_TO_DXT | SOIL_FLAG_TEXTURE_REPEATS
	);

	/* check for an error during the load process */
	if (texID[ID_TEXTURA_CHAO] == 0)
	{
		printf("SOIL texture loading error: '%s'\n", SOIL_last_result());
		exit(1);
	}


	//  Call the SOIL Library 
	SOIL_load_OGL_texture
	(
		NOME_TEXTURA_MADEIRA,
		SOIL_LOAD_AUTO,
		texID[ID_TEXTURA_MADEIRA],
		SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB |
		SOIL_FLAG_COMPRESS_TO_DXT | SOIL_FLAG_TEXTURE_REPEATS
	);

	/* check for an error during the load process */
	if (texID[ID_TEXTURA_MADEIRA] == 0)
	{
		printf("SOIL texture loading error: '%s'\n", SOIL_last_result());
		exit(1);
	}

	SOIL_load_OGL_texture
	(
		NOME_TEXTURA_METAL,
		SOIL_LOAD_AUTO,
		texID[ID_TEXTURA_METAL],
		SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB |
		SOIL_FLAG_COMPRESS_TO_DXT | SOIL_FLAG_TEXTURE_REPEATS
	);

	/* check for an error during the load process */
	if (texID[ID_TEXTURA_METAL] == 0)
	{
		printf("SOIL texture loading error: '%s'\n", SOIL_last_result());
		exit(1);
	}

	SOIL_load_OGL_texture
	(
		NOME_TEXTURA_PVC,
		SOIL_LOAD_AUTO,
		texID[ID_TEXTURA_PVC],
		SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB |
		SOIL_FLAG_COMPRESS_TO_DXT | SOIL_FLAG_TEXTURE_REPEATS
	);

	/* check for an error during the load process */
	if (texID[ID_TEXTURA_PVC] == 0)
	{
		printf("SOIL texture loading error: '%s'\n", SOIL_last_result());
		exit(1);
	}

	glBindTexture(GL_TEXTURE_2D, 0);
}

void listaMateriais() {
	printf("%d - Madeira\n", ID_TEXTURA_MADEIRA);
	printf("%d - Metal\n", ID_TEXTURA_METAL);
	printf("%d - PVC\n", ID_TEXTURA_PVC);
}

void listaAcabamentos() {
	printf("%d - Mate\n", ID_ACABAMENTO_MATE);
	printf("%d - Brilhante\n", ID_ACABAMENTO_BRILHANTE);
}

void menuArmario(int value) {
	switch (value)
	{
	case 1:
	{
		int n_armario = 0;

		//validar numero do modulo
		printf("Numero do modulo:\n");
		scanf("%d", &n_armario);

		//length temporario
		if (n_armario >= modelo.num_armarios) {
			printf("Modulo invalido\n");
		}
		else {

			printf("Largura:\n");
			scanf("%f", &modelo.armario[n_armario].largura);

		}
	}

	break;
	case 2: {
		printf("Altura:\n");

		GLfloat novaAltura = 0;
		scanf("%f", &novaAltura);
		//length temporario
		for (int a = 0; a < modelo.num_armarios; a++) {
			modelo.armario[a].altura = novaAltura;
		}

		estado.camera.center.z = modelo.armario[0].altura*0.5;
	}
			break;

	case 3: {

		GLfloat novaLargura = 0;
		GLfloat novaProfundidade = modelo.armario[0].profundidade;

		GLint numero_armarios = modelo.num_armarios;

		printf("Largura:\n");
		scanf("%f", &novaLargura);


		modelo.armario[numero_armarios].grossura = GROSSURA_DEFAULT;
		modelo.armario[numero_armarios].altura = modelo.armario[0].altura;
		modelo.armario[numero_armarios].largura = novaLargura;
		modelo.armario[numero_armarios].profundidade = novaProfundidade;
		modelo.armario[numero_armarios].material = modelo.armario[0].material;
		modelo.armario[numero_armarios].acabamento = modelo.armario[0].acabamento;
		modelo.armario[numero_armarios].num_prateleiras = 0;
		modelo.armario[numero_armarios].num_suportes = 0;
		modelo.armario[numero_armarios].num_gavetas = 0;
		modelo.armario[numero_armarios].num_suportes = 0;

		modelo.num_armarios++;

	}
			break;
	case 4:
	{
		GLint numero_armario = 0;

		printf("Numero do Modulo\n");
		scanf("%d", &numero_armario);

		if (numero_armario == 0) {
			printf("Modulo principal nao pode ser eliminado\n");
		}
		else {
			for (int a = numero_armario; a < modelo.num_armarios - 1; a++) {
				modelo.armario[a] = modelo.armario[a + 1];
			}

			modelo.num_armarios--;
		}
	}
	break;
	case 5:
	{
		printf("Profundidade:\n");

		GLfloat novaProfundidade = 0;
		scanf("%f", &novaProfundidade);
		//length temporario
		for (int a = 0; a < modelo.num_armarios; a++) {
			modelo.armario[a].profundidade = novaProfundidade;
		}
	}
	break;

	case 6: {
		listaMateriais();

		GLint textID = 0;

		scanf("%d", &textID);
		//length temporario
		for (int a = 0; a < modelo.num_armarios; a++) {
			modelo.armario[a].material = textID;
			modelo.material_portas = textID;
		}
	}
			break;

	case 7: {
		listaAcabamentos();

		GLint acabID = 0;

		scanf("%d", &acabID);
		//length temporario
		for (int a = 0; a < modelo.num_armarios; a++) {
			modelo.armario[a].acabamento = acabID;
			modelo.acabamento_portas = acabID;
		}
	}
			break;



	case 8: {
		if (modelo.portas_abertas == GL_TRUE) {
			modelo.portas_abertas = GL_FALSE;
		}
		else {
			modelo.portas_abertas = GL_TRUE;
		}


	}
			break;
	}

	glutPostRedisplay();
}


void menuPrateleira(int value) {
	switch (value)
	{
	case 1:
	{
		GLint n_armario = 0;
		GLfloat altura_prat = 0;
		GLint material = 0;
		GLint acabamento = 0;



		//validar numero do modulo
		printf("Numero do modulo:\n");
		scanf("%d", &n_armario);

		//length temporario
		if (n_armario >= modelo.num_armarios) {
			printf("Modulo invalido\n");
		}
		else {
			printf("Altura prateleira:\n");
			scanf("%f", &altura_prat);

			printf("Material:\n");
			listaMateriais();

			scanf("%d", &material);

			printf("Acabamento:\n");
			listaAcabamentos();

			scanf("%d", &acabamento);

			modelo.armario[n_armario].prateleira[modelo.armario[n_armario].num_prateleiras].altura_prateleira = altura_prat;
			modelo.armario[n_armario].prateleira[modelo.armario[n_armario].num_prateleiras].material = material;
			modelo.armario[n_armario].prateleira[modelo.armario[n_armario].num_prateleiras].acabamento = acabamento;

			modelo.armario[n_armario].num_prateleiras++;

		}
	}
	break;
	case 2:
	{
		GLint numero_armario = 0;
		GLint numero_prateleira = 0;

		printf("Numero do Modulo\n");
		scanf("%d", &numero_armario);

		printf("Numero do Prateleira\n");
		scanf("%d", &numero_prateleira);


		for (int p = numero_prateleira; p < modelo.armario[numero_armario].num_prateleiras - 1; p++) {
			modelo.armario[numero_armario].prateleira[p] = modelo.armario[numero_armario].prateleira[p + 1];
		}

		modelo.armario[numero_armario].num_prateleiras--;
	}
	break;
	case 3:
	{
		int n_armario = 0;

		//validar numero do modulo
		printf("Numero do modulo:\n");
		scanf("%d", &n_armario);

		//length temporario
		if (n_armario >= modelo.num_armarios) {
			printf("Modulo invalido\n");
		}
		else {
			int n_prateleira = 0;

			//validar numero prateleira
			printf("Numero da prateleira:\n");
			scanf("%d", &n_prateleira);

			//length temporario
			if (n_prateleira >= modelo.armario[n_armario].num_prateleiras) {
				printf("Prateleira invalida\n");
			}
			else {
				printf("Nova Altura:\n");
				scanf("%f", &modelo.armario[n_armario].prateleira[n_prateleira].altura_prateleira);
			}
		}

	}
	break;

	case 4:
	{
		int n_armario = 0;

		//validar numero do modulo
		printf("Numero do modulo:\n");
		scanf("%d", &n_armario);

		//length temporario
		if (n_armario >= modelo.num_armarios) {
			printf("Modulo invalido\n");
		}
		else {
			int n_prateleira = 0;

			//validar numero prateleira
			printf("Numero da prateleira:\n");
			scanf("%d", &n_prateleira);

			//length temporario
			if (n_prateleira >= modelo.armario[n_armario].num_prateleiras) {
				printf("Prateleira invalida\n");
			}
			else {
				listaMateriais();
				scanf("%d", &modelo.armario[n_armario].prateleira[n_prateleira].material);

			}
		}
	}
	break;

	break;
	case 5:
	{
		int n_armario = 0;

		//validar numero do modulo
		printf("Numero do modulo:\n");
		scanf("%d", &n_armario);

		//length temporario
		if (n_armario >= modelo.num_armarios) {
			printf("Modulo invalido\n");
		}
		else {
			int n_prateleira = 0;

			//validar numero prateleira
			printf("Numero da prateleira:\n");
			scanf("%d", &n_prateleira);

			//length temporario
			if (n_prateleira >= modelo.armario[n_armario].num_prateleiras) {
				printf("Prateleira invalida\n");
			}
			else {
				listaAcabamentos();
				scanf("%d", &modelo.armario[n_armario].prateleira[n_prateleira].acabamento);

			}
		}
	}
	break;
	}
	glutPostRedisplay();
}

void menuGaveta(int value) {
	switch (value)
	{
	case 1:
	{
		GLint n_armario = 0;
		GLfloat altura_gav = 0;
		GLint material = 0;
		GLint acabamento = 0;

		//validar numero do modulo
		printf("Numero do modulo:\n");
		scanf("%d", &n_armario);

		//length temporario
		if (n_armario >= modelo.num_armarios) {
			printf("Modulo invalido\n");
		}
		else {
			printf("Altura gaveta:\n");
			scanf("%f", &altura_gav);

			printf("Material:\n");
			listaMateriais();

			scanf("%d", &material);

			printf("Acabamento:\n");
			listaAcabamentos();

			scanf("%d", &acabamento);

			modelo.armario[n_armario].gaveta[modelo.armario[n_armario].num_gavetas].altura_gaveta = altura_gav;
			modelo.armario[n_armario].gaveta[modelo.armario[n_armario].num_gavetas].material = material;
			modelo.armario[n_armario].gaveta[modelo.armario[n_armario].num_gavetas].acabamento = acabamento;
			modelo.armario[n_armario].gaveta[modelo.armario[n_armario].num_gavetas].posicao_gaveta = 0.1;
			modelo.armario[n_armario].gaveta[modelo.armario[n_armario].num_gavetas].gaveta_aberta = GL_FALSE;


			modelo.armario[n_armario].num_gavetas++;
		}
	}
	break;
	case 2:
	{
		GLint numero_armario = 0;
		GLint numero_gaveta = 0;

		printf("Numero do Modulo\n");
		scanf("%d", &numero_armario);

		printf("Numero da Gaveta\n");
		scanf("%d", &numero_gaveta);


		for (int g = numero_gaveta; g < modelo.armario[numero_armario].num_gavetas - 1; g++) {
			modelo.armario[numero_armario].gaveta[g] = modelo.armario[numero_armario].gaveta[g + 1];
		}

		modelo.armario[numero_armario].num_gavetas--;
	}
	break;
	case 3:
	{
		int n_armario = 0;

		//validar numero do modulo
		printf("Numero do modulo:\n");
		scanf("%d", &n_armario);

		//length temporario
		if (n_armario >= modelo.num_armarios) {
			printf("Modulo invalido\n");
		}
		else {
			int n_gaveta = 0;

			//validar numero gaveta
			printf("Numero da gaveta:\n");
			scanf("%d", &n_gaveta);

			//length temporario
			if (n_gaveta >= modelo.armario[n_armario].num_gavetas) {
				printf("Gaveta invalida\n");
			}
			else {
				printf("Nova Altura:\n");
				scanf("%f", &modelo.armario[n_armario].gaveta[n_gaveta].altura_gaveta);

			}
		}
	}
	break;

	case 4:
	{
		int n_armario = 0;

		//validar numero do modulo
		printf("Numero do modulo:\n");
		scanf("%d", &n_armario);

		//length temporario
		if (n_armario >= modelo.num_armarios) {
			printf("Modulo invalido\n");
		}
		else {
			int n_gaveta = 0;

			//validar numero gaveta
			printf("Numero da gaveta:\n");
			scanf("%d", &n_gaveta);

			//length temporario
			if (n_gaveta >= modelo.armario[n_armario].num_gavetas) {
				printf("Gaveta invalida\n");
			}
			else {
				listaMateriais();
				scanf("%d", &modelo.armario[n_armario].gaveta[n_gaveta].material);

			}
		}
	}
	break;

	case 5:
	{
		int n_armario = 0;

		//validar numero do modulo
		printf("Numero do modulo:\n");
		scanf("%d", &n_armario);

		//length temporario
		if (n_armario >= modelo.num_armarios) {
			printf("Modulo invalido\n");
		}
		else {
			int n_gaveta = 0;

			//validar numero gaveta
			printf("Numero da gaveta:\n");
			scanf("%d", &n_gaveta);

			//length temporario
			if (n_gaveta >= modelo.armario[n_armario].num_gavetas) {
				printf("Gaveta invalida\n");
			}
			else {
				listaAcabamentos();
				scanf("%d", &modelo.armario[n_armario].gaveta[n_gaveta].acabamento);

			}
		}
	}
	break;

	case 6:
	{
		int n_armario = 0;

		//validar numero do modulo
		printf("Numero do modulo:\n");
		scanf("%d", &n_armario);

		//length temporario
		if (n_armario >= modelo.num_armarios) {
			printf("Modulo invalido\n");
		}
		else {
			int n_gaveta = 0;

			//validar numero gaveta
			printf("Numero da gaveta:\n");
			scanf("%d", &n_gaveta);

			//length temporario
			if (n_gaveta >= modelo.armario[n_armario].num_gavetas) {
				printf("Gaveta invalida\n");
			}
			else {
				if (modelo.armario[n_armario].gaveta[n_gaveta].gaveta_aberta == GL_TRUE) {
					modelo.armario[n_armario].gaveta[n_gaveta].gaveta_aberta = GL_FALSE;
				}
				else {
					modelo.armario[n_armario].gaveta[n_gaveta].gaveta_aberta = GL_TRUE;
				}
			}
		}
	}
	break;
	}
	glutPostRedisplay();
}

void menuSuporte(int value) {
	switch (value)
	{
	case 1:
	{
		int n_armario = 0;
		GLfloat altura_suporte = 0;

		//validar numero do modulo
		printf("Numero do modulo:\n");
		scanf("%d", &n_armario);

		//length temporario
		if (n_armario >= modelo.num_armarios) {
			printf("Modulo invalido\n");
		}
		else {
			printf("Altura Suporte:\n");
			scanf("%f", &altura_suporte);

			modelo.armario[n_armario].suporte[modelo.armario[n_armario].num_suportes].altura_suporte = altura_suporte;

			modelo.armario[n_armario].num_suportes++;
		}


	}break;
	}
}

void menuPrincipal(int value) {
	switch (value)
	{
	case 0:
		exit(0);
		break;
	}
}


void criaMenu() {
	int menu_armario = glutCreateMenu(menuArmario);
	glutAddMenuEntry("Definir Modulo", 1);
	glutAddMenuEntry("Definir Altura Armario", 2);
	glutAddMenuEntry("Definir Profundidade Armario", 5);
	glutAddMenuEntry("Adicionar Modulo", 3);
	glutAddMenuEntry("Eliminar Modulo", 4);
	glutAddMenuEntry("Definir Material", 6);
	glutAddMenuEntry("Definir Acabamento", 7);
	glutAddMenuEntry("Abrir/Fechar Portas", 8);

	int menu_prateleira = glutCreateMenu(menuPrateleira);
	glutAddMenuEntry("Adicionar Prateleira", 1);
	glutAddMenuEntry("Remover Prateleira", 2);
	glutAddMenuEntry("Mover Prateleira", 3);
	glutAddMenuEntry("Definir Material", 4);
	glutAddMenuEntry("Definir Acabamento", 5);


	int menu_gaveta = glutCreateMenu(menuGaveta);
	glutAddMenuEntry("Adicionar Gaveta", 1);
	glutAddMenuEntry("Remover Gaveta", 2);
	glutAddMenuEntry("Mover Gaveta", 3);
	glutAddMenuEntry("Definir Material", 4);
	glutAddMenuEntry("Definir Acabamento", 5);
	glutAddMenuEntry("Abrir/Fechar Gaveta", 6);


	int menu_suporte = glutCreateMenu(menuSuporte);
	glutAddMenuEntry("Adicionar Suporte Cruzetas", 1);

	glutCreateMenu(menuPrincipal);
	glutAddSubMenu("Armario", menu_armario);
	glutAddSubMenu("Prateleiras", menu_prateleira);
	glutAddSubMenu("Gavetas", menu_gaveta);
	glutAddSubMenu("Suporte Cruzetas", menu_suporte);

	glutAddMenuEntry("Sair", 0);
	glutAttachMenu(GLUT_RIGHT_BUTTON);

}

void Display(void)
{
	estado.mode = GL_RENDER;
	Draw();
}

void processHits(GLint hits, GLuint buffer[])
{
	int i;
	unsigned int j;
	GLuint names, *ptr;

	printf("hits = %d\n", hits);
	ptr = (GLuint *)buffer;
	for (i = 0; i < hits; i++) {  /* for each hit  */
		names = *ptr;
		printf(" number of names for hit = %d\n", names);
		ptr++;
		printf("  z1 is %g;", (float)*ptr / 0xffffffff);
		ptr++;
		printf(" z2 is %g\n", (float)*ptr / 0xffffffff);
		ptr++;
		printf("   the name is ");
		for (j = 0; j < names; j++) {  /* for each name */
			printf("%d ", *ptr);
			ptr++;
		}
		printf("\n");
	}
}

void Mouse(int bt, int st, int x, int y)
{
	int i, n;
	double zmin = 10.0;
	GLuint buffer[100], *ptr, names;
	GLint vp[4];
	GLdouble proj[16], mv[16];
	int width=8, height=8;
	double obj[][3] = { {0.0, 0.0, 0.0}, {-0.7, -0.7, 0.1}, {0.7, -0.7, 0.0}, {0.7, 0.7, 0.1} };
	int id = 0;

	if (bt == GLUT_LEFT_BUTTON && st == GLUT_DOWN)
	{

		if (modelo.portas_abertas == GL_TRUE) {
			for (int a = 0; a < modelo.num_armarios; a++) {
				for (int g = 0; g < modelo.armario[a].num_gavetas; g++) {
					modelo.armario[a].gaveta[g].gaveta_aberta = GL_FALSE;
				}
			}
			modelo.portas_abertas = GL_FALSE;
		}
		else {
			modelo.portas_abertas = GL_TRUE;
		}
		glSelectBuffer(100, buffer);
		glRenderMode(GL_SELECT);
		glInitNames();
		glPushMatrix();
		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
		glLoadIdentity();
		glGetIntegerv(GL_VIEWPORT, vp);
		gluPickMatrix(x, height - y - 1, 1, 1, vp);
		gluPerspective(45.0, (double)width / (double)height, 0.1, 10.0);
		glMatrixMode(GL_MODELVIEW);
		estado.mode = GL_SELECT;
		Draw();
		n = glRenderMode(GL_RENDER);
		if (n > 0)
		{

			ptr = buffer;
			for (i = 0; i < n; i++)
			{
				names = *ptr;

				if (zmin > (double) ptr[1] / 0xffffffff)
				{
					zmin = (double)ptr[1] / 0xffffffff;
					id = ptr[3];
				}

				ptr += 4;


				for (int j = 0; j < names; j++) {  /* execucao para cada nome */

					if (names == 1) {
						modelo.portas_abertas = GL_TRUE; 
					}
					ptr += 4;
				}
			}
			glGetDoublev(GL_PROJECTION_MATRIX, proj);
			glGetDoublev(GL_MODELVIEW_MATRIX, mv);
			gluUnProject(x, height - y - 1, zmin, mv, proj, vp, &obj[0][0], &obj[0][1], &obj[0][2]);
		}
		else
			id = 0;
		glMatrixMode(GL_PROJECTION);
		glPopMatrix();
		glMatrixMode(GL_MODELVIEW);
		glPopMatrix();
	}
}

void Drag(int x, int y)
{
	int n;
	double newx, newy, newz;
	GLuint buffer[100];
	GLint vp[4];
	GLdouble proj[16], mv[16];
	int id = 0;
	int width = 8, height = 8;

	if (id > 0)
	{
		glSelectBuffer(100, buffer);
		glRenderMode(GL_SELECT);
		glInitNames();
		glPushMatrix();
		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
		glLoadIdentity();
		glGetIntegerv(GL_VIEWPORT, vp);
		gluPickMatrix(x, height - y - 1, 1, 1, vp);
		gluPerspective(45.0, (double)width / (double)height, 0.1, 10.0);
		glMatrixMode(GL_MODELVIEW);
		estado.mode = GL_SELECT;
		Draw();
		n = glRenderMode(GL_RENDER);
		if (n > 0)
		{
			glGetDoublev(GL_PROJECTION_MATRIX, proj);
			glGetDoublev(GL_MODELVIEW_MATRIX, mv);
			gluUnProject(x, height - y - 1, (double)buffer[1] / 0xffffffff, mv, proj, vp, &newx, &newy, &newz);
			estado.camera.eye.x = newx;
			estado.camera.eye.y = newy;
		}
		glMatrixMode(GL_PROJECTION);
		glPopMatrix();
		glMatrixMode(GL_MODELVIEW);
		glPopMatrix();
		glutPostRedisplay();
	}
}


int main(int argc, char **argv)
{
	//char str[] = " makefile MAKEFILE Makefile ";
	estado.doubleBuffer = 1;

	glutInit(&argc, argv);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(640, 480);
	glutInitDisplayMode(((estado.doubleBuffer) ? GLUT_DOUBLE : GLUT_SINGLE) | GLUT_RGB | GLUT_DEPTH);
	if (glutCreateWindow("OpenGL Armario 3D") == GL_FALSE)
		exit(1);

	Init();


	criaMenu();

	createTextures(modelo.texID);


	imprime_ajuda();

	// Registar callbacks do GLUT

	  // callbacks de janelas/desenho
	glutMouseFunc(Mouse);
	glutReshapeFunc(Reshape);
	glutDisplayFunc(Display);
	glutMotionFunc(Drag);

	// Callbacks de teclado
	glutKeyboardFunc(Key);
	glutKeyboardUpFunc(KeyUp);
	glutSpecialFunc(SpecialKey);
	glutSpecialUpFunc(SpecialKeyUp);

	// callbacks timer/idle
	glutTimerFunc(estado.delayMovimento, Timer, 0);

	// COMECAR...
	glutMainLoop();
	return 0;
}

